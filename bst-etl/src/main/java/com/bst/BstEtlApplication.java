package com.bst;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author ruoyi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@MapperScan(value = {"com.bst.**.mapper"})
@EnableBatchProcessing
public class BstEtlApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(BstEtlApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  恭喜启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}