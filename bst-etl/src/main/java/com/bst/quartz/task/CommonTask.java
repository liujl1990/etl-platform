package com.bst.quartz.task;

import com.bst.common.base.mapper.BaseMapper;
import com.bst.common.constant.JobConstant;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.exception.base.BaseRuntimeException;
import com.bst.common.utils.DateUtil;
import com.bst.common.vo.JobExecParamVO;
import com.bst.etl.domain.EtlTaskExec;
import com.bst.etl.service.IEtlJobRunService;
import com.bst.etl.service.IEtlTaskExecService;
import com.bst.system.vo.OutInterParamVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;

@Component("commonTask")
public class CommonTask {

    @Autowired
    IEtlJobRunService jobRunService;
    @Autowired
    BaseMapper baseMapper;
    /**
     *  根据定时任务编码查询关联的基础任务
     *  获得跟基础任务相关的dw任务
     *  执行所有天指标
     */
    public void sysDayMethod(Long jobId,Long jobLogId) throws ParseException {
        String endDay = DateUtil.toDateStrByFormat(new Date(),DateUtil.yyyyMMdd);
        Date endDate = DateUtil.FORMAT_YYYYMMDD.parse(endDay);
        Date yesterday = DateUtil.calculate(endDate, Calendar.DAY_OF_MONTH, -1);
        String startDate = DateUtil.toDateStrByFormat(yesterday,DateUtil.yyyyMMdd);
        JobExecParamVO paramVO = new JobExecParamVO();
        paramVO.setStartDay(startDate);
        paramVO.setEndDay(endDay);
        paramVO.setExecType(JobConstant.JOB_EXE_TYPE_SCHEDULE);
        paramVO.setType(JobConstant.JOB_TYPE_ALL);
        paramVO.setIdTasks(jobId+"");
        paramVO.setJobLogId(jobLogId);
        jobRunService.run(paramVO);
        if(!isSuccess(jobLogId)) {
            throw new BaseRuntimeException(startDate+"执行结果不正确");
        }
    }

    public void sysNowMethod(Long jobId,Long jobLogId) throws ParseException {
        Date now = new Date();
        String startDate = DateUtil.toDateStrByFormat(now,DateUtil.yyyyMMdd);
        String endDate = DateUtil.toDateStrByFormat(DateUtil.calculate(now,Calendar.DAY_OF_MONTH,1),DateUtil.yyyyMMdd);
        JobExecParamVO paramVO = new JobExecParamVO();
        paramVO.setStartDay(startDate);
        paramVO.setEndDay(endDate);
        paramVO.setExecType(JobConstant.JOB_EXE_TYPE_SCHEDULE);
        paramVO.setType(JobConstant.JOB_TYPE_ALL);
        paramVO.setIdTasks(jobId+"");
        paramVO.setJobLogId(jobLogId);
        jobRunService.run(paramVO);
        if(!isSuccess(jobLogId)) {
            throw new BaseRuntimeException(startDate+"执行结果不正确");
        }
    }

    public void sysHistoryMethod(Long jobId,Long jobLogId) throws ParseException {

    }

    @Autowired
    IEtlTaskExecService taskExecService;
    private boolean isSuccess(Long idJoblog) {
        EtlTaskExec taskExec = new EtlTaskExec();
        taskExec.setIdJoblog(idJoblog);
        taskExec.setEuStatus("FAILED");
        List<EtlTaskExec> taskExecList = taskExecService.selectEtlTaskExecList(taskExec);
        if(taskExecList.size()>0) {
            return false;
        }
        return true;
    }
}
