package com.bst.quartz.vo;

import java.io.Serializable;

public class SysJobLogGroupVO implements Serializable {
    private String jobName;
    private Integer total;
    private Long maxLogid;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getMaxLogid() {
        return maxLogid;
    }

    public void setMaxLogid(Long maxLogid) {
        this.maxLogid = maxLogid;
    }
}
