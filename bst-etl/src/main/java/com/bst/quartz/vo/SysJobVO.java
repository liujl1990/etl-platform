package com.bst.quartz.vo;

import com.bst.quartz.domain.SysJob;

import java.io.Serializable;

public class SysJobVO implements Serializable {
    private SysJob sysJob;
    private SysJobLogGroupVO groupVO;
    private Boolean fgError;
    private String dateDvalue;

    public String getDateDvalue() {
        return dateDvalue;
    }

    public void setDateDvalue(String dateDvalue) {
        this.dateDvalue = dateDvalue;
    }

    public SysJob getSysJob() {
        return sysJob;
    }

    public void setSysJob(SysJob sysJob) {
        this.sysJob = sysJob;
    }

    public SysJobLogGroupVO getGroupVO() {
        return groupVO;
    }

    public void setGroupVO(SysJobLogGroupVO groupVO) {
        this.groupVO = groupVO;
    }

    public Boolean getFgError() {
        return fgError;
    }

    public void setFgError(Boolean fgError) {
        this.fgError = fgError;
    }
}
