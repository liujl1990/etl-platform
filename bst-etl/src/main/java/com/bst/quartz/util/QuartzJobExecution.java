package com.bst.quartz.util;

import org.quartz.JobExecutionContext;
import com.bst.quartz.domain.SysJob;

/**
 * 定时任务处理（允许并发执行）
 * 
 * @author ruoyi
 *
 */
public class QuartzJobExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, SysJob sysJob, Long jobLogId) throws Exception
    {
        JobInvokeUtil.invokeMethod(sysJob,jobLogId);
    }
}
