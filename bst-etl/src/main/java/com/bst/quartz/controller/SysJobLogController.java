package com.bst.quartz.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.bst.common.utils.DateUtil;
import com.bst.etl.domain.EtlTaskExec;
import com.bst.etl.service.IEtlTaskExecService;
import com.bst.quartz.vo.SysJobLogGroupVO;
import com.bst.quartz.vo.SysJobVO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.core.page.TableDataInfo;
import com.bst.common.enums.BusinessType;
import com.bst.common.utils.StringUtils;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.quartz.domain.SysJob;
import com.bst.quartz.domain.SysJobLog;
import com.bst.quartz.service.ISysJobLogService;
import com.bst.quartz.service.ISysJobService;

/**
 * 调度日志操作处理
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/monitor/jobLog")
public class SysJobLogController extends BaseController
{
    private String prefix = "monitor/job";

    @Autowired
    private ISysJobService jobService;

    @Autowired
    private ISysJobLogService jobLogService;

    @RequiresPermissions("monitor:job:view")
    @GetMapping()
    public String jobLog(@RequestParam(value = "jobId", required = false) Long jobId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(jobId))
        {
            SysJob job = jobService.selectJobById(jobId);
            mmap.put("job", job);
        }
        return prefix + "/jobLog";
    }

    @RequiresPermissions("monitor:job:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysJobLog jobLog)
    {
        startPage();
        List<SysJobLog> list = jobLogService.selectJobLogList(jobLog);
        return getDataTable(list);
    }

    @Log(title = "调度日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("monitor:job:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysJobLog jobLog)
    {
        List<SysJobLog> list = jobLogService.selectJobLogList(jobLog);
        ExcelUtil<SysJobLog> util = new ExcelUtil<SysJobLog>(SysJobLog.class);
        return util.exportExcel(list, "调度日志");
    }

    @Log(title = "调度日志", businessType = BusinessType.DELETE)
    @RequiresPermissions("monitor:job:remove")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(jobLogService.deleteJobLogByIds(ids));
    }

    @RequiresPermissions("monitor:job:detail")
    @GetMapping("/detail/{jobLogId}")
    public String detail(@PathVariable("jobLogId") Long jobLogId, ModelMap mmap)
    {
        mmap.put("name", "jobLog");
        mmap.put("jobLog", jobLogService.selectJobLogById(jobLogId));
        return prefix + "/detail";
    }

    @Log(title = "调度日志", businessType = BusinessType.CLEAN)
    @RequiresPermissions("monitor:job:remove")
    @PostMapping("/clean")
    @ResponseBody
    public AjaxResult clean()
    {
        jobLogService.cleanJobLog();
        return success();
    }

    @Autowired
    IEtlTaskExecService taskExecService;
    @GetMapping("/showDetail/{idJoblog}")
    public String showDetail(@PathVariable("idJoblog") Long idJoblog, ModelMap mmap)
    {
        EtlTaskExec taskExec = new EtlTaskExec();
        taskExec.setIdJoblog(idJoblog);
        List<EtlTaskExec> taskExecList = taskExecService.selectEtlTaskExecList(taskExec);
        if(taskExecList.size()>0) {
            taskExec = taskExecList.get(0);
            List<Map<String,Object>> returnData = taskExecService.findBatchJobData(taskExec.getIdExec());
            mmap.put("data", returnData);
        }
        return "etl/exec/detail";
    }

    @GetMapping("/queryJobStatus")
    @ResponseBody
    public AjaxResult queryJobStatus(String day) throws ParseException {
        List<SysJob> jobList = jobService.selectJobList(new SysJob());
        List<SysJobLogGroupVO> jobLogData = jobLogService.queryOneDayJobLog(DateUtil.FORMAT_YYYY_MM_DD.parse(day));
        Map<String,SysJobLogGroupVO> groupVOMap = jobLogData.stream().collect(Collectors.toMap(SysJobLogGroupVO::getJobName,e->e));
        List<SysJobVO> jobVOS = new ArrayList<>();
        SysJobVO sysJobVO;
        SysJobLogGroupVO jobLogGroupVO;
        for(SysJob job:jobList) {
            sysJobVO = new SysJobVO();
            sysJobVO.setSysJob(job);
            jobLogGroupVO = null;
            sysJobVO.setDateDvalue(DateUtil.dateDvalue(job.getNextValidTime()));
            if("0".equals(job.getStatus())) {
                jobLogGroupVO = groupVOMap.get(job.getJobName());
                if(jobLogGroupVO!=null) {
                    EtlTaskExec taskExec = new EtlTaskExec();
                    taskExec.setIdJoblog(jobLogGroupVO.getMaxLogid());
                    taskExec.setEuStatus("FAILED");
                    List<EtlTaskExec> taskExecList = taskExecService.selectEtlTaskExecList(taskExec);
                    if(taskExecList.size()>0) {
                        sysJobVO.setFgError(true);
                    }
                }
            }
            if(jobLogGroupVO==null) {
                jobLogGroupVO = new SysJobLogGroupVO();
                jobLogGroupVO.setTotal(0);
            }
            sysJobVO.setGroupVO(jobLogGroupVO);
            if(sysJobVO.getFgError()==null) {
                sysJobVO.setFgError(false);
            }
            jobVOS.add(sysJobVO);
        }
        return AjaxResult.success(jobVOS);
    }
}
