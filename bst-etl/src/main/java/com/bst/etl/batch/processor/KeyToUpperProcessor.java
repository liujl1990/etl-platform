package com.bst.etl.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by liujunlei on 2021/6/15.
 */
@Component
public class KeyToUpperProcessor implements ItemProcessor<Map<String,Object>,Map<String,Object>> {


    /**
     * 空值补全
     * @param stringObjectMap
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> process(Map<String, Object> stringObjectMap) throws Exception {
        Map<String, Object> map = new HashMap<>();
        for(Map.Entry<String, Object> entry:stringObjectMap.entrySet()) {
            map.put(entry.getKey().toUpperCase(),entry.getValue());
        }
        return map;
    }
}
