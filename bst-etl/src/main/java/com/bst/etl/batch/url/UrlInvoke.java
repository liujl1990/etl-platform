package com.bst.etl.batch.url;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bst.common.utils.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class UrlInvoke {

    public List<Map<String,Object>> vteGetInvoke(String url,String urlParam) {
        RestTemplate template = new RestTemplate();
        if(StringUtils.isNotEmpty(urlParam)) {
            url = url+"?"+urlParam;
        }
        ResponseEntity<String> res = template.getForEntity(url,String.class);
        JSONObject object = JSONObject.parseObject(res.getBody());
        List<Map<String,Object>> list = null;
        if(res.getStatusCode().value()==200) {
            JSONArray arr = object.getJSONObject("content").getJSONArray("detail");
            list = (List<Map<String,Object>>)JSONArray.parse(arr.toJSONString());
            for (Map<String, Object> map : list) {
                map.remove("precautions");
                map.remove("scanId");
                map.remove("node");
                map.remove("checkTime");
                map.put("createTime",new Date(Long.parseLong(map.get("createTime").toString())*1000));
            }
        }
        return list;
    }

    public List<Map<String,Object>> commonPostInvoke(String url,String urlParam) {
        RestTemplate template = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        //httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + assessToken);
        HttpEntity<String> httpEntity = new HttpEntity<String>(urlParam, httpHeaders);
        ResponseEntity<String> list = template.postForEntity(url,httpEntity,String.class);
        return null;
    }
}
