package com.bst.etl.batch.tasklet;

import com.bst.common.utils.spring.SpringUtils;
import com.bst.etl.vo.JobShareDataVO;
import com.bst.base.domain.BaseMsg;
import com.bst.md.service.IMdMedDimService;
import com.bst.md.service.impl.MdMedDimServiceImpl;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.List;


public class InsertDimTasklet implements Tasklet {
    private JobShareDataVO jobShareDataVO;

    public InsertDimTasklet(JobShareDataVO jobShareDataVO) {
        this.jobShareDataVO = jobShareDataVO;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        IMdMedDimService dataService = SpringUtils.getBean(MdMedDimServiceImpl.class);
        List<BaseMsg> list = dataService.dimDataCheck(jobShareDataVO.getDimDataMap(),jobShareDataVO.getJobDimVOMap());
        jobShareDataVO.setMsgList(list);
        return RepeatStatus.FINISHED;
    }
}