package com.bst.etl.batch.job;

import com.bst.etl.batch.tasklet.DelSQLExecTasklet;
import com.bst.etl.batch.writer.InsertWriter;
import com.bst.etl.batch.writer.NoPkInsertWriter;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class BaseJob {

    @Autowired
    JobBuilderFactory jobBuilderFactory;
    @Autowired
    StepBuilderFactory stepBuilderFactory;
    @Autowired
    @Qualifier("delSQLExecTasklet")
    DelSQLExecTasklet delSQLExecTasklet;
    @Resource(name = "selfJdbcCursorItemReader")
    JdbcCursorItemReader selfJdbcCursorItemReader;
    @Autowired
    InsertWriter insertWriter;
    @Autowired
    NoPkInsertWriter noPkInsertWriter;
    @Autowired
    ItemReader dwReaderBean;



    protected TaskletStep buildBeforeStep() {
        TaskletStep step = this.stepBuilderFactory.get("before_step")
                .tasklet(delSQLExecTasklet)
                .build();
        return step;
    }
}
