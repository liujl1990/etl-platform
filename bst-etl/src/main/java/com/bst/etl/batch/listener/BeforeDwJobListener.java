package com.bst.etl.batch.listener;

import com.bst.common.vo.JobDimVO;
import com.bst.etl.utils.SpringBeanUtil;
import com.bst.etl.vo.JobShareDataVO;
import com.bst.md.service.impl.MdMedDimServiceImpl;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import java.util.HashMap;
import java.util.Map;

public class BeforeDwJobListener implements JobExecutionListener {

    private JobShareDataVO jobShareDataVO;

    public BeforeDwJobListener(JobShareDataVO jobShareDataVO) {
        this.jobShareDataVO = jobShareDataVO;
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        //Map<String,Map<String,Object>> dimDataMap,Map<String, JobDimVO> jobDimVOMap
        Map<String,Map<String,Object>> dimDataMap = new HashMap<>();
        MdMedDimServiceImpl mdMedDimService = SpringBeanUtil.getBean(MdMedDimServiceImpl.class);
        Map<String, JobDimVO> jobDimVOMap = mdMedDimService.findAllDimData(jobShareDataVO.getPubfldList());
        jobShareDataVO.setDimDataMap(dimDataMap);
        jobShareDataVO.setJobDimVOMap(jobDimVOMap);
    }

    @Override
    public void afterJob(JobExecution jobExecution) {

    }
}
