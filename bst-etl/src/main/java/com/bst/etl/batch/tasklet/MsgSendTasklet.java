package com.bst.etl.batch.tasklet;

import com.bst.common.utils.spring.SpringUtils;
import com.bst.etl.vo.JobShareDataVO;
import com.bst.base.domain.BaseMsg;
import com.bst.base.service.IBaseMsgService;
import com.bst.base.service.impl.BaseMsgServiceImpl;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.List;

public class MsgSendTasklet implements Tasklet {
    private JobShareDataVO jobShareDataVO;

    public MsgSendTasklet(JobShareDataVO jobShareDataVO) {
        this.jobShareDataVO = jobShareDataVO;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        List<BaseMsg> msgList = jobShareDataVO.getMsgList();
        if(msgList!=null && msgList.size()>0) {
            IBaseMsgService baseMsgService = SpringUtils.getBean(BaseMsgServiceImpl.class);
            baseMsgService.insertList(msgList);
        }
        return RepeatStatus.FINISHED;
    }
}
