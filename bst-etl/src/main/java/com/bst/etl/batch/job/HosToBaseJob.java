package com.bst.etl.batch.job;

import com.bst.common.exception.base.BaseException;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class HosToBaseJob extends BaseJob{

    public HosToBaseJob() {
    }

    @Bean
    public Job hosToHdwJobBean() throws BaseException {
        TaskletStep beforeStep = this.buildBeforeStep();
        TaskletStep mainStep = this.buildMainStep();
        Job job = jobBuilderFactory.get("hosToHdwJobBean")
                .incrementer(new RunIdIncrementer())
                .start(beforeStep).next(mainStep)
                .build();
        return job;
    }

    protected TaskletStep buildMainStep() throws BaseException {
        TaskletStep step = stepBuilderFactory.get("main_step")
                .<Map, Map>chunk(1024)
                .reader(selfJdbcCursorItemReader)
                .writer(insertWriter)
                .build();
        return step;
    }
}
