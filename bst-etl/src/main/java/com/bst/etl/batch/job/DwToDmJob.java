package com.bst.etl.batch.job;

import com.bst.etl.batch.tasklet.DelDmSQLExecTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by liujunlei on 2021/5/20.
 */
@Component
public class DwToDmJob extends BaseJob {
    @Autowired
    @Qualifier("delDmSQLExecTasklet")
    DelDmSQLExecTasklet delDmSQLExecTasklet;
    /**
     * 简单的查询，写入。适用于医院数据到ods的过程
     * @return
     */
    @Bean
    public Job dwToDmJobBean(){
        return jobBuilderFactory.get("dwToDmJobBean")
                .incrementer(new RunIdIncrementer()) //防止重复的
                .start(delDmSqlStep()).next(dwToDmStepBean())//为Job指定Step
                .build();
    }

    private TaskletStep delDmSqlStep() {
        TaskletStep step = this.stepBuilderFactory.get("delDmSqlStep")
                .tasklet(delDmSQLExecTasklet)
                .build();
        return step;
    }

    public Step dwToDmStepBean() {
        return this.stepBuilderFactory.get("dwToDmStepBean")
                .<Map<String,Object>, Map<String,Object>>chunk(1024)
                .reader(dwReaderBean)
                .writer(noPkInsertWriter)
                .build();
    }


}
