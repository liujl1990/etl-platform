package com.bst.etl.batch.reader;

import com.bst.common.constant.JobConstant;
import com.bst.system.framework.datasource.DynamicDataSource;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by liujunlei on 2021/5/26.
 */
@Component
public class SimpleReader {


    @Bean(name = "dwReaderBean",destroyMethod = "")
    @StepScope
    public JdbcCursorItemReader<Map<String,Object>> dwReaderBean(@Value("#{jobParameters[querySql]}") String qSql) {
        return new JdbcCursorItemReaderBuilder<Map<String,Object>>()
                .dataSource(DynamicDataSource.getDataSourceById(JobConstant.DB_CLS_DW))
                .name("simpleReaderBean")
                .sql(qSql)
                .rowMapper(new ColumnMapRowMapper())
                .build();
    }

    @Bean(name = "mdMedTbReaderBean",destroyMethod = "")
    @StepScope
    public JdbcCursorItemReader<Map<String,Object>>  mdMedTbReaderBean(@Value("#{jobParameters[idDbSou]}") String idDbSou) {
        String sql = "select * from md_med_tb where FG_DEL=0";
        return new JdbcCursorItemReaderBuilder<Map<String,Object>>()
                .dataSource(DynamicDataSource.getDataSourceById(idDbSou))
                .name("mdMedTbReaderBean")
                .sql(sql)
                .rowMapper(new ColumnMapRowMapper())
                .build();

    }
}
