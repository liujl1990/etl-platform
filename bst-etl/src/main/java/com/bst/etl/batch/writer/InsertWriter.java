package com.bst.etl.batch.writer;

import com.bst.common.annotation.DataSource;
import com.bst.common.base.mapper.BaseMapper;
import com.bst.common.constant.JobConstant;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by liujunlei on 2021/5/25.
 */
@Component
@StepScope
public class InsertWriter implements ItemWriter {

    @Value("#{jobParameters['tableName']}")
    private String tableName;

    @Autowired
    BaseMapper baseMapper;

    @Override
    @DataSource(value = JobConstant.DB_CLS_DW)
    public void write(List list) throws Exception {
        List<Map<String,Object>> mapList = (List<Map<String,Object>>)list;
        if(mapList!=null) {
            int nowSize=0,lastSize=0;
            int mapSize = mapList.get(0).keySet().size();
            int num = 2100/(mapSize+2)-1;
            for(Map<String,Object> map:mapList) {
                nowSize++;
                if(nowSize%num==0) {
                    baseMapper.insertMapList(tableName,mapList.get(0),list.subList(lastSize,nowSize));
                    lastSize = nowSize;
                }
                if(nowSize==mapList.size() && nowSize!=lastSize) {
                    baseMapper.insertMapList(tableName,mapList.get(0),list.subList(lastSize,nowSize));
                }
            }
        }
    }
}
