package com.bst.etl.batch.dao;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.repository.dao.JdbcJobExecutionDao;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public class JdbcJobExecutionExDao extends JdbcJobExecutionDao {

    private JdbcJobExecutionExDao(){};

    private static volatile JdbcJobExecutionExDao executionExDao;

    public static JdbcJobExecutionExDao getInstance(JdbcTemplate jdbcTemplate) {
        if(executionExDao==null) {
            synchronized (JdbcJobExecutionExDao.class) {
                if(executionExDao==null) {
                    executionExDao = new JdbcJobExecutionExDao();
                }
            }
        }
        executionExDao.setJdbcTemplate(jdbcTemplate);
        return executionExDao;
    }

    public List<JobExecution> findByIds(List<Long> executionIds) {
        JobExecution execution;
        List<JobExecution> executions = new ArrayList<>();
        for(Long id:executionIds) {
            execution = super.getJobExecution(id);
            executions.add(execution);
        }
        return executions;
    }
}
