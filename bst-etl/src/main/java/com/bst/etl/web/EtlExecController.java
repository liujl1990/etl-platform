package com.bst.etl.web;

import com.bst.common.constant.JobConstant;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.core.page.TableDataInfo;
import com.bst.common.vo.JobExecParamVO;
import com.bst.etl.batch.dao.JdbcJobExecutionExDao;
import com.bst.etl.batch.dao.JdbcStepExecutionExDao;
import com.bst.etl.domain.EtlTaskExec;
import com.bst.etl.service.*;
import com.bst.quartz.domain.SysJob;
import com.bst.quartz.service.ISysJobService;
import com.bst.etl.domain.EtlDrawBase;
import com.bst.etl.domain.EtlDrawDw;
import io.swagger.annotations.ApiOperation;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.*;

/**
 * 基础抽取Controller
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
@Controller
@RequestMapping("/etl/exec")
public class EtlExecController extends BaseController
{
    @Autowired
    IEtlDrawBaseService drawBaseService;
    @Autowired
    IEtlDrawDwService drawDwService;
    @Autowired
    IEtlJobRunService jobRunService;
    @Autowired
    IEtlTaskExecService taskExecService;
    @Autowired
    IEtlTaskExecItemService taskExecItemService;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    ISysJobService sysJobService;


    @GetMapping()
    public String exec()
    {
        return "etl/exec/exec";
    }

    @GetMapping("/query/{sdExeccls}")
    @ResponseBody
    public TableDataInfo query(@PathVariable("sdExeccls") String sdExeccls)
    {
        List<Map<String,Object>> dataList = new ArrayList<>();
        Map<String,Object> dataMap;
        TableDataInfo tableDataInfo = getDataTable(dataList);
        if("base".equals(sdExeccls)) {
            EtlDrawBase drawBase = new EtlDrawBase();
            //drawBase.setSdDrawcls("dim");
            drawBase.setFgAct(1);
            startPage();
            List<EtlDrawBase> list = drawBaseService.selectEtlDrawBaseList(drawBase);
            tableDataInfo = getDataTable(list);
            for(EtlDrawBase base:list) {
                dataMap = new HashMap<>();
                dataMap.put("id",base.getIdDrawBase());
                dataMap.put("table",base.getCdTbTar());
                dataMap.put("des",base.getNa());
                dataMap.put("db",base.getIdDb());
                dataList.add(dataMap);
            }
        } else if("dw".equals(sdExeccls)) {
            EtlDrawDw drawBase = new EtlDrawDw();
            drawBase.setFgAct(1);
            startPage();
            List<EtlDrawDw> list = drawDwService.selectEtlDrawDwList(drawBase);
            tableDataInfo = getDataTable(list);
            for(EtlDrawDw base:list) {
                dataMap = new HashMap<>();
                dataMap.put("id",base.getIdDrawDw());
                dataMap.put("table",base.getCdTb());
                dataMap.put("des",base.getNa());
                dataMap.put("db", JobConstant.DB_CLS_DW);
                dataList.add(dataMap);
            }
        } else if("all".equals(sdExeccls)) {
            SysJob sysJob = new SysJob();
            startPage();
            List<SysJob> list = sysJobService.selectJobList(sysJob);
            tableDataInfo = getDataTable(list);
            for(SysJob job:list) {
                dataMap = new HashMap<>();
                dataMap.put("id",job.getJobId());
                dataMap.put("table",job.getCronExpression());
                dataMap.put("des",job.getJobName());
                dataMap.put("db", JobConstant.DB_CLS_DW);
                dataList.add(dataMap);
            }
        }
        tableDataInfo.setRows(dataList);
        return tableDataInfo;
    }

    @GetMapping("/queryExec/{sdExeccls}/{euTp}")
    @ResponseBody
    public TableDataInfo queryExec(@PathVariable("sdExeccls") String sdExeccls,@PathVariable("euTp") Integer euTp)
    {
        EtlTaskExec taskExec = new EtlTaskExec();
        taskExec.setSdEtlcls(sdExeccls);
        taskExec.setEuTp(euTp);
        List<EtlTaskExec> dataList = taskExecService.selectEtlTaskExecList(taskExec);
        TableDataInfo tableDataInfo = getDataTable(dataList);
        return tableDataInfo;
    }

    @GetMapping("/queryStep/{idJob}")
    @ResponseBody
    public AjaxResult queryStep(@PathVariable("idJob") Long idJob, ModelMap mmap)
    {
        JobExecution job = JdbcJobExecutionExDao.getInstance(jdbcTemplate).getJobExecution(idJob);
        List<StepExecution> stepExecutions = JdbcStepExecutionExDao.getInstance(jdbcTemplate).getStepExecution(job);
        Map<String,Object> map;
        List<Map<String,Object>> returnData = new ArrayList<>();
        if(stepExecutions==null) {
            return AjaxResult.success();
        }
        for(StepExecution step:stepExecutions) {
            map = new LinkedHashMap<>();
            map.put("stepName",step.getStepName());
            map.put("status",step.getStatus().name());
            map.put("readCount",step.getReadCount());
            map.put("writeCount",step.getWriteCount());
            map.put("skipCount",step.getSkipCount());
            returnData.add(map);
        }
        Map<String,Object> dataMap = new HashMap();
        dataMap.put("id",idJob);
        dataMap.put("data",returnData);
        return AjaxResult.success(dataMap);
    }

    @GetMapping("/showDetail/{idExec}")
    public String showDetail(@PathVariable("idExec") Long idExec, ModelMap mmap)
    {
        List<Map<String,Object>> returnData = taskExecService.findBatchJobData(idExec);
        mmap.put("data", returnData);
        return "etl/exec/detail";
    }

    @ApiOperation(value = "手工执行任务")
    @RequestMapping(value = "jobExecByHand", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult jobExecByHand(@RequestBody JobExecParamVO jobExecParamVO) throws ParseException {
        jobExecParamVO.setExecType(JobConstant.JOB_EXE_TYPE_MANUAL);
        return jobRunService.run(jobExecParamVO);
    }


}
