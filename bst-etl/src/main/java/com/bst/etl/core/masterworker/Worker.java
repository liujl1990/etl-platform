package com.bst.etl.core.masterworker;

import com.bst.etl.batch.BatchExecService;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Worker implements Runnable {

    private ConcurrentLinkedQueue<JobParameters> workQueue;
    private ConcurrentHashMap<Long, JobExecution> resultMap;
    private BatchExecService batchExecService;

    public Worker(BatchExecService batchExecService) {
        this.batchExecService = batchExecService;
    }

    public void setWorkerQueue(ConcurrentLinkedQueue<JobParameters> workQueue) {
        this.workQueue = workQueue;
    }

    public void setResultMap(ConcurrentHashMap<Long, JobExecution> resultMap) {
        this.resultMap = resultMap;
    }

    @Override
    public void run() {
        while(workQueue!=null && workQueue.size()>0){
            JobParameters parameters = this.workQueue.poll();
            System.out.println("当前线程"+Thread.currentThread().getName()+"关于["+parameters.getString("des")+"]开始处理");
            if(null == parameters) {
                break;
            }
            try {
                batchExecService.runJob(parameters,resultMap);
                System.out.println("当前线程"+Thread.currentThread().getName()+"关于["+parameters.getString("des")+"]执行完毕");
            }catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }
    }
}
