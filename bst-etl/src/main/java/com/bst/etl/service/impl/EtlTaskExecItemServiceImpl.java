package com.bst.etl.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.etl.mapper.EtlTaskExecItemMapper;
import com.bst.etl.domain.EtlTaskExecItem;
import com.bst.etl.service.IEtlTaskExecItemService;
import com.bst.common.core.text.Convert;

/**
 * 任务执行记录明细Service业务层处理
 * 
 * @author 老刘
 * @date 2022-06-30
 */
@Service
public class EtlTaskExecItemServiceImpl implements IEtlTaskExecItemService 
{
    @Autowired
    private EtlTaskExecItemMapper etlTaskExecItemMapper;

    /**
     * 查询任务执行记录明细
     * 
     * @param idExecItem 任务执行记录明细主键
     * @return 任务执行记录明细
     */
    @Override
    public EtlTaskExecItem selectEtlTaskExecItemByIdExecItem(Long idExecItem)
    {
        return etlTaskExecItemMapper.selectEtlTaskExecItemByIdExecItem(idExecItem);
    }

    /**
     * 查询任务执行记录明细列表
     * 
     * @param etlTaskExecItem 任务执行记录明细
     * @return 任务执行记录明细
     */
    @Override
    public List<EtlTaskExecItem> selectEtlTaskExecItemList(EtlTaskExecItem etlTaskExecItem)
    {
        return etlTaskExecItemMapper.selectEtlTaskExecItemList(etlTaskExecItem);
    }

    /**
     * 新增任务执行记录明细
     * 
     * @param etlTaskExecItem 任务执行记录明细
     * @return 结果
     */
    @Override
    public int insertEtlTaskExecItem(EtlTaskExecItem etlTaskExecItem)
    {
        return etlTaskExecItemMapper.insertEtlTaskExecItem(etlTaskExecItem);
    }

    /**
     * 修改任务执行记录明细
     * 
     * @param etlTaskExecItem 任务执行记录明细
     * @return 结果
     */
    @Override
    public int updateEtlTaskExecItem(EtlTaskExecItem etlTaskExecItem)
    {
        return etlTaskExecItemMapper.updateEtlTaskExecItem(etlTaskExecItem);
    }

    /**
     * 批量删除任务执行记录明细
     * 
     * @param idExecItems 需要删除的任务执行记录明细主键
     * @return 结果
     */
    @Override
    public int deleteEtlTaskExecItemByIdExecItems(String idExecItems)
    {
        return etlTaskExecItemMapper.deleteEtlTaskExecItemByIdExecItems(Convert.toStrArray(idExecItems));
    }

    /**
     * 删除任务执行记录明细信息
     * 
     * @param idExecItem 任务执行记录明细主键
     * @return 结果
     */
    @Override
    public int deleteEtlTaskExecItemByIdExecItem(Long idExecItem)
    {
        return etlTaskExecItemMapper.deleteEtlTaskExecItemByIdExecItem(idExecItem);
    }
}
