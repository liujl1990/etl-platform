package com.bst.etl.service;

import com.bst.common.core.domain.AjaxResult;
import com.bst.common.vo.JobExecParamVO;

import java.text.ParseException;

public interface IEtlJobRunService {

    AjaxResult run(JobExecParamVO jobExecParamVO) throws ParseException;
}
