package com.bst.etl.service;

import java.util.List;
import java.util.Map;

import com.bst.etl.domain.EtlTaskExec;

/**
 * 任务执行记录Service接口
 * 
 * @author ruoyi
 * @date 2022-06-30
 */
public interface IEtlTaskExecService 
{
    /**
     * 查询任务执行记录
     * 
     * @param idExec 任务执行记录主键
     * @return 任务执行记录
     */
    public EtlTaskExec selectEtlTaskExecByIdExec(Long idExec);

    /**
     * 查询任务执行记录列表
     * 
     * @param etlTaskExec 任务执行记录
     * @return 任务执行记录集合
     */
    public List<EtlTaskExec> selectEtlTaskExecList(EtlTaskExec etlTaskExec);

    /**
     * 新增任务执行记录
     * 
     * @param etlTaskExec 任务执行记录
     * @return 结果
     */
    public Long insertEtlTaskExec(EtlTaskExec etlTaskExec);

    /**
     * 修改任务执行记录
     * 
     * @param etlTaskExec 任务执行记录
     * @return 结果
     */
    public int updateEtlTaskExec(EtlTaskExec etlTaskExec);

    /**
     * 批量删除任务执行记录
     * 
     * @param idExecs 需要删除的任务执行记录主键集合
     * @return 结果
     */
    public int deleteEtlTaskExecByIdExecs(String idExecs);

    /**
     * 删除任务执行记录信息
     * 
     * @param idExec 任务执行记录主键
     * @return 结果
     */
    public int deleteEtlTaskExecByIdExec(Long idExec);

    boolean dayBasedataExec(String dateStr,Long xxlJobId);

    List<Map<String,Object>> findBatchJobData(Long idExec);
}
