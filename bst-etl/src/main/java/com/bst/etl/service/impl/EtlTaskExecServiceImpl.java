package com.bst.etl.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.bst.common.utils.DateUtil;
import com.bst.etl.batch.dao.JdbcJobExecutionExDao;
import com.bst.etl.domain.EtlTaskExecItem;
import com.bst.etl.service.IEtlTaskExecItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.bst.etl.mapper.EtlTaskExecMapper;
import com.bst.etl.domain.EtlTaskExec;
import com.bst.etl.service.IEtlTaskExecService;
import com.bst.common.core.text.Convert;

/**
 * 任务执行记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-30
 */
@Service
public class EtlTaskExecServiceImpl implements IEtlTaskExecService 
{
    @Autowired
    private EtlTaskExecMapper etlTaskExecMapper;
    @Autowired
    private IEtlTaskExecItemService etlTaskExecItemService;
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 查询任务执行记录
     * 
     * @param idExec 任务执行记录主键
     * @return 任务执行记录
     */
    @Override
    public EtlTaskExec selectEtlTaskExecByIdExec(Long idExec)
    {
        return etlTaskExecMapper.selectEtlTaskExecByIdExec(idExec);
    }

    /**
     * 查询任务执行记录列表
     * 
     * @param etlTaskExec 任务执行记录
     * @return 任务执行记录
     */
    @Override
    public List<EtlTaskExec> selectEtlTaskExecList(EtlTaskExec etlTaskExec)
    {
        return etlTaskExecMapper.selectEtlTaskExecList(etlTaskExec);
    }

    /**
     * 新增任务执行记录
     * 
     * @param etlTaskExec 任务执行记录
     * @return 结果
     */
    @Override
    public Long insertEtlTaskExec(EtlTaskExec etlTaskExec)
    {
        return etlTaskExecMapper.insertEtlTaskExec(etlTaskExec);
    }

    /**
     * 修改任务执行记录
     * 
     * @param etlTaskExec 任务执行记录
     * @return 结果
     */
    @Override
    public int updateEtlTaskExec(EtlTaskExec etlTaskExec)
    {
        return etlTaskExecMapper.updateEtlTaskExec(etlTaskExec);
    }

    /**
     * 批量删除任务执行记录
     * 
     * @param idExecs 需要删除的任务执行记录主键
     * @return 结果
     */
    @Override
    public int deleteEtlTaskExecByIdExecs(String idExecs)
    {
        return etlTaskExecMapper.deleteEtlTaskExecByIdExecs(Convert.toStrArray(idExecs));
    }

    /**
     * 删除任务执行记录信息
     * 
     * @param idExec 任务执行记录主键
     * @return 结果
     */
    @Override
    public int deleteEtlTaskExecByIdExec(Long idExec)
    {
        return etlTaskExecMapper.deleteEtlTaskExecByIdExec(idExec);
    }

    @Override
    public boolean dayBasedataExec(String dateStr, Long xxlJobId) {
        return false;
    }

    @Override
    public List<Map<String,Object>> findBatchJobData(Long idExec) {
        EtlTaskExecItem taskExecItem = new EtlTaskExecItem();
        taskExecItem.setIdExec(idExec);
        List<EtlTaskExecItem> dataList = etlTaskExecItemService.selectEtlTaskExecItemList(taskExecItem);
        List<Long> idExecutions = new ArrayList<>();
        for(EtlTaskExecItem gb:dataList) {
            idExecutions.add(gb.getIdJob());
        }
        JdbcJobExecutionExDao jobExecutionExDao = JdbcJobExecutionExDao.getInstance(jdbcTemplate);
        List<JobExecution> executionList = jobExecutionExDao.findByIds(idExecutions);
        List<Map<String,Object>> returnData = new ArrayList<>();
        Map<String,Object> map;
        String dBegin,dEnd;
        for(JobExecution job:executionList) {
            dBegin = job.getJobParameters().getString("dBegin");
            if(StringUtils.isNotEmpty(dBegin) && dBegin.length()>8) {
                dBegin = dBegin.substring(0,8);
            }
            dEnd = job.getJobParameters().getString("dEnd");
            if(StringUtils.isNotEmpty(dEnd) && dEnd.length()>=8) {
                dEnd = dEnd.substring(0,8);
            }
            map = new LinkedHashMap<>();
            map.put("id",job.getId());
            map.put("dBegin",dBegin);
            map.put("dEnd",dEnd);
            map.put("status",job.getStatus().name());
            map.put("tableName",job.getJobParameters().getString("tableName"));
            map.put("des",job.getJobParameters().getString("des"));
            map.put("errorMsg",job.getExitStatus().getExitDescription());
            map.put("startDate", DateUtil.FORMAT_HHMMSS.format(job.getStartTime()));
            if(job.getEndTime()!=null) {
                map.put("endDate",DateUtil.FORMAT_HHMMSS.format(job.getEndTime()));
                map.put("times",(job.getEndTime().getTime()-job.getStartTime().getTime())/1000);
            } else {
                map.put("endDate","抽取中");
                map.put("times",0);
            }
            returnData.add(map);
        }
        return returnData;
    }
}
