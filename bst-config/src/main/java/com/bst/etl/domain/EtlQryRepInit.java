package com.bst.etl.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 报表数据初始化对象 etl_qry_rep_init
 * 
 * @author ruoyi
 * @date 2023-03-25
 */
public class EtlQryRepInit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idQryRepInit;

    /** 外键 */
    @Excel(name = "外键")
    private Long idQryRep;

    /** 编码 */
    @Excel(name = "编码")
    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 表名 */
    @Excel(name = "表名")
    private String cdTb;

    /** 日期 */
    @Excel(name = "日期")
    private String mm;

    private Integer num;

    /** 核对标志 */
    @Excel(name = "核对标志")
    private Integer fgCheck;

    /** 启用标志 */
    @Excel(name = "启用标志")
    private Integer fgAct;

    /** 初始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "初始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtInit;

    /** 初始人 */
    @Excel(name = "初始人")
    private String empInit;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public void setIdQryRepInit(Long idQryRepInit)
    {
        this.idQryRepInit = idQryRepInit;
    }

    public Long getIdQryRepInit() 
    {
        return idQryRepInit;
    }
    public void setIdQryRep(Long idQryRep) 
    {
        this.idQryRep = idQryRep;
    }

    public Long getIdQryRep() 
    {
        return idQryRep;
    }
    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setCdTb(String cdTb) 
    {
        this.cdTb = cdTb;
    }

    public String getCdTb() 
    {
        return cdTb;
    }
    public void setMm(String mm) 
    {
        this.mm = mm;
    }

    public String getMm() 
    {
        return mm;
    }
    public void setFgCheck(Integer fgCheck) 
    {
        this.fgCheck = fgCheck;
    }

    public Integer getFgCheck() 
    {
        return fgCheck;
    }
    public void setFgAct(Integer fgAct) 
    {
        this.fgAct = fgAct;
    }

    public Integer getFgAct() 
    {
        return fgAct;
    }
    public void setDtInit(Date dtInit) 
    {
        this.dtInit = dtInit;
    }

    public Date getDtInit() 
    {
        return dtInit;
    }
    public void setEmpInit(String empInit) 
    {
        this.empInit = empInit;
    }

    public String getEmpInit() 
    {
        return empInit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idQryRepInit", getIdQryRepInit())
            .append("idQryRep", getIdQryRep())
            .append("cd", getCd())
            .append("na", getNa())
            .append("cdTb", getCdTb())
            .append("mm", getMm())
            .append("fgCheck", getFgCheck())
            .append("fgAct", getFgAct())
            .append("dtInit", getDtInit())
            .append("empInit", getEmpInit())
            .toString();
    }
}
