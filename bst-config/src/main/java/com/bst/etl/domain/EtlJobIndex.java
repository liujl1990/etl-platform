package com.bst.etl.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 etl_job_index
 * 
 * @author ruoyi
 * @date 2022-09-01
 */
public class EtlJobIndex extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long idJobIndex;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idJob;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String idIndex;

    public void setIdJobIndex(Long idJobIndex) 
    {
        this.idJobIndex = idJobIndex;
    }

    public Long getIdJobIndex() 
    {
        return idJobIndex;
    }
    public void setIdJob(Long idJob) 
    {
        this.idJob = idJob;
    }

    public Long getIdJob() 
    {
        return idJob;
    }
    public void setIdIndex(String idIndex) 
    {
        this.idIndex = idIndex;
    }

    public String getIdIndex() 
    {
        return idIndex;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idJobIndex", getIdJobIndex())
            .append("idJob", getIdJob())
            .append("idIndex", getIdIndex())
            .toString();
    }
}
