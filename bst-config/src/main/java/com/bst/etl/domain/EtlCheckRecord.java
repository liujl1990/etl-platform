package com.bst.etl.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 核对记录对象 etl_check_record
 * 
 * @author ruoyi
 * @date 2023-03-09
 */
public class EtlCheckRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idRecord;

    /**  */
    @Excel(name = "")
    private Long idConfig;

    private String idIndex;

    /**  */
    @Excel(name = "")
    private String mm;

    /**  */
    @Excel(name = "")
    private Integer euStatus;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtCheck;

    /**  */
    @Excel(name = "")
    private String empCheck;

    /**  */
    @Excel(name = "")
    private String des;

    /**  */
    @Excel(name = "")
    private String naEmpCre;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public void setIdRecord(Long idRecord) 
    {
        this.idRecord = idRecord;
    }

    public Long getIdRecord() 
    {
        return idRecord;
    }
    public void setIdConfig(Long idConfig) 
    {
        this.idConfig = idConfig;
    }

    public Long getIdConfig() 
    {
        return idConfig;
    }
    public void setMm(String mm) 
    {
        this.mm = mm;
    }

    public String getMm() 
    {
        return mm;
    }
    public void setEuStatus(Integer euStatus) 
    {
        this.euStatus = euStatus;
    }

    public Integer getEuStatus() 
    {
        return euStatus;
    }
    public void setDtCheck(Date dtCheck) 
    {
        this.dtCheck = dtCheck;
    }

    public Date getDtCheck() 
    {
        return dtCheck;
    }
    public void setEmpCheck(String empCheck) 
    {
        this.empCheck = empCheck;
    }

    public String getEmpCheck() 
    {
        return empCheck;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    public String getIdIndex() {
        return idIndex;
    }

    public void setIdIndex(String idIndex) {
        this.idIndex = idIndex;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idRecord", getIdRecord())
            .append("idConfig", getIdConfig())
            .append("mm", getMm())
            .append("euStatus", getEuStatus())
            .append("dtCheck", getDtCheck())
            .append("empCheck", getEmpCheck())
            .append("des", getDes())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
