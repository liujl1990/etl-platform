package com.bst.etl.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 任务执行记录明细对象 etl_task_exec_item
 * 
 * @author 老刘
 * @date 2022-06-30
 */
public class EtlTaskExecItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idExecItem;

    /**  */
    @Excel(name = "")
    private Long idExec;

    /**  */
    @Excel(name = "")
    private Long idJob;

    /**  */
    @Excel(name = "")
    private String euStatus;

    public void setIdExecItem(Long idExecItem) 
    {
        this.idExecItem = idExecItem;
    }

    public Long getIdExecItem() 
    {
        return idExecItem;
    }
    public void setIdExec(Long idExec) 
    {
        this.idExec = idExec;
    }

    public Long getIdExec() 
    {
        return idExec;
    }
    public void setIdJob(Long idJob) 
    {
        this.idJob = idJob;
    }

    public Long getIdJob() 
    {
        return idJob;
    }
    public void setEuStatus(String euStatus) 
    {
        this.euStatus = euStatus;
    }

    public String getEuStatus() 
    {
        return euStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idExecItem", getIdExecItem())
            .append("idExec", getIdExec())
            .append("idJob", getIdJob())
            .append("euStatus", getEuStatus())
            .toString();
    }
}
