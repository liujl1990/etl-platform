package com.bst.etl.domain;

import java.io.Serializable;

public class EtlRepData implements Serializable {

    private String tbName;
    private Long id;
    private String cd;
    private String na;
    private String mm;
    private String data;
    private Integer num;//数据量
    private String cdOrg;

    public String getCdOrg() {
        return cdOrg;
    }

    public void setCdOrg(String cdOrg) {
        this.cdOrg = cdOrg;
    }

    public String getTbName() {
        return tbName;
    }

    public void setTbName(String tbName) {
        this.tbName = tbName;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getNa() {
        return na;
    }

    public void setNa(String na) {
        this.na = na;
    }

    public String getMm() {
        return mm;
    }

    public void setMm(String mm) {
        this.mm = mm;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
