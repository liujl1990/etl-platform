package com.bst.etl.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 抽取变量对象 etl_base_vari
 * 
 * @author ruoyi
 * @date 2023-03-10
 */
public class EtlBaseVari extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private String cd;

    /**  */
    @Excel(name = "")
    private String na;

    /**  */
    @Excel(name = "")
    private String valDef;

    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setValDef(String valDef) 
    {
        this.valDef = valDef;
    }

    public String getValDef() 
    {
        return valDef;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cd", getCd())
            .append("na", getNa())
            .append("valDef", getValDef())
            .toString();
    }
}
