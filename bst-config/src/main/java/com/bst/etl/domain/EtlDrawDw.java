package com.bst.etl.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 数据仓库对象 etl_draw_dw
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
public class EtlDrawDw extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idDrawDw;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** DW表 */
    @Excel(name = "DW表")
    private String cdTb;

    /** 所属系统 */
    @Excel(name = "所属系统")
    private String sdSys;

    /** 外键 */
    @Excel(name = "外键")
    private Long idDrawBase;

    /** 日期字段 */
    @Excel(name = "日期字段")
    private String fldDt;

    /** ods目标表 */
    @Excel(name = "ods目标表")
    private String cdTbTarOds;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    /** 启用标志 */
    @Excel(name = "启用标志")
    private Integer fgAct;

    private Integer euStatus;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    private String sdDwcls;

    public String getSdDwcls() {
        return sdDwcls;
    }

    public void setSdDwcls(String sdDwcls) {
        this.sdDwcls = sdDwcls;
    }

    public Integer getEuStatus() {
        return euStatus;
    }

    public void setEuStatus(Integer euStatus) {
        this.euStatus = euStatus;
    }

    public void setIdDrawDw(Long idDrawDw)
    {
        this.idDrawDw = idDrawDw;
    }

    public Long getIdDrawDw() 
    {
        return idDrawDw;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setCdTb(String cdTb) 
    {
        this.cdTb = cdTb;
    }

    public String getCdTb() 
    {
        return cdTb;
    }
    public void setSdSys(String sdSys) 
    {
        this.sdSys = sdSys;
    }

    public String getSdSys() 
    {
        return sdSys;
    }
    public void setIdDrawBase(Long idDrawBase) 
    {
        this.idDrawBase = idDrawBase;
    }

    public Long getIdDrawBase() 
    {
        return idDrawBase;
    }
    public void setFldDt(String fldDt) 
    {
        this.fldDt = fldDt;
    }

    public String getFldDt() 
    {
        return fldDt;
    }
    public void setCdTbTarOds(String cdTbTarOds) 
    {
        this.cdTbTarOds = cdTbTarOds;
    }

    public String getCdTbTarOds() 
    {
        return cdTbTarOds;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setFgAct(Integer fgAct) 
    {
        this.fgAct = fgAct;
    }

    public Integer getFgAct() 
    {
        return fgAct;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDrawDw", getIdDrawDw())
            .append("na", getNa())
            .append("cdTb", getCdTb())
            .append("sdSys", getSdSys())
            .append("idDrawBase", getIdDrawBase())
            .append("fldDt", getFldDt())
            .append("cdTbTarOds", getCdTbTarOds())
            .append("des", getDes())
            .append("fgAct", getFgAct())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
