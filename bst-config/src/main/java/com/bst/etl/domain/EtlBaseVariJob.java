package com.bst.etl.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 抽取变量关联定时任务对象 etl_base_vari_job
 * 
 * @author ruoyi
 * @date 2023-03-10
 */
public class EtlBaseVariJob extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idVariJob;

    /**  */
    @Excel(name = "")
    private String cdVari;

    /**  */
    @Excel(name = "")
    private Long idJob;

    /**  */
    @Excel(name = "")
    private String naJob;

    /**  */
    @Excel(name = "")
    private String val;

    public void setIdVariJob(Long idVariJob) 
    {
        this.idVariJob = idVariJob;
    }

    public Long getIdVariJob() 
    {
        return idVariJob;
    }
    public void setCdVari(String cdVari) 
    {
        this.cdVari = cdVari;
    }

    public String getCdVari() 
    {
        return cdVari;
    }
    public void setIdJob(Long idJob) 
    {
        this.idJob = idJob;
    }

    public Long getIdJob() 
    {
        return idJob;
    }
    public void setVal(String val) 
    {
        this.val = val;
    }

    public String getVal() 
    {
        return val;
    }

    public String getNaJob() {
        return naJob;
    }

    public void setNaJob(String naJob) {
        this.naJob = naJob;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idVariJob", getIdVariJob())
            .append("cdVari", getCdVari())
            .append("idJob", getIdJob())
            .append("val", getVal())
            .toString();
    }
}
