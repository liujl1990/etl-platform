package com.bst.etl.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 任务执行记录对象 etl_task_exec
 * 
 * @author ruoyi
 * @date 2022-06-30
 */
public class EtlTaskExec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idExec;

    /**  */
    @Excel(name = "")
    private String na;

    /** 0 自动  1手动 */
    @Excel(name = "0 自动  1手动")
    private Integer euTp;

    /**  */
    @Excel(name = "")
    private String sdEtlcls;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date dtBegin;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date dtEnd;

    /**  */
    @Excel(name = "")
    private Long times;

    private Long idJoblog;

    /**  */
    @Excel(name = "")
    private String scope;

    /**  */
    @Excel(name = "")
    private String euStatus;

    /**  */
    @Excel(name = "")
    private String naEmpCre;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public Long getIdJoblog() {
        return idJoblog;
    }

    public void setIdJoblog(Long idJoblog) {
        this.idJoblog = idJoblog;
    }

    public void setIdExec(Long idExec)
    {
        this.idExec = idExec;
    }

    public Long getIdExec() 
    {
        return idExec;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setEuTp(Integer euTp) 
    {
        this.euTp = euTp;
    }

    public Integer getEuTp() 
    {
        return euTp;
    }
    public void setSdEtlcls(String sdEtlcls) 
    {
        this.sdEtlcls = sdEtlcls;
    }

    public String getSdEtlcls() 
    {
        return sdEtlcls;
    }
    public void setDtBegin(Date dtBegin) 
    {
        this.dtBegin = dtBegin;
    }

    public Date getDtBegin() 
    {
        return dtBegin;
    }
    public void setDtEnd(Date dtEnd) 
    {
        this.dtEnd = dtEnd;
    }

    public Date getDtEnd() 
    {
        return dtEnd;
    }
    public void setTimes(Long times) 
    {
        this.times = times;
    }

    public Long getTimes() 
    {
        return times;
    }
    public void setScope(String scope) 
    {
        this.scope = scope;
    }

    public String getScope() 
    {
        return scope;
    }
    public void setEuStatus(String euStatus) 
    {
        this.euStatus = euStatus;
    }

    public String getEuStatus() 
    {
        return euStatus;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idExec", getIdExec())
            .append("na", getNa())
            .append("euTp", getEuTp())
            .append("sdEtlcls", getSdEtlcls())
            .append("dtBegin", getDtBegin())
            .append("dtEnd", getDtEnd())
            .append("times", getTimes())
            .append("scope", getScope())
            .append("euStatus", getEuStatus())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
