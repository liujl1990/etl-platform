package com.bst.etl.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 etl_draw_dw_fld
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
public class EtlDrawDwFld extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long idDrawDwFld;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDrawDw;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String cdTbDw;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fldOds;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fldDwMain;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fldDwSlave;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer fgIndex;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer odsFldType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date dtSysCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpModi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date dtSysModi;

    public void setIdDrawDwFld(Long idDrawDwFld) 
    {
        this.idDrawDwFld = idDrawDwFld;
    }

    public Long getIdDrawDwFld() 
    {
        return idDrawDwFld;
    }
    public void setIdDrawDw(Long idDrawDw) 
    {
        this.idDrawDw = idDrawDw;
    }

    public Long getIdDrawDw() 
    {
        return idDrawDw;
    }
    public void setCdTbDw(String cdTbDw) 
    {
        this.cdTbDw = cdTbDw;
    }

    public String getCdTbDw() 
    {
        return cdTbDw;
    }
    public void setFldOds(String fldOds) 
    {
        this.fldOds = fldOds;
    }

    public String getFldOds() 
    {
        return fldOds;
    }
    public void setFldDwMain(String fldDwMain) 
    {
        this.fldDwMain = fldDwMain;
    }

    public String getFldDwMain() 
    {
        return fldDwMain;
    }
    public void setFldDwSlave(String fldDwSlave) 
    {
        this.fldDwSlave = fldDwSlave;
    }

    public String getFldDwSlave() 
    {
        return fldDwSlave;
    }
    public void setFgIndex(Integer fgIndex) 
    {
        this.fgIndex = fgIndex;
    }

    public Integer getFgIndex() 
    {
        return fgIndex;
    }
    public void setOdsFldType(Integer odsFldType) 
    {
        this.odsFldType = odsFldType;
    }

    public Integer getOdsFldType() 
    {
        return odsFldType;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDrawDwFld", getIdDrawDwFld())
            .append("idDrawDw", getIdDrawDw())
            .append("cdTbDw", getCdTbDw())
            .append("fldOds", getFldOds())
            .append("fldDwMain", getFldDwMain())
            .append("fldDwSlave", getFldDwSlave())
            .append("fgIndex", getFgIndex())
            .append("odsFldType", getOdsFldType())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
