package com.bst.etl.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 审核配置对象 etl_check_config
 * 
 * @author ruoyi
 * @date 2023-03-09
 */
public class EtlCheckConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idConfig;

    /** 外键 */
    @Excel(name = "外键")
    private Long idDrawDm;

    /** 指标 */
    @Excel(name = "指标")
    private String idIndex;

    private String naIndex;

    /** 是否允许修改 */
    @Excel(name = "是否允许修改")
    private Integer fgModi;

    /** 系统 */
    @Excel(name = "系统")
    private String sdSys;

    /** dm表 */
    @Excel(name = "dm表")
    private String tbDm;

    /** app表 */
    @Excel(name = "app表")
    private String tbApp;

    /** 维度集 */
    @Excel(name = "维度集")
    private String grpFlds;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    private String totalFld; //统计字段

    public String getTotalFld() {
        return totalFld;
    }

    public void setTotalFld(String totalFld) {
        this.totalFld = totalFld;
    }

    public String getNaIndex() {
        return naIndex;
    }

    public void setNaIndex(String naIndex) {
        this.naIndex = naIndex;
    }

    public void setIdConfig(Long idConfig)
    {
        this.idConfig = idConfig;
    }

    public Long getIdConfig() 
    {
        return idConfig;
    }
    public void setIdDrawDm(Long idDrawDm) 
    {
        this.idDrawDm = idDrawDm;
    }

    public Long getIdDrawDm() 
    {
        return idDrawDm;
    }
    public void setIdIndex(String idIndex) 
    {
        this.idIndex = idIndex;
    }

    public String getIdIndex() 
    {
        return idIndex;
    }
    public void setFgModi(Integer fgModi) 
    {
        this.fgModi = fgModi;
    }

    public Integer getFgModi() 
    {
        return fgModi;
    }
    public void setSdSys(String sdSys) 
    {
        this.sdSys = sdSys;
    }

    public String getSdSys() 
    {
        return sdSys;
    }
    public void setTbDm(String tbDm) 
    {
        this.tbDm = tbDm;
    }

    public String getTbDm() 
    {
        return tbDm;
    }
    public void setTbApp(String tbApp) 
    {
        this.tbApp = tbApp;
    }

    public String getTbApp() 
    {
        return tbApp;
    }
    public void setGrpFlds(String grpFlds) 
    {
        this.grpFlds = grpFlds;
    }

    public String getGrpFlds() 
    {
        return grpFlds;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idConfig", getIdConfig())
            .append("idDrawDm", getIdDrawDm())
            .append("idIndex", getIdIndex())
            .append("fgModi", getFgModi())
            .append("sdSys", getSdSys())
            .append("tbDm", getTbDm())
            .append("tbApp", getTbApp())
            .append("grpFlds", getGrpFlds())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
