package com.bst.etl.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 报表数据表对象 etl_qry_rep
 * 
 * @author ruoyi
 * @date 2023-03-25
 */
public class EtlQryRep extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idQryRep;

    /** 编码 */
    @Excel(name = "编码")
    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 表名 */
    @Excel(name = "表名")
    private String cdTb;

    public void setIdQryRep(Long idQryRep) 
    {
        this.idQryRep = idQryRep;
    }

    public Long getIdQryRep() 
    {
        return idQryRep;
    }
    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setCdTb(String cdTb) 
    {
        this.cdTb = cdTb;
    }

    public String getCdTb() 
    {
        return cdTb;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idQryRep", getIdQryRep())
            .append("cd", getCd())
            .append("na", getNa())
            .append("cdTb", getCdTb())
            .toString();
    }
}
