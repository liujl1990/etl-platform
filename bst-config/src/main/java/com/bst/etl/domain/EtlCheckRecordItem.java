package com.bst.etl.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 审核配置对象 etl_check_record_item
 * 
 * @author ruoyi
 * @date 2023-03-09
 */
public class EtlCheckRecordItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idRecordItem;

    /**  */
    @Excel(name = "")
    private Long idRecord;

    /**  */
    @Excel(name = "")
    private String oldVal;

    /**  */
    @Excel(name = "")
    private String newVal;

    /**  */
    @Excel(name = "")
    private String naEmpCre;

    private Long idData;

    public Long getIdData() {
        return idData;
    }

    public void setIdData(Long idData) {
        this.idData = idData;
    }

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public void setIdRecordItem(Long idRecordItem) 
    {
        this.idRecordItem = idRecordItem;
    }

    public Long getIdRecordItem() 
    {
        return idRecordItem;
    }
    public void setIdRecord(Long idRecord) 
    {
        this.idRecord = idRecord;
    }

    public Long getIdRecord() 
    {
        return idRecord;
    }
    public void setOldVal(String oldVal) 
    {
        this.oldVal = oldVal;
    }

    public String getOldVal() 
    {
        return oldVal;
    }
    public void setNewVal(String newVal) 
    {
        this.newVal = newVal;
    }

    public String getNewVal() 
    {
        return newVal;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idRecordItem", getIdRecordItem())
            .append("idRecord", getIdRecord())
            .append("oldVal", getOldVal())
            .append("newVal", getNewVal())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
