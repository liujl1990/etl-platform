package com.bst.etl.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 基础抽取对象 etl_draw_base
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
public class EtlDrawBase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idDrawBase;

    /** 编码 */
    @Excel(name = "编码")
    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 系统 */
    @Excel(name = "系统")
    private String sdSys;

    /** 数据源 */
    @Excel(name = "数据源")
    private Integer idDb;

    /** 前置SQL */
    @Excel(name = "前置SQL")
    private String sqlBefore;

    /** 查询SQL */
    @Excel(name = "查询SQL")
    private String sqlQry;

    /** 启用标志 */
    @Excel(name = "启用标志")
    private Integer fgAct;

    /** 启用标志 */
    @Excel(name = "建表标志")
    private Integer fgTbCre;

    /** 抽取分类 */
    @Excel(name = "抽取分类")
    private String sdDrawcls;

    /** 目标表 */
    @Excel(name = "目标表")
    private String cdTbTar;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    /** $column.columnComment */
    private String euReq;

    /** $column.columnComment */
    private String url;

    /** $column.columnComment */
    private String inter;

    public String getEuReq() {
        return euReq;
    }

    public void setEuReq(String euReq) {
        this.euReq = euReq;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getInter() {
        return inter;
    }

    public void setInter(String inter) {
        this.inter = inter;
    }

    public Integer getFgTbCre() {
        return fgTbCre;
    }

    public void setFgTbCre(Integer fgTbCre) {
        this.fgTbCre = fgTbCre;
    }

    public void setIdDrawBase(Long idDrawBase)
    {
        this.idDrawBase = idDrawBase;
    }

    public Long getIdDrawBase() 
    {
        return idDrawBase;
    }
    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setSdSys(String sdSys) 
    {
        this.sdSys = sdSys;
    }

    public String getSdSys() 
    {
        return sdSys;
    }
    public void setIdDb(Integer idDb) 
    {
        this.idDb = idDb;
    }

    public Integer getIdDb() 
    {
        return idDb;
    }
    public void setSqlBefore(String sqlBefore) 
    {
        this.sqlBefore = sqlBefore;
    }

    public String getSqlBefore() 
    {
        return sqlBefore;
    }
    public void setSqlQry(String sqlQry) 
    {
        this.sqlQry = sqlQry;
    }

    public String getSqlQry() 
    {
        return sqlQry;
    }
    public void setFgAct(Integer fgAct) 
    {
        this.fgAct = fgAct;
    }

    public Integer getFgAct() 
    {
        return fgAct;
    }
    public void setSdDrawcls(String sdDrawcls) 
    {
        this.sdDrawcls = sdDrawcls;
    }

    public String getSdDrawcls() 
    {
        return sdDrawcls;
    }
    public void setCdTbTar(String cdTbTar) 
    {
        this.cdTbTar = cdTbTar;
    }

    public String getCdTbTar() 
    {
        return cdTbTar;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDrawBase", getIdDrawBase())
            .append("cd", getCd())
            .append("na", getNa())
            .append("sdSys", getSdSys())
            .append("idDb", getIdDb())
            .append("sqlBefore", getSqlBefore())
            .append("sqlQry", getSqlQry())
            .append("fgAct", getFgAct())
            .append("sdDrawcls", getSdDrawcls())
            .append("cdTbTar", getCdTbTar())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
