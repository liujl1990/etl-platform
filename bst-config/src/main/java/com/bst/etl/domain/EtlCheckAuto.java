package com.bst.etl.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 智能对比对象 etl_check_auto
 * 
 * @author ruoyi
 * @date 2023-04-04
 */
public class EtlCheckAuto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idCheckAuto;

    /**  */
    @Excel(name = "")
    private String dtType;

    /**  */
    @Excel(name = "")
    private String dt;

    /**  */
    @Excel(name = "")
    private Long hosSize;

    /**  */
    @Excel(name = "")
    private String hosTotal;

    /**  */
    @Excel(name = "")
    private String diff; //差值

    /**  */
    @Excel(name = "")
    private Long dmSize;

    /**  */
    @Excel(name = "")
    private String dmTotal;

    /**  */
    @Excel(name = "")
    private String naEmpCre;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dtSysModi;

    private String idIndex;

    private String naIndex;

    public String getDiff() {
        return diff;
    }

    public void setDiff(String diff) {
        this.diff = diff;
    }

    public String getNaIndex() {
        return naIndex;
    }

    public void setNaIndex(String naIndex) {
        this.naIndex = naIndex;
    }

    public String getIdIndex() {
        return idIndex;
    }

    public void setIdIndex(String idIndex) {
        this.idIndex = idIndex;
    }

    public void setIdCheckAuto(Long idCheckAuto)
    {
        this.idCheckAuto = idCheckAuto;
    }

    public Long getIdCheckAuto() 
    {
        return idCheckAuto;
    }
    public void setDtType(String dtType) 
    {
        this.dtType = dtType;
    }

    public String getDtType() 
    {
        return dtType;
    }
    public void setDt(String dt) 
    {
        this.dt = dt;
    }

    public String getDt() 
    {
        return dt;
    }
    public void setHosSize(Long hosSize) 
    {
        this.hosSize = hosSize;
    }

    public Long getHosSize() 
    {
        return hosSize;
    }
    public void setHosTotal(String hosTotal) 
    {
        this.hosTotal = hosTotal;
    }

    public String getHosTotal() 
    {
        return hosTotal;
    }

    public Long getDmSize() 
    {
        return dmSize;
    }
    public void setDmTotal(String dmTotal) 
    {
        this.dmTotal = dmTotal;
    }

    public String getDmTotal() 
    {
        return dmTotal;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    public void setDmSize(Long dmSize) {
        this.dmSize = dmSize;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idCheckAuto", getIdCheckAuto())
            .append("dtType", getDtType())
            .append("dt", getDt())
            .append("hosSize", getHosSize())
            .append("hosTotal", getHosTotal())
            .append("dmSize", getDmSize())
            .append("dmTotal", getDmTotal())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
