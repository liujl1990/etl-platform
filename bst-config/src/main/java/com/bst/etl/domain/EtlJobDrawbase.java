package com.bst.etl.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 定时调度对象 etl_job_drawbase
 * 
 * @author ruoyi
 * @date 2022-08-07
 */
public class EtlJobDrawbase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idJobDrawbase;

    /**  */
    @Excel(name = "")
    private Long idJob;

    /**  */
    @Excel(name = "")
    private Long idDrawBase;

    public void setIdJobDrawbase(Long idJobDrawbase) 
    {
        this.idJobDrawbase = idJobDrawbase;
    }

    public Long getIdJobDrawbase() 
    {
        return idJobDrawbase;
    }
    public void setIdJob(Long idJob) 
    {
        this.idJob = idJob;
    }

    public Long getIdJob() 
    {
        return idJob;
    }
    public void setIdDrawBase(Long idDrawBase) 
    {
        this.idDrawBase = idDrawBase;
    }

    public Long getIdDrawBase() 
    {
        return idDrawBase;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idJobDrawbase", getIdJobDrawbase())
            .append("idJob", getIdJob())
            .append("idDrawBase", getIdDrawBase())
            .toString();
    }
}
