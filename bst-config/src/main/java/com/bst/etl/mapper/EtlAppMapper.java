package com.bst.etl.mapper;

import com.bst.etl.vo.AppDataVO;
import com.bst.etl.vo.EtlAppInsertVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EtlAppMapper {
    int insertAppFromDm(EtlAppInsertVO insertVO);
    int deleteAppTable(@Param("tbNameApp")String tbNameApp,@Param("month")String month);
    List<Map<String,Object>> queryAppData(@Param("tbName")String tbName,@Param("month")String month,@Param("flds")String flds);
    int updateBatchAppData(List<AppDataVO> list);
}
