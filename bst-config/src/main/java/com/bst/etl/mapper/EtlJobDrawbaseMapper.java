package com.bst.etl.mapper;

import java.util.List;
import com.bst.etl.domain.EtlJobDrawbase;
import org.apache.ibatis.annotations.Param;

/**
 * 定时调度Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-07
 */
public interface EtlJobDrawbaseMapper 
{
    /**
     * 查询定时调度
     * 
     * @param idJobDrawbase 定时调度主键
     * @return 定时调度
     */
    public EtlJobDrawbase selectEtlJobDrawbaseByIdJobDrawbase(Long idJobDrawbase);

    /**
     * 查询定时调度列表
     * 
     * @param etlJobDrawbase 定时调度
     * @return 定时调度集合
     */
    public List<EtlJobDrawbase> selectEtlJobDrawbaseList(EtlJobDrawbase etlJobDrawbase);

    /**
     * 新增定时调度
     * 
     * @param etlJobDrawbase 定时调度
     * @return 结果
     */
    public int insertEtlJobDrawbase(EtlJobDrawbase etlJobDrawbase);

    /**
     * 修改定时调度
     * 
     * @param etlJobDrawbase 定时调度
     * @return 结果
     */
    public int updateEtlJobDrawbase(EtlJobDrawbase etlJobDrawbase);

    /**
     * 删除定时调度
     * 
     * @param idJobDrawbase 定时调度主键
     * @return 结果
     */
    public int deleteEtlJobDrawbaseByIdJobDrawbase(Long idJobDrawbase);

    /**
     * 批量删除定时调度
     * 
     * @param idJobDrawbases 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEtlJobDrawbaseByIdJobDrawbases(String[] idJobDrawbases);

    void deleteByIdDrawBase(@Param("idJob")Long idJob,@Param("idDrawBase")Long idDrawBase);
}
