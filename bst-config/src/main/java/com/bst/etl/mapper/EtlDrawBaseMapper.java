package com.bst.etl.mapper;

import java.util.List;
import com.bst.etl.domain.EtlDrawBase;
import org.apache.ibatis.annotations.Param;

/**
 * 基础抽取Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
public interface EtlDrawBaseMapper 
{
    /**
     * 查询基础抽取
     * 
     * @param idDrawBase 基础抽取主键
     * @return 基础抽取
     */
    public EtlDrawBase selectEtlDrawBaseByIdDrawBase(Long idDrawBase);

    /**
     * 查询基础抽取列表
     * 
     * @param etlDrawBase 基础抽取
     * @return 基础抽取集合
     */
    public List<EtlDrawBase> selectEtlDrawBaseList(EtlDrawBase etlDrawBase);

    List<EtlDrawBase> selectNotInJobList(@Param("idJob")Integer idJob);

    List<EtlDrawBase> selectByJobId(@Param("idJob")Integer idJob);
    /**
     * 新增基础抽取
     * 
     * @param etlDrawBase 基础抽取
     * @return 结果
     */
    public int insertEtlDrawBase(EtlDrawBase etlDrawBase);

    /**
     * 修改基础抽取
     * 
     * @param etlDrawBase 基础抽取
     * @return 结果
     */
    public int updateEtlDrawBase(EtlDrawBase etlDrawBase);

    /**
     * 删除基础抽取
     * 
     * @param idDrawBase 基础抽取主键
     * @return 结果
     */
    public int deleteEtlDrawBaseByIdDrawBase(Long idDrawBase);

    /**
     * 批量删除基础抽取
     * 
     * @param idDrawBases 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEtlDrawBaseByIdDrawBases(String[] idDrawBases);

    List<EtlDrawBase> selectByIdIndex(@Param("idIndex")String idIndex);

}
