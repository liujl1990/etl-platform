package com.bst.etl.mapper;

import java.util.List;
import com.bst.etl.domain.EtlTaskExecItem;

/**
 * 任务执行记录明细Mapper接口
 * 
 * @author 老刘
 * @date 2022-06-30
 */
public interface EtlTaskExecItemMapper 
{
    /**
     * 查询任务执行记录明细
     * 
     * @param idExecItem 任务执行记录明细主键
     * @return 任务执行记录明细
     */
    public EtlTaskExecItem selectEtlTaskExecItemByIdExecItem(Long idExecItem);

    /**
     * 查询任务执行记录明细列表
     * 
     * @param etlTaskExecItem 任务执行记录明细
     * @return 任务执行记录明细集合
     */
    public List<EtlTaskExecItem> selectEtlTaskExecItemList(EtlTaskExecItem etlTaskExecItem);

    /**
     * 新增任务执行记录明细
     * 
     * @param etlTaskExecItem 任务执行记录明细
     * @return 结果
     */
    public int insertEtlTaskExecItem(EtlTaskExecItem etlTaskExecItem);

    /**
     * 修改任务执行记录明细
     * 
     * @param etlTaskExecItem 任务执行记录明细
     * @return 结果
     */
    public int updateEtlTaskExecItem(EtlTaskExecItem etlTaskExecItem);

    /**
     * 删除任务执行记录明细
     * 
     * @param idExecItem 任务执行记录明细主键
     * @return 结果
     */
    public int deleteEtlTaskExecItemByIdExecItem(Long idExecItem);

    /**
     * 批量删除任务执行记录明细
     * 
     * @param idExecItems 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEtlTaskExecItemByIdExecItems(String[] idExecItems);
}
