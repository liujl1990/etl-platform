package com.bst.etl.mapper;

import java.util.List;
import com.bst.etl.domain.EtlDrawDw;
import org.apache.ibatis.annotations.Param;

/**
 * 数据仓库Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
public interface EtlDrawDwMapper 
{
    /**
     * 查询数据仓库
     * 
     * @param idDrawDw 数据仓库主键
     * @return 数据仓库
     */
    public EtlDrawDw selectEtlDrawDwByIdDrawDw(Long idDrawDw);

    /**
     * 查询数据仓库列表
     * 
     * @param etlDrawDw 数据仓库
     * @return 数据仓库集合
     */
    public List<EtlDrawDw> selectEtlDrawDwList(EtlDrawDw etlDrawDw);

    /**
     * 新增数据仓库
     * 
     * @param etlDrawDw 数据仓库
     * @return 结果
     */
    public int insertEtlDrawDw(EtlDrawDw etlDrawDw);

    /**
     * 修改数据仓库
     * 
     * @param etlDrawDw 数据仓库
     * @return 结果
     */
    public int updateEtlDrawDw(EtlDrawDw etlDrawDw);

    /**
     * 删除数据仓库
     * 
     * @param idDrawDw 数据仓库主键
     * @return 结果
     */
    public int deleteEtlDrawDwByIdDrawDw(Long idDrawDw);

    /**
     * 批量删除数据仓库
     * 
     * @param idDrawDws 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEtlDrawDwByIdDrawDws(String[] idDrawDws);

    List<EtlDrawDw> findByJobId(@Param("jobId")Long jobId);

    List<EtlDrawDw> selectEtlDrawDwByDwclses(@Param("list")List<String> sdDwclses);
}
