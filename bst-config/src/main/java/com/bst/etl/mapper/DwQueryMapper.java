package com.bst.etl.mapper;

import com.bst.etl.vo.DwQueryVO;

import java.util.List;
import java.util.Map;

public interface DwQueryMapper {

    List<Map<String,Object>> queryDwData(DwQueryVO queryVO);
}
