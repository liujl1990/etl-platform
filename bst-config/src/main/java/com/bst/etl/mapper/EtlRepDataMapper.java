package com.bst.etl.mapper;

import com.bst.etl.domain.EtlRepData;
import org.apache.ibatis.annotations.Param;

public interface EtlRepDataMapper {

    int deleteByMonthAndCd(@Param("cd")String cd,@Param("mm")String mm,@Param("cdOrg")String cdOrg,@Param("tbName")String tbName);

    int insertRepData(EtlRepData vo);

    EtlRepData selectDataByMonthAndCd(@Param("cd")String cd,@Param("mm")String mm,@Param("cdOrg")String cdOrg,@Param("tbName")String tbName);
}
