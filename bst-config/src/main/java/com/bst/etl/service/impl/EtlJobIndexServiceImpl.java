package com.bst.etl.service.impl;

import java.util.List;

import com.bst.etl.domain.EtlJobIndex;
import com.bst.etl.mapper.EtlJobIndexMapper;
import com.bst.etl.service.IEtlJobIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-09-01
 */
@Service
public class EtlJobIndexServiceImpl implements IEtlJobIndexService
{
    @Autowired
    private EtlJobIndexMapper etlJobIndexMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idJobIndex 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public EtlJobIndex selectEtlJobIndexByIdJobIndex(Long idJobIndex)
    {
        return etlJobIndexMapper.selectEtlJobIndexByIdJobIndex(idJobIndex);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param etlJobIndex 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<EtlJobIndex> selectEtlJobIndexList(EtlJobIndex etlJobIndex)
    {
        return etlJobIndexMapper.selectEtlJobIndexList(etlJobIndex);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param etlJobIndex 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertEtlJobIndex(EtlJobIndex etlJobIndex)
    {
        return etlJobIndexMapper.insertEtlJobIndex(etlJobIndex);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param etlJobIndex 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateEtlJobIndex(EtlJobIndex etlJobIndex)
    {
        return etlJobIndexMapper.updateEtlJobIndex(etlJobIndex);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idJobIndexs 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteEtlJobIndexByIdJobIndexs(String idJobIndexs)
    {
        return etlJobIndexMapper.deleteEtlJobIndexByIdJobIndexs(Convert.toStrArray(idJobIndexs));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idJobIndex 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteEtlJobIndexByIdJobIndex(Long idJobIndex)
    {
        return etlJobIndexMapper.deleteEtlJobIndexByIdJobIndex(idJobIndex);
    }

    @Override
    public int insertList(List<EtlJobIndex> list) {
        for(EtlJobIndex index:list) {
            this.insertEtlJobIndex(index);
        }
        return list.size();
    }

    @Override
    public void deleteByIdIndex(Long idJob, String idIndex) {
        etlJobIndexMapper.deleteByIdIndex(idJob,idIndex);
    }
}
