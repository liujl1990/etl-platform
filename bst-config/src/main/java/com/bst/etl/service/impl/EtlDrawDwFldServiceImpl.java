package com.bst.etl.service.impl;

import java.util.Date;
import java.util.List;

import com.bst.common.utils.self.LoginAPIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.etl.mapper.EtlDrawDwFldMapper;
import com.bst.etl.domain.EtlDrawDwFld;
import com.bst.etl.service.IEtlDrawDwFldService;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
@Service
public class EtlDrawDwFldServiceImpl implements IEtlDrawDwFldService 
{
    @Autowired
    private EtlDrawDwFldMapper etlDrawDwFldMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idDrawDwFld 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public EtlDrawDwFld selectEtlDrawDwFldByIdDrawDwFld(Long idDrawDwFld)
    {
        return etlDrawDwFldMapper.selectEtlDrawDwFldByIdDrawDwFld(idDrawDwFld);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param etlDrawDwFld 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<EtlDrawDwFld> selectEtlDrawDwFldList(EtlDrawDwFld etlDrawDwFld)
    {
        return etlDrawDwFldMapper.selectEtlDrawDwFldList(etlDrawDwFld);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param etlDrawDwFld 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertEtlDrawDwFld(EtlDrawDwFld etlDrawDwFld)
    {
        etlDrawDwFld.setDtSysCre(new Date());
        etlDrawDwFld.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        return etlDrawDwFldMapper.insertEtlDrawDwFld(etlDrawDwFld);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param etlDrawDwFld 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateEtlDrawDwFld(EtlDrawDwFld etlDrawDwFld)
    {
        return etlDrawDwFldMapper.updateEtlDrawDwFld(etlDrawDwFld);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDrawDwFlds 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteEtlDrawDwFldByIdDrawDwFlds(String idDrawDwFlds)
    {
        return etlDrawDwFldMapper.deleteEtlDrawDwFldByIdDrawDwFlds(Convert.toStrArray(idDrawDwFlds));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDrawDwFld 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteEtlDrawDwFldByIdDrawDwFld(Long idDrawDwFld)
    {
        return etlDrawDwFldMapper.deleteEtlDrawDwFldByIdDrawDwFld(idDrawDwFld);
    }

    @Override
    public void saveList(List<EtlDrawDwFld> fldList) {
        EtlDrawDwFld etlDrawDwFld = new EtlDrawDwFld();
        etlDrawDwFld.setIdDrawDw(fldList.get(0).getIdDrawDw());
        etlDrawDwFldMapper.deleteEtlDrawDwFld(etlDrawDwFld);
        for(EtlDrawDwFld fld:fldList) {
            this.insertEtlDrawDwFld(fld);
        }
    }
}
