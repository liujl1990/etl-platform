package com.bst.etl.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.bst.etl.domain.EtlBaseVariJob;
import com.bst.etl.service.IEtlBaseVariJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.etl.mapper.EtlBaseVariMapper;
import com.bst.etl.domain.EtlBaseVari;
import com.bst.etl.service.IEtlBaseVariService;
import com.bst.common.core.text.Convert;

/**
 * 抽取变量Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-10
 */
@Service
public class EtlBaseVariServiceImpl implements IEtlBaseVariService 
{
    @Autowired
    private EtlBaseVariMapper etlBaseVariMapper;
    @Autowired
    private IEtlBaseVariJobService etlBaseVariJobService;

    /**
     * 查询抽取变量
     * 
     * @param cd 抽取变量主键
     * @return 抽取变量
     */
    @Override
    public EtlBaseVari selectEtlBaseVariByCd(String cd)
    {
        return etlBaseVariMapper.selectEtlBaseVariByCd(cd);
    }

    /**
     * 查询抽取变量列表
     * 
     * @param etlBaseVari 抽取变量
     * @return 抽取变量
     */
    @Override
    public List<EtlBaseVari> selectEtlBaseVariList(EtlBaseVari etlBaseVari)
    {
        return etlBaseVariMapper.selectEtlBaseVariList(etlBaseVari);
    }

    /**
     * 新增抽取变量
     * 
     * @param etlBaseVari 抽取变量
     * @return 结果
     */
    @Override
    public int insertEtlBaseVari(EtlBaseVari etlBaseVari)
    {
        return etlBaseVariMapper.insertEtlBaseVari(etlBaseVari);
    }

    /**
     * 修改抽取变量
     * 
     * @param etlBaseVari 抽取变量
     * @return 结果
     */
    @Override
    public int updateEtlBaseVari(EtlBaseVari etlBaseVari)
    {
        return etlBaseVariMapper.updateEtlBaseVari(etlBaseVari);
    }

    /**
     * 批量删除抽取变量
     * 
     * @param cds 需要删除的抽取变量主键
     * @return 结果
     */
    @Override
    public int deleteEtlBaseVariByCds(String cds)
    {
        return etlBaseVariMapper.deleteEtlBaseVariByCds(Convert.toStrArray(cds));
    }

    /**
     * 删除抽取变量信息
     * 
     * @param cd 抽取变量主键
     * @return 结果
     */
    @Override
    public int deleteEtlBaseVariByCd(String cd)
    {
        return etlBaseVariMapper.deleteEtlBaseVariByCd(cd);
    }

    public Map<String,String> queryAllVariByIdJob(Long idJob) {
        List<EtlBaseVari> variList = this.selectEtlBaseVariList(new EtlBaseVari());
        if(idJob!=null) {
            EtlBaseVariJob jobModel = new EtlBaseVariJob();
            jobModel.setIdJob(idJob);
            List<EtlBaseVariJob> jobList = etlBaseVariJobService.selectEtlBaseVariJobList(jobModel);
            Map<String,String> jobMap = jobList.stream().collect(Collectors.toMap(EtlBaseVariJob::getCdVari,EtlBaseVariJob::getVal));
            String value;
            for (EtlBaseVari vari : variList) {
                if((value=jobMap.get(vari.getCd()))!=null) {
                    vari.setValDef(value);
                }
            }
        }
        Map<String,String> variMap = variList.stream().collect(Collectors.toMap(EtlBaseVari::getCd,EtlBaseVari::getValDef));
        return variMap;
    }
}
