package com.bst.etl.service;

import java.util.List;
import java.util.Map;

import com.bst.etl.domain.EtlBaseVari;

/**
 * 抽取变量Service接口
 * 
 * @author ruoyi
 * @date 2023-03-10
 */
public interface IEtlBaseVariService 
{
    /**
     * 查询抽取变量
     * 
     * @param cd 抽取变量主键
     * @return 抽取变量
     */
    public EtlBaseVari selectEtlBaseVariByCd(String cd);

    /**
     * 查询抽取变量列表
     * 
     * @param etlBaseVari 抽取变量
     * @return 抽取变量集合
     */
    public List<EtlBaseVari> selectEtlBaseVariList(EtlBaseVari etlBaseVari);

    /**
     * 新增抽取变量
     * 
     * @param etlBaseVari 抽取变量
     * @return 结果
     */
    public int insertEtlBaseVari(EtlBaseVari etlBaseVari);

    /**
     * 修改抽取变量
     * 
     * @param etlBaseVari 抽取变量
     * @return 结果
     */
    public int updateEtlBaseVari(EtlBaseVari etlBaseVari);

    /**
     * 批量删除抽取变量
     * 
     * @param cds 需要删除的抽取变量主键集合
     * @return 结果
     */
    public int deleteEtlBaseVariByCds(String cds);

    /**
     * 删除抽取变量信息
     * 
     * @param cd 抽取变量主键
     * @return 结果
     */
    public int deleteEtlBaseVariByCd(String cd);

    Map<String,String> queryAllVariByIdJob(Long idJob);
}
