package com.bst.etl.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.etl.mapper.EtlJobDrawbaseMapper;
import com.bst.etl.domain.EtlJobDrawbase;
import com.bst.etl.service.IEtlJobDrawbaseService;
import com.bst.common.core.text.Convert;
import org.springframework.transaction.annotation.Transactional;

/**
 * 定时调度Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-07
 */
@Service
public class EtlJobDrawbaseServiceImpl implements IEtlJobDrawbaseService 
{
    @Autowired
    private EtlJobDrawbaseMapper etlJobDrawbaseMapper;

    /**
     * 查询定时调度
     * 
     * @param idJobDrawbase 定时调度主键
     * @return 定时调度
     */
    @Override
    public EtlJobDrawbase selectEtlJobDrawbaseByIdJobDrawbase(Long idJobDrawbase)
    {
        return etlJobDrawbaseMapper.selectEtlJobDrawbaseByIdJobDrawbase(idJobDrawbase);
    }

    /**
     * 查询定时调度列表
     * 
     * @param etlJobDrawbase 定时调度
     * @return 定时调度
     */
    @Override
    public List<EtlJobDrawbase> selectEtlJobDrawbaseList(EtlJobDrawbase etlJobDrawbase)
    {
        return etlJobDrawbaseMapper.selectEtlJobDrawbaseList(etlJobDrawbase);
    }

    /**
     * 新增定时调度
     * 
     * @param etlJobDrawbase 定时调度
     * @return 结果
     */
    @Override
    public int insertEtlJobDrawbase(EtlJobDrawbase etlJobDrawbase)
    {
        return etlJobDrawbaseMapper.insertEtlJobDrawbase(etlJobDrawbase);
    }

    /**
     * 修改定时调度
     * 
     * @param etlJobDrawbase 定时调度
     * @return 结果
     */
    @Override
    public int updateEtlJobDrawbase(EtlJobDrawbase etlJobDrawbase)
    {
        return etlJobDrawbaseMapper.updateEtlJobDrawbase(etlJobDrawbase);
    }

    /**
     * 批量删除定时调度
     * 
     * @param idJobDrawbases 需要删除的定时调度主键
     * @return 结果
     */
    @Override
    public int deleteEtlJobDrawbaseByIdJobDrawbases(String idJobDrawbases)
    {
        return etlJobDrawbaseMapper.deleteEtlJobDrawbaseByIdJobDrawbases(Convert.toStrArray(idJobDrawbases));
    }

    /**
     * 删除定时调度信息
     * 
     * @param idJobDrawbase 定时调度主键
     * @return 结果
     */
    @Override
    public int deleteEtlJobDrawbaseByIdJobDrawbase(Long idJobDrawbase)
    {
        return etlJobDrawbaseMapper.deleteEtlJobDrawbaseByIdJobDrawbase(idJobDrawbase);
    }

    @Override
    @Transactional
    public int insertList(List<EtlJobDrawbase> list) {
        for(EtlJobDrawbase jobDrawbase:list) {
            this.insertEtlJobDrawbase(jobDrawbase);
        }
        return list.size();
    }

    @Override
    public void deleteByIdDrawBase(Long idJob, Long idDrawBase) {
        etlJobDrawbaseMapper.deleteByIdDrawBase(idJob,idDrawBase);
    }
}
