package com.bst.etl.service;

import java.util.Date;
import java.util.List;

import com.bst.common.exception.base.BaseException;
import com.bst.common.vo.DataExecParamVO;
import com.bst.common.vo.TableDDLColumnVO;
import com.bst.etl.domain.EtlDrawBase;
import org.apache.ibatis.annotations.Param;

/**
 * 基础抽取Service接口
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
public interface IEtlDrawBaseService 
{
    /**
     * 查询基础抽取
     * 
     * @param idDrawBase 基础抽取主键
     * @return 基础抽取
     */
    public EtlDrawBase selectEtlDrawBaseByIdDrawBase(Long idDrawBase);

    /**
     * 查询基础抽取列表
     * 
     * @param etlDrawBase 基础抽取
     * @return 基础抽取集合
     */
    public List<EtlDrawBase> selectEtlDrawBaseList(EtlDrawBase etlDrawBase);

    /**
     * 新增基础抽取
     * 
     * @param etlDrawBase 基础抽取
     * @return 结果
     */
    public int insertEtlDrawBase(EtlDrawBase etlDrawBase);

    /**
     * 修改基础抽取
     * 
     * @param etlDrawBase 基础抽取
     * @return 结果
     */
    public int updateEtlDrawBase(EtlDrawBase etlDrawBase);

    /**
     * 批量删除基础抽取
     * 
     * @param idDrawBases 需要删除的基础抽取主键集合
     * @return 结果
     */
    public int deleteEtlDrawBaseByIdDrawBases(String idDrawBases);

    /**
     * 删除基础抽取信息
     * 
     * @param idDrawBase 基础抽取主键
     * @return 结果
     */
    public int deleteEtlDrawBaseByIdDrawBase(Long idDrawBase);

    /**
     * 根据ID获取可执行基础任务
     * @param ids
     * @param startDate
     * @param endDate
     * @return
     */
    List<DataExecParamVO> findBaseTaskByIds(List<String> ids, Date startDate, Date endDate);

    List<TableDDLColumnVO> createOrModiTableColumnList(Long idTaskOds) throws BaseException;

    void execTableDDLColumns(Long id) throws BaseException;

    List<String> findDateFld(Long idDrawBase);

    List<EtlDrawBase> selectNotInJobList(@Param("idJob")Integer idJob);

    List<EtlDrawBase> selectByJobId(@Param("idJob")Integer idJob);
}
