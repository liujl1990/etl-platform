package com.bst.etl.service;

import java.util.Date;
import java.util.List;

import com.bst.common.core.domain.AjaxResult;
import com.bst.common.vo.DataExecParamVO;
import com.bst.etl.vo.EtlTaskDwFldVO;
import com.bst.etl.domain.EtlDrawDw;
import org.apache.ibatis.annotations.Param;

/**
 * 数据仓库Service接口
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
public interface IEtlDrawDwService 
{
    /**
     * 查询数据仓库
     * 
     * @param idDrawDw 数据仓库主键
     * @return 数据仓库
     */
    public EtlDrawDw selectEtlDrawDwByIdDrawDw(Long idDrawDw);

    /**
     * 查询数据仓库列表
     * 
     * @param etlDrawDw 数据仓库
     * @return 数据仓库集合
     */
    public List<EtlDrawDw> selectEtlDrawDwList(EtlDrawDw etlDrawDw);

    /**
     * 新增数据仓库
     * 
     * @param etlDrawDw 数据仓库
     * @return 结果
     */
    public int insertEtlDrawDw(EtlDrawDw etlDrawDw);

    /**
     * 修改数据仓库
     * 
     * @param etlDrawDw 数据仓库
     * @return 结果
     */
    public int updateEtlDrawDw(EtlDrawDw etlDrawDw);

    /**
     * 批量删除数据仓库
     * 
     * @param idDrawDws 需要删除的数据仓库主键集合
     * @return 结果
     */
    public int deleteEtlDrawDwByIdDrawDws(String idDrawDws);

    /**
     * 删除数据仓库信息
     * 
     * @param idDrawDw 数据仓库主键
     * @return 结果
     */
    public int deleteEtlDrawDwByIdDrawDw(Long idDrawDw);
    List<EtlTaskDwFldVO> findAllFld(Long idDrawDw,Long idDrawBase);
    AjaxResult createTable(Long idDrawDw);
    List<DataExecParamVO> findParamVO(List<String> ids, Date startDate, Date endDate);
    List<DataExecParamVO> findParamVOByJobId(Long jobId, Date startDate, Date endDate);

    List<EtlDrawDw> selectEtlDrawDwByDwclses(List<String> sdDwclses);
}
