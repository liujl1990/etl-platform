package com.bst.etl.service;

import com.bst.etl.domain.EtlJobIndex;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2022-09-01
 */
public interface IEtlJobIndexService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idJobIndex 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public EtlJobIndex selectEtlJobIndexByIdJobIndex(Long idJobIndex);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param etlJobIndex 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<EtlJobIndex> selectEtlJobIndexList(EtlJobIndex etlJobIndex);

    /**
     * 新增【请填写功能名称】
     * 
     * @param etlJobIndex 【请填写功能名称】
     * @return 结果
     */
    public int insertEtlJobIndex(EtlJobIndex etlJobIndex);

    int insertList(List<EtlJobIndex> list);

    /**
     * 修改【请填写功能名称】
     * 
     * @param etlJobIndex 【请填写功能名称】
     * @return 结果
     */
    public int updateEtlJobIndex(EtlJobIndex etlJobIndex);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idJobIndexs 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteEtlJobIndexByIdJobIndexs(String idJobIndexs);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idJobIndex 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteEtlJobIndexByIdJobIndex(Long idJobIndex);
    void deleteByIdIndex(@Param("idJob")Long idJob, @Param("idIndex")String idIndex);
}
