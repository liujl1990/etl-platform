package com.bst.etl.service;

import com.bst.common.exception.base.BaseException;
import com.bst.common.vo.TableDDLColumnVO;
import com.bst.common.vo.TableDDLSqlVO;
import com.bst.md.domain.MdMedTbFld;

import java.util.List;

/**
 * 1、生成创建与修改SQL，使用SQL执行表DDL操作  -  SQL
 * 2、生成表每列的定义列表，使用定义列表执行表DDL操作  -  ColumnList
 * 3、通过新增/修改元数据来执行表DDL操作   - MdMedTbFld
 * <p>
 * Created by eleven on 2021/11/24.
 */
public interface ITableDDLService {

    /**
     * 指定数据源 保存元数据并修改/创建对应的表
     * 没表时将创建; 参数中的List<MdMedTbFld>认为是全部列，所以不在List<MdMedTbFld>中的元数据与对应表的列将删除
     *
     * @param mdMedTbFlds  必填：idTb idPubfld 其余字段空则使用默认值
     * @param dataSourceID
     * @return 执行的SQL
     */
    String saveMdMedTbFldAndSynchroniseTable(List<MdMedTbFld> mdMedTbFlds, String dataSourceID) throws BaseException;

    /**
     * 数据仓库中 保存元数据并修改/创建对应的表
     * 没表时将创建; 参数中的List<MdMedTbFld>认为是全部列，所以不在List<MdMedTbFld>中的元数据与对应表的列将删除
     *
     * @param mdMedTbFlds 必填：idTb idPubfld 其余字段空则使用默认值
     * @return 执行的SQL
     */
    String saveMdMedTbFldAndSynchroniseTableOfDW(List<MdMedTbFld> mdMedTbFlds) throws BaseException;


    /**
     * 指定数据源 修改元数据并修改/创建对应的表
     * 没表时将创建; 参数中的List<MdMedTbFld>认为是部分列,执行新增/修改列，所以不在List<MdMedTbFld>中的元数据将不会动
     *
     * @param mdMedTbFlds  必填：idTb idPubfld 其余字段空则使用默认值
     * @param dataSourceID
     * @return 执行的SQL
     */
    String updateMdMedTbFldAndSynchroniseTable(List<MdMedTbFld> mdMedTbFlds, String dataSourceID) throws BaseException;

    /**
     * 数据仓库中 修改元数据并修改/创建对应的表
     * 没表时将创建; 参数中的List<MdMedTbFld>认为是部分列,执行新增/修改列，所以不在List<MdMedTbFld>中的元数据将不会动
     *
     * @param mdMedTbFlds 必填：idTb idPubfld 其余字段空则使用默认值
     * @return 执行的SQL
     */
    String updateMdMedTbFldAndSynchroniseTableOfDW(List<MdMedTbFld> mdMedTbFlds) throws BaseException;


    /**
     * 指定数据源 删除元数据并删除对应的表的列
     * 删除指定元数据与对应表的列
     *
     * @param mdMedTbFlds  必填：idTbFld 或者 idTb idPubfld
     * @param dataSourceID
     * @return 执行的SQL
     */
    String deleteMdMedTbFldAndSynchroniseTable(List<MdMedTbFld> mdMedTbFlds, String dataSourceID) throws BaseException;

    /**
     * 数据仓库中 删除元数据并删除对应的表的列
     * 删除指定元数据与对应表的列
     *
     * @param mdMedTbFlds 必填：idTbFld 或者 idTb idPubfld
     * @return 执行的SQL
     */
    String deleteMdMedTbFldAndSynchroniseTableOfDW(List<MdMedTbFld> mdMedTbFlds) throws BaseException;

    /**
     * 数据仓库中 复制表
     *
     * @param sourTable
     * @param tarTable
     * @return
     * @throws BaseException
     * @throws IllegalAccessException
     */
    TableDDLSqlVO generateCopyTableDDLSQLOfDW(String sourTable, String tarTable) throws BaseException;


    /**
     * 数据仓库中 根据查询语句生成创建与修改对应的表的SQL
     *
     * @param querySQL
     * @param tarTable
     * @return
     * @throws BaseException
     * @throws IllegalAccessException
     */
    TableDDLSqlVO generateTableDDLSQLOfDW(String querySQL, String tarTable) throws BaseException;

    /**
     * 指定数据源 根据查询语句生成创建与修改对应的表的SQL
     *
     * @param querySQL
     * @param sourDataSourceID
     * @param tarTable
     * @param tarDataSourceID
     * @return
     * @throws BaseException
     * @throws IllegalAccessException
     */
    TableDDLSqlVO generateTableDDLSQL(String querySQL, String sourDataSourceID, String tarTable, String tarDataSourceID) throws BaseException;


    /**
     * 数据仓库中 执行DDL语句
     *
     * @param sql
     * @throws BaseException
     */
    void execTableDDLSQLOfDW(String sql) throws BaseException;

    /**
     * 指定数据源 执行DDL语句
     *
     * @param sqls
     * @param dataSourceID
     * @throws BaseException
     */
    void execTableDDLSQL(String sqls, String dataSourceID) throws BaseException;


    /**
     * 指定数据源 根据查询语句生成列的变动情况的集合 列定义列表
     *
     * @param querySQL
     * @param sourDataSourceID
     * @param tarTable
     * @param tarDataSourceID
     * @return
     * @throws BaseException
     * @throws IllegalAccessException
     */
    List<TableDDLColumnVO> generateTableDDLColumnList(String querySQL, String sourDataSourceID, String tarTable, String tarDataSourceID) throws BaseException;

    /**
     * 数据仓库中 根据查询语句生成列的变动情况的集合 列定义列表
     *
     * @param querySQL
     * @param tarTable
     * @return
     * @throws BaseException
     * @throws IllegalAccessException
     */
    List<TableDDLColumnVO> generateTableDDLColumnListOfDW(String querySQL, String tarTable) throws BaseException;

    /**
     * 数据仓库中 根据列定义列表创建或修改表
     *
     * @param createTableDDLVO
     * @param tbTar
     * @throws BaseException
     */
    void execTableDDLColumnListOfDW(List<TableDDLColumnVO> createTableDDLVO, String tbTar) throws BaseException;

    /**
     * 指定数据源 根据列定义列表创建或修改表
     *
     * @param createTableDDLVO
     * @param tbTar
     * @throws BaseException
     */
    void execTableDDLColumnList(List<TableDDLColumnVO> createTableDDLVO, String tbTar, String dataSourceID) throws BaseException;
}
