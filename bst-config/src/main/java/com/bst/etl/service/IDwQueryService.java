package com.bst.etl.service;
import com.bst.common.core.domain.AjaxResult;
import com.bst.etl.vo.DwQueryVO;

import java.util.List;
import java.util.Map;

public interface IDwQueryService {

    List<Map<String,Object>> queryFromDw(DwQueryVO queryVO);


    AjaxResult findDwDataWinthHeader(DwQueryVO queryVO);

}
