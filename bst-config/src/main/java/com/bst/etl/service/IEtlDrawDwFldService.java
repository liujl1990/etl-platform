package com.bst.etl.service;

import java.util.List;
import com.bst.etl.domain.EtlDrawDwFld;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
public interface IEtlDrawDwFldService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idDrawDwFld 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public EtlDrawDwFld selectEtlDrawDwFldByIdDrawDwFld(Long idDrawDwFld);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param etlDrawDwFld 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<EtlDrawDwFld> selectEtlDrawDwFldList(EtlDrawDwFld etlDrawDwFld);

    /**
     * 新增【请填写功能名称】
     * 
     * @param etlDrawDwFld 【请填写功能名称】
     * @return 结果
     */
    public int insertEtlDrawDwFld(EtlDrawDwFld etlDrawDwFld);

    /**
     * 修改【请填写功能名称】
     * 
     * @param etlDrawDwFld 【请填写功能名称】
     * @return 结果
     */
    public int updateEtlDrawDwFld(EtlDrawDwFld etlDrawDwFld);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDrawDwFlds 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteEtlDrawDwFldByIdDrawDwFlds(String idDrawDwFlds);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDrawDwFld 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteEtlDrawDwFldByIdDrawDwFld(Long idDrawDwFld);

    void saveList(List<EtlDrawDwFld> fldList);
}
