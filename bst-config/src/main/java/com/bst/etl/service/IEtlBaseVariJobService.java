package com.bst.etl.service;

import java.util.List;
import com.bst.etl.domain.EtlBaseVariJob;

/**
 * 抽取变量关联定时任务Service接口
 * 
 * @author ruoyi
 * @date 2023-03-10
 */
public interface IEtlBaseVariJobService 
{
    /**
     * 查询抽取变量关联定时任务
     * 
     * @param idVariJob 抽取变量关联定时任务主键
     * @return 抽取变量关联定时任务
     */
    public EtlBaseVariJob selectEtlBaseVariJobByIdVariJob(Long idVariJob);

    /**
     * 查询抽取变量关联定时任务列表
     * 
     * @param etlBaseVariJob 抽取变量关联定时任务
     * @return 抽取变量关联定时任务集合
     */
    public List<EtlBaseVariJob> selectEtlBaseVariJobList(EtlBaseVariJob etlBaseVariJob);

    /**
     * 新增抽取变量关联定时任务
     * 
     * @param etlBaseVariJob 抽取变量关联定时任务
     * @return 结果
     */
    public int insertEtlBaseVariJob(EtlBaseVariJob etlBaseVariJob);

    /**
     * 修改抽取变量关联定时任务
     * 
     * @param etlBaseVariJob 抽取变量关联定时任务
     * @return 结果
     */
    public int updateEtlBaseVariJob(EtlBaseVariJob etlBaseVariJob);

    /**
     * 批量删除抽取变量关联定时任务
     * 
     * @param idVariJobs 需要删除的抽取变量关联定时任务主键集合
     * @return 结果
     */
    public int deleteEtlBaseVariJobByIdVariJobs(String idVariJobs);

    /**
     * 删除抽取变量关联定时任务信息
     * 
     * @param idVariJob 抽取变量关联定时任务主键
     * @return 结果
     */
    public int deleteEtlBaseVariJobByIdVariJob(Long idVariJob);
}
