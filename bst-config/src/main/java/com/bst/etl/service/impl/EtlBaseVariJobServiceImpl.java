package com.bst.etl.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.etl.mapper.EtlBaseVariJobMapper;
import com.bst.etl.domain.EtlBaseVariJob;
import com.bst.etl.service.IEtlBaseVariJobService;
import com.bst.common.core.text.Convert;

/**
 * 抽取变量关联定时任务Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-10
 */
@Service
public class EtlBaseVariJobServiceImpl implements IEtlBaseVariJobService 
{
    @Autowired
    private EtlBaseVariJobMapper etlBaseVariJobMapper;

    /**
     * 查询抽取变量关联定时任务
     * 
     * @param idVariJob 抽取变量关联定时任务主键
     * @return 抽取变量关联定时任务
     */
    @Override
    public EtlBaseVariJob selectEtlBaseVariJobByIdVariJob(Long idVariJob)
    {
        return etlBaseVariJobMapper.selectEtlBaseVariJobByIdVariJob(idVariJob);
    }

    /**
     * 查询抽取变量关联定时任务列表
     * 
     * @param etlBaseVariJob 抽取变量关联定时任务
     * @return 抽取变量关联定时任务
     */
    @Override
    public List<EtlBaseVariJob> selectEtlBaseVariJobList(EtlBaseVariJob etlBaseVariJob)
    {
        return etlBaseVariJobMapper.selectEtlBaseVariJobList(etlBaseVariJob);
    }

    /**
     * 新增抽取变量关联定时任务
     * 
     * @param etlBaseVariJob 抽取变量关联定时任务
     * @return 结果
     */
    @Override
    public int insertEtlBaseVariJob(EtlBaseVariJob etlBaseVariJob)
    {
        return etlBaseVariJobMapper.insertEtlBaseVariJob(etlBaseVariJob);
    }

    /**
     * 修改抽取变量关联定时任务
     * 
     * @param etlBaseVariJob 抽取变量关联定时任务
     * @return 结果
     */
    @Override
    public int updateEtlBaseVariJob(EtlBaseVariJob etlBaseVariJob)
    {
        return etlBaseVariJobMapper.updateEtlBaseVariJob(etlBaseVariJob);
    }

    /**
     * 批量删除抽取变量关联定时任务
     * 
     * @param idVariJobs 需要删除的抽取变量关联定时任务主键
     * @return 结果
     */
    @Override
    public int deleteEtlBaseVariJobByIdVariJobs(String idVariJobs)
    {
        return etlBaseVariJobMapper.deleteEtlBaseVariJobByIdVariJobs(Convert.toStrArray(idVariJobs));
    }

    /**
     * 删除抽取变量关联定时任务信息
     * 
     * @param idVariJob 抽取变量关联定时任务主键
     * @return 结果
     */
    @Override
    public int deleteEtlBaseVariJobByIdVariJob(Long idVariJob)
    {
        return etlBaseVariJobMapper.deleteEtlBaseVariJobByIdVariJob(idVariJob);
    }
}
