package com.bst.etl.utils;

import com.bst.common.constant.JobConstant;
import com.bst.etl.vo.SelectFldVO;

public class MedUtil {

    public static String betweenSelAndFromSql(SelectFldVO fldVO) {
        String valueName = fldVO.getValueAlias()==null?fldVO.getValueFld():fldVO.getValueAlias();
        return "select "+fldVO.getDimsAndNames()+fldVO.getValue()+" "+valueName+","+fldVO.getDay()+" from ";
    }

    //1 小数,2 整数,3 百分数,4 比值
    public static String getValueFld(String euValtp) {

        if(JobConstant.VALUE_FLOAT.equals(euValtp)) { //小数插入fl
            return "VALUE_FLOAT";
        } else if(JobConstant.VALUE_INT.equals(euValtp)) { //小数插入fl
            return "VALUE_INT";
        }
        return null;
    }

    public static String getDateFlter(String dateType) {
        switch (dateType) {
            case "M":
                return "dd.mm";
            case "Q":
                return "dd.qq";
            case "Y":
                return "dd.yy";
            default:
                return "dm.id_dim_day_occur";
        }
    }
}
