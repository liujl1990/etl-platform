package com.bst.etl.utils;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;

import java.util.Map;

public class FormulaUtil {
    // 计算的 方法
    public static String calculation(String jexlExp, Map<String, Object> map){
        JexlEngine jexl = new JexlEngine();
        Expression expression = jexl.createExpression(jexlExp);
        JexlContext jc = new MapContext();
        if(map!=null) {
            for (String key : map.keySet()) {
                jc.set(key, map.get(key));
            }
        }
        if (null == expression.evaluate(jc)) {
            return "";
        }
        return expression.evaluate(jc).toString();
    }
}
