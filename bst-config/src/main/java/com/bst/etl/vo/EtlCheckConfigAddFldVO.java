package com.bst.etl.vo;

import com.bst.md.domain.MdMedTbFld;

import java.io.Serializable;
import java.util.List;

public class EtlCheckConfigAddFldVO implements Serializable {

    private Long idConfig;

    private List<MdMedTbFld> fldList;

    public Long getIdConfig() {
        return idConfig;
    }

    public void setIdConfig(Long idConfig) {
        this.idConfig = idConfig;
    }

    public List<MdMedTbFld> getFldList() {
        return fldList;
    }

    public void setFldList(List<MdMedTbFld> fldList) {
        this.fldList = fldList;
    }
}
