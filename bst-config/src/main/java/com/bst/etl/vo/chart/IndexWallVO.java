package com.bst.etl.vo.chart;

import java.io.Serializable;

public class IndexWallVO implements Serializable {

    private String idIndex;
    private String naIndex;
    private String shapeData;
    private Object value;
    private Object lastValue;//昨日值
    private String mom; //环比
    private Double percent;
    private Boolean fgWarning;//是否警告

    public Boolean getFgWarning() {
        return fgWarning;
    }

    public void setFgWarning(Boolean fgWarning) {
        this.fgWarning = fgWarning;
    }

    public String getIdIndex() {
        return idIndex;
    }

    public void setIdIndex(String idIndex) {
        this.idIndex = idIndex;
    }

    public String getNaIndex() {
        return naIndex;
    }

    public void setNaIndex(String naIndex) {
        this.naIndex = naIndex;
    }

    public String getShapeData() {
        return shapeData;
    }

    public void setShapeData(String shapeData) {
        this.shapeData = shapeData;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getLastValue() {
        return lastValue;
    }

    public void setLastValue(Object lastValue) {
        this.lastValue = lastValue;
    }

    public String getMom() {
        return mom;
    }

    public void setMom(String mom) {
        this.mom = mom;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }
}
