package com.bst.etl.vo.chart;

import java.io.Serializable;
import java.math.BigDecimal;

public class ChartYVO implements Serializable {

    private String fieldName;
    private String type="value";
    private BigDecimal min;
    private BigDecimal max;
    private String position="left";
    private String unit; //单位
    private String chartType;

    public ChartYVO() {

    }

    public ChartYVO(String fieldName,String unit) {
        this.fieldName = fieldName;
        this.unit = unit;
    };

    public String getChartType() {
        return chartType;
    }

    public void setChartType(String chartType) {
        this.chartType = chartType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getMin() {
        return min;
    }

    public void setMin(BigDecimal min) {
        this.min = min;
    }

    public BigDecimal getMax() {
        return max;
    }

    public void setMax(BigDecimal max) {
        this.max = max;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
