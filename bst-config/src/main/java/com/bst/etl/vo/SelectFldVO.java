package com.bst.etl.vo;

import java.io.Serializable;

public class SelectFldVO implements Serializable {
    private String dims;
    private String dimsAndNames;
    private String valueFld;
    private String value;
    private String valueAlias;
    private String day="'${D_BEGIN}' id_dim_day_occur ";

    public String getValueAlias() {
        return valueAlias;
    }

    public void setValueAlias(String valueAlias) {
        this.valueAlias = valueAlias;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDims() {
        return dims;
    }

    public void setDims(String dims) {
        this.dims = dims;
    }

    public String getDimsAndNames() {
        return dimsAndNames;
    }

    public void setDimsAndNames(String dimsAndNames) {
        this.dimsAndNames = dimsAndNames;
    }

    public String getValueFld() {
        return valueFld;
    }

    public void setValueFld(String valueFld) {
        this.valueFld = valueFld;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
