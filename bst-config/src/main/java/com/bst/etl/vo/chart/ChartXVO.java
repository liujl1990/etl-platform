package com.bst.etl.vo.chart;

import java.io.Serializable;

public class ChartXVO implements Serializable {

    private String type="category";
    private String fieldName;
    private String num;
    private String axisLabel; //interval：0x轴全显示，rotate倾斜角度

    public ChartXVO() {

    }

    public String getAxisLabel() {
        return axisLabel;
    }

    public void setAxisLabel(String axisLabel) {
        this.axisLabel = axisLabel;
    }

    public ChartXVO(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
