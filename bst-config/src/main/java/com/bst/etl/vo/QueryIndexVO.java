package com.bst.etl.vo;

import java.io.Serializable;
import java.util.Map;

public class QueryIndexVO implements Serializable {

    private String idIndex;
    private String orderType="desc"; //排序类型 asc,desc,默认降序
    private Integer fgMain; //主指标标志，指标过多时排序以主指标为主
    private Boolean fgIgno=false;
    private Map<String,String> fieldAlias;

    public QueryIndexVO(){

    }

    public QueryIndexVO(String idIndex){
       this.idIndex = idIndex;
    }

    public Map<String, String> getFieldAlias() {
        return fieldAlias;
    }

    public void setFieldAlias(Map<String, String> fieldAlias) {
        this.fieldAlias = fieldAlias;
    }

    public Boolean getFgIgno() {
        return fgIgno;
    }

    public void setFgIgno(Boolean fgIgno) {
        this.fgIgno = fgIgno;
    }

    public String getIdIndex() {
        return idIndex;
    }

    public void setIdIndex(String idIndex) {
        this.idIndex = idIndex;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getFgMain() {
        return fgMain;
    }

    public void setFgMain(Integer fgMain) {
        this.fgMain = fgMain;
    }
}
