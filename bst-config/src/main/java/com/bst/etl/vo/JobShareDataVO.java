package com.bst.etl.vo;


import com.bst.common.vo.JobDimVO;
import com.bst.base.domain.BaseMsg;
import com.bst.md.domain.MdMedPubfld;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class JobShareDataVO implements Serializable {

    private Map<String, Map<String,Object>> dimDataMap;
    private Map<String, JobDimVO> jobDimVOMap;
    private List<BaseMsg> msgList;
    private List<MdMedPubfld> pubfldList;

    public List<MdMedPubfld> getPubfldList() {
        return pubfldList;
    }

    public void setPubfldList(List<MdMedPubfld> pubfldList) {
        this.pubfldList = pubfldList;
    }

    public List<BaseMsg> getMsgList() {
        return msgList;
    }

    public void setMsgList(List<BaseMsg> msgList) {
        this.msgList = msgList;
    }

    public Map<String, Map<String, Object>> getDimDataMap() {
        return dimDataMap;
    }

    public void setDimDataMap(Map<String, Map<String, Object>> dimDataMap) {
        this.dimDataMap = dimDataMap;
    }

    public Map<String, JobDimVO> getJobDimVOMap() {
        return jobDimVOMap;
    }

    public void setJobDimVOMap(Map<String, JobDimVO> jobDimVOMap) {
        this.jobDimVOMap = jobDimVOMap;
    }
}
