package com.bst.etl.vo;

import com.bst.common.vo.HeaderAndBodyVO;

import java.io.Serializable;
import java.util.List;

public class EtlRepDataVO implements Serializable {

    private HeaderAndBodyVO data;
    private String month;
    private List<String> idIndexs;
    private String cdOrg;//机构编码
    private String cdFunc;//功能编码
    private String naFunc;//功能编码

    public String getCdOrg() {
        return cdOrg;
    }

    public void setCdOrg(String cdOrg) {
        this.cdOrg = cdOrg;
    }

    public String getNaFunc() {
        return naFunc;
    }

    public void setNaFunc(String naFunc) {
        this.naFunc = naFunc;
    }

    public HeaderAndBodyVO getData() {
        return data;
    }

    public void setData(HeaderAndBodyVO data) {
        this.data = data;
    }

    public String getMonth() {
        return month.replaceAll("-","");
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public List<String> getIdIndexs() {
        return idIndexs;
    }

    public void setIdIndexs(List<String> idIndexs) {
        this.idIndexs = idIndexs;
    }

    public String getCdFunc() {
        return cdFunc;
    }

    public void setCdFunc(String cdFunc) {
        this.cdFunc = cdFunc;
    }
}
