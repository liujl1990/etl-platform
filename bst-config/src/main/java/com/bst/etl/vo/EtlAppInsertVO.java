package com.bst.etl.vo;

import java.io.Serializable;

public class EtlAppInsertVO implements Serializable {
    private String tbNameApp;
    private String tbNameDm;
    private String queryFlds;
    private String insertFlds;
    private String groupFlds;
    private String month;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTbNameApp() {
        return tbNameApp;
    }

    public void setTbNameApp(String tbNameApp) {
        this.tbNameApp = tbNameApp;
    }

    public String getTbNameDm() {
        return tbNameDm;
    }

    public void setTbNameDm(String tbNameDm) {
        this.tbNameDm = tbNameDm;
    }

    public String getQueryFlds() {
        return queryFlds;
    }

    public void setQueryFlds(String queryFlds) {
        this.queryFlds = queryFlds;
    }

    public String getInsertFlds() {
        return insertFlds;
    }

    public void setInsertFlds(String insertFlds) {
        this.insertFlds = insertFlds;
    }

    public String getGroupFlds() {
        return groupFlds;
    }

    public void setGroupFlds(String groupFlds) {
        this.groupFlds = groupFlds;
    }
}
