package com.bst.etl.vo;

import com.bst.etl.vo.chart.ChartParamVO;

import java.io.Serializable;
import java.util.List;

public class QueryCompIndexVO implements Serializable {
    private String startDate;
    private String endDate;
    private ChartParamVO rowToColIndex;
    private ChartParamVO ohterIndexs;
    //private Boolean fgOrg;//是否需要机构的合计值
    private String cdOrg; //头行为机构时，过滤机构用的
    private String mainDimPubflds;

    //数据存储时冗余
    private String cdFunc;
    private String naFunc;

    private List<String> idIndexs; //涉及的所有指标，用于判断是否审核

    public String getCdOrg() {
        return cdOrg;
    }

    public void setCdOrg(String cdOrg) {
        this.cdOrg = cdOrg;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getMainDimPubflds() {
        return mainDimPubflds;
    }

    public void setMainDimPubflds(String mainDimPubflds) {
        this.mainDimPubflds = mainDimPubflds;
    }

    public String getCdFunc() {
        return cdFunc;
    }

    public void setCdFunc(String cdFunc) {
        this.cdFunc = cdFunc;
    }

    public String getNaFunc() {
        return naFunc;
    }

    public void setNaFunc(String naFunc) {
        this.naFunc = naFunc;
    }

    public List<String> getIdIndexs() {
        return idIndexs;
    }

    public void setIdIndexs(List<String> idIndexs) {
        this.idIndexs = idIndexs;
    }


    public String getStartDate() {
        return startDate.replaceAll("-","");
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public ChartParamVO getRowToColIndex() {
        return rowToColIndex;
    }

    public void setRowToColIndex(ChartParamVO rowToColIndex) {
        this.rowToColIndex = rowToColIndex;
    }

    public ChartParamVO getOhterIndexs() {
        return ohterIndexs;
    }

    public void setOhterIndexs(ChartParamVO ohterIndexs) {
        this.ohterIndexs = ohterIndexs;
    }
}
