package com.bst.etl.vo;

import java.io.Serializable;

public class BaseQueryVO implements Serializable {

    private String shapeType;
    private Object data;

    public String getShapeType() {
        return shapeType;
    }

    public void setShapeType(String shapeType) {
        this.shapeType = shapeType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
