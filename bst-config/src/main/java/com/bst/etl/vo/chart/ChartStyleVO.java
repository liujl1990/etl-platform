package com.bst.etl.vo.chart;

import java.io.Serializable;
import java.util.List;

public class ChartStyleVO implements Serializable {
    public static final String CHART_BAR = "bar";
    public static final String CHART_LINE = "line";
    public static final String CHART_PIE = "pie";
    public static final String CHART_BARANDLINE = "barAndLine";
    private String legendPosition;
    private String title;
    private String titlePosition; // 此处为解决饼图中空显示
    private String chartType; //图形类别 饼图 pie,折线图 line, 柱图 bar, 柱图+折线混合图 barAndLine
    private ChartXVO chartXVO; //X轴
    private List<ChartYVO> chartYVOList; //Y轴
    private String itemStyle;
    private List<String> showFlds;//显示列 (主要应用于表格)
    private String showFldsScale;//显示列每行的比例(主要应用于表格)
    private String rowToColFld; //行转列字段
    private Boolean fgRowToColOrder=false;
    private Boolean fgRowToShowAll=false;//显示全部行转列字段

    public String getLegendPosition() {
        return legendPosition;
    }

    public void setLegendPosition(String legendPosition) {
        this.legendPosition = legendPosition;
    }

    public String getTitlePosition() {
        return titlePosition;
    }

    public void setTitlePosition(String titlePosition) {
        this.titlePosition = titlePosition;
    }

    public String getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(String itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Boolean getFgRowToShowAll() {
        return fgRowToShowAll;
    }

    public void setFgRowToShowAll(Boolean fgRowToShowAll) {
        this.fgRowToShowAll = fgRowToShowAll;
    }

    public Boolean getFgRowToColOrder() {
        return fgRowToColOrder;
    }

    public void setFgRowToColOrder(Boolean fgRowToColOrder) {
        this.fgRowToColOrder = fgRowToColOrder;
    }

    public String getRowToColFld() {
        return rowToColFld;
    }

    public void setRowToColFld(String rowToColFld) {
        this.rowToColFld = rowToColFld;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChartType() {
        return chartType;
    }

    public void setChartType(String chartType) {
        this.chartType = chartType;
    }

    public ChartXVO getChartXVO() {
        return chartXVO;
    }

    public void setChartXVO(ChartXVO chartXVO) {
        this.chartXVO = chartXVO;
    }

    public List<ChartYVO> getChartYVOList() {
        return chartYVOList;
    }

    public void setChartYVOList(List<ChartYVO> chartYVOList) {
        this.chartYVOList = chartYVOList;
    }

    public List<String> getShowFlds() {
        return showFlds;
    }

    public void setShowFlds(List<String> showFlds) {
        this.showFlds = showFlds;
    }

    public String getShowFldsScale() {
        return showFldsScale;
    }

    public void setShowFldsScale(String showFldsScale) {
        this.showFldsScale = showFldsScale;
    }
}
