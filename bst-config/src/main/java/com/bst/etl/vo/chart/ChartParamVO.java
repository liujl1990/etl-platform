package com.bst.etl.vo.chart;

import com.bst.etl.vo.DwQueryVO;
import com.bst.etl.vo.QueryParamVO;

import java.io.Serializable;

public class ChartParamVO implements Serializable {

    private ChartStyleVO chartStyleVO;
    private QueryParamVO queryParamVO;
    private DwQueryVO dwQueryVO;

    public DwQueryVO getDwQueryVO() {
        return dwQueryVO;
    }

    public void setDwQueryVO(DwQueryVO dwQueryVO) {
        this.dwQueryVO = dwQueryVO;
    }

    public ChartStyleVO getChartStyleVO() {
        return chartStyleVO;
    }

    public void setChartStyleVO(ChartStyleVO chartStyleVO) {
        this.chartStyleVO = chartStyleVO;
    }

    public QueryParamVO getQueryParamVO() {
        return queryParamVO;
    }

    public void setQueryParamVO(QueryParamVO queryParamVO) {
        this.queryParamVO = queryParamVO;
    }
}
