package com.bst.etl.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class QueryFieldVO implements Serializable {

    private String whereSql;
    private List<String> otherFldList;
    private List<String> dimFldList;
    private String groupFlds;
    private String selectFlds;
    private String otherFlds;//主要是指冗余的名称
    private List<String> joinList;
    private Map<String,String> replaceMap;

    public Map<String, String> getReplaceMap() {
        return replaceMap;
    }

    public void setReplaceMap(Map<String, String> replaceMap) {
        this.replaceMap = replaceMap;
    }

    public List<String> getJoinList() {
        return joinList;
    }

    public void setJoinList(List<String> joinList) {
        this.joinList = joinList;
    }

    public String getWhereSql() {
        return whereSql;
    }

    public void setWhereSql(String whereSql) {
        this.whereSql = whereSql;
    }

    public List<String> getOtherFldList() {
        return otherFldList;
    }

    public void setOtherFldList(List<String> otherFldList) {
        this.otherFldList = otherFldList;
    }

    public List<String> getDimFldList() {
        return dimFldList;
    }

    public void setDimFldList(List<String> dimFldList) {
        this.dimFldList = dimFldList;
    }

    public String getGroupFlds() {
        return groupFlds;
    }

    public void setGroupFlds(String groupFlds) {
        this.groupFlds = groupFlds;
    }

    public String getSelectFlds() {
        return selectFlds;
    }

    public void setSelectFlds(String selectFlds) {
        this.selectFlds = selectFlds;
    }

    public String getOtherFlds() {
        return otherFlds;
    }

    public void setOtherFlds(String otherFlds) {
        this.otherFlds = otherFlds;
    }
}
