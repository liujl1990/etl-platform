package com.bst.etl.vo;

import java.io.Serializable;

public class AppDataVO implements Serializable {
    private Long id;
    private Object value;
    private String tableName;
    private String updateFld;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getUpdateFld() {
        return updateFld;
    }

    public void setUpdateFld(String updateFld) {
        this.updateFld = updateFld;
    }
}
