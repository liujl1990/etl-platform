package com.bst.etl.vo;

import java.io.Serializable;

public class DimVO implements Serializable {

    private String idPubfld;
    private String naPubfld;
    private String filter;//过滤
    private boolean visible=true;
    private String showCdField;
    private String showNaField;

    public DimVO() {

    }

    public DimVO(String idPubfld,String filter) {
           this.idPubfld = idPubfld;
           this.filter = filter;
    }

    public DimVO(String idPubfld,String filter,boolean visible) {
        this.idPubfld = idPubfld;
        this.filter = filter;
        this.visible = visible;
    }

    public String getShowCdField() {
        return showCdField;
    }

    public void setShowCdField(String showCdField) {
        this.showCdField = showCdField;
    }

    public String getShowNaField() {
        return showNaField;
    }

    public void setShowNaField(String showNaField) {
        this.showNaField = showNaField;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getNaPubfld() {
        return naPubfld;
    }

    public void setNaPubfld(String naPubfld) {
        this.naPubfld = naPubfld;
    }

    public String getIdPubfld() {
        return idPubfld;
    }

    public void setIdPubfld(String idPubfld) {
        this.idPubfld = idPubfld;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
}
