package com.bst.etl.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QueryParamVO implements Serializable {

    private String startDate;
    private String endDate;
    private String dateType; // D天，M月，Q季, Y年
    private List<QueryIndexVO> indexVOS; //指标集合
    private Boolean fgTb=false; //同比
    private Boolean fgHb=false; //环比
    private List<DimVO> dimVOList; //维度集合
    private String fgYearTotal; //年累计标志（作用于天）
    private Integer pageNum;
    private Integer pageSize=10;
    private Boolean fgGroupByDate=true;//是否需要根据时间分组

    /** 通用对照逻辑  **/
    private String cdComps;
    private String orderBy;
    private String resultOrderBy; //对最终的结果排序
    private String fileName; //excel导出文件名

    public Map<String, Map<String,String>> getIndexFldAliasMap() {
        Map<String, Map<String,String>> dataMap = new HashMap<>();
        for(QueryIndexVO vo:indexVOS) {
            if(vo.getFieldAlias()!=null) {
                dataMap.put(vo.getIdIndex(),vo.getFieldAlias());
            }
        }
        return dataMap;
    }

    public String getResultOrderBy() {
        return resultOrderBy;
    }

    public void setResultOrderBy(String resultOrderBy) {
        this.resultOrderBy = resultOrderBy;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Boolean getFgGroupByDate() {
        return fgGroupByDate;
    }

    public void setFgGroupByDate(Boolean fgGroupByDate) {
        this.fgGroupByDate = fgGroupByDate;
    }

    public String getCdComps() {
        return cdComps;
    }

    public void setCdComps(String cdComps) {
        this.cdComps = cdComps;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public List<QueryIndexVO> getIndexVOS() {
        return indexVOS;
    }

    public void setIndexVOS(List<QueryIndexVO> indexVOS) {
        this.indexVOS = indexVOS;
    }

    public List<String> getIdIndexs() {
        List<QueryIndexVO> list = indexVOS;
        List<String> ids = new ArrayList<>();
        for (QueryIndexVO vo : list) {
            if(!vo.getFgIgno()) {
                ids.add(vo.getIdIndex());
            }
        }
        return ids;
    }

    public List<String> getAllIdIndex() {
        return indexVOS.stream().map(QueryIndexVO::getIdIndex).collect(Collectors.toList());
    }


    public Boolean getFgTb() {
        return fgTb;
    }

    public void setFgTb(Boolean fgTb) {
        this.fgTb = fgTb;
    }

    public Boolean getFgHb() {
        return fgHb;
    }

    public void setFgHb(Boolean fgHb) {
        this.fgHb = fgHb;
    }

    public List<DimVO> getDimVOList() {
        return dimVOList;
    }

    public void setDimVOList(List<DimVO> dimVOList) {
        this.dimVOList = dimVOList;
    }

    public String getFgYearTotal() {
        return fgYearTotal;
    }

    public void setFgYearTotal(String fgYearTotal) {
        this.fgYearTotal = fgYearTotal;
    }

    public String getStartDate() {
        String newDate = startDate.replaceAll("-","");
        if("M".equals(dateType)) {
            newDate = newDate.substring(0,6);
        } else if("Y".equals(dateType)) {
            newDate = newDate.substring(0,4);
        } else if("Q".equals(dateType)) {
            String year = newDate.substring(0,4);
            Integer month = Integer.parseInt(newDate.substring(4,6));
            if(month>0 && month<4) {
                newDate = year+"1";
            } else if(month>3 && month<7) {
                newDate = year+"2";
            } else if(month>6 && month<10) {
                newDate = year+"3";
            } else if(month>9 && month<13) {
                newDate = year+"4";
            }
        }
        return newDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        String newDate = endDate;
        if(newDate!=null) {
            newDate = endDate.replaceAll("-","");
            if("M".equals(dateType)) {
                newDate = newDate.substring(0,6);
            } else if("Y".equals(dateType)) {
                newDate = newDate.substring(0,4);
            } else if("Q".equals(dateType)) {
                String year = newDate.substring(0,4);
                Integer month = Integer.parseInt(newDate.substring(4,6));
                if(month>0 && month<4) {
                    newDate = year+"1";
                } else if(month>3 && month<7) {
                    newDate = year+"2";
                } else if(month>6 && month<10) {
                    newDate = year+"3";
                } else if(month>9 && month<13) {
                    newDate = year+"4";
                }
            }
        }
        return newDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
