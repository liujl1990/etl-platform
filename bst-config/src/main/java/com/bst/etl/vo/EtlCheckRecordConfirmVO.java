package com.bst.etl.vo;

import com.bst.etl.domain.EtlCheckRecord;
import com.bst.etl.domain.EtlCheckRecordItem;

import java.io.Serializable;
import java.util.List;

public class EtlCheckRecordConfirmVO implements Serializable {
    private String idRecord;
    private EtlCheckRecord record;
    private String tableName;
    private String updateFld;
    List<EtlCheckRecordItem> items;

    public EtlCheckRecord getRecord() {
        return record;
    }

    public void setRecord(EtlCheckRecord record) {
        this.record = record;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getUpdateFld() {
        return updateFld;
    }

    public void setUpdateFld(String updateFld) {
        this.updateFld = updateFld;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }

    public List<EtlCheckRecordItem> getItems() {
        return items;
    }

    public void setItems(List<EtlCheckRecordItem> items) {
        this.items = items;
    }
}
