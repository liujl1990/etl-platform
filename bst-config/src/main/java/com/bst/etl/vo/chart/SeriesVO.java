package com.bst.etl.vo.chart;

import java.io.Serializable;

public class SeriesVO implements Serializable {

    private String radius;
    private String formartType; //格式化类型

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getFormartType() {
        return formartType;
    }

    public void setFormartType(String formartType) {
        this.formartType = formartType;
    }
}
