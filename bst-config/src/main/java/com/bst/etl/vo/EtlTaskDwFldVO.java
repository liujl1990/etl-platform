package com.bst.etl.vo;

import java.io.Serializable;

public class EtlTaskDwFldVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String odsFid;
    //1 正常列  2额外维度添加列
    private Integer odsFidType;
    private String dwFid1;
    private String dwFid2;
    private Integer fgIndex;

    public Integer getFgIndex() {
        return fgIndex;
    }

    public void setFgIndex(Integer fgIndex) {
        this.fgIndex = fgIndex;
    }

    public Integer getOdsFidType() {
        return odsFidType;
    }

    public void setOdsFidType(Integer odsFidType) {
        this.odsFidType = odsFidType;
    }

    public String getOdsFid() {
        return odsFid;
    }

    public void setOdsFid(String odsFid) {
        this.odsFid = odsFid;
    }

    public String getDwFid1() {
        return dwFid1;
    }

    public void setDwFid1(String dwFid1) {
        this.dwFid1 = dwFid1;
    }

    public String getDwFid2() {
        return dwFid2;
    }

    public void setDwFid2(String dwFid2) {
        this.dwFid2 = dwFid2;
    }
}