package com.bst.system.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 接口用户对象 sys_user_outer
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
public class SysUserOuter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idUserOuter;

    /** 登录名 */
    @Excel(name = "登录名")
    private String loginName;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 盐 */
    @Excel(name = "盐")
    private String salt;

    /** 启用标志 */
    @Excel(name = "启用标志")
    private Integer fgAct;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    public void setIdUserOuter(Long idUserOuter) 
    {
        this.idUserOuter = idUserOuter;
    }

    public Long getIdUserOuter() 
    {
        return idUserOuter;
    }
    public void setLoginName(String loginName) 
    {
        this.loginName = loginName;
    }

    public String getLoginName() 
    {
        return loginName;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setSalt(String salt) 
    {
        this.salt = salt;
    }

    public String getSalt() 
    {
        return salt;
    }
    public void setFgAct(Integer fgAct) 
    {
        this.fgAct = fgAct;
    }

    public Integer getFgAct() 
    {
        return fgAct;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idUserOuter", getIdUserOuter())
            .append("loginName", getLoginName())
            .append("userName", getUserName())
            .append("password", getPassword())
            .append("salt", getSalt())
            .append("fgAct", getFgAct())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
