package com.bst.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hos_dise_dept
 * 
 * @author ruoyi
 * @date 2023-05-15
 */
public class HosDiseDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long idDiseDept;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDise;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String cdDept;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naDept;

    public void setIdDiseDept(Long idDiseDept) 
    {
        this.idDiseDept = idDiseDept;
    }

    public Long getIdDiseDept() 
    {
        return idDiseDept;
    }
    public void setIdDise(Long idDise) 
    {
        this.idDise = idDise;
    }

    public Long getIdDise() 
    {
        return idDise;
    }
    public void setCdDept(String cdDept) 
    {
        this.cdDept = cdDept;
    }

    public String getCdDept() 
    {
        return cdDept;
    }
    public void setNaDept(String naDept) 
    {
        this.naDept = naDept;
    }

    public String getNaDept() 
    {
        return naDept;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDiseDept", getIdDiseDept())
            .append("idDise", getIdDise())
            .append("cdDept", getCdDept())
            .append("naDept", getNaDept())
            .toString();
    }
}
