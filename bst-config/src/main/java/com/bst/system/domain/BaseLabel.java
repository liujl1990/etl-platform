package com.bst.system.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 指标标签对象 md_index_label
 * 
 * @author 老刘
 * @date 2023-02-18
 */
public class BaseLabel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idLabel;

    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 父ID */
    @Excel(name = "父ID")
    private Long parentId;

    /** 停用标志 */
    @Excel(name = "停用标志")
    private Integer fgAct;

    /** 父ID集合 */
    @Excel(name = "父ID集合")
    private String parentIds;

    private Integer fgSys;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public Integer getFgSys() {
        return fgSys;
    }

    public void setFgSys(Integer fgSys) {
        this.fgSys = fgSys;
    }

    public void setIdLabel(Long idLabel)
    {
        this.idLabel = idLabel;
    }

    public Long getIdLabel() 
    {
        return idLabel;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }

    public Integer getFgAct() {
        return fgAct;
    }

    public void setFgAct(Integer fgAct) {
        this.fgAct = fgAct;
    }

    public void setParentIds(String parentIds)
    {
        this.parentIds = parentIds;
    }

    public String getParentIds() 
    {
        return parentIds;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idLabel", getIdLabel())
            .append("na", getNa())
            .append("parentId", getParentId())
            .append("fgAct", getFgAct())
            .append("parentIds", getParentIds())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
