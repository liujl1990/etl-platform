package com.bst.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hos_jxkh_tb
 * 
 * @author ruoyi
 * @date 2023-06-05
 */
public class HosJxkhTb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long idJxkhTb;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idJxkh;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String idIndex;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naIndex;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idDept;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naDept;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long euDttp;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dDes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String val;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dtSysCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpModi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dtSysModi;

    public void setIdJxkhTb(Long idJxkhTb) 
    {
        this.idJxkhTb = idJxkhTb;
    }

    public Long getIdJxkhTb() 
    {
        return idJxkhTb;
    }
    public void setIdJxkh(Long idJxkh) 
    {
        this.idJxkh = idJxkh;
    }

    public Long getIdJxkh() 
    {
        return idJxkh;
    }
    public void setIdIndex(String idIndex) 
    {
        this.idIndex = idIndex;
    }

    public String getIdIndex() 
    {
        return idIndex;
    }
    public void setNaIndex(String naIndex) 
    {
        this.naIndex = naIndex;
    }

    public String getNaIndex() 
    {
        return naIndex;
    }
    public void setIdDept(Long idDept) 
    {
        this.idDept = idDept;
    }

    public Long getIdDept() 
    {
        return idDept;
    }
    public void setNaDept(String naDept) 
    {
        this.naDept = naDept;
    }

    public String getNaDept() 
    {
        return naDept;
    }
    public void setEuDttp(Long euDttp) 
    {
        this.euDttp = euDttp;
    }

    public Long getEuDttp() 
    {
        return euDttp;
    }
    public void setdDes(String dDes) 
    {
        this.dDes = dDes;
    }

    public String getdDes() 
    {
        return dDes;
    }
    public void setVal(String val) 
    {
        this.val = val;
    }

    public String getVal() 
    {
        return val==null?"":val;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idJxkhTb", getIdJxkhTb())
            .append("idJxkh", getIdJxkh())
            .append("idIndex", getIdIndex())
            .append("naIndex", getNaIndex())
            .append("idDept", getIdDept())
            .append("naDept", getNaDept())
            .append("euDttp", getEuDttp())
            .append("dDes", getdDes())
            .append("val", getVal())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
