package com.bst.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 用户接口权限对象 sys_user_outer_inter
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
public class SysUserOuterInter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idOuterInter;

    /**  */
    @Excel(name = "")
    private Long idUserOuter;

    /**  */
    @Excel(name = "")
    private Long idInter;

    public void setIdOuterInter(Long idOuterInter) 
    {
        this.idOuterInter = idOuterInter;
    }

    public Long getIdOuterInter() 
    {
        return idOuterInter;
    }
    public void setIdUserOuter(Long idUserOuter) 
    {
        this.idUserOuter = idUserOuter;
    }

    public Long getIdUserOuter() 
    {
        return idUserOuter;
    }
    public void setIdInter(Long idInter) 
    {
        this.idInter = idInter;
    }

    public Long getIdInter() 
    {
        return idInter;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idOuterInter", getIdOuterInter())
            .append("idUserOuter", getIdUserOuter())
            .append("idInter", getIdInter())
            .toString();
    }
}
