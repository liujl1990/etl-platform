package com.bst.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 收费项目快速分组对象 hos_sfxm_group
 * 
 * @author ruoyi
 * @date 2023-05-19
 */
public class HosSfxmGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idSfxmGroup;

    /** 分类编码 */
    @Excel(name = "分类编码")
    private String sdCd;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String sdNa;

    /** 项目编码 */
    @Excel(name = "项目编码")
    private String cdSfxm;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String naSfxm;

    /**  */
    @Excel(name = "")
    private String naEmpCre;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public void setIdSfxmGroup(Long idSfxmGroup) 
    {
        this.idSfxmGroup = idSfxmGroup;
    }

    public Long getIdSfxmGroup() 
    {
        return idSfxmGroup;
    }
    public void setSdCd(String sdCd) 
    {
        this.sdCd = sdCd;
    }

    public String getSdCd() 
    {
        return sdCd;
    }
    public void setSdNa(String sdNa) 
    {
        this.sdNa = sdNa;
    }

    public String getSdNa() 
    {
        return sdNa;
    }
    public void setCdSfxm(String cdSfxm) 
    {
        this.cdSfxm = cdSfxm;
    }

    public String getCdSfxm() 
    {
        return cdSfxm;
    }
    public void setNaSfxm(String naSfxm) 
    {
        this.naSfxm = naSfxm;
    }

    public String getNaSfxm() 
    {
        return naSfxm;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idSfxmGroup", getIdSfxmGroup())
            .append("sdCd", getSdCd())
            .append("sdNa", getSdNa())
            .append("cdSfxm", getCdSfxm())
            .append("naSfxm", getNaSfxm())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
