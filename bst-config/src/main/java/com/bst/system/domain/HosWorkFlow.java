package com.bst.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.bst.common.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 单据提交对象 hos_work_flow
 * 
 * @author ruoyi
 * @date 2023-04-08
 */
public class HosWorkFlow implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**  */
    private int workId;

    /**  */
    @Excel(name = "")
    private Long deptId;

    /**  */
    @Excel(name = "")
    private String deptName;

    /**  */
    @Excel(name = "")
    private String title;

    /**  */
    @Excel(name = "")
    private String content;

    /**  */
    @Excel(name = "")
    private String naEmpCre;

    @Excel(name = "")
    private String statusCode;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    private String naEmp;

    private Date dtEnd;

    private Integer fgUrgent; //紧急标志

    private Integer euFreq; //使用频次

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastDate; //最后交付日期

    private BigDecimal days;

    /** $table.subTable.functionName信息 */
    private List<HosWorkFlowNote> hosWorkFlowNoteList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    public BigDecimal getDays() {
        return days;
    }

    public void setDays(BigDecimal days) {
        this.days = days;
    }

    public Integer getFgUrgent() {
        return fgUrgent;
    }

    public void setFgUrgent(Integer fgUrgent) {
        this.fgUrgent = fgUrgent;
    }

    public Integer getEuFreq() {
        return euFreq;
    }

    public void setEuFreq(Integer euFreq) {
        this.euFreq = euFreq;
    }

    public String getNaEmp() {
        return naEmp;
    }

    public void setNaEmp(String naEmp) {
        this.naEmp = naEmp;
    }

    public Date getDtEnd() {
        return dtEnd;
    }

    public void setDtEnd(Date dtEnd) {
        this.dtEnd = dtEnd;
    }

    public void setWorkId(int workId)
    {
        this.workId = workId;
    }

    public int getWorkId()
    {
        return workId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId()
    {
        return deptId;
    }
    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public String getDeptName()
    {
        return deptName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setNaEmpCre(String naEmpCre)
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    public void setStatusCode(String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getStatusCode()
    {
        return statusCode;
    }

    public List<HosWorkFlowNote> getHosWorkFlowNoteList()
    {
        return hosWorkFlowNoteList;
    }

    public void setHosWorkFlowNoteList(List<HosWorkFlowNote> hosWorkFlowNoteList)
    {
        this.hosWorkFlowNoteList = hosWorkFlowNoteList;
    }
}
