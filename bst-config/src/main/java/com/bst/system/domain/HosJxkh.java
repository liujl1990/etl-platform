package com.bst.system.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 绩效考核对象 hos_jxkh
 * 
 * @author ruoyi
 * @date 2023-06-02
 */
public class HosJxkh extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idJxkh;

    /** 指标编码 */
    @Excel(name = "指标编码")
    private String idIndex;

    /** 指标名称 */
    @Excel(name = "指标名称")
    private String naIndex;

    @Excel(name = "分类编码")
    private String cdCa;

    /** 月启用 */
    @Excel(name = "月启用")
    private Integer fgM;

    /** 季度启用 */
    @Excel(name = "季度启用")
    private Integer fgQ;

    @Excel(name = "p 百分数 n 数值 s 字符串")
    private String euValtp;

    /** 填报标志 */
    @Excel(name = "填报标志")
    private Integer fgFill;

    /** 年份 */
    @Excel(name = "年份")
    private Long year;

    private String naDept;

    private Long idDept;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    private Integer sort;

    private String note;

    public String getEuValtp() {
        return euValtp;
    }

    public void setEuValtp(String euValtp) {
        this.euValtp = euValtp;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getNaDept() {
        return naDept;
    }

    public void setNaDept(String naDept) {
        this.naDept = naDept;
    }

    public Long getIdDept() {
        return idDept;
    }

    public void setIdDept(Long idDept) {
        this.idDept = idDept;
    }

    public String getCdCa() {
        return cdCa;
    }

    public void setCdCa(String cdCa) {
        this.cdCa = cdCa;
    }

    public void setIdJxkh(Long idJxkh)
    {
        this.idJxkh = idJxkh;
    }

    public Long getIdJxkh() 
    {
        return idJxkh;
    }
    public void setIdIndex(String idIndex) 
    {
        this.idIndex = idIndex;
    }

    public String getIdIndex() 
    {
        return idIndex;
    }
    public void setNaIndex(String naIndex) 
    {
        this.naIndex = naIndex;
    }

    public String getNaIndex() 
    {
        return naIndex;
    }
    public void setFgM(Integer fgM) 
    {
        this.fgM = fgM;
    }

    public Integer getFgM() 
    {
        return fgM;
    }
    public void setFgQ(Integer fgQ) 
    {
        this.fgQ = fgQ;
    }

    public Integer getFgQ() 
    {
        return fgQ;
    }
    public void setFgFill(Integer fgFill) 
    {
        this.fgFill = fgFill;
    }

    public Integer getFgFill() 
    {
        return fgFill;
    }
    public void setYear(Long year) 
    {
        this.year = year;
    }

    public Long getYear() 
    {
        return year;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idJxkh", getIdJxkh())
            .append("idIndex", getIdIndex())
            .append("naIndex", getNaIndex())
            .append("fgM", getFgM())
            .append("fgQ", getFgQ())
            .append("fgFill", getFgFill())
            .append("year", getYear())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
