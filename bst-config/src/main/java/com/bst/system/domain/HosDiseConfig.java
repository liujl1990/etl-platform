package com.bst.system.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 hos_dise_config
 * 
 * @author ruoyi
 * @date 2023-05-13
 */
public class HosDiseConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long idDiseConfig;

    private Long idDise;

    private Integer sort;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String andOr;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pLeft;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pRight;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fldQry;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String filterQry;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String valQry;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date dtSysCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpModi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date dtSysModi;

    public Long getIdDise() {
        return idDise;
    }

    public void setIdDise(Long idDise) {
        this.idDise = idDise;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setIdDiseConfig(Long idDiseConfig)
    {
        this.idDiseConfig = idDiseConfig;
    }

    public Long getIdDiseConfig() 
    {
        return idDiseConfig;
    }
    public void setAndOr(String andOr) 
    {
        this.andOr = andOr;
    }

    public String getAndOr() 
    {
        return andOr;
    }
    public void setpLeft(String pLeft) 
    {
        this.pLeft = pLeft;
    }

    public String getpLeft() 
    {
        return pLeft;
    }
    public void setpRight(String pRight) 
    {
        this.pRight = pRight;
    }

    public String getpRight() 
    {
        return pRight;
    }
    public void setFldQry(String fldQry) 
    {
        this.fldQry = fldQry;
    }

    public String getFldQry() 
    {
        return fldQry;
    }
    public void setFilterQry(String filterQry) 
    {
        this.filterQry = filterQry;
    }

    public String getFilterQry() 
    {
        return filterQry;
    }
    public void setValQry(String valQry) 
    {
        this.valQry = valQry;
    }

    public String getValQry() 
    {
        return valQry;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDiseConfig", getIdDiseConfig())
            .append("andOr", getAndOr())
            .append("pLeft", getpLeft())
            .append("pRight", getpRight())
            .append("fldQry", getFldQry())
            .append("filterQry", getFilterQry())
            .append("valQry", getValQry())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
