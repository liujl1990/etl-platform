package com.bst.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 单据评论对象 hos_work_flow_note
 * 
 * @author cbh
 * @date 2023-04-08
 */
public class HosWorkFlowNote extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private int noteId;

    /** 工作信息id */
    private int workId;

    /** 科室id */
    private Long deptId;

    /** 科室名称 */
    @Excel(name = "科室名称")
    private String deptName;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 创建人 */
    private String naEmpCre;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dtSysCre;

    /** 修改人 */
    @Excel(name = "修改人")
    private String naEmpModi;

    /** 修改日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "修改日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public void setNoteId(int noteId)
    {
        this.noteId = noteId;
    }

    public int getNoteId()
    {
        return noteId;
    }
    public void setWorkId(int workId)
    {
        this.workId = workId;
    }

    public int getWorkId()
    {
        return workId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setDeptName(String deptName) 
    {
        this.deptName = deptName;
    }

    public String getDeptName() 
    {
        return deptName;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("noteId", getNoteId())
            .append("workId", getWorkId())
            .append("deptId", getDeptId())
            .append("deptName", getDeptName())
            .append("note", getNote())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
