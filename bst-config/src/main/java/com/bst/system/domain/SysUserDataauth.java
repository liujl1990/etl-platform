package com.bst.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 用户数据权限对象 sys_user_dataauth
 * 
 * @author ruoyi
 * @date 2023-01-20
 */
public class SysUserDataauth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long dataauthId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 权限类型 */
    @Excel(name = "权限类型")
    private String sdDataauthType;

    /** 编码 */
    @Excel(name = "编码")
    private String dataauthCd;

    /** 名称 */
    @Excel(name = "名称")
    private String dataauthName;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public void setDataauthId(Long dataauthId) 
    {
        this.dataauthId = dataauthId;
    }

    public Long getDataauthId() 
    {
        return dataauthId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setSdDataauthType(String sdDataauthType) 
    {
        this.sdDataauthType = sdDataauthType;
    }

    public String getSdDataauthType() 
    {
        return sdDataauthType;
    }
    public void setDataauthCd(String dataauthCd) 
    {
        this.dataauthCd = dataauthCd;
    }

    public String getDataauthCd() 
    {
        return dataauthCd;
    }
    public void setDataauthName(String dataauthName) 
    {
        this.dataauthName = dataauthName;
    }

    public String getDataauthName() 
    {
        return dataauthName;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dataauthId", getDataauthId())
            .append("userId", getUserId())
            .append("sdDataauthType", getSdDataauthType())
            .append("dataauthCd", getDataauthCd())
            .append("dataauthName", getDataauthName())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
