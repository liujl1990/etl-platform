package com.bst.system.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;

/**
 * 病种管理对象 hos_dise
 * 
 * @author ruoyi
 * @date 2023-05-13
 */
public class HosDise
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idDise;

    /** 编码 */
    @Excel(name = "编码")
    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 过滤条件 */
    private String sqlWhere;

    /** 启用标志 */
    @Excel(name = "启用标志")
    private Integer fgAct;

    /**  */
    private String naEmpCre;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dtSysModi;

    private List<HosDiseConfig> hosDiseConfigs;

    public List<HosDiseConfig> getHosDiseConfigs() {
        return hosDiseConfigs;
    }

    public void setHosDiseConfigs(List<HosDiseConfig> hosDiseConfigs) {
        this.hosDiseConfigs = hosDiseConfigs;
    }

    public Long getIdDise() {
        return idDise;
    }

    public void setIdDise(Long idDise) {
        this.idDise = idDise;
    }

    public void setCd(String cd)
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setSqlWhere(String sqlWhere) 
    {
        this.sqlWhere = sqlWhere;
    }

    public String getSqlWhere() 
    {
        return sqlWhere;
    }
    public void setFgAct(Integer fgAct) 
    {
        this.fgAct = fgAct;
    }

    public Integer getFgAct() 
    {
        return fgAct;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDise", getIdDise())
            .append("cd", getCd())
            .append("na", getNa())
            .append("sqlWhere", getSqlWhere())
            .append("fgAct", getFgAct())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
