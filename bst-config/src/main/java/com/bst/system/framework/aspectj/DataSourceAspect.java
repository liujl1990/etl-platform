package com.bst.system.framework.aspectj;

import com.bst.common.annotation.DataSource;
import com.bst.common.constant.JobConstant;
import com.bst.system.framework.datasource.DynamicDataSource;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
/**
 * 多数据源处理
 * 
 * @author ruoyi
 */
@Aspect
@Component
public class DataSourceAspect
{
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("@annotation(com.bst.common.annotation.DataSource)"
            + "|| @within(com.bst.common.annotation.DataSource)")
    public void dsPointCut()
    {

    }

    @After("dsPointCut()")
    public void afterAspect(JoinPoint joinPoint) {
        DynamicDataSource.clearDataSource();
    }

    @Before("dsPointCut() && @annotation(routingDataSource)")
    public void beforeAspect(JoinPoint joinPoint, DataSource routingDataSource) {
        if (JobConstant.DB_CLS_DW.equals(routingDataSource.value())) {
            DynamicDataSource.setDataSource(JobConstant.DB_CLS_DW);
        } else {
            DynamicDataSource.setDataSource(routingDataSource.value());
        }
    }

    @AfterReturning("dsPointCut()")
    public void afterReturningAspect(JoinPoint joinPoint) {
        DynamicDataSource.clearDataSource();
    }

//    @Around("dsPointCut() && @annotation(routingDataSource)")
//    public void beforeAspect(ProceedingJoinPoint point, DataSource routingDataSource) {
//        String dsType;
//        if (JobConstant.DB_CLS_DW.equals(routingDataSource.value())) {
//            dsType = JobConstant.DB_CLS_DW;
//        } else {
//            dsType = routingDataSource.value();
//        }
//        DynamicDataSource.setDataSource(dsType);
//        try
//        {
//            point.proceed();
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        } finally
//        {
//            DynamicDataSource.clearDataSource();
//        }
//    }

    /*@Around("dsPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable
    {
        DataSource dataSource = getDataSource(point);

        if (StringUtils.isNotNull(dataSource))
        {
            DynamicDataSourceContextHolder.setDataSourceType(dataSource.value());
        }

        try
        {
            return point.proceed();
        }
        finally
        {
            // 销毁数据源 在执行方法之后
            DynamicDataSourceContextHolder.clearDataSourceType();
        }
    }

    *//**
     * 获取需要切换的数据源
     *//*
    public DataSource getDataSource(ProceedingJoinPoint point)
    {
        MethodSignature signature = (MethodSignature) point.getSignature();
        DataSource dataSource = AnnotationUtils.findAnnotation(signature.getMethod(), DataSource.class);
        if (Objects.nonNull(dataSource))
        {
            return dataSource;
        }

        return AnnotationUtils.findAnnotation(signature.getDeclaringType(), DataSource.class);
    }*/
}
