package com.bst.system.framework.web.service;

import com.bst.base.domain.BaseSd;
import com.bst.base.domain.BaseSdItem;
import com.bst.base.service.IBaseSdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("baseSd")
public class BaseSdService {

    @Autowired
    private IBaseSdService baseSdService;

    public List<BaseSdItem> getItem(String cdSd) {
        BaseSd baseSd = baseSdService.selectBaseSdByCd(cdSd);
        if(baseSd==null) {
            return null;
        }
        return baseSd.getBaseSdItemList();
    }
}
