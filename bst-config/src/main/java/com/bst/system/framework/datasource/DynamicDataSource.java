package com.bst.system.framework.datasource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;
import com.bst.common.constant.JobConstant;
import com.bst.system.framework.utils.DBUtil;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态数据源
 * 
 * @author ruoyi
 */
public class DynamicDataSource extends AbstractRoutingDataSource
{
    //线程安全的map，用来存储数据源。
    private static Map<Object, Object> dataSourceMap = new ConcurrentHashMap<>();
    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();

    public static DruidDataSource getDataSourceById(String id) {
        Object o = dataSourceMap.get(id);
        if (o != null && o.getClass().equals(DruidDataSource.class)) {
            return (DruidDataSource) o;
        } else {
            return null;
        }
    }

    @Override
    public void setTargetDataSources(Map<Object, Object> targetDataSources) {
        dataSourceMap.putAll(targetDataSources);
        super.setTargetDataSources(dataSourceMap);
        super.afterPropertiesSet();// 必须添加该句，否则新添加数据源无法识别到
    }

    public DynamicDataSource()
    {
    }

    public DynamicDataSource(DataSource defaultTargetDataSource, Map<Object, Object> targetDataSources)
    {
        super.setDefaultTargetDataSource(defaultTargetDataSource);
        this.setTargetDataSources(targetDataSources);
        super.afterPropertiesSet();
    }

    /**
     * 添加新的数据源
     *
     * @param key
     * @param object
     */
    public void addDataSource(String key, Object object) {
        try {
            Object put = dataSourceMap.put(key, object);
            super.afterPropertiesSet();// 必须添加该句，否则新添加数据源无法识别到
            DBUtil.closeDataSource(put);
        } catch (Exception e) {
            removeDataSource(key);
            e.printStackTrace();

        }
    }

    /**
     * 添加新的数据源
     *
     * @param key
     */
    public void removeDataSource(String key) {
        Object put = dataSourceMap.remove(key);
        super.afterPropertiesSet();// 必须添加该句，否则新添加数据源无法识别到
        DBUtil.closeDataSource(put);
    }

    public static Map<Object, Object> getDataSourceMap() {
        return dataSourceMap;
    }

    public static void setDataSourceMap(Map<Object, Object> dataSourceMap) {
        DynamicDataSource.dataSourceMap = dataSourceMap;
    }

    public static void setDataSource(String dataSource) {
        CONTEXT_HOLDER.set(dataSource);
    }

    /**
     * 获取数据源
     *
     * @return
     */
    public static String getDataSource() {
        String dataSource = CONTEXT_HOLDER.get();
        // 如果没有指定数据源，使用默认数据源
        if (null == dataSource) {
            DynamicDataSource.setDataSource(JobConstant.DB_CLS_META);
        }
        return CONTEXT_HOLDER.get();
    }

    public static String getHolder() {
        return CONTEXT_HOLDER.get();
    }

    /**
     * 清除数据源
     */
    public static void clearDataSource() {
        CONTEXT_HOLDER.remove();
    }

    @Override
    protected Object determineCurrentLookupKey()
    {
        return getDataSource();
    }
}