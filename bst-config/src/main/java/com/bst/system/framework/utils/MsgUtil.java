package com.bst.system.framework.utils;

import com.bst.common.utils.spring.SpringUtils;
import com.bst.base.service.IBaseMsgService;
import com.bst.base.service.impl.BaseMsgServiceImpl;

public class MsgUtil {

    public static void sendMsg(String cd,String msg,String des) {
        IBaseMsgService msgService = (IBaseMsgService) SpringUtils.getBean(BaseMsgServiceImpl.class);
        msgService.insert(cd,msg,des);
    }
}
