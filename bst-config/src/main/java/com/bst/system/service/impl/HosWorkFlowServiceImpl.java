package com.bst.system.service.impl;

import com.bst.common.core.domain.entity.SysUser;
import com.bst.common.core.text.Convert;
import com.bst.common.utils.StringUtils;
import com.bst.common.utils.self.LoginAPIUtils;
import com.bst.system.domain.HosWorkFlow;
import com.bst.system.domain.HosWorkFlowNote;
import com.bst.system.mapper.HosWorkFlowMapper;
import com.bst.system.service.IHosWorkFlowService;
import com.bst.system.service.IHosWorkFlowStateService;
import com.bst.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 单据提交Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-08
 */
@Service
public class HosWorkFlowServiceImpl implements IHosWorkFlowService 
{
    @Autowired
    private HosWorkFlowMapper hosWorkFlowMapper;
    @Autowired
    private IHosWorkFlowStateService hosWorkFlowStateService;
    @Autowired
    ISysUserService sysUserService;
    /**
     * 查询单据提交
     * 
     * @param idIndex 单据提交主键
     * @return 单据提交
     */
    @Override
    public HosWorkFlow selectHosWorkFlowListById(int workId)
    {
        return hosWorkFlowMapper.selectHosWorkFlowListById(workId);
    }

    /**
     * 查询单据提交列表
     * 
     * @param hosWorkFlow 单据提交
     * @return 单据提交
     */
    @Override
    public List<HosWorkFlow> selectHosWorkFlowList(HosWorkFlow hosWorkFlow)
    {
        return hosWorkFlowMapper.selectHosWorkFlowList(hosWorkFlow);
    }
    /**
     * 新增单据提交
     * 
     * @param hosWorkFlow 单据提交
     * @return 结果
     */
    @Transactional
    @Override
    public int insertHosWorkFlow(HosWorkFlow hosWorkFlow)
    {
        hosWorkFlow.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        hosWorkFlow.setDtSysCre(new Date());
        Long userId = LoginAPIUtils.getLoginUserId();
        SysUser user = sysUserService.selectUserById(userId);
        hosWorkFlow.setDeptId(user.getDeptId());
        hosWorkFlow.setDeptName(user.getDept()==null?null:user.getDept().getDeptName());
        int rows = hosWorkFlowMapper.insertHosWorkFlow(hosWorkFlow);
        insertHosWorkFlowNote(hosWorkFlow);
        return rows;
    }

    /**
     * 修改单据提交
     * 
     * @param hosWorkFlow 单据提交
     * @return 结果
     */
    @Transactional
    @Override
    public int updateHosWorkFlow(HosWorkFlow hosWorkFlow)
    {
        // hosWorkFlowMapper.deleteHosWorkFlowNoteByWorkId(hosWorkFlow.getWorkId());
        // insertHosWorkFlowNote(hosWorkFlow);
        return hosWorkFlowMapper.updateHosWorkFlow(hosWorkFlow);
    }

    /**
     * 批量删除单据提交
     * 
     * @param idIndexs 需要删除的单据提交主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteHosWorkFlowByIdIndexs(String idIndexs)
    {
        hosWorkFlowMapper.deleteHosWorkFlowNoteByWorkIds(Convert.toStrArray(idIndexs));
        return hosWorkFlowMapper.deleteHosWorkFlowByIdIndexs(Convert.toStrArray(idIndexs));
    }

    /**
     * 删除单据提交信息
     * 
     * @param idIndex 单据提交主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteHosWorkFlowByIdIndex(int idIndex)
    {
        hosWorkFlowMapper.deleteHosWorkFlowNoteByWorkId(idIndex);
        return hosWorkFlowMapper.deleteHosWorkFlowByIdIndex(idIndex);
    }

    //只能查看本科室
    @Override
    public List<HosWorkFlow> selectHosWorkFlowListWithAuth(HosWorkFlow hosWorkFlow) {
        if(hosWorkFlow.getDeptId()==null) {
            Long userId = LoginAPIUtils.getLoginUserId();
            SysUser user = sysUserService.selectUserById(userId);
            hosWorkFlow.setDeptId(user.getDeptId());
        }
        return this.selectHosWorkFlowList(hosWorkFlow);
    }

    @Override
    public List<HosWorkFlow> selectDeptsFromHosWorkFlow() {
        return hosWorkFlowMapper.selectDeptsFromHosWorkFlow();
    }

    /**
     * 新增${subTable.functionName}信息
     * 
     * @param hosWorkFlow 单据提交对象
     */
    public void insertHosWorkFlowNote(HosWorkFlow hosWorkFlow)
    {
        List<HosWorkFlowNote> hosWorkFlowNoteList = hosWorkFlow.getHosWorkFlowNoteList();
        int idIndex = hosWorkFlow.getWorkId();
        if (StringUtils.isNotNull(hosWorkFlowNoteList))
        {
            List<HosWorkFlowNote> list = new ArrayList<HosWorkFlowNote>();
            for (HosWorkFlowNote hosWorkFlowNote : hosWorkFlowNoteList)
            {
                hosWorkFlowNote.setWorkId(idIndex);
                list.add(hosWorkFlowNote);
            }
            if (list.size() > 0)
            {
                hosWorkFlowMapper.batchHosWorkFlowNote(list);
            }
        }
    }
}
