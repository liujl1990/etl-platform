package com.bst.system.service;

import java.util.List;
import com.bst.system.domain.SysUserOuter;

/**
 * 接口用户Service接口
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
public interface ISysUserOuterService 
{
    /**
     * 查询接口用户
     * 
     * @param idUserOuter 接口用户主键
     * @return 接口用户
     */
    public SysUserOuter selectSysUserOuterByIdUserOuter(Long idUserOuter);

    /**
     * 查询接口用户列表
     * 
     * @param sysUserOuter 接口用户
     * @return 接口用户集合
     */
    public List<SysUserOuter> selectSysUserOuterList(SysUserOuter sysUserOuter);

    /**
     * 新增接口用户
     * 
     * @param sysUserOuter 接口用户
     * @return 结果
     */
    public int insertSysUserOuter(SysUserOuter sysUserOuter);

    /**
     * 修改接口用户
     * 
     * @param sysUserOuter 接口用户
     * @return 结果
     */
    public int updateSysUserOuter(SysUserOuter sysUserOuter);

    /**
     * 批量删除接口用户
     * 
     * @param idUserOuters 需要删除的接口用户主键集合
     * @return 结果
     */
    public int deleteSysUserOuterByIdUserOuters(String idUserOuters);

    /**
     * 删除接口用户信息
     * 
     * @param idUserOuter 接口用户主键
     * @return 结果
     */
    public int deleteSysUserOuterByIdUserOuter(Long idUserOuter);

    SysUserOuter selectByLoginName(String loginName);
}
