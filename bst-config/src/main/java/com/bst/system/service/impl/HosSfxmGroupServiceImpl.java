package com.bst.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.bst.common.utils.self.LoginAPIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.HosSfxmGroupMapper;
import com.bst.system.domain.HosSfxmGroup;
import com.bst.system.service.IHosSfxmGroupService;
import com.bst.common.core.text.Convert;

/**
 * 收费项目快速分组Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-19
 */
@Service
public class HosSfxmGroupServiceImpl implements IHosSfxmGroupService 
{
    @Autowired
    private HosSfxmGroupMapper hosSfxmGroupMapper;

    /**
     * 查询收费项目快速分组
     * 
     * @param idSfxmGroup 收费项目快速分组主键
     * @return 收费项目快速分组
     */
    @Override
    public HosSfxmGroup selectHosSfxmGroupByIdSfxmGroup(Long idSfxmGroup)
    {
        return hosSfxmGroupMapper.selectHosSfxmGroupByIdSfxmGroup(idSfxmGroup);
    }

    /**
     * 查询收费项目快速分组列表
     * 
     * @param hosSfxmGroup 收费项目快速分组
     * @return 收费项目快速分组
     */
    @Override
    public List<HosSfxmGroup> selectHosSfxmGroupList(HosSfxmGroup hosSfxmGroup)
    {
        return hosSfxmGroupMapper.selectHosSfxmGroupList(hosSfxmGroup);
    }

    /**
     * 新增收费项目快速分组
     * 
     * @param hosSfxmGroup 收费项目快速分组
     * @return 结果
     */
    @Override
    public int insertHosSfxmGroup(HosSfxmGroup hosSfxmGroup)
    {
        return hosSfxmGroupMapper.insertHosSfxmGroup(hosSfxmGroup);
    }

    /**
     * 修改收费项目快速分组
     * 
     * @param hosSfxmGroup 收费项目快速分组
     * @return 结果
     */
    @Override
    public int updateHosSfxmGroup(HosSfxmGroup hosSfxmGroup)
    {
        return hosSfxmGroupMapper.updateHosSfxmGroup(hosSfxmGroup);
    }

    /**
     * 批量删除收费项目快速分组
     * 
     * @param idSfxmGroups 需要删除的收费项目快速分组主键
     * @return 结果
     */
    @Override
    public int deleteHosSfxmGroupByIdSfxmGroups(String idSfxmGroups)
    {
        return hosSfxmGroupMapper.deleteHosSfxmGroupByIdSfxmGroups(Convert.toStrArray(idSfxmGroups));
    }

    /**
     * 删除收费项目快速分组信息
     * 
     * @param idSfxmGroup 收费项目快速分组主键
     * @return 结果
     */
    @Override
    public int deleteHosSfxmGroupByIdSfxmGroup(Long idSfxmGroup)
    {
        return hosSfxmGroupMapper.deleteHosSfxmGroupByIdSfxmGroup(idSfxmGroup);
    }

    @Override
    public int batchSaveSfxmGroup(List<HosSfxmGroup> list) {
        HosSfxmGroup group = new HosSfxmGroup();
        group.setSdCd(list.get(0).getSdCd());
        List<HosSfxmGroup> oldList = this.selectHosSfxmGroupList(group);
        List<String> existCdList = oldList.stream().map(HosSfxmGroup::getCdSfxm).collect(Collectors.toList());
        List<HosSfxmGroup> newList = new ArrayList<>();
        for (HosSfxmGroup sg : list) {
            if(existCdList.contains(sg.getCdSfxm())) {
                continue;
            }
            sg.setDtSysCre(new Date());
            sg.setNaEmpCre(LoginAPIUtils.getLoginUsename());
            newList.add(sg);
        }
        if(newList.size()>0) {
            hosSfxmGroupMapper.batchSaveSfxmGroup(newList);
        }
        return list.size();
    }
}
