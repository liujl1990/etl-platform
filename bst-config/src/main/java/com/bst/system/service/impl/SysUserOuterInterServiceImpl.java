package com.bst.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.SysUserOuterInterMapper;
import com.bst.system.domain.SysUserOuterInter;
import com.bst.system.service.ISysUserOuterInterService;
import com.bst.common.core.text.Convert;

/**
 * 用户接口权限Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
@Service
public class SysUserOuterInterServiceImpl implements ISysUserOuterInterService 
{
    @Autowired
    private SysUserOuterInterMapper sysUserOuterInterMapper;

    /**
     * 查询用户接口权限
     * 
     * @param idOuterInter 用户接口权限主键
     * @return 用户接口权限
     */
    @Override
    public SysUserOuterInter selectSysUserOuterInterByIdOuterInter(Long idOuterInter)
    {
        return sysUserOuterInterMapper.selectSysUserOuterInterByIdOuterInter(idOuterInter);
    }

    /**
     * 查询用户接口权限列表
     * 
     * @param sysUserOuterInter 用户接口权限
     * @return 用户接口权限
     */
    @Override
    public List<SysUserOuterInter> selectSysUserOuterInterList(SysUserOuterInter sysUserOuterInter)
    {
        return sysUserOuterInterMapper.selectSysUserOuterInterList(sysUserOuterInter);
    }

    /**
     * 新增用户接口权限
     * 
     * @param sysUserOuterInter 用户接口权限
     * @return 结果
     */
    @Override
    public int insertSysUserOuterInter(SysUserOuterInter sysUserOuterInter)
    {
        SysUserOuterInter oi = new SysUserOuterInter();
        oi.setIdInter(sysUserOuterInter.getIdInter());
        oi.setIdUserOuter(sysUserOuterInter.getIdUserOuter());
        List<SysUserOuterInter> list = this.selectSysUserOuterInterList(oi);
        if(list.size()==0) {
            return sysUserOuterInterMapper.insertSysUserOuterInter(sysUserOuterInter);
        }
        return 0;
    }

    @Override
    public int insertBatchSysUserOuterInter(List<SysUserOuterInter> list)
    {
        for (SysUserOuterInter oi : list) {
            this.insertSysUserOuterInter(oi);
        }
        return list.size();
    }

    /**
     * 修改用户接口权限
     * 
     * @param sysUserOuterInter 用户接口权限
     * @return 结果
     */
    @Override
    public int updateSysUserOuterInter(SysUserOuterInter sysUserOuterInter)
    {
        return sysUserOuterInterMapper.updateSysUserOuterInter(sysUserOuterInter);
    }

    /**
     * 批量删除用户接口权限
     * 
     * @param idOuterInters 需要删除的用户接口权限主键
     * @return 结果
     */
    @Override
    public int deleteSysUserOuterInterByIdOuterInters(String idOuterInters)
    {
        return sysUserOuterInterMapper.deleteSysUserOuterInterByIdOuterInters(Convert.toStrArray(idOuterInters));
    }

    /**
     * 删除用户接口权限信息
     * 
     * @param idOuterInter 用户接口权限主键
     * @return 结果
     */
    @Override
    public int deleteSysUserOuterInterByIdOuterInter(Long idOuterInter)
    {
        return sysUserOuterInterMapper.deleteSysUserOuterInterByIdOuterInter(idOuterInter);
    }
}
