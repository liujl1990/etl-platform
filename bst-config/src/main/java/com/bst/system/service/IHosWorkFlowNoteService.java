package com.bst.system.service;

import com.bst.system.domain.HosWorkFlowNote;

import java.util.List;

/**
 * 单据评论Service接口
 * 
 * @author cbh
 * @date 2023-04-08
 */
public interface IHosWorkFlowNoteService 
{
    /**
     * 查询单据评论
     * 
     * @param noteId 单据评论主键
     * @return 单据评论
     */
    public HosWorkFlowNote selectHosWorkFlowNoteByNoteId(String noteId);

    /**
     * 查询单据评论列表
     * 
     * @param hosWorkFlowNote 单据评论
     * @return 单据评论集合
     */
    public List<HosWorkFlowNote> selectHosWorkFlowNoteList(HosWorkFlowNote hosWorkFlowNote);

    /**
     * 新增单据评论
     * 
     * @param hosWorkFlowNote 单据评论
     * @return 结果
     */
    public int insertHosWorkFlowNote(HosWorkFlowNote hosWorkFlowNote);

    /**
     * 修改单据评论
     * 
     * @param hosWorkFlowNote 单据评论
     * @return 结果
     */
    public int updateHosWorkFlowNote(HosWorkFlowNote hosWorkFlowNote);

    /**
     * 批量删除单据评论
     * 
     * @param noteIds 需要删除的单据评论主键集合
     * @return 结果
     */
    public int deleteHosWorkFlowNoteByNoteIds(String noteIds);

    /**
     * 删除单据评论信息
     * 
     * @param noteId 单据评论主键
     * @return 结果
     */
    public int deleteHosWorkFlowNoteByNoteId(String noteId);
}
