package com.bst.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.bst.common.annotation.DataSource;
import com.bst.common.constant.JobConstant;
import com.bst.system.mapper.HosEscalationMapper;
import com.bst.system.service.IHosEscalationService;
import com.bst.system.vo.HosEscalationVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class HosEscalationServiceImpl implements IHosEscalationService {

    @Autowired
    HosEscalationMapper hosEscalationMapper;

    @Override
    @DataSource(value= JobConstant.DB_CLS_DW)
    public List<Map<String, Object>> selectMxbfz(HosEscalationVO vo) {
        PageHelper.clearPage();
        return hosEscalationMapper.selectMxbfz(vo);
    }

    @Override
    @DataSource(value= JobConstant.DB_CLS_DW)
    public List<Map<String, Object>> selectCfcc(HosEscalationVO vo) {
        return hosEscalationMapper.selectCfcc(vo);
    }
}
