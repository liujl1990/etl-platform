package com.bst.system.service;

import java.util.List;
import com.bst.system.domain.HosDiseDept;
import org.apache.ibatis.annotations.Param;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-05-15
 */
public interface IHosDiseDeptService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idDiseDept 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public HosDiseDept selectHosDiseDeptByIdDiseDept(Long idDiseDept);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosDiseDept 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HosDiseDept> selectHosDiseDeptList(HosDiseDept hosDiseDept);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosDiseDept 【请填写功能名称】
     * @return 结果
     */
    public int insertHosDiseDept(HosDiseDept hosDiseDept);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosDiseDept 【请填写功能名称】
     * @return 结果
     */
    public int updateHosDiseDept(HosDiseDept hosDiseDept);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDiseDepts 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteHosDiseDeptByIdDiseDepts(String idDiseDepts);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDiseDept 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteHosDiseDeptByIdDiseDept(Long idDiseDept);

    int saveBatchHosDiseDept(List<HosDiseDept> list);

    List<HosDiseDept> selectHosDiseDeptByCdDepts(@Param("list") List<String> cdDepts);
}
