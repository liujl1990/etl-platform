package com.bst.system.service.impl;

import java.util.List;

import com.bst.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.SysUserOuterMapper;
import com.bst.system.domain.SysUserOuter;
import com.bst.system.service.ISysUserOuterService;
import com.bst.common.core.text.Convert;

/**
 * 接口用户Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
@Service
public class SysUserOuterServiceImpl implements ISysUserOuterService 
{
    @Autowired
    private SysUserOuterMapper sysUserOuterMapper;

    /**
     * 查询接口用户
     * 
     * @param idUserOuter 接口用户主键
     * @return 接口用户
     */
    @Override
    public SysUserOuter selectSysUserOuterByIdUserOuter(Long idUserOuter)
    {
        return sysUserOuterMapper.selectSysUserOuterByIdUserOuter(idUserOuter);
    }

    /**
     * 查询接口用户列表
     * 
     * @param sysUserOuter 接口用户
     * @return 接口用户
     */
    @Override
    public List<SysUserOuter> selectSysUserOuterList(SysUserOuter sysUserOuter)
    {
        return sysUserOuterMapper.selectSysUserOuterList(sysUserOuter);
    }

    /**
     * 新增接口用户
     * 
     * @param sysUserOuter 接口用户
     * @return 结果
     */
    @Override
    public int insertSysUserOuter(SysUserOuter sysUserOuter)
    {
        sysUserOuter.setPassword(IdUtils.fastSimpleUUID());
        return sysUserOuterMapper.insertSysUserOuter(sysUserOuter);
    }

    /**
     * 修改接口用户
     * 
     * @param sysUserOuter 接口用户
     * @return 结果
     */
    @Override
    public int updateSysUserOuter(SysUserOuter sysUserOuter)
    {
        return sysUserOuterMapper.updateSysUserOuter(sysUserOuter);
    }

    /**
     * 批量删除接口用户
     * 
     * @param idUserOuters 需要删除的接口用户主键
     * @return 结果
     */
    @Override
    public int deleteSysUserOuterByIdUserOuters(String idUserOuters)
    {
        return sysUserOuterMapper.deleteSysUserOuterByIdUserOuters(Convert.toStrArray(idUserOuters));
    }

    /**
     * 删除接口用户信息
     * 
     * @param idUserOuter 接口用户主键
     * @return 结果
     */
    @Override
    public int deleteSysUserOuterByIdUserOuter(Long idUserOuter)
    {
        return sysUserOuterMapper.deleteSysUserOuterByIdUserOuter(idUserOuter);
    }

    @Override
    public SysUserOuter selectByLoginName(String loginName) {
        SysUserOuter outer = new SysUserOuter();
        outer.setLoginName(loginName);
        List<SysUserOuter> list = this.selectSysUserOuterList(outer);
        if(list.size()>0) {
            return list.get(0);
        }
        return null;
    }
}
