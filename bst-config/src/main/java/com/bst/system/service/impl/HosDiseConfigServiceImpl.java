package com.bst.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.HosDiseConfigMapper;
import com.bst.system.domain.HosDiseConfig;
import com.bst.system.service.IHosDiseConfigService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-13
 */
@Service
public class HosDiseConfigServiceImpl implements IHosDiseConfigService 
{
    @Autowired
    private HosDiseConfigMapper hosDiseConfigMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idDiseConfig 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public HosDiseConfig selectHosDiseConfigByIdDiseConfig(Long idDiseConfig)
    {
        return hosDiseConfigMapper.selectHosDiseConfigByIdDiseConfig(idDiseConfig);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosDiseConfig 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<HosDiseConfig> selectHosDiseConfigList(HosDiseConfig hosDiseConfig)
    {
        return hosDiseConfigMapper.selectHosDiseConfigList(hosDiseConfig);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosDiseConfig 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertHosDiseConfig(HosDiseConfig hosDiseConfig)
    {
        return hosDiseConfigMapper.insertHosDiseConfig(hosDiseConfig);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosDiseConfig 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateHosDiseConfig(HosDiseConfig hosDiseConfig)
    {
        return hosDiseConfigMapper.updateHosDiseConfig(hosDiseConfig);
    }

    /**
     * 批量删除【请填写功能名称
     * @return 结果
     */
    @Override
    public int deleteHosDiseConfigByIdDise(Long idDise)
    {
        return hosDiseConfigMapper.deleteHosDiseConfigByIdDise(idDise);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDiseConfig 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosDiseConfigByIdDiseConfig(Long idDiseConfig)
    {
        return hosDiseConfigMapper.deleteHosDiseConfigByIdDiseConfig(idDiseConfig);
    }

    @Override
    public void insertBatchHosDiseConfig(List<HosDiseConfig> configs) {
        for (HosDiseConfig config : configs) {
            this.insertHosDiseConfig(config);
        }
    }
}
