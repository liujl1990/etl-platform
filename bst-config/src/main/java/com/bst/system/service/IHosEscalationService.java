package com.bst.system.service;

import com.bst.system.vo.HosEscalationVO;

import java.util.List;
import java.util.Map;

public interface IHosEscalationService {

    List<Map<String,Object>> selectMxbfz(HosEscalationVO vo);

    List<Map<String,Object>> selectCfcc(HosEscalationVO vo);
}
