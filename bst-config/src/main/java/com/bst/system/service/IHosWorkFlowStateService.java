package com.bst.system.service;

import java.util.List;
import com.bst.system.domain.HosWorkFlowState;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-04-18
 */
public interface IHosWorkFlowStateService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param stateId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public HosWorkFlowState selectHosWorkFlowStateByStateId(Integer stateId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HosWorkFlowState> selectHosWorkFlowStateList(HosWorkFlowState hosWorkFlowState);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 结果
     */
    public int insertHosWorkFlowState(HosWorkFlowState hosWorkFlowState);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 结果
     */
    public int updateHosWorkFlowState(HosWorkFlowState hosWorkFlowState);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param stateIds 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteHosWorkFlowStateByStateIds(String stateIds);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param stateId 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteHosWorkFlowStateByStateId(Integer stateId);
}
