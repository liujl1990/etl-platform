package com.bst.system.service.impl;

import java.util.Date;
import java.util.List;

import com.bst.common.utils.StringUtils;
import com.bst.common.utils.self.LoginAPIUtils;
import com.bst.system.domain.HosDiseConfig;
import com.bst.system.service.IHosDiseConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.HosDiseMapper;
import com.bst.system.domain.HosDise;
import com.bst.system.service.IHosDiseService;

/**
 * 病种管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-13
 */
@Service
public class HosDiseServiceImpl implements IHosDiseService 
{
    @Autowired
    private HosDiseMapper hosDiseMapper;
    @Autowired
    private IHosDiseConfigService hosDiseConfigService;

    /**
     * 查询病种管理
     * 
     * @param id 病种管理主键
     * @return 病种管理
     */
    @Override
    public HosDise selectHosDiseById(Long id)
    {
        HosDise dise =  hosDiseMapper.selectHosDiseById(id);
        HosDiseConfig config = new HosDiseConfig();
        config.setIdDise(id);
        List<HosDiseConfig> configs = hosDiseConfigService.selectHosDiseConfigList(config);
        dise.setHosDiseConfigs(configs);
        return dise;
    }

    /**
     * 查询病种管理列表
     * 
     * @param hosDise 病种管理
     * @return 病种管理
     */
    @Override
    public List<HosDise> selectHosDiseList(HosDise hosDise)
    {
        return hosDiseMapper.selectHosDiseList(hosDise);
    }

    /**
     * 新增病种管理
     * 
     * @param hosDise 病种管理
     * @return 结果
     */
    @Override
    public int insertHosDise(HosDise hosDise)
    {


        hosDise.setDtSysCre(new Date());
        hosDise.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        hosDiseMapper.insertHosDise(hosDise);
        String sqlWhere = getSqlWhere(hosDise);
        hosDise.setSqlWhere(sqlWhere);
        hosDise.setFgAct(StringUtils.isEmpty(sqlWhere)?0:1);
        hosDiseMapper.updateHosDise(hosDise);
        return 1;
    }

    private String getSqlWhere(HosDise hosDise) {
        List<HosDiseConfig> configs = hosDise.getHosDiseConfigs();
        String sqlWhere="";
        hosDiseConfigService.deleteHosDiseConfigByIdDise(hosDise.getIdDise());
        if(configs!=null && configs.size()>0) {
            int i=0;
            for(HosDiseConfig config:configs) {
                config.setSort(i++);
                config.setIdDise(hosDise.getIdDise());
                sqlWhere += config.getAndOr()+" "+config.getpLeft()+fieldReplace(config.getFldQry())+" "+likeFilter(config.getFilterQry(),config.getValQry())+config.getpRight();
            }
            hosDiseConfigService.insertBatchHosDiseConfig(configs);
        }
        return sqlWhere;
    }

    private String fieldReplace(String fld) {
        if("cd".equals(fld)) {
            return "ID_DIM_ICD";
        } else {
            return "NA_DIM_ICD";
        }
    }

    private String likeFilter(String oper,String val) {
        if(oper.equals("=")) {
            return "='"+val+"'";
        } else if(oper.equals("all")) {
            return "like '%"+val+"%' ";
        } else if(oper.equals("left")) {
            return "like '"+val+"%' ";
        }
        return "";
    }

    /**
     * 修改病种管理
     * 
     * @param hosDise 病种管理
     * @return 结果
     */
    @Override
    public int updateHosDise(HosDise hosDise)
    {
        String sqlWhere = getSqlWhere(hosDise);
        hosDise.setSqlWhere(sqlWhere);
        return hosDiseMapper.updateHosDise(hosDise);
    }

    /**
     * 批量删除病种管理
     * 
     * @param ids 需要删除的病种管理主键
     * @return 结果
     */
    @Override
    public int deleteHosDiseByIds(String ids)
    {
        for(String id:ids.split(",")) {
            this.deleteHosDiseById(Long.parseLong(id));
        }
        return 1;
    }

    /**
     * 删除病种管理信息
     * 
     * @param id 病种管理主键
     * @return 结果
     */
    @Override
    public int deleteHosDiseById(Long id)
    {
        hosDiseConfigService.deleteHosDiseConfigByIdDise(id);
        return hosDiseMapper.deleteHosDiseById(id);
    }

    @Override
    public String selectMaxCdFromHosDise() {
        return hosDiseMapper.selectMaxCdFromHosDise();
    }

    @Override
    public List<HosDise> selectHosDiseByCds(List<String> cds) {
        return hosDiseMapper.selectHosDiseByCds(cds);
    }
}
