package com.bst.system.service;

import java.util.List;

import com.bst.system.domain.HosJxkh;
import com.bst.system.domain.HosJxkhTb;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-06-05
 */
public interface IHosJxkhTbService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idJxkhTb 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public HosJxkhTb selectHosJxkhTbByIdJxkhTb(Long idJxkhTb);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosJxkhTb 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HosJxkhTb> selectHosJxkhTbList(HosJxkhTb hosJxkhTb);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosJxkhTb 【请填写功能名称】
     * @return 结果
     */
    public int insertHosJxkhTb(HosJxkhTb hosJxkhTb);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosJxkhTb 【请填写功能名称】
     * @return 结果
     */
    public int updateHosJxkhTb(HosJxkhTb hosJxkhTb);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idJxkhTbs 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteHosJxkhTbByIdJxkhTbs(String idJxkhTbs);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idJxkhTb 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteHosJxkhTbByIdJxkhTb(Long idJxkhTb);

    List<HosJxkhTb> initHosJxkhTb(HosJxkh jxhk) ;

    String beginNext(Long euDttp,String dDes);
}
