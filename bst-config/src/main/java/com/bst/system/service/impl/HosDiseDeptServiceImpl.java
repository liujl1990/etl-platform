package com.bst.system.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.HosDiseDeptMapper;
import com.bst.system.domain.HosDiseDept;
import com.bst.system.service.IHosDiseDeptService;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-15
 */
@Service
public class HosDiseDeptServiceImpl implements IHosDiseDeptService 
{
    @Autowired
    private HosDiseDeptMapper hosDiseDeptMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idDiseDept 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public HosDiseDept selectHosDiseDeptByIdDiseDept(Long idDiseDept)
    {
        return hosDiseDeptMapper.selectHosDiseDeptByIdDiseDept(idDiseDept);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosDiseDept 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<HosDiseDept> selectHosDiseDeptList(HosDiseDept hosDiseDept)
    {
        return hosDiseDeptMapper.selectHosDiseDeptList(hosDiseDept);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosDiseDept 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertHosDiseDept(HosDiseDept hosDiseDept)
    {
        return hosDiseDeptMapper.insertHosDiseDept(hosDiseDept);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosDiseDept 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateHosDiseDept(HosDiseDept hosDiseDept)
    {
        return hosDiseDeptMapper.updateHosDiseDept(hosDiseDept);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDiseDepts 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosDiseDeptByIdDiseDepts(String idDiseDepts)
    {
        return hosDiseDeptMapper.deleteHosDiseDeptByIdDiseDepts(Convert.toStrArray(idDiseDepts));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDiseDept 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosDiseDeptByIdDiseDept(Long idDiseDept)
    {
        return hosDiseDeptMapper.deleteHosDiseDeptByIdDiseDept(idDiseDept);
    }

    @Override
    public int saveBatchHosDiseDept(List<HosDiseDept> list) {
        Long idDise = list.get(0).getIdDise();
        HosDiseDept diseDept = new HosDiseDept();
        diseDept.setIdDise(idDise);
        List<HosDiseDept> diseDepts = this.selectHosDiseDeptList(diseDept);
        List<String> cdDepts = diseDepts.stream().map(HosDiseDept::getCdDept).collect(Collectors.toList());
        for(HosDiseDept dd:list) {
            if(!cdDepts.contains(dd.getCdDept())) {
                this.insertHosDiseDept(dd);
            }
        }
        return list.size();
    }

    @Override
    public List<HosDiseDept> selectHosDiseDeptByCdDepts(List<String> cdDepts) {
        return hosDiseDeptMapper.selectHosDiseDeptByCdDepts(cdDepts);
    }
}
