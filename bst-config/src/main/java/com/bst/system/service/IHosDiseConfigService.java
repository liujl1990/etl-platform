package com.bst.system.service;

import java.util.List;
import com.bst.system.domain.HosDiseConfig;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-05-13
 */
public interface IHosDiseConfigService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idDiseConfig 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public HosDiseConfig selectHosDiseConfigByIdDiseConfig(Long idDiseConfig);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosDiseConfig 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HosDiseConfig> selectHosDiseConfigList(HosDiseConfig hosDiseConfig);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosDiseConfig 【请填写功能名称】
     * @return 结果
     */
    public int insertHosDiseConfig(HosDiseConfig hosDiseConfig);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosDiseConfig 【请填写功能名称】
     * @return 结果
     */
    public int updateHosDiseConfig(HosDiseConfig hosDiseConfig);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDiseConfigs 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteHosDiseConfigByIdDise(Long idDise);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDiseConfig 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteHosDiseConfigByIdDiseConfig(Long idDiseConfig);

    void insertBatchHosDiseConfig(List<HosDiseConfig> configs);
}
