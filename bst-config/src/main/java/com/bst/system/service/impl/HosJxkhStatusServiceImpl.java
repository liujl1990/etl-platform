package com.bst.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.HosJxkhStatusMapper;
import com.bst.system.domain.HosJxkhStatus;
import com.bst.system.service.IHosJxkhStatusService;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-06-05
 */
@Service
public class HosJxkhStatusServiceImpl implements IHosJxkhStatusService 
{
    @Autowired
    private HosJxkhStatusMapper hosJxkhStatusMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idJxkhStatus 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public HosJxkhStatus selectHosJxkhStatusByIdJxkhStatus(Long idJxkhStatus)
    {
        return hosJxkhStatusMapper.selectHosJxkhStatusByIdJxkhStatus(idJxkhStatus);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosJxkhStatus 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<HosJxkhStatus> selectHosJxkhStatusList(HosJxkhStatus hosJxkhStatus)
    {
        return hosJxkhStatusMapper.selectHosJxkhStatusList(hosJxkhStatus);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosJxkhStatus 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertHosJxkhStatus(HosJxkhStatus hosJxkhStatus)
    {
        return hosJxkhStatusMapper.insertHosJxkhStatus(hosJxkhStatus);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosJxkhStatus 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateHosJxkhStatus(HosJxkhStatus hosJxkhStatus)
    {
        return hosJxkhStatusMapper.updateHosJxkhStatus(hosJxkhStatus);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idJxkhStatuss 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosJxkhStatusByIdJxkhStatuss(String idJxkhStatuss)
    {
        return hosJxkhStatusMapper.deleteHosJxkhStatusByIdJxkhStatuss(Convert.toStrArray(idJxkhStatuss));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idJxkhStatus 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosJxkhStatusByIdJxkhStatus(Long idJxkhStatus)
    {
        return hosJxkhStatusMapper.deleteHosJxkhStatusByIdJxkhStatus(idJxkhStatus);
    }
}
