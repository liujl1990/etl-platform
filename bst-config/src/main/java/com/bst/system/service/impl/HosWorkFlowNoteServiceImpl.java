package com.bst.system.service.impl;

import com.bst.common.core.text.Convert;
import com.bst.common.utils.self.LoginAPIUtils;
import com.bst.system.domain.HosWorkFlowNote;
import com.bst.system.mapper.HosWorkFlowNoteMapper;
import com.bst.system.service.IHosWorkFlowNoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 单据评论Service业务层处理
 * 
 * @author cbh
 * @date 2023-04-08
 */
@Service
public class HosWorkFlowNoteServiceImpl implements IHosWorkFlowNoteService 
{
    @Autowired
    private HosWorkFlowNoteMapper hosWorkFlowNoteMapper;

    /**
     * 查询单据评论
     * 
     * @param noteId 单据评论主键
     * @return 单据评论
     */
    @Override
    public HosWorkFlowNote selectHosWorkFlowNoteByNoteId(String noteId)
    {
        return hosWorkFlowNoteMapper.selectHosWorkFlowNoteByNoteId(noteId);
    }

    /**
     * 查询单据评论列表
     * 
     * @param hosWorkFlowNote 单据评论
     * @return 单据评论
     */
    @Override
    public List<HosWorkFlowNote> selectHosWorkFlowNoteList(HosWorkFlowNote hosWorkFlowNote)
    {
        return hosWorkFlowNoteMapper.selectHosWorkFlowNoteList(hosWorkFlowNote);
    }

    /**
     * 新增单据评论
     * 
     * @param hosWorkFlowNote 单据评论
     * @return 结果
     */
    @Override
    public int insertHosWorkFlowNote(HosWorkFlowNote hosWorkFlowNote)
    {
        hosWorkFlowNote.setDtSysCre(new Date());
        hosWorkFlowNote.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        return hosWorkFlowNoteMapper.insertHosWorkFlowNote(hosWorkFlowNote);
    }

    /**
     * 修改单据评论
     * 
     * @param hosWorkFlowNote 单据评论
     * @return 结果
     */
    @Override
    public int updateHosWorkFlowNote(HosWorkFlowNote hosWorkFlowNote)
    {
        return hosWorkFlowNoteMapper.updateHosWorkFlowNote(hosWorkFlowNote);
    }

    /**
     * 批量删除单据评论
     * 
     * @param noteIds 需要删除的单据评论主键
     * @return 结果
     */
    @Override
    public int deleteHosWorkFlowNoteByNoteIds(String noteIds)
    {
        return hosWorkFlowNoteMapper.deleteHosWorkFlowNoteByNoteIds(Convert.toStrArray(noteIds));
    }

    /**
     * 删除单据评论信息
     * 
     * @param noteId 单据评论主键
     * @return 结果
     */
    @Override
    public int deleteHosWorkFlowNoteByNoteId(String noteId)
    {
        return hosWorkFlowNoteMapper.deleteHosWorkFlowNoteByNoteId(noteId);
    }
}
