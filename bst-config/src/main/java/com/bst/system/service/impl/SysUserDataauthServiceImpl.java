package com.bst.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.bst.common.utils.self.LoginAPIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.SysUserDataauthMapper;
import com.bst.system.domain.SysUserDataauth;
import com.bst.system.service.ISysUserDataauthService;
import com.bst.common.core.text.Convert;

/**
 * 用户数据权限Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-01-20
 */
@Service
public class SysUserDataauthServiceImpl implements ISysUserDataauthService 
{
    @Autowired
    private SysUserDataauthMapper sysUserDataauthMapper;

    /**
     * 查询用户数据权限
     * 
     * @param dataauthId 用户数据权限主键
     * @return 用户数据权限
     */
    @Override
    public SysUserDataauth selectSysUserDataauthByDataauthId(Long dataauthId)
    {
        return sysUserDataauthMapper.selectSysUserDataauthByDataauthId(dataauthId);
    }

    /**
     * 查询用户数据权限列表
     * 
     * @param sysUserDataauth 用户数据权限
     * @return 用户数据权限
     */
    @Override
    public List<SysUserDataauth> selectSysUserDataauthList(SysUserDataauth sysUserDataauth)
    {
        List<SysUserDataauth> authList = sysUserDataauthMapper.selectSysUserDataauthList(sysUserDataauth);
        Boolean fgAll = false;
        for (SysUserDataauth da : authList) {
            if("000".equals(da.getDataauthCd())) {
                authList = new ArrayList<>();
                authList.add(da);
                break;
            }
        }
        return authList;
    }

    /**
     * 新增用户数据权限
     * 
     * @param sysUserDataauth 用户数据权限
     * @return 结果
     */
    @Override
    public int insertSysUserDataauth(SysUserDataauth sysUserDataauth)
    {
        sysUserDataauth.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        sysUserDataauth.setDtSysCre(new Date());
        return sysUserDataauthMapper.insertSysUserDataauth(sysUserDataauth);
    }

    @Override
    public int batchInsertSysUserDataauth(List<SysUserDataauth> list) {
        SysUserDataauth model = new SysUserDataauth();
        model.setSdDataauthType(list.get(0).getSdDataauthType());
        model.setUserId(list.get(0).getUserId());
        List<SysUserDataauth> allUserAuth = this.selectSysUserDataauthList(model);
        List<String> cds = allUserAuth.stream().map(SysUserDataauth::getDataauthCd).collect(Collectors.toList());
        for(SysUserDataauth auth:list) {
            if(!cds.contains(auth.getDataauthCd())) {
                this.insertSysUserDataauth(auth);
            }
        }
        return list.size();
    }

    /**
     * 修改用户数据权限
     * 
     * @param sysUserDataauth 用户数据权限
     * @return 结果
     */
    @Override
    public int updateSysUserDataauth(SysUserDataauth sysUserDataauth)
    {
        return sysUserDataauthMapper.updateSysUserDataauth(sysUserDataauth);
    }

    /**
     * 批量删除用户数据权限
     * 
     * @param dataauthIds 需要删除的用户数据权限主键
     * @return 结果
     */
    @Override
    public int deleteSysUserDataauthByDataauthIds(String dataauthIds)
    {
        return sysUserDataauthMapper.deleteSysUserDataauthByDataauthIds(Convert.toStrArray(dataauthIds));
    }

    /**
     * 删除用户数据权限信息
     * 
     * @param dataauthId 用户数据权限主键
     * @return 结果
     */
    @Override
    public int deleteSysUserDataauthByDataauthId(Long dataauthId)
    {
        return sysUserDataauthMapper.deleteSysUserDataauthByDataauthId(dataauthId);
    }
}
