package com.bst.system.service.impl;

import java.util.Date;
import java.util.List;

import com.bst.common.utils.self.LoginAPIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.HosWorkFlowStateMapper;
import com.bst.system.domain.HosWorkFlowState;
import com.bst.system.service.IHosWorkFlowStateService;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-18
 */
@Service
public class HosWorkFlowStateServiceImpl implements IHosWorkFlowStateService 
{
    @Autowired
    private HosWorkFlowStateMapper hosWorkFlowStateMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param stateId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public HosWorkFlowState selectHosWorkFlowStateByStateId(Integer stateId)
    {
        return hosWorkFlowStateMapper.selectHosWorkFlowStateByStateId(stateId);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<HosWorkFlowState> selectHosWorkFlowStateList(HosWorkFlowState hosWorkFlowState)
    {
        return hosWorkFlowStateMapper.selectHosWorkFlowStateList(hosWorkFlowState);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertHosWorkFlowState(HosWorkFlowState hosWorkFlowState)
    {
        hosWorkFlowState.setDtSysCre(new Date());
        hosWorkFlowState.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        return hosWorkFlowStateMapper.insertHosWorkFlowState(hosWorkFlowState);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateHosWorkFlowState(HosWorkFlowState hosWorkFlowState)
    {
        return hosWorkFlowStateMapper.updateHosWorkFlowState(hosWorkFlowState);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param stateIds 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosWorkFlowStateByStateIds(String stateIds)
    {
        return hosWorkFlowStateMapper.deleteHosWorkFlowStateByStateIds(Convert.toStrArray(stateIds));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param stateId 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosWorkFlowStateByStateId(Integer stateId)
    {
        return hosWorkFlowStateMapper.deleteHosWorkFlowStateByStateId(stateId);
    }
}
