package com.bst.system.service;


import com.bst.common.core.domain.AjaxResult;
import com.bst.common.exception.base.BaseException;
import com.bst.system.vo.AlterTableParamVO;

public interface ICommonTableDDLService {
    /**
     * 根据元数据修改表
     *
     * @return
     */
    AjaxResult alterTableByMed(AlterTableParamVO paramVO) ;

    AjaxResult alterTableByMedOtherDB(AlterTableParamVO paramVO, String DataSourceId);

    /**
     * 根据元数据删除表
     *
     * @return
     */
    AjaxResult dropTableByMed(String cdTb, Long idTblog);

    AjaxResult dropTableByMedOtherDB(String cdTb, Long idTblog, String DataSourceId);

    AjaxResult execDDLSQL(String sqls, String dataSourceName) throws BaseException;


}
