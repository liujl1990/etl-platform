package com.bst.system.service;

import com.bst.system.domain.HosWorkFlow;

import java.util.List;

/**
 * 单据提交Service接口
 * 
 * @author ruoyi
 * @date 2023-04-08
 */
public interface IHosWorkFlowService 
{
    /**
     * 查询单据提交
     * 
     * @param idIndex 单据提交主键
     * @return 单据提交
     */
    HosWorkFlow selectHosWorkFlowListById(int workId);

    /**
     * 查询单据提交列表
     * 
     * @param hosWorkFlow 单据提交
     * @return 单据提交集合
     */
    public List<HosWorkFlow> selectHosWorkFlowList(HosWorkFlow hosWorkFlow);

    /**
     * 新增单据提交
     * 
     * @param hosWorkFlow 单据提交
     * @return 结果
     */
    public int insertHosWorkFlow(HosWorkFlow hosWorkFlow);

    /**
     * 修改单据提交
     * 
     * @param hosWorkFlow 单据提交
     * @return 结果
     */
    public int updateHosWorkFlow(HosWorkFlow hosWorkFlow);

    /**
     * 批量删除单据提交
     * 
     * @param idIndexs 需要删除的单据提交主键集合
     * @return 结果
     */
    public int deleteHosWorkFlowByIdIndexs(String idIndexs);

    /**
     * 删除单据提交信息
     * 
     * @param idIndex 单据提交主键
     * @return 结果
     */
    public int deleteHosWorkFlowByIdIndex(int idIndex);

    List<HosWorkFlow> selectHosWorkFlowListWithAuth(HosWorkFlow hosWorkFlow);

    List<HosWorkFlow> selectDeptsFromHosWorkFlow();
}
