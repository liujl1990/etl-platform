package com.bst.system.service.impl;

import java.util.Date;
import java.util.List;

import com.bst.common.utils.self.LoginAPIUtils;
import com.bst.system.domain.HosJxkh;
import com.bst.system.domain.HosJxkhStatus;
import com.bst.system.service.IHosJxkhStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.HosJxkhTbMapper;
import com.bst.system.domain.HosJxkhTb;
import com.bst.system.service.IHosJxkhTbService;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-06-05
 */
@Service
public class HosJxkhTbServiceImpl implements IHosJxkhTbService 
{
    @Autowired
    private HosJxkhTbMapper hosJxkhTbMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idJxkhTb 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public HosJxkhTb selectHosJxkhTbByIdJxkhTb(Long idJxkhTb)
    {
        return hosJxkhTbMapper.selectHosJxkhTbByIdJxkhTb(idJxkhTb);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosJxkhTb 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<HosJxkhTb> selectHosJxkhTbList(HosJxkhTb hosJxkhTb)
    {
        return hosJxkhTbMapper.selectHosJxkhTbList(hosJxkhTb);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosJxkhTb 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertHosJxkhTb(HosJxkhTb hosJxkhTb)
    {
        return hosJxkhTbMapper.insertHosJxkhTb(hosJxkhTb);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosJxkhTb 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateHosJxkhTb(HosJxkhTb hosJxkhTb)
    {
        hosJxkhTb.setNaEmpModi(LoginAPIUtils.getLoginUsename());
        hosJxkhTb.setDtSysModi(new Date());
        return hosJxkhTbMapper.updateHosJxkhTb(hosJxkhTb);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idJxkhTbs 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosJxkhTbByIdJxkhTbs(String idJxkhTbs)
    {
        return hosJxkhTbMapper.deleteHosJxkhTbByIdJxkhTbs(Convert.toStrArray(idJxkhTbs));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idJxkhTb 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteHosJxkhTbByIdJxkhTb(Long idJxkhTb)
    {
        return hosJxkhTbMapper.deleteHosJxkhTbByIdJxkhTb(idJxkhTb);
    }

    @Override
    public List<HosJxkhTb> initHosJxkhTb(HosJxkh jxhk) {
        return hosJxkhTbMapper.initHosJxkhTb(jxhk);
    }

    @Autowired
    IHosJxkhStatusService hosJxkhStatusService;

    /**
     * 如果时间大于当前，则不允许初始化
     * 如果存在未结转数据，则不允许初始化
     * @param euDttp
     * @param dDes
     */
    @Override
    public String beginNext(Long euDttp,String dDes) {
        HosJxkhStatus status = new HosJxkhStatus();
        status.setEuDttp(euDttp);status.setdDes(dDes);status.setFgEnd(0L);
        List<HosJxkhStatus> jxkhStatuses = hosJxkhStatusService.selectHosJxkhStatusList(status);
        if(jxkhStatuses==null) {
            return dDes+"不满足结转条件，请确认是否有未结转数据";
        }
        HosJxkhTb tb = new HosJxkhTb();
        tb.setVal("1");
        tb.setdDes(dDes);tb.setEuDttp(euDttp);
        List<HosJxkhTb> tbs = this.selectHosJxkhTbList(tb);
        if(tbs.size()>0) {
            return dDes+"有数据未录入";
        }

        status.setFgEnd(1L);
        status.setDtSysModi(new Date());
        status.setNaEmpModi(LoginAPIUtils.getLoginUsename());
        hosJxkhStatusService.updateHosJxkhStatus(status);
        status.setIdJxkhStatus(null);
        status.setFgEnd(0L);
        status.setdDes(getNextDDes(euDttp,dDes));
        hosJxkhStatusService.insertHosJxkhStatus(status);
        HosJxkh jxkh = new HosJxkh();
        jxkh.setYear(Long.parseLong(status.getdDes().substring(0,4)));
        if(euDttp==1) {
            jxkh.setFgQ(1);
        } else {
            jxkh.setFgM(1);
        }
        List<HosJxkhTb> tbList = this.initHosJxkhTb(jxkh);
        for (HosJxkhTb tb2 : tbList) {
            tb2.setEuDttp(euDttp);
            tb2.setdDes(dDes);
            this.insertHosJxkhTb(tb2);
        }
        return "";
    }

    private String getNextDDes(Long euDttp,String dDes) {
        Integer year = Integer.parseInt(dDes.substring(0,4));
        if(euDttp==1) { //季度
            Integer q = Integer.parseInt(dDes.substring(4,5));
            if(q==4) {
                year = year+1;
                q = 1;
            } else {
                q = q+1;
            }
            return year+""+1;
        } else if(euDttp==2) { //季度
            Integer m = Integer.parseInt(dDes.substring(4,6));
            if(m<12) {
                m = m+1;
            } else {
                year = year+1;
                m = 1;
            }
            String mm;
            if(m.toString().length()<2) {
                mm = "0"+m;
            } else {
                mm = m.toString();
            }
            return year+""+mm;
         }
        return null;
    }
}
