package com.bst.system.service;

import java.util.List;
import com.bst.system.domain.HosJxkhStatus;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-06-05
 */
public interface IHosJxkhStatusService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idJxkhStatus 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public HosJxkhStatus selectHosJxkhStatusByIdJxkhStatus(Long idJxkhStatus);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosJxkhStatus 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HosJxkhStatus> selectHosJxkhStatusList(HosJxkhStatus hosJxkhStatus);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosJxkhStatus 【请填写功能名称】
     * @return 结果
     */
    public int insertHosJxkhStatus(HosJxkhStatus hosJxkhStatus);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosJxkhStatus 【请填写功能名称】
     * @return 结果
     */
    public int updateHosJxkhStatus(HosJxkhStatus hosJxkhStatus);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idJxkhStatuss 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteHosJxkhStatusByIdJxkhStatuss(String idJxkhStatuss);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idJxkhStatus 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteHosJxkhStatusByIdJxkhStatus(Long idJxkhStatus);
}
