package com.bst.system.service.impl;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.bst.common.utils.StringUtils;
import com.bst.system.domain.HosJxkhTb;
import com.bst.system.service.IHosJxkhTbService;
import com.bst.system.vo.HosJxkhDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.system.mapper.HosJxkhMapper;
import com.bst.system.domain.HosJxkh;
import com.bst.system.service.IHosJxkhService;
import com.bst.common.core.text.Convert;

/**
 * 绩效考核Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-06-02
 */
@Service
public class HosJxkhServiceImpl implements IHosJxkhService 
{
    @Autowired
    private HosJxkhMapper hosJxkhMapper;

    /**
     * 查询绩效考核
     * 
     * @param idJxkh 绩效考核主键
     * @return 绩效考核
     */
    @Override
    public HosJxkh selectHosJxkhByIdJxkh(Long idJxkh)
    {
        return hosJxkhMapper.selectHosJxkhByIdJxkh(idJxkh);
    }

    /**
     * 查询绩效考核列表
     * 
     * @param hosJxkh 绩效考核
     * @return 绩效考核
     */
    @Override
    public List<HosJxkh> selectHosJxkhList(HosJxkh hosJxkh)
    {
        return hosJxkhMapper.selectHosJxkhList(hosJxkh);
    }

    /**
     * 新增绩效考核
     * 
     * @param hosJxkh 绩效考核
     * @return 结果
     */
    @Override
    public int insertHosJxkh(HosJxkh hosJxkh)
    {
        return hosJxkhMapper.insertHosJxkh(hosJxkh);
    }

    /**
     * 修改绩效考核
     * 
     * @param hosJxkh 绩效考核
     * @return 结果
     */
    @Override
    public int updateHosJxkh(HosJxkh hosJxkh)
    {
        return hosJxkhMapper.updateHosJxkh(hosJxkh);
    }

    /**
     * 批量删除绩效考核
     * 
     * @param idJxkhs 需要删除的绩效考核主键
     * @return 结果
     */
    @Override
    public int deleteHosJxkhByIdJxkhs(String idJxkhs)
    {
        return hosJxkhMapper.deleteHosJxkhByIdJxkhs(Convert.toStrArray(idJxkhs));
    }

    /**
     * 删除绩效考核信息
     * 
     * @param idJxkh 绩效考核主键
     * @return 结果
     */
    @Override
    public int deleteHosJxkhByIdJxkh(Long idJxkh)
    {
        return hosJxkhMapper.deleteHosJxkhByIdJxkh(idJxkh);
    }

    @Autowired
    private IHosJxkhTbService hosJxkhTbService;

    @Override
    public Map<String,List<HosJxkhDataVO>> loadJxkhData(String cdCa,Long euDttp,String dDes) {
        HosJxkh jxkh = new HosJxkh();
        jxkh.setCdCa(cdCa);
        List<HosJxkh> jxkhs = this.selectHosJxkhList(jxkh);
        Map<String,List<HosJxkhDataVO>> dataMap =new LinkedHashMap<>();
        List<HosJxkhDataVO> sunJxkhs;
        HosJxkhTb tb = new HosJxkhTb();
        tb.setEuDttp(euDttp);tb.setdDes(dDes);
        List<HosJxkhTb> tbList = hosJxkhTbService.selectHosJxkhTbList(tb);
        Map<String,Object> indexValMap = tbList.stream().collect(Collectors.toMap(HosJxkhTb::getIdIndex,HosJxkhTb::getVal));
        List<HosJxkhDataVO> dataVOS = new ArrayList<>();
        HosJxkhDataVO dataVO;
        for(HosJxkh kh:jxkhs) {
            if((sunJxkhs=dataMap.get(kh.getCdCa()))==null) {
                sunJxkhs = new ArrayList<>();
                dataMap.put(kh.getCdCa(),sunJxkhs);
            }
            dataVO = new HosJxkhDataVO();
            if(kh.getFgFill()==1) { //填报
                dataVO.setIdIndex(kh.getIdIndex());
                dataVO.setNaIndex(kh.getNaIndex());
                dataVO.setValue(indexValMap.get(kh.getIdIndex())==null?"":indexValMap.get(kh.getIdIndex()).toString());
                dataVO.setValue(resultHandler2(dataVO.getValue(),kh.getEuValtp()));
                dataVO.setDiff(0D);
                dataVO.setSort(kh.getSort());
                dataVO.setDiffDes("10%");
                dataVO.setTarget("待定");
                dataVO.setScore("0分");
            }
            sunJxkhs.add(dataVO);
        }
        return dataMap;
    }

    public static final DecimalFormat FLOAT_FORMAT = new DecimalFormat("#.###");
    public static final DecimalFormat PERCENT_FORMAT = new DecimalFormat("#.##%");

    public Object resultHandler2(Object value, String sdValtp) {
        if(value==null || StringUtils.isEmpty(value.toString())) {
            return null;
        }
        if("p".equals(sdValtp)) {
            value = PERCENT_FORMAT.format(Double.parseDouble(value.toString()));
        }
        return value;
    }
}
