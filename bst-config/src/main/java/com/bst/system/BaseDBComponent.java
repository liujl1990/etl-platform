package com.bst.system;

import com.alibaba.druid.pool.DruidDataSource;
import com.bst.common.constant.JobConstant;
import com.bst.system.framework.config.properties.DruidProperties;
import com.bst.system.framework.datasource.DynamicDataSource;
import com.bst.system.framework.utils.DBUtil;
import com.bst.system.framework.utils.MsgUtil;
import com.bst.base.domain.BaseDb;
import com.bst.base.service.IBaseDbService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Component
public class BaseDBComponent implements InitializingBean {
    private static Logger logger = LoggerFactory.getLogger(BaseDBComponent.class);
    @Autowired
    private IBaseDbService baseDbService;
    @Autowired
    private DynamicDataSource dynamicDataSource;
    @Autowired
    private DruidProperties druidProperties;

    public DruidDataSource createDataSource(BaseDb baseDb) {
        DruidDataSource datasource = DataSourceBuilder.create()
                .type(DruidDataSource.class)
                .driverClassName(baseDb.getDriver())
                .url(baseDb.getUrl())
                .username(baseDb.getNaUse())
                .password(baseDb.getPwd())
                .build();
        datasource.setDbType(baseDb.getSdDbtp());
        try {
            datasource.getConnection();
        } catch (SQLException throwables) {
            datasource.close();
            MsgUtil.sendMsg(JobConstant.MSG_CODE_A01,"数据源"+baseDb.getNa()+"不可用",null);
        }
        datasource.setConnectionErrorRetryAttempts(3);
        return datasource;
    }


    public void removeDataSource(String dbId) {
       //dynamicDataSource.removeDataSource(dbId);
        logger.info(String.format("----------> DynamicDataSource remove [%s].", dbId));
    }

    public void addDataSource(BaseDb baseDb) {
        DruidDataSource dataSource = createDataSource(baseDb);
        if(dataSource==null) {
            return;
        }
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            if ("dw".equals(baseDb.getSdBuscls())) {
                dynamicDataSource.addDataSource(JobConstant.DB_CLS_DW, dataSource);
            } else {
                dynamicDataSource.addDataSource(baseDb.getIdDb().toString(), dataSource);
            }
        } catch (Exception e) {
        } finally {
            DBUtil.closeDBResources(null, null, connection);
        }
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        BaseDb baseDb1 = new BaseDb();
        baseDb1.setFgAct(1);
        List<BaseDb> pageVO = baseDbService.selectBaseDbList(baseDb1);
        for (BaseDb baseDb : pageVO) {
            addDataSource(baseDb);
        }
    }


}
