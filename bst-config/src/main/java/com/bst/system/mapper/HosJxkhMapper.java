package com.bst.system.mapper;

import java.util.List;
import com.bst.system.domain.HosJxkh;

/**
 * 绩效考核Mapper接口
 * 
 * @author ruoyi
 * @date 2023-06-02
 */
public interface HosJxkhMapper 
{
    /**
     * 查询绩效考核
     * 
     * @param idJxkh 绩效考核主键
     * @return 绩效考核
     */
    public HosJxkh selectHosJxkhByIdJxkh(Long idJxkh);

    /**
     * 查询绩效考核列表
     * 
     * @param hosJxkh 绩效考核
     * @return 绩效考核集合
     */
    public List<HosJxkh> selectHosJxkhList(HosJxkh hosJxkh);

    /**
     * 新增绩效考核
     * 
     * @param hosJxkh 绩效考核
     * @return 结果
     */
    public int insertHosJxkh(HosJxkh hosJxkh);

    /**
     * 修改绩效考核
     * 
     * @param hosJxkh 绩效考核
     * @return 结果
     */
    public int updateHosJxkh(HosJxkh hosJxkh);

    /**
     * 删除绩效考核
     * 
     * @param idJxkh 绩效考核主键
     * @return 结果
     */
    public int deleteHosJxkhByIdJxkh(Long idJxkh);

    /**
     * 批量删除绩效考核
     * 
     * @param idJxkhs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHosJxkhByIdJxkhs(String[] idJxkhs);
}
