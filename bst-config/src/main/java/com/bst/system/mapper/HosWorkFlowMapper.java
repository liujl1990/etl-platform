package com.bst.system.mapper;

import com.bst.system.domain.HosWorkFlow;
import com.bst.system.domain.HosWorkFlowNote;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 单据提交Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-08
 */
public interface HosWorkFlowMapper 
{
    /**
     * 查询单据提交
     * 
     * @param idIndex 单据提交主键
     * @return 单据提交
     */
    public HosWorkFlow selectHosWorkFlowListById(@Param("workId") int workId);

    /**
     * 查询单据提交列表
     * 
     * @param hosWorkFlow 单据提交
     * @return 单据提交集合
     */
    public List<HosWorkFlow> selectHosWorkFlowList(HosWorkFlow hosWorkFlow);

    /**
     * 新增单据提交
     * 
     * @param hosWorkFlow 单据提交
     * @return 结果
     */
    public int insertHosWorkFlow(HosWorkFlow hosWorkFlow);

    /**
     * 修改单据提交
     * 
     * @param hosWorkFlow 单据提交
     * @return 结果
     */
    public int updateHosWorkFlow(HosWorkFlow hosWorkFlow);

    /**
     * 删除单据提交
     * 
     * @param idIndex 单据提交主键
     * @return 结果
     */
    public int deleteHosWorkFlowByIdIndex(int idIndex);

    /**
     * 批量删除单据提交
     * 
     * @param idIndexs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHosWorkFlowByIdIndexs(String[] idIndexs);

    /**
     * 批量删除${subTable.functionName}
     * 
     * @param idIndexs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHosWorkFlowNoteByWorkIds(String[] idIndexs);
    
    /**
     * 批量新增${subTable.functionName}
     * 
     * @param hosWorkFlowNoteList ${subTable.functionName}列表
     * @return 结果
     */
    public int batchHosWorkFlowNote(List<HosWorkFlowNote> hosWorkFlowNoteList);
    

    /**
     * 通过单据提交主键删除${subTable.functionName}信息
     * 
     * @param idIndex 单据提交ID
     * @return 结果
     */
    public int deleteHosWorkFlowNoteByWorkId(int idIndex);

    List<HosWorkFlow> selectDeptsFromHosWorkFlow();
}
