package com.bst.system.mapper;

import java.util.List;
import com.bst.system.domain.HosWorkFlowState;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-18
 */
public interface HosWorkFlowStateMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param stateId 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public HosWorkFlowState selectHosWorkFlowStateByStateId(Integer stateId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HosWorkFlowState> selectHosWorkFlowStateList(HosWorkFlowState hosWorkFlowState);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 结果
     */
    public int insertHosWorkFlowState(HosWorkFlowState hosWorkFlowState);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hosWorkFlowState 【请填写功能名称】
     * @return 结果
     */
    public int updateHosWorkFlowState(HosWorkFlowState hosWorkFlowState);

    /**
     * 删除【请填写功能名称】
     * 
     * @param stateId 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteHosWorkFlowStateByStateId(Integer stateId);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param stateIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHosWorkFlowStateByStateIds(String[] stateIds);
}
