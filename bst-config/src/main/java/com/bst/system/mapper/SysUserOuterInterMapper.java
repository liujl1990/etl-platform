package com.bst.system.mapper;

import java.util.List;
import com.bst.system.domain.SysUserOuterInter;

/**
 * 用户接口权限Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
public interface SysUserOuterInterMapper 
{
    /**
     * 查询用户接口权限
     * 
     * @param idOuterInter 用户接口权限主键
     * @return 用户接口权限
     */
    public SysUserOuterInter selectSysUserOuterInterByIdOuterInter(Long idOuterInter);

    /**
     * 查询用户接口权限列表
     * 
     * @param sysUserOuterInter 用户接口权限
     * @return 用户接口权限集合
     */
    public List<SysUserOuterInter> selectSysUserOuterInterList(SysUserOuterInter sysUserOuterInter);

    /**
     * 新增用户接口权限
     * 
     * @param sysUserOuterInter 用户接口权限
     * @return 结果
     */
    public int insertSysUserOuterInter(SysUserOuterInter sysUserOuterInter);

    /**
     * 修改用户接口权限
     * 
     * @param sysUserOuterInter 用户接口权限
     * @return 结果
     */
    public int updateSysUserOuterInter(SysUserOuterInter sysUserOuterInter);

    /**
     * 删除用户接口权限
     * 
     * @param idOuterInter 用户接口权限主键
     * @return 结果
     */
    public int deleteSysUserOuterInterByIdOuterInter(Long idOuterInter);

    /**
     * 批量删除用户接口权限
     * 
     * @param idOuterInters 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserOuterInterByIdOuterInters(String[] idOuterInters);
}
