package com.bst.system.mapper;

import java.util.List;
import com.bst.system.domain.HosDise;
import org.apache.ibatis.annotations.Param;

/**
 * 病种管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-13
 */
public interface HosDiseMapper 
{
    /**
     * 查询病种管理
     * 
     * @param id 病种管理主键
     * @return 病种管理
     */
    public HosDise selectHosDiseById(Long id);

    /**
     * 查询病种管理列表
     * 
     * @param hosDise 病种管理
     * @return 病种管理集合
     */
    public List<HosDise> selectHosDiseList(HosDise hosDise);

    /**
     * 新增病种管理
     * 
     * @param hosDise 病种管理
     * @return 结果
     */
    public int insertHosDise(HosDise hosDise);

    /**
     * 修改病种管理
     * 
     * @param hosDise 病种管理
     * @return 结果
     */
    public int updateHosDise(HosDise hosDise);

    /**
     * 删除病种管理
     * 
     * @param id 病种管理主键
     * @return 结果
     */
    public int deleteHosDiseById(Long id);

    /**
     * 批量删除病种管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHosDiseByIds(String[] ids);

    String selectMaxCdFromHosDise();

    List<HosDise> selectHosDiseByCds(@Param("list") List<String> cds);
}
