package com.bst.system.mapper;

import java.util.List;
import com.bst.system.domain.HosSfxmGroup;

/**
 * 收费项目快速分组Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-19
 */
public interface HosSfxmGroupMapper 
{
    /**
     * 查询收费项目快速分组
     * 
     * @param idSfxmGroup 收费项目快速分组主键
     * @return 收费项目快速分组
     */
    public HosSfxmGroup selectHosSfxmGroupByIdSfxmGroup(Long idSfxmGroup);

    /**
     * 查询收费项目快速分组列表
     * 
     * @param hosSfxmGroup 收费项目快速分组
     * @return 收费项目快速分组集合
     */
    public List<HosSfxmGroup> selectHosSfxmGroupList(HosSfxmGroup hosSfxmGroup);

    /**
     * 新增收费项目快速分组
     * 
     * @param hosSfxmGroup 收费项目快速分组
     * @return 结果
     */
    public int insertHosSfxmGroup(HosSfxmGroup hosSfxmGroup);

    /**
     * 修改收费项目快速分组
     * 
     * @param hosSfxmGroup 收费项目快速分组
     * @return 结果
     */
    public int updateHosSfxmGroup(HosSfxmGroup hosSfxmGroup);

    int batchSaveSfxmGroup(List<HosSfxmGroup> list);

    /**
     * 删除收费项目快速分组
     * 
     * @param idSfxmGroup 收费项目快速分组主键
     * @return 结果
     */
    public int deleteHosSfxmGroupByIdSfxmGroup(Long idSfxmGroup);

    /**
     * 批量删除收费项目快速分组
     * 
     * @param idSfxmGroups 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHosSfxmGroupByIdSfxmGroups(String[] idSfxmGroups);
}
