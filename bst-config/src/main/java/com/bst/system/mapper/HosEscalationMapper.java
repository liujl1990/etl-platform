package com.bst.system.mapper;

import com.bst.system.vo.HosEscalationVO;

import java.util.List;
import java.util.Map;

/**
 * 病种管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-13
 */
public interface HosEscalationMapper
{
    List<Map<String,Object>> selectMxbfz(HosEscalationVO vo);

    List<Map<String,Object>> selectCfcc(HosEscalationVO vo);
}
