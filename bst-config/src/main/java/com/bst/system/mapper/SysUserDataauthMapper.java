package com.bst.system.mapper;

import java.util.List;
import com.bst.system.domain.SysUserDataauth;

/**
 * 用户数据权限Mapper接口
 * 
 * @author ruoyi
 * @date 2023-01-20
 */
public interface SysUserDataauthMapper 
{
    /**
     * 查询用户数据权限
     * 
     * @param dataauthId 用户数据权限主键
     * @return 用户数据权限
     */
    public SysUserDataauth selectSysUserDataauthByDataauthId(Long dataauthId);

    /**
     * 查询用户数据权限列表
     * 
     * @param sysUserDataauth 用户数据权限
     * @return 用户数据权限集合
     */
    public List<SysUserDataauth> selectSysUserDataauthList(SysUserDataauth sysUserDataauth);

    /**
     * 新增用户数据权限
     * 
     * @param sysUserDataauth 用户数据权限
     * @return 结果
     */
    public int insertSysUserDataauth(SysUserDataauth sysUserDataauth);

    /**
     * 修改用户数据权限
     * 
     * @param sysUserDataauth 用户数据权限
     * @return 结果
     */
    public int updateSysUserDataauth(SysUserDataauth sysUserDataauth);

    /**
     * 删除用户数据权限
     * 
     * @param dataauthId 用户数据权限主键
     * @return 结果
     */
    public int deleteSysUserDataauthByDataauthId(Long dataauthId);

    /**
     * 批量删除用户数据权限
     * 
     * @param dataauthIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserDataauthByDataauthIds(String[] dataauthIds);
}
