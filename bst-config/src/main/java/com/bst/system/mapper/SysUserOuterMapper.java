package com.bst.system.mapper;

import java.util.List;
import com.bst.system.domain.SysUserOuter;

/**
 * 接口用户Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
public interface SysUserOuterMapper 
{
    /**
     * 查询接口用户
     * 
     * @param idUserOuter 接口用户主键
     * @return 接口用户
     */
    public SysUserOuter selectSysUserOuterByIdUserOuter(Long idUserOuter);

    /**
     * 查询接口用户列表
     * 
     * @param sysUserOuter 接口用户
     * @return 接口用户集合
     */
    public List<SysUserOuter> selectSysUserOuterList(SysUserOuter sysUserOuter);

    /**
     * 新增接口用户
     * 
     * @param sysUserOuter 接口用户
     * @return 结果
     */
    public int insertSysUserOuter(SysUserOuter sysUserOuter);

    /**
     * 修改接口用户
     * 
     * @param sysUserOuter 接口用户
     * @return 结果
     */
    public int updateSysUserOuter(SysUserOuter sysUserOuter);

    /**
     * 删除接口用户
     * 
     * @param idUserOuter 接口用户主键
     * @return 结果
     */
    public int deleteSysUserOuterByIdUserOuter(Long idUserOuter);

    /**
     * 批量删除接口用户
     * 
     * @param idUserOuters 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserOuterByIdUserOuters(String[] idUserOuters);
}
