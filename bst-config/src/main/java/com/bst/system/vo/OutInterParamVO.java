package com.bst.system.vo;

import java.io.Serializable;
import java.util.Map;

public class OutInterParamVO implements Serializable {

    private Long idInter; //接口编号
    private Map<String,String> param;

    public Long getIdInter() {
        return idInter;
    }

    public void setIdInter(Long idInter) {
        this.idInter = idInter;
    }

    public Map<String, String> getParam() {
        return param;
    }

    public void setParam(Map<String, String> param) {
        this.param = param;
    }
}
