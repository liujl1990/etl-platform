package com.bst.system.vo;

import com.bst.md.domain.MdMedTbFld;

import java.io.Serializable;

public class MdMedTbFldVO implements Serializable {

    private MdMedTbFld mdMedTbFld;
    private String tableName;

    public MdMedTbFld getMdMedTbFld() {
        return mdMedTbFld;
    }

    public void setMdMedTbFld(MdMedTbFld mdMedTbFld) {
        this.mdMedTbFld = mdMedTbFld;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
