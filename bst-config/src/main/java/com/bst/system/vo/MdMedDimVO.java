package com.bst.system.vo;

import java.io.Serializable;

public class MdMedDimVO implements Serializable {
    private String month;//月份

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
