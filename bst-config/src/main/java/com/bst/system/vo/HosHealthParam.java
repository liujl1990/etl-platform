package com.bst.system.vo;

import java.io.Serializable;
import java.util.List;

public class HosHealthParam implements Serializable {

    private List<Integer> euStatusList;
    private Integer euStatus;
    /** 活动主题 */
    private String title;
    private String sdHealthtp;

    public List<Integer> getEuStatusList() {
        return euStatusList;
    }

    public void setEuStatusList(List<Integer> euStatusList) {
        this.euStatusList = euStatusList;
    }

    public Integer getEuStatus() {
        return euStatus;
    }

    public void setEuStatus(Integer euStatus) {
        this.euStatus = euStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSdHealthtp() {
        return sdHealthtp;
    }

    public void setSdHealthtp(String sdHealthtp) {
        this.sdHealthtp = sdHealthtp;
    }
}
