package com.bst.system.vo;

import java.io.Serializable;

public class IndexWallParamVO implements Serializable {

    private String startDate;
    private String name;
    private String sdSys;
    private String idLabel;
    private String dateType;

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSdSys() {
        return sdSys;
    }

    public void setSdSys(String sdSys) {
        this.sdSys = sdSys;
    }

    public String getIdLabel() {
        return idLabel;
    }

    public void setIdLabel(String idLabel) {
        this.idLabel = idLabel;
    }
}
