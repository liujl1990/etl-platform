package com.bst.system.vo;

import java.io.Serializable;
import java.util.List;

public class HjDiseParamVO implements Serializable {
    private List<String> cdDises;
    // private String cdDises;
    private String na;//疾病名称
    private String dBegin; //开始时间
    private String dEnd;  //结束时间

    public List<String> getCdDises() {
        return cdDises;
    }

    public void setCdDises(List<String> cdDises) {
        this.cdDises = cdDises;
    }

    public String getNa() {
        return na;
    }

    public void setNa(String na) {
        this.na = na;
    }

    public String getdBegin() {
        return dBegin;
    }

    public void setdBegin(String dBegin) {
        this.dBegin = dBegin;
    }

    public String getdEnd() {
        return dEnd;
    }

    public void setdEnd(String dEnd) {
        this.dEnd = dEnd;
    }
}
