package com.bst.system.vo;

public class HosJxkhDataVO {
    private Integer sort; //指标序号
    private String idIndex;
    private String naIndex;
    private String score;
    private Object value;
    private String target;
    private String diffDes; //差距
    private Double diff; //差距

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIdIndex() {
        return idIndex;
    }

    public void setIdIndex(String idIndex) {
        this.idIndex = idIndex;
    }

    public String getNaIndex() {
        return naIndex;
    }

    public void setNaIndex(String naIndex) {
        this.naIndex = naIndex;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getDiffDes() {
        return diffDes;
    }

    public void setDiffDes(String diffDes) {
        this.diffDes = diffDes;
    }

    public Double getDiff() {
        return diff;
    }

    public void setDiff(Double diff) {
        this.diff = diff;
    }
}
