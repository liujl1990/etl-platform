package com.bst.system.vo;

import com.bst.md.domain.MdMedTb;
import com.bst.md.domain.MdMedTbFld;

import java.io.Serializable;
import java.util.List;

public class AlterTableParamVO implements Serializable {

    private MdMedTb newTb;
    private List<MdMedTbFld> newtbFldList;
    private String oldTableName;
    private Long idTblog;

    public AlterTableParamVO() {

    }

    public AlterTableParamVO(MdMedTb newTb, List<MdMedTbFld> newtbFldList, String oldTableName) {
        this.newTb = newTb;
        this.newtbFldList = newtbFldList;
        this.oldTableName = oldTableName;
    }

    public Long getIdTblog() {
        return idTblog;
    }

    public void setIdTblog(Long idTblog) {
        this.idTblog = idTblog;
    }

    public MdMedTb getNewTb() {
        return newTb;
    }

    public void setNewTb(MdMedTb newTb) {
        this.newTb = newTb;
    }

    public List<MdMedTbFld> getNewtbFldList() {
        return newtbFldList;
    }

    public void setNewtbFldList(List<MdMedTbFld> newtbFldList) {
        this.newtbFldList = newtbFldList;
    }

    public String getOldTableName() {
        return oldTableName;
    }

    public void setOldTableName(String oldTableName) {
        this.oldTableName = oldTableName;
    }
}
