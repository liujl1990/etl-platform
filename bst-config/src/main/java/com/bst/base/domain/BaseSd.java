package com.bst.base.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 系统字典对象 base_sd
 * 
 * @author ruoyi
 * @date 2022-05-26
 */
public class BaseSd extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编码 */
    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    /** 创建人 */
    @Excel(name = "创建人")
    private String naEmpCre;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysCre;

    /** 修改人 */
    @Excel(name = "修改人")
    private String naEmpModi;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    /** 系统字典明细信息 */
    private List<BaseSdItem> baseSdItemList;

    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    public List<BaseSdItem> getBaseSdItemList()
    {
        return baseSdItemList;
    }

    public void setBaseSdItemList(List<BaseSdItem> baseSdItemList)
    {
        this.baseSdItemList = baseSdItemList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cd", getCd())
            .append("na", getNa())
            .append("des", getDes())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .append("baseSdItemList", getBaseSdItemList())
            .toString();
    }
}
