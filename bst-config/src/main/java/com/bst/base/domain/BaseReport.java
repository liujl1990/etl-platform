package com.bst.base.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 base_report
 * 
 * @author ruoyi
 * @date 2023-05-23
 */
public class BaseReport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long idReport;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naUnit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String na;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String url;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmp;

    private String monthBegin;

    private String monthEnd;

    private String des;

    private String note;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer fgAct;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date dtSysCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpModi;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date dtSysModi;

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getMonthBegin() {
        return monthBegin;
    }

    public void setMonthBegin(String monthBegin) {
        this.monthBegin = monthBegin;
    }

    public String getMonthEnd() {
        return monthEnd;
    }

    public void setMonthEnd(String monthEnd) {
        this.monthEnd = monthEnd;
    }

    public void setIdReport(Long idReport)
    {
        this.idReport = idReport;
    }

    public Long getIdReport() 
    {
        return idReport;
    }
    public void setNaUnit(String naUnit) 
    {
        this.naUnit = naUnit;
    }

    public String getNaUnit() 
    {
        return naUnit;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setNaEmp(String naEmp) 
    {
        this.naEmp = naEmp;
    }

    public String getNaEmp() 
    {
        return naEmp;
    }
    public void setFgAct(Integer fgAct) 
    {
        this.fgAct = fgAct;
    }

    public Integer getFgAct() 
    {
        return fgAct;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idReport", getIdReport())
            .append("naUnit", getNaUnit())
            .append("na", getNa())
            .append("url", getUrl())
            .append("naEmp", getNaEmp())
            .append("fgAct", getFgAct())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
