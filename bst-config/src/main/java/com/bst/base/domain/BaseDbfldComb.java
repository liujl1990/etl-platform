package com.bst.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 数据库字段映射对象 base_dbfld_comb
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public class BaseDbfldComb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Integer idDbfldComb;

    /** 数据库类型 */
    @Excel(name = "数据库类型")
    private String sdDbtp;

    /** 数据库字段类型 */
    @Excel(name = "数据库字段类型")
    private String euDbfldtp;

    /** JAVA字段类型 */
    @Excel(name = "JAVA字段类型")
    private Integer euJavatp;

    /** JAVA字段类型名称 */
    @Excel(name = "JAVA字段类型名称")
    private String naJavatp;

    /** 精度处理标志 */
    @Excel(name = "精度处理标志")
    private Integer fgPrec;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public void setIdDbfldComb(Integer idDbfldComb) 
    {
        this.idDbfldComb = idDbfldComb;
    }

    public Integer getIdDbfldComb() 
    {
        return idDbfldComb;
    }
    public void setSdDbtp(String sdDbtp) 
    {
        this.sdDbtp = sdDbtp;
    }

    public String getSdDbtp() 
    {
        return sdDbtp;
    }
    public void setEuDbfldtp(String euDbfldtp) 
    {
        this.euDbfldtp = euDbfldtp;
    }

    public String getEuDbfldtp() 
    {
        return euDbfldtp;
    }
    public void setEuJavatp(Integer euJavatp) 
    {
        this.euJavatp = euJavatp;
    }

    public Integer getEuJavatp() 
    {
        return euJavatp;
    }
    public void setNaJavatp(String naJavatp) 
    {
        this.naJavatp = naJavatp;
    }

    public String getNaJavatp() 
    {
        return naJavatp;
    }
    public void setFgPrec(Integer fgPrec) 
    {
        this.fgPrec = fgPrec;
    }

    public Integer getFgPrec() 
    {
        return fgPrec;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDbfldComb", getIdDbfldComb())
            .append("sdDbtp", getSdDbtp())
            .append("euDbfldtp", getEuDbfldtp())
            .append("euJavatp", getEuJavatp())
            .append("naJavatp", getNaJavatp())
            .append("fgPrec", getFgPrec())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
