package com.bst.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * DW查询配置对象 base_dw_qry_param
 * 
 * @author laoliu
 * @date 2023-02-12
 */
public class BaseDwQryParam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idDwQryParam;

    /** 分类 */
    @Excel(name = "分类")
    private String sdDwtbClass;

    /** 表名 */
    @Excel(name = "表名")
    private String cdDwTb;

    /** 字段集合 */
    @Excel(name = "字段集合")
    private String fldList;

    /** 字段集合 */
    @Excel(name = "字段集合描述")
    private String fldListDes;

    /** 过滤条件 */
    @Excel(name = "过滤条件")
    private String whereSql;

    /**  */
    @Excel(name = "")
    private String naEmpCre;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public String getFldListDes() {
        return fldListDes;
    }

    public void setFldListDes(String fldListDes) {
        this.fldListDes = fldListDes;
    }

    public void setIdDwQryParam(Long idDwQryParam)
    {
        this.idDwQryParam = idDwQryParam;
    }

    public Long getIdDwQryParam() 
    {
        return idDwQryParam;
    }
    public void setSdDwtbClass(String sdDwtbClass) 
    {
        this.sdDwtbClass = sdDwtbClass;
    }

    public String getSdDwtbClass() 
    {
        return sdDwtbClass;
    }
    public void setCdDwTb(String cdDwTb) 
    {
        this.cdDwTb = cdDwTb;
    }

    public String getCdDwTb() 
    {
        return cdDwTb;
    }
    public void setFldList(String fldList) 
    {
        this.fldList = fldList;
    }

    public String getFldList() 
    {
        return fldList;
    }
    public void setWhereSql(String whereSql) 
    {
        this.whereSql = whereSql;
    }

    public String getWhereSql() 
    {
        return whereSql;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDwQryParam", getIdDwQryParam())
            .append("sdDwtbClass", getSdDwtbClass())
            .append("cdDwTb", getCdDwTb())
            .append("fldList", getFldList())
            .append("whereSql", getWhereSql())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
