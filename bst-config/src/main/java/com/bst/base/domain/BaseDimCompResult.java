package com.bst.base.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 维度对照结果对象 base_dim_comp_result
 * 
 * @author laoliu
 * @date 2023-02-12
 */
public class BaseDimCompResult extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idDimCompResult;

    private String dimTbSource; //源目标表

    private String idPubfldTar;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    /** 源编码 */
    @Excel(name = "源编码")
    private String cdSource;

    /** 源名称 */
    @Excel(name = "源名称")
    private String naSource;

    /** 目标编码 */
    @Excel(name = "目标编码")
    private String cdTar;

    /** 目标名称 */
    @Excel(name = "目标名称")
    private String naTar;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    public String getIdPubfldTar() {
        return idPubfldTar;
    }

    public void setIdPubfldTar(String idPubfldTar) {
        this.idPubfldTar = idPubfldTar;
    }

    public String getDimTbSource() {
        return dimTbSource;
    }

    public void setDimTbSource(String dimTbSource) {
        this.dimTbSource = dimTbSource;
    }

    public void setIdDimCompResult(Long idDimCompResult)
    {
        this.idDimCompResult = idDimCompResult;
    }

    public Long getIdDimCompResult() 
    {
        return idDimCompResult;
    }

    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setCdSource(String cdSource) 
    {
        this.cdSource = cdSource;
    }

    public String getCdSource() 
    {
        return cdSource;
    }
    public void setNaSource(String naSource) 
    {
        this.naSource = naSource;
    }

    public String getNaSource() 
    {
        return naSource;
    }
    public void setCdTar(String cdTar) 
    {
        this.cdTar = cdTar;
    }

    public String getCdTar() 
    {
        return cdTar;
    }
    public void setNaTar(String naTar) 
    {
        this.naTar = naTar;
    }

    public String getNaTar() 
    {
        return naTar;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDimCompResult", getIdDimCompResult())
            .append("des", getDes())
            .append("cdSource", getCdSource())
            .append("naSource", getNaSource())
            .append("cdTar", getCdTar())
            .append("naTar", getNaTar())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
