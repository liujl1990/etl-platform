package com.bst.base.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 维度对照对象 base_dim_comp
 * 
 * @author laoliu
 * @date 2023-02-12
 */
public class BaseDimComp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idDimComp;

    /** 编码 */
    @Excel(name = "编码")
    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 源字段 */
    @Excel(name = "源字段")
    private String idPubfldSource;

    private String dimTbSource;

    /** 目标字段 */
    @Excel(name = "目标字段")
    private String idPubfldTar;

    /** 校验类别 */
    @Excel(name = "校验类别")
    private String sdCombCheckStyle;

    /** 对照分类 */
    @Excel(name = "对照分类")
    private String sdCompClass;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    public String getDimTbSource() {
        return dimTbSource;
    }

    public void setDimTbSource(String dimTbSource) {
        this.dimTbSource = dimTbSource;
    }

    public void setIdDimComp(Long idDimComp)
    {
        this.idDimComp = idDimComp;
    }

    public Long getIdDimComp() 
    {
        return idDimComp;
    }
    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setIdPubfldSource(String idPubfldSource) 
    {
        this.idPubfldSource = idPubfldSource;
    }

    public String getIdPubfldSource() 
    {
        return idPubfldSource;
    }
    public void setIdPubfldTar(String idPubfldTar) 
    {
        this.idPubfldTar = idPubfldTar;
    }

    public String getIdPubfldTar() 
    {
        return idPubfldTar;
    }
    public void setSdCombCheckStyle(String sdCombCheckStyle) 
    {
        this.sdCombCheckStyle = sdCombCheckStyle;
    }

    public String getSdCombCheckStyle() 
    {
        return sdCombCheckStyle;
    }
    public void setSdCompClass(String sdCompClass) 
    {
        this.sdCompClass = sdCompClass;
    }

    public String getSdCompClass() 
    {
        return sdCompClass;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idDimComp", getIdDimComp())
            .append("cd", getCd())
            .append("na", getNa())
            .append("idPubfldSource", getIdPubfldSource())
            .append("idPubfldTar", getIdPubfldTar())
            .append("sdCombCheckStyle", getSdCombCheckStyle())
            .append("sdCompClass", getSdCompClass())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .append("des", getDes())
            .toString();
    }
}
