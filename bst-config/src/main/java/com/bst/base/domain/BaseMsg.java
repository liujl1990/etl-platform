package com.bst.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 消息中心对象 base_msg
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public class BaseMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idMsg;

    /** 编码 */
    @Excel(name = "编码")
    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 提醒级别 */
    @Excel(name = "提醒级别")
    private String euLevel;

    /** 内容 */
    @Excel(name = "内容")
    private String cont;

    /** 处理标志 */
    @Excel(name = "处理标志")
    private Integer fgHand;

    /** 处理时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "处理时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtHand;

    /** 处理人 */
    @Excel(name = "处理人")
    private String naEmpHand;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    public BaseMsg(){}

    public BaseMsg(String cd,String msg) {
        this.cd = cd;
        this.cont = msg;
    }

    public BaseMsg(String cd,String msg,String des) {
        this.cd = cd;
        this.cont = msg;
        this.des = des;
    }

    public void setIdMsg(Long idMsg) 
    {
        this.idMsg = idMsg;
    }

    public Long getIdMsg() 
    {
        return idMsg;
    }
    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setEuLevel(String euLevel) 
    {
        this.euLevel = euLevel;
    }

    public String getEuLevel() 
    {
        return euLevel;
    }
    public void setCont(String cont) 
    {
        this.cont = cont;
    }

    public String getCont() 
    {
        return cont;
    }
    public void setFgHand(Integer fgHand) 
    {
        this.fgHand = fgHand;
    }

    public Integer getFgHand() 
    {
        return fgHand;
    }
    public void setDtHand(Date dtHand) 
    {
        this.dtHand = dtHand;
    }

    public Date getDtHand() 
    {
        return dtHand;
    }
    public void setNaEmpHand(String naEmpHand) 
    {
        this.naEmpHand = naEmpHand;
    }

    public String getNaEmpHand() 
    {
        return naEmpHand;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idMsg", getIdMsg())
            .append("cd", getCd())
            .append("na", getNa())
            .append("euLevel", getEuLevel())
            .append("cont", getCont())
            .append("fgHand", getFgHand())
            .append("dtHand", getDtHand())
            .append("naEmpHand", getNaEmpHand())
            .append("des", getDes())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
