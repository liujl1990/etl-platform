package com.bst.base.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.bst.common.core.domain.Ztree;
import com.bst.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.base.mapper.BaseLabelMapper;
import com.bst.system.domain.BaseLabel;
import com.bst.base.service.IBaseLabelService;
import com.bst.common.core.text.Convert;

/**
 * 指标标签Service业务层处理
 * 
 * @author 老刘
 * @date 2023-02-18
 */
@Service
public class BaseLabelServiceImpl implements IBaseLabelService
{
    @Autowired
    private BaseLabelMapper baseLabelMapper;

    /**
     * 查询指标标签
     * 
     * @param idLabel 指标标签主键
     * @return 指标标签
     */
    @Override
    public BaseLabel selectBaseLabelByIdLabel(Long idLabel)
    {
        return baseLabelMapper.selectBaseLabelByIdLabel(idLabel);
    }

    /**
     * 查询指标标签列表
     * 
     * @param mdIndexLabel 指标标签
     * @return 指标标签
     */
    @Override
    public List<BaseLabel> selectBaseLabelList(BaseLabel mdIndexLabel)
    {
        return baseLabelMapper.selectBaseLabelList(mdIndexLabel);
    }

    /**
     * 新增指标标签
     * 
     * @param mdIndexLabel 指标标签
     * @return 结果
     */
    @Override
    public int insertBaseLabel(BaseLabel mdIndexLabel)
    {
        mdIndexLabel.setFgSys(0);
        return baseLabelMapper.insertBaseLabel(mdIndexLabel);
    }

    /**
     * 修改指标标签
     * 
     * @param mdIndexLabel 指标标签
     * @return 结果
     */
    @Override
    public int updateBaseLabel(BaseLabel mdIndexLabel)
    {
        return baseLabelMapper.updateBaseLabel(mdIndexLabel);
    }

    /**
     * 批量删除指标标签
     * 
     * @param idLabels 需要删除的指标标签主键
     * @return 结果
     */
    @Override
    public int deleteBaseLabelByIdLabels(String idLabels)
    {
        return baseLabelMapper.deleteBaseLabelByIdLabels(Convert.toStrArray(idLabels));
    }

    /**
     * 删除指标标签信息
     * 
     * @param idLabel 指标标签主键
     * @return 结果
     */
    @Override
    public int deleteBaseLabelByIdLabel(Long idLabel)
    {
        return baseLabelMapper.deleteBaseLabelByIdLabel(idLabel);
    }

    @Override
    public List<Ztree> selectLabelTree(BaseLabel mdIndexLabel) {
        List<BaseLabel> deptList = baseLabelMapper.selectBaseLabelList(mdIndexLabel);
        List<Ztree> ztrees = initZtree(deptList,null);
        return ztrees;
    }

    @Override
    public List<BaseLabel> findByIdIndex(String idIndex) {
        return baseLabelMapper.findByIdIndex(idIndex);
    }

    private List<Ztree> initZtree(List<BaseLabel> deptList, List<String> roleDeptList)
    {

        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(roleDeptList);
        for (BaseLabel label : deptList)
        {
            if (label.getFgAct()==1)
            {
                Ztree ztree = new Ztree();
                ztree.setId(label.getIdLabel());
                ztree.setpId(label.getParentId());
                ztree.setName(label.getNa());
                ztree.setTitle(label.getNa());
                if (isCheck)
                {
                    ztree.setChecked(roleDeptList.contains(label.getIdLabel() + label.getNa()));
                }
                ztrees.add(ztree);
            }
        }
        return ztrees;
    }
}
