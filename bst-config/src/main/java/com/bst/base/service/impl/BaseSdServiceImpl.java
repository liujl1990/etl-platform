package com.bst.base.service.impl;

import java.util.Date;
import java.util.List;

import com.bst.common.utils.self.LoginAPIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.bst.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.bst.base.domain.BaseSdItem;
import com.bst.base.mapper.BaseSdMapper;
import com.bst.base.domain.BaseSd;
import com.bst.base.service.IBaseSdService;
import com.bst.common.core.text.Convert;

/**
 * 系统字典Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-26
 */
@Service
public class BaseSdServiceImpl implements IBaseSdService 
{
    @Autowired
    private BaseSdMapper baseSdMapper;

    /**
     * 查询系统字典
     * 
     * @param cd 系统字典主键
     * @return 系统字典
     */
    @Override
    public BaseSd selectBaseSdByCd(String cd)
    {
        return baseSdMapper.selectBaseSdByCd(cd);
    }

    /**
     * 查询系统字典列表
     * 
     * @param baseSd 系统字典
     * @return 系统字典
     */
    @Override
    public List<BaseSd> selectBaseSdList(BaseSd baseSd)
    {
        return baseSdMapper.selectBaseSdList(baseSd);
    }

    /**
     * 新增系统字典
     * 
     * @param baseSd 系统字典
     * @return 结果
     */
    @Transactional
    @Override
    public int insertBaseSd(BaseSd baseSd)
    {
        baseSd.setDtSysCre(new Date());
        baseSd.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        int rows = baseSdMapper.insertBaseSd(baseSd);
        insertBaseSdItem(baseSd);
        return rows;
    }

    /**
     * 修改系统字典
     * 
     * @param baseSd 系统字典
     * @return 结果
     */
    @Transactional
    @Override
    public int updateBaseSd(BaseSd baseSd)
    {
        baseSd.setDtSysModi(new Date());
        baseSd.setNaEmpModi(LoginAPIUtils.getLoginUsename());
        baseSdMapper.deleteBaseSdItemByCdSd(baseSd.getCd());
        insertBaseSdItem(baseSd);
        return baseSdMapper.updateBaseSd(baseSd);
    }

    /**
     * 批量删除系统字典
     * 
     * @param cds 需要删除的系统字典主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteBaseSdByCds(String cds)
    {
        baseSdMapper.deleteBaseSdItemByCdSds(Convert.toStrArray(cds));
        return baseSdMapper.deleteBaseSdByCds(Convert.toStrArray(cds));
    }

    /**
     * 删除系统字典信息
     * 
     * @param cd 系统字典主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteBaseSdByCd(String cd)
    {
        baseSdMapper.deleteBaseSdItemByCdSd(cd);
        return baseSdMapper.deleteBaseSdByCd(cd);
    }

    /**
     * 新增系统字典明细信息
     * 
     * @param baseSd 系统字典对象
     */
    public void insertBaseSdItem(BaseSd baseSd)
    {
        List<BaseSdItem> baseSdItemList = baseSd.getBaseSdItemList();
        String cd = baseSd.getCd();
        if (StringUtils.isNotNull(baseSdItemList))
        {
            List<BaseSdItem> list = new ArrayList<BaseSdItem>();
            for (BaseSdItem baseSdItem : baseSdItemList)
            {
                baseSdItem.setCdSd(cd);
                list.add(baseSdItem);
            }
            if (list.size() > 0)
            {
                baseSdMapper.batchBaseSdItem(list);
            }
        }
    }
}
