package com.bst.base.service;

import java.util.List;
import com.bst.base.domain.BaseMsg;

/**
 * 消息中心Service接口
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public interface IBaseMsgService 
{
    /**
     * 查询消息中心
     * 
     * @param idMsg 消息中心主键
     * @return 消息中心
     */
    public BaseMsg selectBaseMsgByIdMsg(Long idMsg);

    /**
     * 查询消息中心列表
     * 
     * @param baseMsg 消息中心
     * @return 消息中心集合
     */
    public List<BaseMsg> selectBaseMsgList(BaseMsg baseMsg);

    /**
     * 新增消息中心
     * 
     * @param baseMsg 消息中心
     * @return 结果
     */
    public int insertBaseMsg(BaseMsg baseMsg);

    /**
     * 修改消息中心
     * 
     * @param baseMsg 消息中心
     * @return 结果
     */
    public int updateBaseMsg(BaseMsg baseMsg);

    /**
     * 批量删除消息中心
     * 
     * @param idMsgs 需要删除的消息中心主键集合
     * @return 结果
     */
    public int deleteBaseMsgByIdMsgs(String idMsgs);

    /**
     * 删除消息中心信息
     * 
     * @param idMsg 消息中心主键
     * @return 结果
     */
    public int deleteBaseMsgByIdMsg(Long idMsg);

    int insert(String cd,String msg,String des);

    int insertList(List<BaseMsg> list);
}
