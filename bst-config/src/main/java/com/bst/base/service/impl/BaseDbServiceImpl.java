package com.bst.base.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Date;
import java.util.List;

import com.alibaba.druid.pool.DruidDataSource;
import com.bst.common.constant.JobConstant;
import com.bst.system.framework.utils.DBUtil;
import com.bst.common.utils.self.LoginAPIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.bst.base.mapper.BaseDbMapper;
import com.bst.base.domain.BaseDb;
import com.bst.base.service.IBaseDbService;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-17
 */
@Service("baseDbService")
public class BaseDbServiceImpl implements IBaseDbService 
{
    @Autowired
    private BaseDbMapper baseDbMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idDb 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public BaseDb selectBaseDbById(Integer idDb)
    {
        return baseDbMapper.selectBaseDbById(idDb);
    }

    @Override
    public List<BaseDb> selectAll() {
        return baseDbMapper.selectBaseDbList(new BaseDb());
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param baseDb 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<BaseDb> selectBaseDbList(BaseDb baseDb)
    {
        return baseDbMapper.selectBaseDbList(baseDb);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param baseDb 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertBaseDb(BaseDb baseDb)
    {
        baseDb.setFgAct(1);
        baseDb.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        baseDb.setDtSysCre(new Date());
        return baseDbMapper.insertBaseDb(baseDb);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param baseDb 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateBaseDb(BaseDb baseDb)
    {
        baseDb.setNaEmpModi(LoginAPIUtils.getLoginUsename());
        baseDb.setDtSysModi(new Date());
        return baseDbMapper.updateBaseDb(baseDb);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDbs 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBaseDbByIdDbs(String idDbs)
    {
        return baseDbMapper.deleteBaseDbByIdDbs(Convert.toStrArray(idDbs));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDb 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBaseDbByIdDb(Integer idDb)
    {
        return baseDbMapper.deleteBaseDbByIdDb(idDb);
    }

    @Override
    public String testDB(Integer idDb) {
        BaseDb baseDb = this.selectBaseDbById(idDb);
        baseDb.setDriver(getDBDriverClass(baseDb.getSdDbtp()));
        baseDb.setIdDb(-1);
        String msg = null;
        Connection conn = null;
        ResultSet rs = null;
        DruidDataSource datasource = null;
        try {
            datasource = DBUtil.createDataSource(baseDb);
            datasource.setResetStatEnable(false);
            datasource.setMaxWait(1000);
            conn = datasource.getConnection();
            rs = DBUtil.query(conn, getTestSQL(baseDb.getSdDbtp()), 1000, 10000000);
            ResultSetMetaData metaData = rs.getMetaData();
            rs.next();
            byte[] bytes = rs.getBytes(1);
            String s1 = new String(bytes, baseDb.getChara());
            msg = "成功！ 正确显示这句话，则字符集正确：" + s1;
        } catch (Exception e) {
            msg = "失败！具体错误信息为：" + e.getLocalizedMessage();
        } finally {
            DBUtil.closeDBResources(rs, null, conn);
            DBUtil.closeDataSource(datasource);
        }
        return msg;
    }

    public String getTestSQL(String euTp) {
        if (JobConstant.DB_TYPE_MYSQL.equals(euTp)) return "select '字符集编码正确' val";
        else if (JobConstant.DB_TYPE_ORACLE.equals(euTp)) return "select '字符集编码正确' val from dual";
        else if (JobConstant.DB_TYPE_SQLSERVER.equals(euTp)) return "select '字符集编码正确' val";
        else if (JobConstant.DB_TYPE_POSTGREY.equals(euTp)) return "select '字符集编码正确' val";
        else return null;
    }

    @Value("${oracle.datasource.driver-class-name}")
    private String oracleDriver;
    @Value("${sqlserver.datasource.driver-class-name}")
    private String sqlserverDriver;
    @Value("${mysql.datasource.driver-class-name}")
    private String mysqlDriver;
    @Value("${postgresql.datasource.driver-class-name}")
    private String postgresqlDriver;

    public String getDBDriverClass(String euTp) {
        if (JobConstant.DB_TYPE_MYSQL.equals(euTp)) return mysqlDriver;
        else if (JobConstant.DB_TYPE_ORACLE.equals(euTp)) return oracleDriver;
        else if (JobConstant.DB_TYPE_SQLSERVER.equals(euTp)) return sqlserverDriver;
        else if (JobConstant.DB_TYPE_POSTGREY.equals(euTp)) return postgresqlDriver;
        else return null;
    }
}
