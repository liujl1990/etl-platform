package com.bst.base.service.impl;

import java.util.Arrays;
import java.util.List;

import com.bst.md.domain.MdMedPubfld;
import com.bst.md.service.IMdMedPubfldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.base.mapper.BaseDwQryParamMapper;
import com.bst.base.domain.BaseDwQryParam;
import com.bst.base.service.IBaseDwQryParamService;
import com.bst.common.core.text.Convert;

/**
 * DW查询配置Service业务层处理
 * 
 * @author laoliu
 * @date 2023-02-12
 */
@Service
public class BaseDwQryParamServiceImpl implements IBaseDwQryParamService 
{
    @Autowired
    private BaseDwQryParamMapper baseDwQryParamMapper;
    @Autowired
    IMdMedPubfldService pubfldService;

    /**
     * 查询DW查询配置
     * 
     * @param idDwQryParam DW查询配置主键
     * @return DW查询配置
     */
    @Override
    public BaseDwQryParam selectBaseDwQryParamByIdDwQryParam(Long idDwQryParam)
    {
        return baseDwQryParamMapper.selectBaseDwQryParamByIdDwQryParam(idDwQryParam);
    }

    /**
     * 查询DW查询配置列表
     * 
     * @param baseDwQryParam DW查询配置
     * @return DW查询配置
     */
    @Override
    public List<BaseDwQryParam> selectBaseDwQryParamList(BaseDwQryParam baseDwQryParam)
    {
        return baseDwQryParamMapper.selectBaseDwQryParamList(baseDwQryParam);
    }

    /**
     * 新增DW查询配置
     * 
     * @param baseDwQryParam DW查询配置
     * @return 结果
     */
    @Override
    public int insertBaseDwQryParam(BaseDwQryParam baseDwQryParam)
    {
        baseDwQryParam.setFldListDes(getFldListDes(baseDwQryParam.getFldList()));
        return baseDwQryParamMapper.insertBaseDwQryParam(baseDwQryParam);
    }

    /**
     * 修改DW查询配置
     * 
     * @param baseDwQryParam DW查询配置
     * @return 结果
     */
    @Override
    public int updateBaseDwQryParam(BaseDwQryParam baseDwQryParam)
    {
        baseDwQryParam.setFldListDes(getFldListDes(baseDwQryParam.getFldList()));
        return baseDwQryParamMapper.updateBaseDwQryParam(baseDwQryParam);
    }

    private String getFldListDes(String cds) {
        List<String> idPubflds = Arrays.asList(cds.split(","));
        List<MdMedPubfld> list = pubfldService.findByIdList(idPubflds);
        String des="";
        for (MdMedPubfld fld : list) {
            des += fld.getNa()+",";
        }
        return des.length()>0?des.substring(0,des.length()-1):des;
    }

    /**
     * 批量删除DW查询配置
     * 
     * @param idDwQryParams 需要删除的DW查询配置主键
     * @return 结果
     */
    @Override
    public int deleteBaseDwQryParamByIdDwQryParams(String idDwQryParams)
    {
        return baseDwQryParamMapper.deleteBaseDwQryParamByIdDwQryParams(Convert.toStrArray(idDwQryParams));
    }

    /**
     * 删除DW查询配置信息
     * 
     * @param idDwQryParam DW查询配置主键
     * @return 结果
     */
    @Override
    public int deleteBaseDwQryParamByIdDwQryParam(Long idDwQryParam)
    {
        return baseDwQryParamMapper.deleteBaseDwQryParamByIdDwQryParam(idDwQryParam);
    }
}
