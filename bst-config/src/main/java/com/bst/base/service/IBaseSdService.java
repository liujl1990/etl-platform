package com.bst.base.service;

import java.util.List;
import com.bst.base.domain.BaseSd;

/**
 * 系统字典Service接口
 * 
 * @author ruoyi
 * @date 2022-05-26
 */
public interface IBaseSdService 
{
    /**
     * 查询系统字典
     * 
     * @param cd 系统字典主键
     * @return 系统字典
     */
    public BaseSd selectBaseSdByCd(String cd);

    /**
     * 查询系统字典列表
     * 
     * @param baseSd 系统字典
     * @return 系统字典集合
     */
    public List<BaseSd> selectBaseSdList(BaseSd baseSd);

    /**
     * 新增系统字典
     * 
     * @param baseSd 系统字典
     * @return 结果
     */
    public int insertBaseSd(BaseSd baseSd);

    /**
     * 修改系统字典
     * 
     * @param baseSd 系统字典
     * @return 结果
     */
    public int updateBaseSd(BaseSd baseSd);

    /**
     * 批量删除系统字典
     * 
     * @param cds 需要删除的系统字典主键集合
     * @return 结果
     */
    public int deleteBaseSdByCds(String cds);

    /**
     * 删除系统字典信息
     * 
     * @param cd 系统字典主键
     * @return 结果
     */
    public int deleteBaseSdByCd(String cd);
}
