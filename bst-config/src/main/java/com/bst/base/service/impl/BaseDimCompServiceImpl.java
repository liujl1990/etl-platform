package com.bst.base.service.impl;

import java.util.Date;
import java.util.List;

import com.bst.common.utils.self.LoginAPIUtils;
import com.bst.common.utils.uuid.IdUtils;
import com.bst.md.domain.MdMedPubfld;
import com.bst.md.service.IMdMedPubfldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.base.mapper.BaseDimCompMapper;
import com.bst.base.domain.BaseDimComp;
import com.bst.base.service.IBaseDimCompService;
import com.bst.common.core.text.Convert;

/**
 * 维度对照Service业务层处理
 * 
 * @author laoliu
 * @date 2023-02-12
 */
@Service
public class BaseDimCompServiceImpl implements IBaseDimCompService 
{
    @Autowired
    private BaseDimCompMapper baseDimCompMapper;

    /**
     * 查询维度对照
     * 
     * @param idDimComp 维度对照主键
     * @return 维度对照
     */
    @Override
    public BaseDimComp selectBaseDimCompByIdDimComp(Long idDimComp)
    {
        return baseDimCompMapper.selectBaseDimCompByIdDimComp(idDimComp);
    }

    @Override
    public BaseDimComp selectBaseDimCompByCd(String cd) {
        return baseDimCompMapper.selectBaseDimCompByCd(cd);
    }

    /**
     * 查询维度对照列表
     * 
     * @param baseDimComp 维度对照
     * @return 维度对照
     */
    @Override
    public List<BaseDimComp> selectBaseDimCompList(BaseDimComp baseDimComp)
    {
        return baseDimCompMapper.selectBaseDimCompList(baseDimComp);
    }

    /**
     * 新增维度对照
     * 
     * @param baseDimComp 维度对照
     * @return 结果
     */
    @Override
    public int insertBaseDimComp(BaseDimComp baseDimComp)
    {
        baseDimComp.setDtSysCre(new Date());
        baseDimComp.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        baseDimComp.setCd(IdUtils.fastSimpleUUID());
        getDimTbSource(baseDimComp);
        return baseDimCompMapper.insertBaseDimComp(baseDimComp);
    }

    @Autowired
    IMdMedPubfldService mdMedPubfldService;

    private void getDimTbSource(BaseDimComp baseDimComp) {
        MdMedPubfld pubfld = mdMedPubfldService.selectMdMedPubfldByIdPubfld(baseDimComp.getIdPubfldSource());
        if(pubfld!=null) {
            baseDimComp.setDimTbSource(pubfld.getCdTbDim());
        }
    }

    /**
     * 修改维度对照
     * 
     * @param baseDimComp 维度对照
     * @return 结果
     */
    @Override
    public int updateBaseDimComp(BaseDimComp baseDimComp)
    {
        getDimTbSource(baseDimComp);
        return baseDimCompMapper.updateBaseDimComp(baseDimComp);
    }

    /**
     * 批量删除维度对照
     * 
     * @param idDimComps 需要删除的维度对照主键
     * @return 结果
     */
    @Override
    public int deleteBaseDimCompByIdDimComps(String idDimComps)
    {
        return baseDimCompMapper.deleteBaseDimCompByIdDimComps(Convert.toStrArray(idDimComps));
    }

    /**
     * 删除维度对照信息
     * 
     * @param idDimComp 维度对照主键
     * @return 结果
     */
    @Override
    public int deleteBaseDimCompByIdDimComp(Long idDimComp)
    {
        return baseDimCompMapper.deleteBaseDimCompByIdDimComp(idDimComp);
    }
}
