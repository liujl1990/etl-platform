package com.bst.base.service;

import java.util.List;
import com.bst.base.domain.BaseDb;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2022-05-17
 */
public interface IBaseDbService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idDb 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BaseDb selectBaseDbById(Integer idDb);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param baseDb 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BaseDb> selectBaseDbList(BaseDb baseDb);

    /**
     * 新增【请填写功能名称】
     * 
     * @param baseDb 【请填写功能名称】
     * @return 结果
     */
    public int insertBaseDb(BaseDb baseDb);

    /**
     * 修改【请填写功能名称】
     * 
     * @param baseDb 【请填写功能名称】
     * @return 结果
     */
    public int updateBaseDb(BaseDb baseDb);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDbs 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteBaseDbByIdDbs(String idDbs);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idDb 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBaseDbByIdDb(Integer idDb);

    List<BaseDb> selectAll();

    public String testDB(Integer idDb);
}
