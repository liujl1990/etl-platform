package com.bst.base.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.base.mapper.BaseReportMapper;
import com.bst.base.domain.BaseReport;
import com.bst.base.service.IBaseReportService;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-23
 */
@Service
public class BaseReportServiceImpl implements IBaseReportService 
{
    @Autowired
    private BaseReportMapper baseReportMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idReport 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public BaseReport selectBaseReportByIdReport(Long idReport)
    {
        return baseReportMapper.selectBaseReportByIdReport(idReport);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param baseReport 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<BaseReport> selectBaseReportList(BaseReport baseReport)
    {
        return baseReportMapper.selectBaseReportList(baseReport);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param baseReport 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertBaseReport(BaseReport baseReport)
    {
        return baseReportMapper.insertBaseReport(baseReport);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param baseReport 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateBaseReport(BaseReport baseReport)
    {
        return baseReportMapper.updateBaseReport(baseReport);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idReports 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBaseReportByIdReports(String idReports)
    {
        return baseReportMapper.deleteBaseReportByIdReports(Convert.toStrArray(idReports));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idReport 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBaseReportByIdReport(Long idReport)
    {
        return baseReportMapper.deleteBaseReportByIdReport(idReport);
    }
}
