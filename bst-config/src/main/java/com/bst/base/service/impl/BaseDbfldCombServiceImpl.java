package com.bst.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.base.mapper.BaseDbfldCombMapper;
import com.bst.base.domain.BaseDbfldComb;
import com.bst.base.service.IBaseDbfldCombService;
import com.bst.common.core.text.Convert;

/**
 * 数据库字段映射Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Service
public class BaseDbfldCombServiceImpl implements IBaseDbfldCombService 
{
    @Autowired
    private BaseDbfldCombMapper baseDbfldCombMapper;

    /**
     * 查询数据库字段映射
     * 
     * @param idDbfldComb 数据库字段映射主键
     * @return 数据库字段映射
     */
    @Override
    public BaseDbfldComb selectBaseDbfldCombByIdDbfldComb(Integer idDbfldComb)
    {
        return baseDbfldCombMapper.selectBaseDbfldCombByIdDbfldComb(idDbfldComb);
    }

    /**
     * 查询数据库字段映射列表
     * 
     * @param baseDbfldComb 数据库字段映射
     * @return 数据库字段映射
     */
    @Override
    public List<BaseDbfldComb> selectBaseDbfldCombList(BaseDbfldComb baseDbfldComb)
    {
        return baseDbfldCombMapper.selectBaseDbfldCombList(baseDbfldComb);
    }

    /**
     * 新增数据库字段映射
     * 
     * @param baseDbfldComb 数据库字段映射
     * @return 结果
     */
    @Override
    public int insertBaseDbfldComb(BaseDbfldComb baseDbfldComb)
    {
        return baseDbfldCombMapper.insertBaseDbfldComb(baseDbfldComb);
    }

    /**
     * 修改数据库字段映射
     * 
     * @param baseDbfldComb 数据库字段映射
     * @return 结果
     */
    @Override
    public int updateBaseDbfldComb(BaseDbfldComb baseDbfldComb)
    {
        return baseDbfldCombMapper.updateBaseDbfldComb(baseDbfldComb);
    }

    /**
     * 批量删除数据库字段映射
     * 
     * @param idDbfldCombs 需要删除的数据库字段映射主键
     * @return 结果
     */
    @Override
    public int deleteBaseDbfldCombByIdDbfldCombs(String idDbfldCombs)
    {
        return baseDbfldCombMapper.deleteBaseDbfldCombByIdDbfldCombs(Convert.toStrArray(idDbfldCombs));
    }

    /**
     * 删除数据库字段映射信息
     * 
     * @param idDbfldComb 数据库字段映射主键
     * @return 结果
     */
    @Override
    public int deleteBaseDbfldCombByIdDbfldComb(Integer idDbfldComb)
    {
        return baseDbfldCombMapper.deleteBaseDbfldCombByIdDbfldComb(idDbfldComb);
    }

    @Override
    public Map<Integer, BaseDbfldComb> findByDbTypeMap(String dbType) {
        BaseDbfldComb comb = new BaseDbfldComb();
        comb.setSdDbtp(dbType);
        List<BaseDbfldComb> combList = this.selectBaseDbfldCombList(comb);
        Map<Integer,BaseDbfldComb> map = new HashMap<>();
        for(BaseDbfldComb comb1:combList) {
            map.put(comb1.getEuJavatp(),comb1);
        }
        return map;
    }
}
