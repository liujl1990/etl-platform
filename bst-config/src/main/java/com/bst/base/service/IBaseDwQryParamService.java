package com.bst.base.service;

import java.util.List;
import com.bst.base.domain.BaseDwQryParam;

/**
 * DW查询配置Service接口
 * 
 * @author laoliu
 * @date 2023-02-12
 */
public interface IBaseDwQryParamService 
{
    /**
     * 查询DW查询配置
     * 
     * @param idDwQryParam DW查询配置主键
     * @return DW查询配置
     */
    public BaseDwQryParam selectBaseDwQryParamByIdDwQryParam(Long idDwQryParam);

    /**
     * 查询DW查询配置列表
     * 
     * @param baseDwQryParam DW查询配置
     * @return DW查询配置集合
     */
    public List<BaseDwQryParam> selectBaseDwQryParamList(BaseDwQryParam baseDwQryParam);

    /**
     * 新增DW查询配置
     * 
     * @param baseDwQryParam DW查询配置
     * @return 结果
     */
    public int insertBaseDwQryParam(BaseDwQryParam baseDwQryParam);

    /**
     * 修改DW查询配置
     * 
     * @param baseDwQryParam DW查询配置
     * @return 结果
     */
    public int updateBaseDwQryParam(BaseDwQryParam baseDwQryParam);

    /**
     * 批量删除DW查询配置
     * 
     * @param idDwQryParams 需要删除的DW查询配置主键集合
     * @return 结果
     */
    public int deleteBaseDwQryParamByIdDwQryParams(String idDwQryParams);

    /**
     * 删除DW查询配置信息
     * 
     * @param idDwQryParam DW查询配置主键
     * @return 结果
     */
    public int deleteBaseDwQryParamByIdDwQryParam(Long idDwQryParam);
}
