package com.bst.base.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.base.mapper.BaseDimCompResultMapper;
import com.bst.base.domain.BaseDimCompResult;
import com.bst.base.service.IBaseDimCompResultService;
import com.bst.common.core.text.Convert;

/**
 * 维度对照结果Service业务层处理
 * 
 * @author laoliu
 * @date 2023-02-12
 */
@Service
public class BaseDimCompResultServiceImpl implements IBaseDimCompResultService 
{
    @Autowired
    private BaseDimCompResultMapper baseDimCompResultMapper;

    /**
     * 查询维度对照结果
     * 
     * @param idDimCompResult 维度对照结果主键
     * @return 维度对照结果
     */
    @Override
    public BaseDimCompResult selectBaseDimCompResultByIdDimCompResult(Long idDimCompResult)
    {
        return baseDimCompResultMapper.selectBaseDimCompResultByIdDimCompResult(idDimCompResult);
    }

    /**
     * 查询维度对照结果列表
     * 
     * @param baseDimCompResult 维度对照结果
     * @return 维度对照结果
     */
    @Override
    public List<BaseDimCompResult> selectBaseDimCompResultList(BaseDimCompResult baseDimCompResult)
    {
        return baseDimCompResultMapper.selectBaseDimCompResultList(baseDimCompResult);
    }

    /**
     * 新增维度对照结果
     * 
     * @param baseDimCompResult 维度对照结果
     * @return 结果
     */
    @Override
    public int insertBaseDimCompResult(BaseDimCompResult baseDimCompResult)
    {
        return baseDimCompResultMapper.insertBaseDimCompResult(baseDimCompResult);
    }

    /**
     * 修改维度对照结果
     * 
     * @param baseDimCompResult 维度对照结果
     * @return 结果
     */
    @Override
    public int updateBaseDimCompResult(BaseDimCompResult baseDimCompResult)
    {
        return baseDimCompResultMapper.updateBaseDimCompResult(baseDimCompResult);
    }

    /**
     * 批量删除维度对照结果
     * 
     * @param idDimCompResults 需要删除的维度对照结果主键
     * @return 结果
     */
    @Override
    public int deleteBaseDimCompResultByIdDimCompResults(String idDimCompResults)
    {
        return baseDimCompResultMapper.deleteBaseDimCompResultByIdDimCompResults(Convert.toStrArray(idDimCompResults));
    }

    /**
     * 删除维度对照结果信息
     * 
     * @param idDimCompResult 维度对照结果主键
     * @return 结果
     */
    @Override
    public int deleteBaseDimCompResultByIdDimCompResult(Long idDimCompResult)
    {
        return baseDimCompResultMapper.deleteBaseDimCompResultByIdDimCompResult(idDimCompResult);
    }
}
