package com.bst.base.service.impl;

import java.util.Date;
import java.util.List;

import com.bst.common.constant.JobConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.base.mapper.BaseMsgMapper;
import com.bst.base.domain.BaseMsg;
import com.bst.base.service.IBaseMsgService;
import com.bst.common.core.text.Convert;

/**
 * 消息中心Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Service
public class BaseMsgServiceImpl implements IBaseMsgService 
{
    @Autowired
    private BaseMsgMapper baseMsgMapper;

    /**
     * 查询消息中心
     * 
     * @param idMsg 消息中心主键
     * @return 消息中心
     */
    @Override
    public BaseMsg selectBaseMsgByIdMsg(Long idMsg)
    {
        return baseMsgMapper.selectBaseMsgByIdMsg(idMsg);
    }

    /**
     * 查询消息中心列表
     * 
     * @param baseMsg 消息中心
     * @return 消息中心
     */
    @Override
    public List<BaseMsg> selectBaseMsgList(BaseMsg baseMsg)
    {
        return baseMsgMapper.selectBaseMsgList(baseMsg);
    }

    /**
     * 新增消息中心
     * 
     * @param baseMsg 消息中心
     * @return 结果
     */
    @Override
    public int insertBaseMsg(BaseMsg baseMsg)
    {
        return baseMsgMapper.insertBaseMsg(baseMsg);
    }

    /**
     * 修改消息中心
     * 
     * @param baseMsg 消息中心
     * @return 结果
     */
    @Override
    public int updateBaseMsg(BaseMsg baseMsg)
    {
        return baseMsgMapper.updateBaseMsg(baseMsg);
    }

    /**
     * 批量删除消息中心
     * 
     * @param idMsgs 需要删除的消息中心主键
     * @return 结果
     */
    @Override
    public int deleteBaseMsgByIdMsgs(String idMsgs)
    {
        return baseMsgMapper.deleteBaseMsgByIdMsgs(Convert.toStrArray(idMsgs));
    }

    /**
     * 删除消息中心信息
     * 
     * @param idMsg 消息中心主键
     * @return 结果
     */
    @Override
    public int deleteBaseMsgByIdMsg(Long idMsg)
    {
        return baseMsgMapper.deleteBaseMsgByIdMsg(idMsg);
    }

    @Override
    public int insert(String cd, String msg, String des) {
        BaseMsg baseMsg = new BaseMsg();
        baseMsg.setCont(msg);
        baseMsg.setDes(des);
        baseMsg.setCd(cd);
        getBaseMsgByCd(baseMsg);
        return baseMsgMapper.insertBaseMsg(baseMsg);
    }

    @Override
    public int insertList(List<BaseMsg> list) {
        for(BaseMsg baseMsg:list) {
            getBaseMsgByCd(baseMsg);
            baseMsgMapper.insertBaseMsg(baseMsg);
        }
        return list.size();
    }

    private void getBaseMsgByCd(BaseMsg baseMsg) {
        baseMsg.setDtSysCre(new Date());
        baseMsg.setFgHand(0);
        switch (baseMsg.getCd()) {
            case "A01":
                baseMsg.setNa("数据源不可用");
                baseMsg.setEuLevel(JobConstant.MSG_TYPE_ERROR);
                break;
            case "B01":
                baseMsg.setNa("未知");
                baseMsg.setEuLevel(JobConstant.MSG_TYPE_ERROR);
                break;
            case "C01":
                baseMsg.setNa("维度缺少数据");
                baseMsg.setEuLevel(JobConstant.MSG_TYPE_ERROR);
                break;
            case "D01":
                baseMsg.setNa("新增维度");
                baseMsg.setEuLevel(JobConstant.MSG_TYPE_INFO);
                break;
            case "D02":
                baseMsg.setNa("新增维度失败");
                baseMsg.setEuLevel(JobConstant.MSG_TYPE_WARING);
                break;
        }
    }
}
