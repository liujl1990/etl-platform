package com.bst.base.service;

import java.util.List;
import com.bst.base.domain.BaseDimComp;

/**
 * 维度对照Service接口
 * 
 * @author laoliu
 * @date 2023-02-12
 */
public interface IBaseDimCompService 
{
    /**
     * 查询维度对照
     * 
     * @param idDimComp 维度对照主键
     * @return 维度对照
     */
    public BaseDimComp selectBaseDimCompByIdDimComp(Long idDimComp);

    BaseDimComp selectBaseDimCompByCd(String cd);

    /**
     * 查询维度对照列表
     * 
     * @param baseDimComp 维度对照
     * @return 维度对照集合
     */
    public List<BaseDimComp> selectBaseDimCompList(BaseDimComp baseDimComp);

    /**
     * 新增维度对照
     * 
     * @param baseDimComp 维度对照
     * @return 结果
     */
    public int insertBaseDimComp(BaseDimComp baseDimComp);

    /**
     * 修改维度对照
     * 
     * @param baseDimComp 维度对照
     * @return 结果
     */
    public int updateBaseDimComp(BaseDimComp baseDimComp);

    /**
     * 批量删除维度对照
     * 
     * @param idDimComps 需要删除的维度对照主键集合
     * @return 结果
     */
    public int deleteBaseDimCompByIdDimComps(String idDimComps);

    /**
     * 删除维度对照信息
     * 
     * @param idDimComp 维度对照主键
     * @return 结果
     */
    public int deleteBaseDimCompByIdDimComp(Long idDimComp);
}
