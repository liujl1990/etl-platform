package com.bst.base.mapper;

import java.util.List;
import com.bst.base.domain.BaseDimCompResult;

/**
 * 维度对照结果Mapper接口
 * 
 * @author laoliu
 * @date 2023-02-12
 */
public interface BaseDimCompResultMapper 
{
    /**
     * 查询维度对照结果
     * 
     * @param idDimCompResult 维度对照结果主键
     * @return 维度对照结果
     */
    public BaseDimCompResult selectBaseDimCompResultByIdDimCompResult(Long idDimCompResult);

    /**
     * 查询维度对照结果列表
     * 
     * @param baseDimCompResult 维度对照结果
     * @return 维度对照结果集合
     */
    public List<BaseDimCompResult> selectBaseDimCompResultList(BaseDimCompResult baseDimCompResult);

    /**
     * 新增维度对照结果
     * 
     * @param baseDimCompResult 维度对照结果
     * @return 结果
     */
    public int insertBaseDimCompResult(BaseDimCompResult baseDimCompResult);

    /**
     * 修改维度对照结果
     * 
     * @param baseDimCompResult 维度对照结果
     * @return 结果
     */
    public int updateBaseDimCompResult(BaseDimCompResult baseDimCompResult);

    /**
     * 删除维度对照结果
     * 
     * @param idDimCompResult 维度对照结果主键
     * @return 结果
     */
    public int deleteBaseDimCompResultByIdDimCompResult(Long idDimCompResult);

    /**
     * 批量删除维度对照结果
     * 
     * @param idDimCompResults 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseDimCompResultByIdDimCompResults(String[] idDimCompResults);
}
