package com.bst.base.mapper;

import java.util.List;
import com.bst.base.domain.BaseSd;
import com.bst.base.domain.BaseSdItem;

/**
 * 系统字典Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-26
 */
public interface BaseSdMapper 
{
    /**
     * 查询系统字典
     * 
     * @param cd 系统字典主键
     * @return 系统字典
     */
    public BaseSd selectBaseSdByCd(String cd);

    /**
     * 查询系统字典列表
     * 
     * @param baseSd 系统字典
     * @return 系统字典集合
     */
    public List<BaseSd> selectBaseSdList(BaseSd baseSd);

    /**
     * 新增系统字典
     * 
     * @param baseSd 系统字典
     * @return 结果
     */
    public int insertBaseSd(BaseSd baseSd);

    /**
     * 修改系统字典
     * 
     * @param baseSd 系统字典
     * @return 结果
     */
    public int updateBaseSd(BaseSd baseSd);

    /**
     * 删除系统字典
     * 
     * @param cd 系统字典主键
     * @return 结果
     */
    public int deleteBaseSdByCd(String cd);

    /**
     * 批量删除系统字典
     * 
     * @param cds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseSdByCds(String[] cds);

    /**
     * 批量删除系统字典明细
     * 
     * @param cds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseSdItemByCdSds(String[] cds);
    
    /**
     * 批量新增系统字典明细
     * 
     * @param baseSdItemList 系统字典明细列表
     * @return 结果
     */
    public int batchBaseSdItem(List<BaseSdItem> baseSdItemList);
    

    /**
     * 通过系统字典主键删除系统字典明细信息
     * 
     * @param cd 系统字典ID
     * @return 结果
     */
    public int deleteBaseSdItemByCdSd(String cd);
}
