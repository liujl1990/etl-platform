package com.bst.base.mapper;

import java.util.List;
import com.bst.base.domain.BaseDimComp;

/**
 * 维度对照Mapper接口
 * 
 * @author laoliu
 * @date 2023-02-12
 */
public interface BaseDimCompMapper 
{
    /**
     * 查询维度对照
     * 
     * @param idDimComp 维度对照主键
     * @return 维度对照
     */
    public BaseDimComp selectBaseDimCompByIdDimComp(Long idDimComp);

    public BaseDimComp selectBaseDimCompByCd(String cd);

    /**
     * 查询维度对照列表
     * 
     * @param baseDimComp 维度对照
     * @return 维度对照集合
     */
    public List<BaseDimComp> selectBaseDimCompList(BaseDimComp baseDimComp);

    /**
     * 新增维度对照
     * 
     * @param baseDimComp 维度对照
     * @return 结果
     */
    public int insertBaseDimComp(BaseDimComp baseDimComp);

    /**
     * 修改维度对照
     * 
     * @param baseDimComp 维度对照
     * @return 结果
     */
    public int updateBaseDimComp(BaseDimComp baseDimComp);

    /**
     * 删除维度对照
     * 
     * @param idDimComp 维度对照主键
     * @return 结果
     */
    public int deleteBaseDimCompByIdDimComp(Long idDimComp);

    /**
     * 批量删除维度对照
     * 
     * @param idDimComps 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseDimCompByIdDimComps(String[] idDimComps);
}
