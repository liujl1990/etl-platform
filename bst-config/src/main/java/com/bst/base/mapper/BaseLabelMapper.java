package com.bst.base.mapper;

import java.util.List;
import com.bst.system.domain.BaseLabel;

/**
 * 指标标签Mapper接口
 * 
 * @author 老刘
 * @date 2023-02-18
 */
public interface BaseLabelMapper
{
    /**
     * 查询指标标签
     * 
     * @param idLabel 指标标签主键
     * @return 指标标签
     */
    public BaseLabel selectBaseLabelByIdLabel(Long idLabel);

    /**
     * 查询指标标签列表
     * 
     * @param mdIndexLabel 指标标签
     * @return 指标标签集合
     */
    public List<BaseLabel> selectBaseLabelList(BaseLabel mdIndexLabel);

    /**
     * 新增指标标签
     * 
     * @param mdIndexLabel 指标标签
     * @return 结果
     */
    public int insertBaseLabel(BaseLabel mdIndexLabel);

    /**
     * 修改指标标签
     * 
     * @param mdIndexLabel 指标标签
     * @return 结果
     */
    public int updateBaseLabel(BaseLabel mdIndexLabel);

    /**
     * 删除指标标签
     * 
     * @param idLabel 指标标签主键
     * @return 结果
     */
    public int deleteBaseLabelByIdLabel(Long idLabel);

    /**
     * 批量删除指标标签
     * 
     * @param idLabels 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseLabelByIdLabels(String[] idLabels);

    List<BaseLabel> findByIdIndex(String idIndex);
}
