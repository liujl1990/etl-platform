package com.bst.base.mapper;

import java.util.List;
import com.bst.base.domain.BaseDbfldComb;

/**
 * 数据库字段映射Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public interface BaseDbfldCombMapper 
{
    /**
     * 查询数据库字段映射
     * 
     * @param idDbfldComb 数据库字段映射主键
     * @return 数据库字段映射
     */
    public BaseDbfldComb selectBaseDbfldCombByIdDbfldComb(Integer idDbfldComb);

    /**
     * 查询数据库字段映射列表
     * 
     * @param baseDbfldComb 数据库字段映射
     * @return 数据库字段映射集合
     */
    public List<BaseDbfldComb> selectBaseDbfldCombList(BaseDbfldComb baseDbfldComb);

    /**
     * 新增数据库字段映射
     * 
     * @param baseDbfldComb 数据库字段映射
     * @return 结果
     */
    public int insertBaseDbfldComb(BaseDbfldComb baseDbfldComb);

    /**
     * 修改数据库字段映射
     * 
     * @param baseDbfldComb 数据库字段映射
     * @return 结果
     */
    public int updateBaseDbfldComb(BaseDbfldComb baseDbfldComb);

    /**
     * 删除数据库字段映射
     * 
     * @param idDbfldComb 数据库字段映射主键
     * @return 结果
     */
    public int deleteBaseDbfldCombByIdDbfldComb(Integer idDbfldComb);

    /**
     * 批量删除数据库字段映射
     * 
     * @param idDbfldCombs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseDbfldCombByIdDbfldCombs(String[] idDbfldCombs);
}
