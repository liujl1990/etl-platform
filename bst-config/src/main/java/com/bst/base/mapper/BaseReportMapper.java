package com.bst.base.mapper;

import java.util.List;
import com.bst.base.domain.BaseReport;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-23
 */
public interface BaseReportMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idReport 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BaseReport selectBaseReportByIdReport(Long idReport);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param baseReport 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BaseReport> selectBaseReportList(BaseReport baseReport);

    /**
     * 新增【请填写功能名称】
     * 
     * @param baseReport 【请填写功能名称】
     * @return 结果
     */
    public int insertBaseReport(BaseReport baseReport);

    /**
     * 修改【请填写功能名称】
     * 
     * @param baseReport 【请填写功能名称】
     * @return 结果
     */
    public int updateBaseReport(BaseReport baseReport);

    /**
     * 删除【请填写功能名称】
     * 
     * @param idReport 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBaseReportByIdReport(Long idReport);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idReports 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseReportByIdReports(String[] idReports);
}
