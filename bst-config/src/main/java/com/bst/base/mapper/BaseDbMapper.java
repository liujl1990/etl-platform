package com.bst.base.mapper;

import java.util.List;
import com.bst.base.domain.BaseDb;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-17
 */
public interface BaseDbMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idDb 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BaseDb selectBaseDbById(Integer idDb);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param baseDb 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BaseDb> selectBaseDbList(BaseDb baseDb);

    /**
     * 新增【请填写功能名称】
     * 
     * @param baseDb 【请填写功能名称】
     * @return 结果
     */
    public int insertBaseDb(BaseDb baseDb);

    /**
     * 修改【请填写功能名称】
     * 
     * @param baseDb 【请填写功能名称】
     * @return 结果
     */
    public int updateBaseDb(BaseDb baseDb);

    /**
     * 删除【请填写功能名称】
     * 
     * @param idDb 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBaseDbByIdDb(Integer idDb);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idDbs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseDbByIdDbs(String[] idDbs);
}
