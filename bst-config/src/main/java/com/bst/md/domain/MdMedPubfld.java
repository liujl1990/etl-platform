package com.bst.md.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 公共字段管理对象 md_med_pubfld
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public class MdMedPubfld extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private String idPubfld;

    /** 字段名称 */
    @Excel(name = "字段名称")
    private String na;

    /** JAVA字段类型 */
    @Excel(name = "JAVA字段类型")
    private Integer euJavatp;

    /** JAVA字段类型名称 */
    @Excel(name = "JAVA字段类型名称")
    private String naJavatp;

    /** 长度 */
    @Excel(name = "长度")
    private Integer numLth;

    /** 精度 */
    @Excel(name = "精度")
    private Integer numPrec;

    /** 维度类型 */
    @Excel(name = "维度类型")
    private String sdDimtp;

    /** 维度类型名称 */
    @Excel(name = "维度类型名称")
    private String naDimtp;

    /** 维度表 */
    @Excel(name = "维度表")
    private String cdTbDim;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    public void setIdPubfld(String idPubfld) 
    {
        this.idPubfld = idPubfld;
    }

    public String getIdPubfld() 
    {
        return idPubfld;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setEuJavatp(Integer euJavatp) 
    {
        this.euJavatp = euJavatp;
    }

    public Integer getEuJavatp() 
    {
        return euJavatp;
    }
    public void setNaJavatp(String naJavatp) 
    {
        this.naJavatp = naJavatp;
    }

    public String getNaJavatp() 
    {
        return naJavatp;
    }
    public void setNumLth(Integer numLth)
    {
        this.numLth = numLth;
    }

    public Integer getNumLth()
    {
        return numLth;
    }
    public void setNumPrec(Integer numPrec) 
    {
        this.numPrec = numPrec;
    }

    public void setSdDimtp(String sdDimtp) 
    {
        this.sdDimtp = sdDimtp;
    }

    public String getSdDimtp() 
    {
        return sdDimtp;
    }
    public void setNaDimtp(String naDimtp) 
    {
        this.naDimtp = naDimtp;
    }

    public String getNaDimtp() 
    {
        return naDimtp;
    }
    public void setCdTbDim(String cdTbDim) 
    {
        this.cdTbDim = cdTbDim;
    }

    public String getCdTbDim() 
    {
        return cdTbDim;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    public Integer getNumPrec() {
        return numPrec;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idPubfld", getIdPubfld())
            .append("na", getNa())
            .append("euJavatp", getEuJavatp())
            .append("naJavatp", getNaJavatp())
            .append("numLth", getNumLth())
            .append("numPrec", getNumPrec())
            .append("sdDimtp", getSdDimtp())
            .append("naDimtp", getNaDimtp())
            .append("cdTbDim", getCdTbDim())
            .append("des", getDes())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
