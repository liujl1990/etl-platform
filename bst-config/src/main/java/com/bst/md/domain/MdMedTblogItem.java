package com.bst.md.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 元数据日志明细对象 md_med_tblog_item
 * 
 * @author ruoyi
 * @date 2022-07-17
 */
public class MdMedTblogItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idTblogItem;

    /** 外键 */
    @Excel(name = "外键")
    private Long idTblog;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** SQL */
    @Excel(name = "SQL")
    private String sqlExec;

    /** 执行结果 */
    @Excel(name = "执行结果")
    private String euStatus;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    /**  */
    private String naEmpCre;

    public void setIdTblogItem(Long idTblogItem) 
    {
        this.idTblogItem = idTblogItem;
    }

    public Long getIdTblogItem() 
    {
        return idTblogItem;
    }
    public void setIdTblog(Long idTblog) 
    {
        this.idTblog = idTblog;
    }

    public Long getIdTblog() 
    {
        return idTblog;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setSqlExec(String sqlExec) 
    {
        this.sqlExec = sqlExec;
    }

    public String getSqlExec() 
    {
        return sqlExec;
    }
    public void setEuStatus(String euStatus) 
    {
        this.euStatus = euStatus;
    }

    public String getEuStatus() 
    {
        return euStatus;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idTblogItem", getIdTblogItem())
            .append("idTblog", getIdTblog())
            .append("na", getNa())
            .append("sqlExec", getSqlExec())
            .append("euStatus", getEuStatus())
            .append("des", getDes())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .append("naEmpCre", getNaEmpCre())
            .toString();
    }
}
