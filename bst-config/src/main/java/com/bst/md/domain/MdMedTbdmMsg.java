package com.bst.md.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 md_med_tbdm_msg
 * 
 * @author ruoyi
 * @date 2023-04-25
 */
public class MdMedTbdmMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long idTbdmMsg;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String idIndex;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naIndex;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long num;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String cdTb;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naTb;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dtDay;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String naEmpCre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date dtSysCre;

    public void setIdTbdmMsg(Long idTbdmMsg) 
    {
        this.idTbdmMsg = idTbdmMsg;
    }

    public Long getIdTbdmMsg() 
    {
        return idTbdmMsg;
    }
    public void setIdIndex(String idIndex) 
    {
        this.idIndex = idIndex;
    }

    public String getIdIndex() 
    {
        return idIndex;
    }
    public void setNaIndex(String naIndex) 
    {
        this.naIndex = naIndex;
    }

    public String getNaIndex() 
    {
        return naIndex;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }
    public void setCdTb(String cdTb) 
    {
        this.cdTb = cdTb;
    }

    public String getCdTb() 
    {
        return cdTb;
    }
    public void setNaTb(String naTb) 
    {
        this.naTb = naTb;
    }

    public String getNaTb() 
    {
        return naTb;
    }
    public void setDtDay(String dtDay) 
    {
        this.dtDay = dtDay;
    }

    public String getDtDay() 
    {
        return dtDay;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idTbdmMsg", getIdTbdmMsg())
            .append("idIndex", getIdIndex())
            .append("naIndex", getNaIndex())
            .append("num", getNum())
            .append("cdTb", getCdTb())
            .append("naTb", getNaTb())
            .append("dtDay", getDtDay())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .toString();
    }
}
