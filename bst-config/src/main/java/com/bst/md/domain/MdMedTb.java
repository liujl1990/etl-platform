package com.bst.md.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 表管理对象 md_med_tb
 * 
 * @author ruoyi
 * @date 2022-07-02
 */
public class MdMedTb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idTb;

    /** 编码 */
    @Excel(name = "编码")
    private String cd;

    /** 名称 */
    @Excel(name = "名称")
    private String na;

    /** 表分类 */
    @Excel(name = "表分类")
    private String sdTbca;

    /** 所属系统 */
    @Excel(name = "所属系统")
    private String sdSys;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public void setIdTb(Long idTb) 
    {
        this.idTb = idTb;
    }

    public Long getIdTb() 
    {
        return idTb;
    }
    public void setCd(String cd) 
    {
        this.cd = cd;
    }

    public String getCd() 
    {
        return cd;
    }
    public void setNa(String na) 
    {
        this.na = na;
    }

    public String getNa() 
    {
        return na;
    }
    public void setSdTbca(String sdTbca) 
    {
        this.sdTbca = sdTbca;
    }

    public String getSdTbca() 
    {
        return sdTbca;
    }
    public void setSdSys(String sdSys) 
    {
        this.sdSys = sdSys;
    }

    public String getSdSys() 
    {
        return sdSys;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idTb", getIdTb())
            .append("cd", getCd())
            .append("na", getNa())
            .append("sdTbca", getSdTbca())
            .append("sdSys", getSdSys())
            .append("des", getDes())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
