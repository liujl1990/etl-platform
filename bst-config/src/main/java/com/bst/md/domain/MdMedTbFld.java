package com.bst.md.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 元数据字段对象 md_med_tb_fld
 * 
 * @author ruoyi
 * @date 2022-07-02
 */
public class MdMedTbFld extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long idTbFld;

    /** 外键 */
    @Excel(name = "外键")
    private Long idTb;

    /** 公共字段 */
    private String idPubfld;

    /** 公共字段名称 */
    @Excel(name = "公共字段名称")
    private String naPubfld;

    /** 索引类型 */
    @Excel(name = "索引类型")
    private String sdConstp;

    /** JAVA类型 */
    private Integer euJavatp;

    /** JAVA类型名称 */
    @Excel(name = "JAVA类型名称")
    private String naJavatp;

    /** 长度 */
    @Excel(name = "长度")
    private Integer numLth;

    /** 精度 */
    @Excel(name = "精度")
    private Integer numPrec;

    private String cdTb;

    public String getCdTb() {
        return cdTb;
    }

    public void setCdTb(String cdTb) {
        this.cdTb = cdTb;
    }

    /**  */
    private String naEmpCre;

    /**  */
    private Date dtSysCre;

    /**  */
    private String naEmpModi;

    /**  */
    private Date dtSysModi;

    public void setIdTbFld(Long idTbFld) 
    {
        this.idTbFld = idTbFld;
    }

    public Long getIdTbFld() 
    {
        return idTbFld;
    }
    public void setIdTb(Long idTb) 
    {
        this.idTb = idTb;
    }

    public Long getIdTb() 
    {
        return idTb;
    }
    public void setIdPubfld(String idPubfld) 
    {
        this.idPubfld = idPubfld;
    }

    public String getIdPubfld() 
    {
        return idPubfld;
    }
    public void setNaPubfld(String naPubfld) 
    {
        this.naPubfld = naPubfld;
    }

    public String getNaPubfld() 
    {
        return naPubfld;
    }
    public void setSdConstp(String sdConstp) 
    {
        this.sdConstp = sdConstp;
    }

    public String getSdConstp() 
    {
        return sdConstp;
    }
    public void setEuJavatp(Integer euJavatp) 
    {
        this.euJavatp = euJavatp;
    }

    public Integer getEuJavatp() 
    {
        return euJavatp;
    }
    public void setNaJavatp(String naJavatp) 
    {
        this.naJavatp = naJavatp;
    }

    public String getNaJavatp() 
    {
        return naJavatp;
    }
    public void setNumLth(Integer numLth)
    {
        this.numLth = numLth;
    }

    public Integer getNumLth()
    {
        return numLth;
    }
    public void setNumPrec(Integer numPrec) 
    {
        this.numPrec = numPrec;
    }

    public Integer getNumPrec() 
    {
        return numPrec;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idTbFld", getIdTbFld())
            .append("idTb", getIdTb())
            .append("idPubfld", getIdPubfld())
            .append("naPubfld", getNaPubfld())
            .append("sdConstp", getSdConstp())
            .append("euJavatp", getEuJavatp())
            .append("naJavatp", getNaJavatp())
            .append("numLth", getNumLth())
            .append("numPrec", getNumPrec())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .toString();
    }
}
