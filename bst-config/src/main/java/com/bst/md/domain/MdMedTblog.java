package com.bst.md.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.bst.common.annotation.Excel;
import com.bst.common.core.domain.BaseEntity;

/**
 * 元数据创建日志对象 md_med_tblog
 * 
 * @author ruoyi
 * @date 2022-07-17
 */
public class MdMedTblog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long idTblog;

    /** 表名 */
    @Excel(name = "操作类型")
    private String euTp;

    /** 表名 */
    @Excel(name = "表名")
    private String cdTb;

    /** 参数 */
    private String param;

    /** 数据类型 */
    @Excel(name = "数据类型")
    private String euParamtp;

    /** 方法名 */
    @Excel(name = "方法名")
    private String method;

    /** 执行结果 */
    @Excel(name = "执行结果")
    private String euStatus;

    /** 备注 */
    @Excel(name = "备注")
    private String des;

    /**  */
    @Excel(name = "")
    private String naEmpCre;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysCre;

    /**  */
    @Excel(name = "")
    private String naEmpModi;

    /**  */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dtSysModi;

    public String getEuTp() {
        return euTp;
    }

    public void setEuTp(String euTp) {
        this.euTp = euTp;
    }

    /** 元数据日志明细信息 */
    private List<MdMedTblogItem> mdMedTblogItemList;

    public void setIdTblog(Long idTblog) 
    {
        this.idTblog = idTblog;
    }

    public Long getIdTblog() 
    {
        return idTblog;
    }
    public void setCdTb(String cdTb) 
    {
        this.cdTb = cdTb;
    }

    public String getCdTb() 
    {
        return cdTb;
    }
    public void setParam(String param) 
    {
        this.param = param;
    }

    public String getParam() 
    {
        return param;
    }
    public void setEuParamtp(String euParamtp) 
    {
        this.euParamtp = euParamtp;
    }

    public String getEuParamtp() 
    {
        return euParamtp;
    }
    public void setMethod(String method) 
    {
        this.method = method;
    }

    public String getMethod() 
    {
        return method;
    }
    public void setEuStatus(String euStatus) 
    {
        this.euStatus = euStatus;
    }

    public String getEuStatus() 
    {
        return euStatus;
    }
    public void setDes(String des) 
    {
        this.des = des;
    }

    public String getDes() 
    {
        return des;
    }
    public void setNaEmpCre(String naEmpCre) 
    {
        this.naEmpCre = naEmpCre;
    }

    public String getNaEmpCre() 
    {
        return naEmpCre;
    }
    public void setDtSysCre(Date dtSysCre) 
    {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysCre() 
    {
        return dtSysCre;
    }
    public void setNaEmpModi(String naEmpModi) 
    {
        this.naEmpModi = naEmpModi;
    }

    public String getNaEmpModi() 
    {
        return naEmpModi;
    }
    public void setDtSysModi(Date dtSysModi) 
    {
        this.dtSysModi = dtSysModi;
    }

    public Date getDtSysModi() 
    {
        return dtSysModi;
    }

    public List<MdMedTblogItem> getMdMedTblogItemList()
    {
        return mdMedTblogItemList;
    }

    public void setMdMedTblogItemList(List<MdMedTblogItem> mdMedTblogItemList)
    {
        this.mdMedTblogItemList = mdMedTblogItemList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idTblog", getIdTblog())
            .append("cdTb", getCdTb())
            .append("param", getParam())
            .append("euParamtp", getEuParamtp())
            .append("method", getMethod())
            .append("euStatus", getEuStatus())
            .append("des", getDes())
            .append("naEmpCre", getNaEmpCre())
            .append("dtSysCre", getDtSysCre())
            .append("naEmpModi", getNaEmpModi())
            .append("dtSysModi", getDtSysModi())
            .append("mdMedTblogItemList", getMdMedTblogItemList())
            .toString();
    }
}
