package com.bst.md.mapper;

import java.util.List;
import com.bst.md.domain.MdMedTb;

/**
 * 表管理Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-02
 */
public interface MdMedTbMapper 
{
    /**
     * 查询表管理
     * 
     * @param idTb 表管理主键
     * @return 表管理
     */
    public MdMedTb selectMdMedTbByIdTb(Long idTb);

    /**
     * 查询表管理列表
     * 
     * @param mdMedTb 表管理
     * @return 表管理集合
     */
    public List<MdMedTb> selectMdMedTbList(MdMedTb mdMedTb);

    /**
     * 新增表管理
     * 
     * @param mdMedTb 表管理
     * @return 结果
     */
    public int insertMdMedTb(MdMedTb mdMedTb);

    /**
     * 修改表管理
     * 
     * @param mdMedTb 表管理
     * @return 结果
     */
    public int updateMdMedTb(MdMedTb mdMedTb);

    /**
     * 删除表管理
     * 
     * @param idTb 表管理主键
     * @return 结果
     */
    public int deleteMdMedTbByIdTb(Long idTb);

    /**
     * 批量删除表管理
     * 
     * @param idTbs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMdMedTbByIdTbs(String[] idTbs);
}
