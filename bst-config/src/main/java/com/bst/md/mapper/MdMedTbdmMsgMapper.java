package com.bst.md.mapper;

import java.util.List;
import com.bst.md.domain.MdMedTbdmMsg;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-25
 */
public interface MdMedTbdmMsgMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param idTbdmMsg 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public MdMedTbdmMsg selectMdMedTbdmMsgByIdTbdmMsg(Long idTbdmMsg);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param mdMedTbdmMsg 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MdMedTbdmMsg> selectMdMedTbdmMsgList(MdMedTbdmMsg mdMedTbdmMsg);

    /**
     * 新增【请填写功能名称】
     * 
     * @param mdMedTbdmMsg 【请填写功能名称】
     * @return 结果
     */
    public int insertMdMedTbdmMsg(MdMedTbdmMsg mdMedTbdmMsg);

    /**
     * 修改【请填写功能名称】
     * 
     * @param mdMedTbdmMsg 【请填写功能名称】
     * @return 结果
     */
    public int updateMdMedTbdmMsg(MdMedTbdmMsg mdMedTbdmMsg);

    /**
     * 删除【请填写功能名称】
     * 
     * @param idTbdmMsg 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteMdMedTbdmMsgByIdTbdmMsg(Long idTbdmMsg);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idTbdmMsgs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMdMedTbdmMsgByIdTbdmMsgs(String[] idTbdmMsgs);
}
