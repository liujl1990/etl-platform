package com.bst.md.mapper;

import com.bst.common.vo.DimDataVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * MdMedDim数据接口
 */
@Mapper
public interface MdMedDimDataMapper {

    List<DimDataVO> queryDimData(@Param("tb") String tb);

    List<DimDataVO> queryDimDataWithOrder(@Param("tb") String tb);

    void insert(@Param("dataVO") DimDataVO param,@Param("tb")String tb);

    void insertList(@Param("list") List<DimDataVO> list,@Param("tb")String tb);

    List<Map<String,Object>> queryDayData(@Param("mm")String mm);

    int deleteByCd(@Param("tb")String tb,@Param("cd")String cd);

    List<Map<String,Object>>  queryHisDeptData();
}