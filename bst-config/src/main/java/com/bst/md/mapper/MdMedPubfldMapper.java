package com.bst.md.mapper;

import java.util.List;
import com.bst.md.domain.MdMedPubfld;
import org.apache.ibatis.annotations.Param;

/**
 * 公共字段管理Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public interface MdMedPubfldMapper 
{
    /**
     * 查询公共字段管理
     * 
     * @param idPubfld 公共字段管理主键
     * @return 公共字段管理
     */
    public MdMedPubfld selectMdMedPubfldByIdPubfld(String idPubfld);

    /**
     * 查询公共字段管理列表
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 公共字段管理集合
     */
    public List<MdMedPubfld> selectMdMedPubfldList(MdMedPubfld mdMedPubfld);

    /**
     * 新增公共字段管理
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 结果
     */
    public int insertMdMedPubfld(MdMedPubfld mdMedPubfld);

    /**
     * 修改公共字段管理
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 结果
     */
    public int updateMdMedPubfld(MdMedPubfld mdMedPubfld);

    /**
     * 删除公共字段管理
     * 
     * @param idPubfld 公共字段管理主键
     * @return 结果
     */
    public int deleteMdMedPubfldByIdPubfld(String idPubfld);

    /**
     * 批量删除公共字段管理
     * 
     * @param idPubflds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMdMedPubfldByIdPubflds(String[] idPubflds);

    List<MdMedPubfld> findByIdList(@Param("list") List<String> idList);

    List<MdMedPubfld> findByFldPrefix(String idPubfld);

    List<MdMedPubfld> findDimNotCreatTable(@Param("str")String str);

    List<String> findDimTable();

    List<MdMedPubfld> findDimCreatTable(@Param("na")String na);
}
