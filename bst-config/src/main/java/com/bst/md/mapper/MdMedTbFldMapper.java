package com.bst.md.mapper;

import java.util.List;
import com.bst.md.domain.MdMedTbFld;

/**
 * 元数据字段Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-02
 */
public interface MdMedTbFldMapper 
{
    /**
     * 查询元数据字段
     * 
     * @param idTbFld 元数据字段主键
     * @return 元数据字段
     */
    public MdMedTbFld selectMdMedTbFldByIdTbFld(Long idTbFld);

    /**
     * 查询元数据字段列表
     * 
     * @param mdMedTbFld 元数据字段
     * @return 元数据字段集合
     */
    public List<MdMedTbFld> selectMdMedTbFldList(MdMedTbFld mdMedTbFld);

    /**
     * 新增元数据字段
     * 
     * @param mdMedTbFld 元数据字段
     * @return 结果
     */
    public int insertMdMedTbFld(MdMedTbFld mdMedTbFld);

    /**
     * 修改元数据字段
     * 
     * @param mdMedTbFld 元数据字段
     * @return 结果
     */
    public int updateMdMedTbFld(MdMedTbFld mdMedTbFld);

    /**
     * 删除元数据字段
     * 
     * @param idTbFld 元数据字段主键
     * @return 结果
     */
    public int deleteMdMedTbFldByIdTbFld(Long idTbFld);

    /**
     * 批量删除元数据字段
     * 
     * @param idTbFlds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMdMedTbFldByIdTbFlds(String[] idTbFlds);

    int deleteMdMedTbFldByIdTb(Long idTb);
}
