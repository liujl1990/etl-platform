package com.bst.md.mapper;

import java.util.List;
import com.bst.md.domain.MdMedTblog;
import com.bst.md.domain.MdMedTblogItem;

/**
 * 元数据创建日志Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-17
 */
public interface MdMedTblogMapper 
{
    /**
     * 查询元数据创建日志
     * 
     * @param idTblog 元数据创建日志主键
     * @return 元数据创建日志
     */
    public MdMedTblog selectMdMedTblogByIdTblog(Long idTblog);

    /**
     * 查询元数据创建日志列表
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 元数据创建日志集合
     */
    public List<MdMedTblog> selectMdMedTblogList(MdMedTblog mdMedTblog);

    /**
     * 新增元数据创建日志
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 结果
     */
    public int insertMdMedTblog(MdMedTblog mdMedTblog);

    /**
     * 修改元数据创建日志
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 结果
     */
    public int updateMdMedTblog(MdMedTblog mdMedTblog);

    /**
     * 删除元数据创建日志
     * 
     * @param idTblog 元数据创建日志主键
     * @return 结果
     */
    public int deleteMdMedTblogByIdTblog(Long idTblog);

    /**
     * 批量删除元数据创建日志
     * 
     * @param idTblogs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMdMedTblogByIdTblogs(String[] idTblogs);

    /**
     * 批量删除元数据日志明细
     * 
     * @param idTblogs 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMdMedTblogItemByIdTblogs(String[] idTblogs);
    
    /**
     * 批量新增元数据日志明细
     * 
     * @param mdMedTblogItemList 元数据日志明细列表
     * @return 结果
     */
    public int batchMdMedTblogItem(List<MdMedTblogItem> mdMedTblogItemList);
    

    /**
     * 通过元数据创建日志主键删除元数据日志明细信息
     * 
     * @param idTblog 元数据创建日志ID
     * @return 结果
     */
    public int deleteMdMedTblogItemByIdTblog(Long idTblog);
}
