package com.bst.md.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.md.mapper.MdMedTblogItemMapper;
import com.bst.md.domain.MdMedTblogItem;
import com.bst.md.service.IMdMedTblogItemService;
import com.bst.common.core.text.Convert;

/**
 * 元数据日志明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-17
 */
@Service
public class MdMedTblogItemServiceImpl implements IMdMedTblogItemService 
{
    @Autowired
    private MdMedTblogItemMapper mdMedTblogItemMapper;

    /**
     * 查询元数据日志明细
     * 
     * @param idTblogItem 元数据日志明细主键
     * @return 元数据日志明细
     */
    @Override
    public MdMedTblogItem selectMdMedTblogItemByIdTblogItem(Long idTblogItem)
    {
        return mdMedTblogItemMapper.selectMdMedTblogItemByIdTblogItem(idTblogItem);
    }

    /**
     * 查询元数据日志明细列表
     * 
     * @param mdMedTblogItem 元数据日志明细
     * @return 元数据日志明细
     */
    @Override
    public List<MdMedTblogItem> selectMdMedTblogItemList(MdMedTblogItem mdMedTblogItem)
    {
        return mdMedTblogItemMapper.selectMdMedTblogItemList(mdMedTblogItem);
    }

    /**
     * 新增元数据日志明细
     * 
     * @param mdMedTblogItem 元数据日志明细
     * @return 结果
     */
    @Override
    public int insertMdMedTblogItem(MdMedTblogItem mdMedTblogItem)
    {
        return mdMedTblogItemMapper.insertMdMedTblogItem(mdMedTblogItem);
    }

    /**
     * 修改元数据日志明细
     * 
     * @param mdMedTblogItem 元数据日志明细
     * @return 结果
     */
    @Override
    public int updateMdMedTblogItem(MdMedTblogItem mdMedTblogItem)
    {
        return mdMedTblogItemMapper.updateMdMedTblogItem(mdMedTblogItem);
    }

    /**
     * 批量删除元数据日志明细
     * 
     * @param idTblogItems 需要删除的元数据日志明细主键
     * @return 结果
     */
    @Override
    public int deleteMdMedTblogItemByIdTblogItems(String idTblogItems)
    {
        return mdMedTblogItemMapper.deleteMdMedTblogItemByIdTblogItems(Convert.toStrArray(idTblogItems));
    }

    /**
     * 删除元数据日志明细信息
     * 
     * @param idTblogItem 元数据日志明细主键
     * @return 结果
     */
    @Override
    public int deleteMdMedTblogItemByIdTblogItem(Long idTblogItem)
    {
        return mdMedTblogItemMapper.deleteMdMedTblogItemByIdTblogItem(idTblogItem);
    }
}
