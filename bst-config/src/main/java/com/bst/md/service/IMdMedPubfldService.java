package com.bst.md.service;

import java.util.List;
import java.util.Map;

import com.bst.common.core.domain.AjaxResult;
import com.bst.md.domain.MdMedPubfld;
import org.apache.ibatis.annotations.Param;

/**
 * 公共字段管理Service接口
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public interface IMdMedPubfldService 
{
    /**
     * 查询公共字段管理
     * 
     * @param idPubfld 公共字段管理主键
     * @return 公共字段管理
     */
    public MdMedPubfld selectMdMedPubfldByIdPubfld(String idPubfld);

    /**
     * 查询公共字段管理列表
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 公共字段管理集合
     */
    public List<MdMedPubfld> selectMdMedPubfldList(MdMedPubfld mdMedPubfld);

    /**
     * 新增公共字段管理
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 结果
     */
    public int insertMdMedPubfld(MdMedPubfld mdMedPubfld);

    /**
     * 修改公共字段管理
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 结果
     */
    public AjaxResult updateMdMedPubfld(MdMedPubfld mdMedPubfld);

    /**
     * 批量删除公共字段管理
     * 
     * @param idPubflds 需要删除的公共字段管理主键集合
     * @return 结果
     */
    public int deleteMdMedPubfldByIdPubflds(String idPubflds);

    /**
     * 删除公共字段管理信息
     * 
     * @param idPubfld 公共字段管理主键
     * @return 结果
     */
    public int deleteMdMedPubfldByIdPubfld(String idPubfld);

    List<MdMedPubfld> findByFldPrefix(String idPubfld);

    List<MdMedPubfld> findDimNotCreatTable(String str);

    Map<String,MdMedPubfld> findMapDataByIdList(List<String> idList);

    List<MdMedPubfld> findByIdList(@Param("list") List<String> idList);

    List<MdMedPubfld> findDimCreatTable(@Param("na")String na);


}
