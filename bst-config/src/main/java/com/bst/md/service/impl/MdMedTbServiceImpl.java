package com.bst.md.service.impl;

import java.util.List;

import com.bst.md.mapper.MdMedTbFldMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.md.mapper.MdMedTbMapper;
import com.bst.md.domain.MdMedTb;
import com.bst.md.service.IMdMedTbService;
import com.bst.common.core.text.Convert;

/**
 * 表管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-02
 */
@Service("mdMedTbService")
public class MdMedTbServiceImpl implements IMdMedTbService 
{
    @Autowired
    private MdMedTbMapper mdMedTbMapper;
    @Autowired
    private MdMedTbFldMapper mdMedTbFldMapper;

    /**
     * 查询表管理
     * 
     * @param idTb 表管理主键
     * @return 表管理
     */
    @Override
    public MdMedTb selectMdMedTbByIdTb(Long idTb)
    {
        return mdMedTbMapper.selectMdMedTbByIdTb(idTb);
    }

    @Override
    public List<MdMedTb> selectMdMedTbBySdTbca(String sdTbca) {
        MdMedTb tb = new MdMedTb();
        tb.setSdTbca(sdTbca);
        List<MdMedTb> tbs = this.selectMdMedTbList(tb);
        return tbs;
    }

    @Override
    public MdMedTb selectMdMedTbByCd(String cd) {
        MdMedTb tb = new MdMedTb();
        tb.setCd(cd);
        List<MdMedTb> tbs = this.selectMdMedTbList(tb);
        if(tbs.size()>0) return tbs.get(0);
        return null;
    }

    /**
     * 查询表管理列表
     * 
     * @param mdMedTb 表管理
     * @return 表管理
     */
    @Override
    public List<MdMedTb> selectMdMedTbList(MdMedTb mdMedTb)
    {
        return mdMedTbMapper.selectMdMedTbList(mdMedTb);
    }

    /**
     * 新增表管理
     * 
     * @param mdMedTb 表管理
     * @return 结果
     */
    @Override
    public int insertMdMedTb(MdMedTb mdMedTb)
    {
        return mdMedTbMapper.insertMdMedTb(mdMedTb);
    }

    /**
     * 修改表管理
     * 
     * @param mdMedTb 表管理
     * @return 结果
     */
    @Override
    public int updateMdMedTb(MdMedTb mdMedTb)
    {
        return mdMedTbMapper.updateMdMedTb(mdMedTb);
    }

    /**
     * 批量删除表管理
     * 
     * @param idTbs 需要删除的表管理主键
     * @return 结果
     */
    @Override
    public int deleteMdMedTbByIdTbs(String idTbs)
    {
        return mdMedTbMapper.deleteMdMedTbByIdTbs(Convert.toStrArray(idTbs));
    }

    /**
     * 删除表管理信息
     * 
     * @param idTb 表管理主键
     * @return 结果
     */
    @Override
    public int deleteMdMedTbByIdTb(Long idTb)
    {
        mdMedTbFldMapper.deleteMdMedTbFldByIdTb(idTb);
        return mdMedTbMapper.deleteMdMedTbByIdTb(idTb);
    }
}
