package com.bst.md.service;

import java.util.List;
import com.bst.md.domain.MdMedTblog;

/**
 * 元数据创建日志Service接口
 * 
 * @author ruoyi
 * @date 2022-07-17
 */
public interface IMdMedTblogService 
{
    /**
     * 查询元数据创建日志
     * 
     * @param idTblog 元数据创建日志主键
     * @return 元数据创建日志
     */
    public MdMedTblog selectMdMedTblogByIdTblog(Long idTblog);

    MdMedTblog selectMdMedTblogByCdTb(String cdTb);

    /**
     * 查询元数据创建日志列表
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 元数据创建日志集合
     */
    public List<MdMedTblog> selectMdMedTblogList(MdMedTblog mdMedTblog);

    /**
     * 新增元数据创建日志
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 结果
     */
    public int insertMdMedTblog(MdMedTblog mdMedTblog);

    /**
     * 修改元数据创建日志
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 结果
     */
    public int updateMdMedTblog(MdMedTblog mdMedTblog);

    /**
     * 批量删除元数据创建日志
     * 
     * @param idTblogs 需要删除的元数据创建日志主键集合
     * @return 结果
     */
    public int deleteMdMedTblogByIdTblogs(String idTblogs);

    /**
     * 删除元数据创建日志信息
     * 
     * @param idTblog 元数据创建日志主键
     * @return 结果
     */
    public int deleteMdMedTblogByIdTblog(Long idTblog);
}
