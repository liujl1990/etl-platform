package com.bst.md.service;

import com.bst.common.vo.DimDataVO;
import com.bst.common.vo.JobDimVO;
import com.bst.base.domain.BaseMsg;
import com.bst.md.domain.MdMedPubfld;
import com.bst.md.domain.MdMedTbFld;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * mdMedDim服务类
 * Created by hozeData on 2021/07/05.
 */
public interface IMdMedDimService {

    Map<String, JobDimVO> findAllDimData(List<MdMedPubfld> pubfldList);

    List<BaseMsg> dimDataCheck(Map<String, Map<String,Object>> dataMap, Map<String, JobDimVO> jobDimVOMap);

    Map<String,String> findDimCdAndNa(String tb);

    List<BaseMsg> insertDimDatas(Map<String,List<DimDataVO>> dimDataVOMap) ;

    List<Map<String, Object>> findDimDataByTbname(List<MdMedTbFld> fldList, String tableName);

    List<Map<String, Object>> findDimDataByTbname(List<MdMedTbFld> fldList, String tableName,String na);

    List<DimDataVO> queryDimData(String dimTbName);

    List<DimDataVO> queryDimDataWithOrder(@Param("tb") String tb);

    /**
     * 日期查询
     * @param mm
     * @return
     */
    List<Map<String,Object>> queryDayData(@Param("mm")String mm);
    List<Map<String,Object>>  queryHisDeptData();

    int remove(String tableName,String cd);
}
