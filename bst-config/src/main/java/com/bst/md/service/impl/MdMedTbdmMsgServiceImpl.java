package com.bst.md.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.md.mapper.MdMedTbdmMsgMapper;
import com.bst.md.domain.MdMedTbdmMsg;
import com.bst.md.service.IMdMedTbdmMsgService;
import com.bst.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-25
 */
@Service
public class MdMedTbdmMsgServiceImpl implements IMdMedTbdmMsgService 
{
    @Autowired
    private MdMedTbdmMsgMapper mdMedTbdmMsgMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param idTbdmMsg 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public MdMedTbdmMsg selectMdMedTbdmMsgByIdTbdmMsg(Long idTbdmMsg)
    {
        return mdMedTbdmMsgMapper.selectMdMedTbdmMsgByIdTbdmMsg(idTbdmMsg);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param mdMedTbdmMsg 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MdMedTbdmMsg> selectMdMedTbdmMsgList(MdMedTbdmMsg mdMedTbdmMsg)
    {
        return mdMedTbdmMsgMapper.selectMdMedTbdmMsgList(mdMedTbdmMsg);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param mdMedTbdmMsg 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMdMedTbdmMsg(MdMedTbdmMsg mdMedTbdmMsg)
    {
        return mdMedTbdmMsgMapper.insertMdMedTbdmMsg(mdMedTbdmMsg);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param mdMedTbdmMsg 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMdMedTbdmMsg(MdMedTbdmMsg mdMedTbdmMsg)
    {
        return mdMedTbdmMsgMapper.updateMdMedTbdmMsg(mdMedTbdmMsg);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param idTbdmMsgs 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMdMedTbdmMsgByIdTbdmMsgs(String idTbdmMsgs)
    {
        return mdMedTbdmMsgMapper.deleteMdMedTbdmMsgByIdTbdmMsgs(Convert.toStrArray(idTbdmMsgs));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param idTbdmMsg 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteMdMedTbdmMsgByIdTbdmMsg(Long idTbdmMsg)
    {
        return mdMedTbdmMsgMapper.deleteMdMedTbdmMsgByIdTbdmMsg(idTbdmMsg);
    }
}
