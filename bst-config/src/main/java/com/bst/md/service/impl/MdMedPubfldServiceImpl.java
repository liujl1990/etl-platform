package com.bst.md.service.impl;

import java.util.*;

import com.bst.common.constant.JobConstant;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.self.LoginAPIUtils;
import com.bst.md.domain.MdMedTb;
import com.bst.md.domain.MdMedTbFld;
import com.bst.system.service.ICommonTableDDLService;
import com.bst.md.service.IMdMedTbFldService;
import com.bst.md.service.IMdMedTbService;
import com.bst.system.vo.AlterTableParamVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.md.mapper.MdMedPubfldMapper;
import com.bst.md.domain.MdMedPubfld;
import com.bst.md.service.IMdMedPubfldService;
import com.bst.common.core.text.Convert;

/**
 * 公共字段管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Service("pubfldService")
public class MdMedPubfldServiceImpl implements IMdMedPubfldService 
{
    @Autowired
    private MdMedPubfldMapper mdMedPubfldMapper;

    /**
     * 查询公共字段管理
     * 
     * @param idPubfld 公共字段管理主键
     * @return 公共字段管理
     */
    @Override
    public MdMedPubfld selectMdMedPubfldByIdPubfld(String idPubfld)
    {
        return mdMedPubfldMapper.selectMdMedPubfldByIdPubfld(idPubfld);
    }

    /**
     * 查询公共字段管理列表
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 公共字段管理
     */
    @Override
    public List<MdMedPubfld> selectMdMedPubfldList(MdMedPubfld mdMedPubfld)
    {
        return mdMedPubfldMapper.selectMdMedPubfldList(mdMedPubfld);
    }

    /**
     * 新增公共字段管理
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 结果
     */
    @Override
    public int insertMdMedPubfld(MdMedPubfld mdMedPubfld)
    {
        mdMedPubfld.setDtSysCre(new Date());
        mdMedPubfld.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        AjaxResult result = createDefaultTable(null,mdMedPubfld);
        if(AjaxResult.isSuccess(result)) {
            return mdMedPubfldMapper.insertMdMedPubfld(mdMedPubfld);
        } else {
            return 0;
        }
    }

    /**
     * 修改公共字段管理
     * 
     * @param mdMedPubfld 公共字段管理
     * @return 结果
     */
    @Override
    public AjaxResult updateMdMedPubfld(MdMedPubfld mdMedPubfld)
    {
        MdMedPubfld oldFld = this.selectMdMedPubfldByIdPubfld(mdMedPubfld.getIdPubfld());
        AjaxResult result = createDefaultTable(oldFld,mdMedPubfld);
        if(AjaxResult.isSuccess(result)) {
            mdMedPubfld.setDtSysModi(new Date());
            mdMedPubfld.setNaEmpModi(LoginAPIUtils.getLoginUsename());
            mdMedPubfldMapper.updateMdMedPubfld(mdMedPubfld);
            return AjaxResult.success();
        } else {
            return result;
        }
    }


    @Autowired
    private ICommonTableDDLService commonTableDDLService;
    @Autowired
    IMdMedTbService mdMedTbService;
    @Autowired
    IMdMedTbFldService mdMedTbFldService;

    private AjaxResult createDefaultTable(MdMedPubfld oldMdMedPubfld, MdMedPubfld mdMedPubfld) {
        String oldTable = oldMdMedPubfld==null?null:oldMdMedPubfld.getCdTbDim();
        String newTable = mdMedPubfld.getCdTbDim();
        oldTable = StringUtils.isBlank(oldTable)?null:oldTable;
        newTable = StringUtils.isBlank(newTable)?null:newTable;
        if("0".equals(mdMedPubfld.getSdDimtp())) {
            return AjaxResult.success();
        } else if(oldTable==null && newTable==null) {
            return AjaxResult.success();
        }  else if(newTable!=null && newTable.equals(oldTable)){
            return AjaxResult.success();
        }  else if(newTable==null && oldTable!=null){ //删除
            AjaxResult baseResult = commonTableDDLService.dropTableByMedOtherDB(oldMdMedPubfld.getCdTbDim(), null, JobConstant.DB_CLS_DW);
            return baseResult;
        }  else if(newTable!=null ){ //新建或修改
            MdMedTb mdMedTb = mdMedTbService.selectMdMedTbByCd(mdMedPubfld.getCdTbDim());
            MdMedTb OldMdMedTb = null;
            if(oldMdMedPubfld!=null) {
                OldMdMedTb = mdMedTbService.selectMdMedTbByCd(oldMdMedPubfld.getCdTbDim());
            }
            if (oldMdMedPubfld != null && OldMdMedTb != null
                    && !mdMedPubfld.getCdTbDim().equals(oldMdMedPubfld.getCdTbDim()) && !StringUtils.isEmpty(oldMdMedPubfld.getCdTbDim())) {
                //修改表名
                String oldTableName = oldMdMedPubfld.getCdTbDim();
                OldMdMedTb.setCd(mdMedPubfld.getCdTbDim());
                MdMedTbFld mdMedTbFldModel = new MdMedTbFld();
                mdMedTbFldModel.setIdTb(OldMdMedTb.getIdTb());
                List<MdMedTbFld> mdMedTbFlds = mdMedTbFldService.selectMdMedTbFldList(mdMedTbFldModel);
                return commonTableDDLService.alterTableByMedOtherDB(new AlterTableParamVO(OldMdMedTb, mdMedTbFlds, oldTableName),JobConstant.DB_CLS_DW);
            } else if (mdMedTb == null) {
                //新建表
                mdMedTb = new MdMedTb();
                mdMedTb.setCd(mdMedPubfld.getCdTbDim());
                mdMedTb.setNa(mdMedPubfld.getNa());
                mdMedTb.setSdTbca(JobConstant.SD_TBCA_DIM);
                List<MdMedTbFld> mdMedTbFlds = this.generateDefaultFld(mdMedPubfld);
                return commonTableDDLService.alterTableByMedOtherDB(new AlterTableParamVO(mdMedTb, mdMedTbFlds, null),JobConstant.DB_CLS_DW);
            }
        }
        return AjaxResult.success("操作成功");
    }

    /**
     * 批量删除公共字段管理
     * 
     * @param idPubflds 需要删除的公共字段管理主键
     * @return 结果
     */
    @Override
    public int deleteMdMedPubfldByIdPubflds(String idPubflds)
    {
        return mdMedPubfldMapper.deleteMdMedPubfldByIdPubflds(Convert.toStrArray(idPubflds));
    }

    /**
     * 删除公共字段管理信息
     * 
     * @param idPubfld 公共字段管理主键
     * @return 结果
     */
    @Override
    public int deleteMdMedPubfldByIdPubfld(String idPubfld)
    {
        return mdMedPubfldMapper.deleteMdMedPubfldByIdPubfld(idPubfld);
    }

    @Override
    public List<MdMedPubfld> findByFldPrefix(String idPubfld) {
        return mdMedPubfldMapper.findByFldPrefix(idPubfld);
    }

    @Override
    public List<MdMedPubfld> findDimNotCreatTable(String str) {
        return mdMedPubfldMapper.findDimNotCreatTable(str);
    }

    @Override
    public Map<String, MdMedPubfld> findMapDataByIdList(List<String> idList) {
        List<MdMedPubfld> list = mdMedPubfldMapper.findByIdList(idList);
        Map<String, MdMedPubfld> map = new HashMap<>();
        for (MdMedPubfld fld : list) {
            map.put(fld.getIdPubfld(), fld);
        }
        return map;
    }

    @Override
    public List<MdMedPubfld> findByIdList(List<String> idList) {
        return mdMedPubfldMapper.findByIdList(idList);
    }

    @Override
    public List<MdMedPubfld> findDimCreatTable(String na) {
        return mdMedPubfldMapper.findDimCreatTable(na);
    }

    public static final String DEFAULT_DIM_COLUMN_CD = "cd,编码";
    public static final String DEFAULT_DIM_COLUMN_NA = "na,名称";
    public static final String DEFAULT_DIM_COLUMN_DES = "des,备注";
    public static final String DEFAULT_DIM_COLUMN_EUTP = "eu_tp,来源类型"; // 0 手工导入  1 自动插入
    public static final String DEFAULT_DIM_COLUMN_ORG = "id_dim_org,机构";
    private List<MdMedTbFld> generateDefaultFld(MdMedPubfld mdMedPubfld) {
        List<MdMedTbFld> mdMedTbFlds = new ArrayList();
        MdMedTbFld one = new MdMedTbFld();
        one.setIdPubfld(DEFAULT_DIM_COLUMN_CD.split(",")[0]);
        one.setNaPubfld(DEFAULT_DIM_COLUMN_CD.split(",")[1]);
        one.setSdConstp(JobConstant.SD_CONSTP_PK);
        one.setNumLth(mdMedPubfld.getNumLth());
        one.setNumPrec(mdMedPubfld.getNumPrec());
        one.setEuJavatp(mdMedPubfld.getEuJavatp());
        one.setNaJavatp(mdMedPubfld.getNaJavatp());
        mdMedTbFlds.add(one);
        one = new MdMedTbFld();
        one.setIdPubfld(DEFAULT_DIM_COLUMN_NA.split(",")[0]);
        one.setNaPubfld(DEFAULT_DIM_COLUMN_NA.split(",")[1]);
        one.setNumLth(100);
        one.setNumPrec(0);
        one.setEuJavatp(12);
        one.setNaJavatp("VARCHAR");
        mdMedTbFlds.add(one);
        one = new MdMedTbFld();
        one.setIdPubfld(DEFAULT_DIM_COLUMN_DES.split(",")[0]);
        one.setNaPubfld(DEFAULT_DIM_COLUMN_DES.split(",")[1]);
        one.setNumLth(200);
        one.setNumPrec(0);
        one.setEuJavatp(12);
        one.setNaJavatp("VARCHAR");
        mdMedTbFlds.add(one);
        one = new MdMedTbFld();
        one.setIdPubfld(DEFAULT_DIM_COLUMN_EUTP.split(",")[0]);
        one.setNaPubfld(DEFAULT_DIM_COLUMN_EUTP.split(",")[1]);
        one.setNumLth(0);
        one.setNumPrec(0);
        one.setEuJavatp(4);
        one.setNaJavatp("INTEGER");
        mdMedTbFlds.add(one);
        one = new MdMedTbFld();
        one.setIdPubfld(DEFAULT_DIM_COLUMN_ORG.split(",")[0]);
        one.setNaPubfld(DEFAULT_DIM_COLUMN_ORG.split(",")[1]);
        one.setNumLth(50);
        one.setNumPrec(0);
        one.setEuJavatp(12);
        one.setNaJavatp("VARCHAR");
        mdMedTbFlds.add(one);
        return mdMedTbFlds;
    }
}
