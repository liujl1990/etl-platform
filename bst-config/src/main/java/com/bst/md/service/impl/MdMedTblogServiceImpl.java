package com.bst.md.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.bst.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.bst.md.domain.MdMedTblogItem;
import com.bst.md.mapper.MdMedTblogMapper;
import com.bst.md.domain.MdMedTblog;
import com.bst.md.service.IMdMedTblogService;
import com.bst.common.core.text.Convert;

/**
 * 元数据创建日志Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-17
 */
@Service
public class MdMedTblogServiceImpl implements IMdMedTblogService 
{
    @Autowired
    private MdMedTblogMapper mdMedTblogMapper;

    /**
     * 查询元数据创建日志
     * 
     * @param idTblog 元数据创建日志主键
     * @return 元数据创建日志
     */
    @Override
    public MdMedTblog selectMdMedTblogByIdTblog(Long idTblog)
    {
        return mdMedTblogMapper.selectMdMedTblogByIdTblog(idTblog);
    }

    @Override
    public MdMedTblog selectMdMedTblogByCdTb(String cdTb) {
        MdMedTblog tblog = new MdMedTblog();
        tblog.setCdTb(cdTb);
        List<MdMedTblog> tblogs = this.selectMdMedTblogList(tblog);
        if(tblogs.size()>0) return tblogs.get(0);
        return null;
    }

    /**
     * 查询元数据创建日志列表
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 元数据创建日志
     */
    @Override
    public List<MdMedTblog> selectMdMedTblogList(MdMedTblog mdMedTblog)
    {
        return mdMedTblogMapper.selectMdMedTblogList(mdMedTblog);
    }

    /**
     * 新增元数据创建日志
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 结果
     */
    @Transactional
    @Override
    public int insertMdMedTblog(MdMedTblog mdMedTblog)
    {
        int rows = mdMedTblogMapper.insertMdMedTblog(mdMedTblog);
        insertMdMedTblogItem(mdMedTblog);
        return rows;
    }

    /**
     * 修改元数据创建日志
     * 
     * @param mdMedTblog 元数据创建日志
     * @return 结果
     */
    @Transactional
    @Override
    public int updateMdMedTblog(MdMedTblog mdMedTblog)
    {
        mdMedTblogMapper.deleteMdMedTblogItemByIdTblog(mdMedTblog.getIdTblog());
        insertMdMedTblogItem(mdMedTblog);
        return mdMedTblogMapper.updateMdMedTblog(mdMedTblog);
    }

    /**
     * 批量删除元数据创建日志
     * 
     * @param idTblogs 需要删除的元数据创建日志主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteMdMedTblogByIdTblogs(String idTblogs)
    {
        mdMedTblogMapper.deleteMdMedTblogItemByIdTblogs(Convert.toStrArray(idTblogs));
        return mdMedTblogMapper.deleteMdMedTblogByIdTblogs(Convert.toStrArray(idTblogs));
    }

    /**
     * 删除元数据创建日志信息
     * 
     * @param idTblog 元数据创建日志主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteMdMedTblogByIdTblog(Long idTblog)
    {
        mdMedTblogMapper.deleteMdMedTblogItemByIdTblog(idTblog);
        return mdMedTblogMapper.deleteMdMedTblogByIdTblog(idTblog);
    }

    /**
     * 新增元数据日志明细信息
     * 
     * @param mdMedTblog 元数据创建日志对象
     */
    public void insertMdMedTblogItem(MdMedTblog mdMedTblog)
    {
        List<MdMedTblogItem> mdMedTblogItemList = mdMedTblog.getMdMedTblogItemList();
        Long idTblog = mdMedTblog.getIdTblog();
        if (StringUtils.isNotNull(mdMedTblogItemList))
        {
            List<MdMedTblogItem> list = new ArrayList<MdMedTblogItem>();
            for (MdMedTblogItem mdMedTblogItem : mdMedTblogItemList)
            {
                mdMedTblogItem.setIdTblog(idTblog);
                list.add(mdMedTblogItem);
            }
            if (list.size() > 0)
            {
                mdMedTblogMapper.batchMdMedTblogItem(list);
            }
        }
    }
}
