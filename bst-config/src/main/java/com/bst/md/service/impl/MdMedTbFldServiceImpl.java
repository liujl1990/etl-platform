package com.bst.md.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bst.md.mapper.MdMedTbFldMapper;
import com.bst.md.domain.MdMedTbFld;
import com.bst.md.service.IMdMedTbFldService;
import com.bst.common.core.text.Convert;

/**
 * 元数据字段Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-02
 */
@Service
public class MdMedTbFldServiceImpl implements IMdMedTbFldService 
{
    @Autowired
    private MdMedTbFldMapper mdMedTbFldMapper;

    /**
     * 查询元数据字段
     * 
     * @param idTbFld 元数据字段主键
     * @return 元数据字段
     */
    @Override
    public MdMedTbFld selectMdMedTbFldByIdTbFld(Long idTbFld)
    {
        return mdMedTbFldMapper.selectMdMedTbFldByIdTbFld(idTbFld);
    }

    /**
     * 查询元数据字段列表
     * 
     * @param mdMedTbFld 元数据字段
     * @return 元数据字段
     */
    @Override
    public List<MdMedTbFld> selectMdMedTbFldList(MdMedTbFld mdMedTbFld)
    {
        return mdMedTbFldMapper.selectMdMedTbFldList(mdMedTbFld);
    }

    /**
     * 新增元数据字段
     * 
     * @param mdMedTbFld 元数据字段
     * @return 结果
     */
    @Override
    public int insertMdMedTbFld(MdMedTbFld mdMedTbFld)
    {
        return mdMedTbFldMapper.insertMdMedTbFld(mdMedTbFld);
    }

    /**
     * 修改元数据字段
     * 
     * @param mdMedTbFld 元数据字段
     * @return 结果
     */
    @Override
    public int updateMdMedTbFld(MdMedTbFld mdMedTbFld)
    {
        return mdMedTbFldMapper.updateMdMedTbFld(mdMedTbFld);
    }

    /**
     * 批量删除元数据字段
     * 
     * @param idTbFlds 需要删除的元数据字段主键
     * @return 结果
     */
    @Override
    public int deleteMdMedTbFldByIdTbFlds(String idTbFlds)
    {
        return mdMedTbFldMapper.deleteMdMedTbFldByIdTbFlds(Convert.toStrArray(idTbFlds));
    }

    /**
     * 删除元数据字段信息
     * 
     * @param idTbFld 元数据字段主键
     * @return 结果
     */
    @Override
    public int deleteMdMedTbFldByIdTbFld(Long idTbFld)
    {
        return mdMedTbFldMapper.deleteMdMedTbFldByIdTbFld(idTbFld);
    }

    @Override
    public int deleteMdMedTbFldByIdTb(Long idTb) {
        return mdMedTbFldMapper.deleteMdMedTbFldByIdTb(idTb);
    }
}
