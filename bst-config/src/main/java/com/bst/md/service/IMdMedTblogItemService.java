package com.bst.md.service;

import java.util.List;
import com.bst.md.domain.MdMedTblogItem;

/**
 * 元数据日志明细Service接口
 * 
 * @author ruoyi
 * @date 2022-07-17
 */
public interface IMdMedTblogItemService 
{
    /**
     * 查询元数据日志明细
     * 
     * @param idTblogItem 元数据日志明细主键
     * @return 元数据日志明细
     */
    public MdMedTblogItem selectMdMedTblogItemByIdTblogItem(Long idTblogItem);

    /**
     * 查询元数据日志明细列表
     * 
     * @param mdMedTblogItem 元数据日志明细
     * @return 元数据日志明细集合
     */
    public List<MdMedTblogItem> selectMdMedTblogItemList(MdMedTblogItem mdMedTblogItem);

    /**
     * 新增元数据日志明细
     * 
     * @param mdMedTblogItem 元数据日志明细
     * @return 结果
     */
    public int insertMdMedTblogItem(MdMedTblogItem mdMedTblogItem);

    /**
     * 修改元数据日志明细
     * 
     * @param mdMedTblogItem 元数据日志明细
     * @return 结果
     */
    public int updateMdMedTblogItem(MdMedTblogItem mdMedTblogItem);

    /**
     * 批量删除元数据日志明细
     * 
     * @param idTblogItems 需要删除的元数据日志明细主键集合
     * @return 结果
     */
    public int deleteMdMedTblogItemByIdTblogItems(String idTblogItems);

    /**
     * 删除元数据日志明细信息
     * 
     * @param idTblogItem 元数据日志明细主键
     * @return 结果
     */
    public int deleteMdMedTblogItemByIdTblogItem(Long idTblogItem);
}
