package com.bst.web.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.bst.common.utils.StringUtils;
import com.bst.common.vo.DimDataVO;
import com.bst.base.domain.BaseDimComp;
import com.bst.base.service.IBaseDimCompService;
import com.bst.md.service.IMdMedDimService;
import com.bst.md.service.IMdMedPubfldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.base.domain.BaseDimCompResult;
import com.bst.base.service.IBaseDimCompResultService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 维度对照结果Controller
 * 
 * @author laoliu
 * @date 2023-02-12
 */
@Controller
@RequestMapping("/base/dim/comp/result")
public class BaseDimCompResultController extends BaseController
{
    private String prefix = "system/result";

    @Autowired
    private IBaseDimCompResultService baseDimCompResultService;
    @Autowired
    private IBaseDimCompService baseDimCompService;
    @Autowired
    private IMdMedDimService mdMedDimService;
    @Autowired
    IMdMedPubfldService pubfldService;


    @GetMapping()
    public String result()
    {
        return prefix + "/result";
    }

    /**
     * 查询维度对照结果列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseDimCompResult baseDimCompResult)
    {
        startPage();
        List<BaseDimCompResult> list = baseDimCompResultService.selectBaseDimCompResultList(baseDimCompResult);
        return getDataTable(list);
    }

    /**
     * 查询维度对照结果列表
     */
    @PostMapping("/queryByPid/{pid}")
    @ResponseBody
    public TableDataInfo queryByPid(@PathVariable("pid") Long pid, ModelMap mmap)
    {
        BaseDimComp comp = baseDimCompService.selectBaseDimCompByIdDimComp(pid);
        BaseDimCompResult result = new BaseDimCompResult();
        String dimTbSource = comp.getDimTbSource();
        result.setDimTbSource(dimTbSource);
        result.setIdPubfldTar(comp.getIdPubfldTar());
        List<BaseDimCompResult> results = baseDimCompResultService.selectBaseDimCompResultList(result);
        Map<String,BaseDimCompResult> dataMap = results.stream().collect(Collectors.toMap(BaseDimCompResult::getCdSource,e->e));
        List<DimDataVO> dataVOS = mdMedDimService.queryDimData(dimTbSource);
        List<BaseDimCompResult> resultList = new ArrayList<>();
        for (DimDataVO dataVO : dataVOS) {
            result = dataMap.get(dataVO.getCd());
            if(result==null) {
                result = new BaseDimCompResult();
                result.setCdSource(dataVO.getCd());
                result.setNaSource(dataVO.getNa());
                result.setDimTbSource(dimTbSource);
                result.setIdPubfldTar(comp.getIdPubfldTar());
            }
            resultList.add(result);
        }
        return getDataTable(resultList);
    }



    /**
     * 导出维度对照结果列表
     */
    @Log(title = "维度对照结果", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseDimCompResult baseDimCompResult)
    {
        List<BaseDimCompResult> list = baseDimCompResultService.selectBaseDimCompResultList(baseDimCompResult);
        ExcelUtil<BaseDimCompResult> util = new ExcelUtil<BaseDimCompResult>(BaseDimCompResult.class);
        return util.exportExcel(list, "维度对照结果数据");
    }

    /**
     * 修改保存维度对照结果
     */
    @Log(title = "维度对照结果", businessType = BusinessType.UPDATE)
    @PostMapping("/save")
    @ResponseBody
    public AjaxResult save(@RequestBody BaseDimCompResult baseDimCompResult)
    {
        BaseDimCompResult model = new BaseDimCompResult();
        model.setDimTbSource(baseDimCompResult.getDimTbSource());
        model.setIdPubfldTar(baseDimCompResult.getIdPubfldTar());
        model.setCdSource(baseDimCompResult.getCdSource());
        List<BaseDimCompResult> results = baseDimCompResultService.selectBaseDimCompResultList(model);
        if(results.size()>0) {
            Long id = results.get(0).getIdDimCompResult();
            if(StringUtils.isEmpty(baseDimCompResult.getCdTar())) {
                return toAjax(baseDimCompResultService.deleteBaseDimCompResultByIdDimCompResults(id+""));
            }
            baseDimCompResult.setIdDimCompResult(id);
            return toAjax(baseDimCompResultService.updateBaseDimCompResult(baseDimCompResult));
        } else {
            return toAjax(baseDimCompResultService.insertBaseDimCompResult(baseDimCompResult));
        }
    }

    /**
     * 删除维度对照结果
     */
    @Log(title = "维度对照结果", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseDimCompResultService.deleteBaseDimCompResultByIdDimCompResults(ids));
    }
}
