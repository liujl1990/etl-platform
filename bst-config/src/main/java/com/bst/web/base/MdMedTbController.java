package com.bst.web.base;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.md.domain.MdMedTb;
import com.bst.md.service.IMdMedTbService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 表管理Controller
 * 
 * @author ruoyi
 * @date 2022-07-02
 */
@Controller
@RequestMapping("/md/med/tb")
public class MdMedTbController extends BaseController
{
    private String prefix = "system/md/med/tb";

    @Autowired
    private IMdMedTbService mdMedTbService;

    @GetMapping()
    public String tb()
    {
        return prefix + "/medTb";
    }

    @GetMapping("/detail/{idTb}")
    public String detail(@PathVariable("idTb") Long idTb, ModelMap mmap)
    {
        MdMedTb tb = mdMedTbService.selectMdMedTbByIdTb(idTb);

        return prefix + "/detail";
    }

    /**
     * 查询表管理列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MdMedTb mdMedTb)
    {
        startPage();
        List<MdMedTb> list = mdMedTbService.selectMdMedTbList(mdMedTb);
        return getDataTable(list);
    }

    /**
     * 导出表管理列表
     */
    @Log(title = "表管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MdMedTb mdMedTb)
    {
        List<MdMedTb> list = mdMedTbService.selectMdMedTbList(mdMedTb);
        ExcelUtil<MdMedTb> util = new ExcelUtil<MdMedTb>(MdMedTb.class);
        return util.exportExcel(list, "表管理数据");
    }

    /**
     * 新增表管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存表管理
     */
    @RequiresPermissions("system:tb:add")
    @Log(title = "表管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MdMedTb mdMedTb)
    {
        return toAjax(mdMedTbService.insertMdMedTb(mdMedTb));
    }

    /**
     * 修改表管理
     */
    @GetMapping("/edit/{idTb}")
    public String edit(@PathVariable("idTb") Long idTb, ModelMap mmap)
    {
        MdMedTb mdMedTb = mdMedTbService.selectMdMedTbByIdTb(idTb);
        mmap.put("mdMedTb", mdMedTb);
        return prefix + "/edit";
    }

    /**
     * 修改保存表管理
     */
    @Log(title = "表管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MdMedTb mdMedTb)
    {
        return toAjax(mdMedTbService.updateMdMedTb(mdMedTb));
    }

    /**
     * 删除表管理
     */
    @Log(title = "表管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mdMedTbService.deleteMdMedTbByIdTbs(ids));
    }
}
