package com.bst.web.base;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.md.domain.MdMedTbdmMsg;
import com.bst.md.service.IMdMedTbdmMsgService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-04-25
 */
@Controller
@RequestMapping("/md/med/tbdm/msg")
public class MdMedTbdmMsgController extends BaseController
{
    private String prefix = "system/msg";

    @Autowired
    private IMdMedTbdmMsgService mdMedTbdmMsgService;

    @RequiresPermissions("system:msg:view")
    @GetMapping()
    public String msg()
    {
        return prefix + "/msg";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("system:msg:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MdMedTbdmMsg mdMedTbdmMsg)
    {
        startPage();
        List<MdMedTbdmMsg> list = mdMedTbdmMsgService.selectMdMedTbdmMsgList(mdMedTbdmMsg);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("system:msg:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MdMedTbdmMsg mdMedTbdmMsg)
    {
        List<MdMedTbdmMsg> list = mdMedTbdmMsgService.selectMdMedTbdmMsgList(mdMedTbdmMsg);
        ExcelUtil<MdMedTbdmMsg> util = new ExcelUtil<MdMedTbdmMsg>(MdMedTbdmMsg.class);
        return util.exportExcel(list, "【请填写功能名称】数据");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("system:msg:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MdMedTbdmMsg mdMedTbdmMsg)
    {
        return toAjax(mdMedTbdmMsgService.insertMdMedTbdmMsg(mdMedTbdmMsg));
    }

    /**
     * 修改【请填写功能名称】
     */
    @RequiresPermissions("system:msg:edit")
    @GetMapping("/edit/{idTbdmMsg}")
    public String edit(@PathVariable("idTbdmMsg") Long idTbdmMsg, ModelMap mmap)
    {
        MdMedTbdmMsg mdMedTbdmMsg = mdMedTbdmMsgService.selectMdMedTbdmMsgByIdTbdmMsg(idTbdmMsg);
        mmap.put("mdMedTbdmMsg", mdMedTbdmMsg);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("system:msg:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MdMedTbdmMsg mdMedTbdmMsg)
    {
        return toAjax(mdMedTbdmMsgService.updateMdMedTbdmMsg(mdMedTbdmMsg));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("system:msg:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mdMedTbdmMsgService.deleteMdMedTbdmMsgByIdTbdmMsgs(ids));
    }
}
