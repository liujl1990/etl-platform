package com.bst.web.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.base.domain.BaseDwQryParam;
import com.bst.base.service.IBaseDwQryParamService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * DW查询配置Controller
 * 
 * @author laoliu
 * @date 2023-02-12
 */
@Controller
@RequestMapping("/base/dw/qry")
public class BaseDwQryParamController extends BaseController
{
    private String prefix = "system/base/dw_param";

    @Autowired
    private IBaseDwQryParamService baseDwQryParamService;

    @GetMapping()
    public String param()
    {
        return prefix + "/param";
    }

    /**
     * 查询DW查询配置列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseDwQryParam baseDwQryParam)
    {
        startPage();
        List<BaseDwQryParam> list = baseDwQryParamService.selectBaseDwQryParamList(baseDwQryParam);
        return getDataTable(list);
    }

    /**
     * 导出DW查询配置列表
     */
    @Log(title = "DW查询配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseDwQryParam baseDwQryParam)
    {
        List<BaseDwQryParam> list = baseDwQryParamService.selectBaseDwQryParamList(baseDwQryParam);
        ExcelUtil<BaseDwQryParam> util = new ExcelUtil<BaseDwQryParam>(BaseDwQryParam.class);
        return util.exportExcel(list, "DW查询配置数据");
    }

    /**
     * 新增DW查询配置
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存DW查询配置
     */
    @Log(title = "DW查询配置", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseDwQryParam baseDwQryParam)
    {
        return toAjax(baseDwQryParamService.insertBaseDwQryParam(baseDwQryParam));
    }

    /**
     * 修改DW查询配置
     */
    @GetMapping("/edit/{idDwQryParam}")
    public String edit(@PathVariable("idDwQryParam") Long idDwQryParam, ModelMap mmap)
    {
        BaseDwQryParam baseDwQryParam = baseDwQryParamService.selectBaseDwQryParamByIdDwQryParam(idDwQryParam);
        mmap.put("baseDwQryParam", baseDwQryParam);
        return prefix + "/edit";
    }

    /**
     * 修改保存DW查询配置
     */
    @Log(title = "DW查询配置", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseDwQryParam baseDwQryParam)
    {
        return toAjax(baseDwQryParamService.updateBaseDwQryParam(baseDwQryParam));
    }

    /**
     * 删除DW查询配置
     */
    @Log(title = "DW查询配置", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseDwQryParamService.deleteBaseDwQryParamByIdDwQryParams(ids));
    }
}
