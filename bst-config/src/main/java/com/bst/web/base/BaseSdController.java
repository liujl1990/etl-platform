package com.bst.web.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.base.domain.BaseSd;
import com.bst.base.service.IBaseSdService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 系统字典Controller
 * 
 * @author ruoyi
 * @date 2022-05-26
 */
@Controller
@RequestMapping("/base/sd")
public class BaseSdController extends BaseController
{
    private String prefix = "system/base/sd";

    @Autowired
    private IBaseSdService baseSdService;

    @GetMapping()
    public String sd()
    {
        return prefix + "/sd";
    }

    /**
     * 查询系统字典列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseSd baseSd)
    {
        startPage();
        List<BaseSd> list = baseSdService.selectBaseSdList(baseSd);
        return getDataTable(list);
    }

    /**
     * 导出系统字典列表
     */
    @Log(title = "系统字典", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseSd baseSd)
    {
        List<BaseSd> list = baseSdService.selectBaseSdList(baseSd);
        ExcelUtil<BaseSd> util = new ExcelUtil<BaseSd>(BaseSd.class);
        return util.exportExcel(list, "系统字典数据");
    }

    /**
     * 新增系统字典
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存系统字典
     */
    @Log(title = "系统字典", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseSd baseSd)
    {
        return toAjax(baseSdService.insertBaseSd(baseSd));
    }

    /**
     * 修改系统字典
     */
    @GetMapping("/edit/{cd}")
    public String edit(@PathVariable("cd") String cd, ModelMap mmap)
    {
        BaseSd baseSd = baseSdService.selectBaseSdByCd(cd);
        mmap.put("baseSd", baseSd);
        return prefix + "/edit";
    }

    /**
     * 修改保存系统字典
     */
    @Log(title = "系统字典", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseSd baseSd)
    {
        return toAjax(baseSdService.updateBaseSd(baseSd));
    }

    /**
     * 删除系统字典
     */
    @Log(title = "系统字典", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseSdService.deleteBaseSdByCds(ids));
    }
}
