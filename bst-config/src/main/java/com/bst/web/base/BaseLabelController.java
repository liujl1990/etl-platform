package com.bst.web.base;

import java.util.List;

import com.bst.common.core.domain.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.system.domain.BaseLabel;
import com.bst.base.service.IBaseLabelService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;

/**
 * 指标标签Controller
 * 
 * @author 老刘
 * @date 2023-02-18
 */
@Controller
@RequestMapping("/base/label")
public class BaseLabelController extends BaseController
{
    private String prefix = "system/base/label";

    @Autowired
    private IBaseLabelService baseLabelService;

    @GetMapping()
    public String label()
    {
        return prefix + "/label";
    }

    /**
     * 查询指标标签列表
     */
    @PostMapping("/list")
    @ResponseBody
    public List<BaseLabel> list(BaseLabel mdIndexLabel)
    {
        List<BaseLabel> list = baseLabelService.selectBaseLabelList(mdIndexLabel);
        return list;
    }

    /**
     * 导出指标标签列表
     */
    @Log(title = "指标标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseLabel mdIndexLabel)
    {
        List<BaseLabel> list = baseLabelService.selectBaseLabelList(mdIndexLabel);
        ExcelUtil<BaseLabel> util = new ExcelUtil<BaseLabel>(BaseLabel.class);
        return util.exportExcel(list, "指标标签数据");
    }

    /**
     * 新增指标标签
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存指标标签
     */
    @Log(title = "指标标签", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseLabel mdIndexLabel)
    {
        return toAjax(baseLabelService.insertBaseLabel(mdIndexLabel));
    }

    /**
     * 修改指标标签
     */
    @GetMapping("/edit/{idLabel}")
    public String edit(@PathVariable("idLabel") Long idLabel, ModelMap mmap)
    {
        BaseLabel mdIndexLabel = baseLabelService.selectBaseLabelByIdLabel(idLabel);
        mmap.put("mdIndexLabel", mdIndexLabel);
        return prefix + "/edit";
    }

    /**
     * 修改保存指标标签
     */
    @Log(title = "指标标签", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseLabel mdIndexLabel)
    {
        return toAjax(baseLabelService.updateBaseLabel(mdIndexLabel));
    }

    @GetMapping(value = { "/tree/{id}"})
    public String selectDeptTree(@PathVariable("id") Long id, ModelMap mmap)
    {
        BaseLabel label = baseLabelService.selectBaseLabelByIdLabel(id);
        mmap.put("label",label==null?new BaseLabel():label);
        return prefix + "/tree";
    }

    /**
     * 加载部门列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = baseLabelService.selectLabelTree(new BaseLabel());
        return ztrees;
    }

    /**
     * 删除指标标签
     */
    @Log(title = "指标标签", businessType = BusinessType.DELETE)
    @GetMapping( "/remove/{ids}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("ids")String ids)
    {
        return toAjax(baseLabelService.deleteBaseLabelByIdLabels(ids));
    }

    /**
     * 查询指标标签列表
     */
    @GetMapping("/findByIdIndex/{idIndex}")
    @ResponseBody
    public AjaxResult list(@PathVariable("idIndex") String idIndex)
    {
        List<BaseLabel> list = baseLabelService.findByIdIndex(idIndex);
        return AjaxResult.success(list);
    }


}
