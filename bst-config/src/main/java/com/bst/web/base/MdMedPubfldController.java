package com.bst.web.base;

import com.bst.common.annotation.Log;
import com.bst.common.base.api.BaseDWService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.core.page.TableDataInfo;
import com.bst.common.enums.BusinessType;
import com.bst.common.exception.base.BaseException;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.md.domain.MdMedPubfld;
import com.bst.md.service.IMdMedPubfldService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 公共字段管理Controller
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Controller
@RequestMapping("/md/med/pubfld")
public class MdMedPubfldController extends BaseController
{
    private String prefix = "system/md/med/pubfld";

    @Autowired
    private IMdMedPubfldService mdMedPubfldService;
    @Autowired
    private BaseDWService baseDWService;

    @GetMapping()
    public String pubfld()
    {
        return prefix + "/pubfld";
    }

    /**
     * 查询公共字段管理列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MdMedPubfld mdMedPubfld)
    {
        startPage();
        List<MdMedPubfld> list = mdMedPubfldService.selectMdMedPubfldList(mdMedPubfld);
        return getDataTable(list);
    }

    /**
     * 查询公共字段管理列表
     */
    @GetMapping("/dimList/{na}")
    @ResponseBody
    public TableDataInfo dimList(@PathVariable("na") String na)
    {
        //startPage();
        if("null".equals(na)) {
            na = null;
        }
        List<MdMedPubfld> list = mdMedPubfldService.findDimCreatTable(na);
        int num;
        List<Map<String,Object>> list1;
        for(MdMedPubfld fld:list) {
            list1 = baseDWService.findBySql("select count(*) num from "+fld.getCdTbDim());
            num = list1==null?0:Integer.parseInt(list1.get(0).get("num").toString());
            fld.setNumLth(num);
        }
        return getDataTable(list);
    }

    @ApiOperation(value = "未建表维度建表")
    @RequestMapping(value = "/createAllDimTable", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult createAllDimTable() throws BaseException {
        List<MdMedPubfld> pubfldList = mdMedPubfldService.findDimNotCreatTable(null);
        if(pubfldList.size()==0) {
            return AjaxResult.error("无符合建表数据");
        }
        String cd;
        AjaxResult result;
        StringBuilder errMsg=new StringBuilder();
        for(MdMedPubfld pubfld:pubfldList) {
            cd = pubfld.getIdPubfld().toUpperCase();
            pubfld.setCdTbDim(cd.replace("ID_","MD_"));
            pubfld.setSdDimtp("1");
            pubfld.setNaDimtp("普通维度");
            result = mdMedPubfldService.updateMdMedPubfld(pubfld);
            if(!AjaxResult.isSuccess(result)) {
                return result;
            }
        }
        return AjaxResult.success("成功创建"+pubfldList.size()+"张表,");
    }

    /**
     * 导出公共字段管理列表
     */
    @Log(title = "公共字段管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MdMedPubfld mdMedPubfld)
    {
        List<MdMedPubfld> list = mdMedPubfldService.selectMdMedPubfldList(mdMedPubfld);
        ExcelUtil<MdMedPubfld> util = new ExcelUtil<MdMedPubfld>(MdMedPubfld.class);
        return util.exportExcel(list, "公共字段管理数据");
    }

    /**
     * 新增公共字段管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存公共字段管理
     */
    @Log(title = "公共字段管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MdMedPubfld mdMedPubfld)
    {
        return toAjax(mdMedPubfldService.insertMdMedPubfld(mdMedPubfld));
    }

    /**
     * 修改公共字段管理
     */
    @GetMapping("/edit/{idPubfld}")
    public String edit(@PathVariable("idPubfld") String idPubfld, ModelMap mmap)
    {
        MdMedPubfld mdMedPubfld = mdMedPubfldService.selectMdMedPubfldByIdPubfld(idPubfld);
        mmap.put("mdMedPubfld", mdMedPubfld);
        return prefix + "/edit";
    }

    /**
     * 修改保存公共字段管理
     */
    @Log(title = "公共字段管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MdMedPubfld mdMedPubfld)
    {
        return mdMedPubfldService.updateMdMedPubfld(mdMedPubfld);
    }

    /**
     * 删除公共字段管理
     */
    @Log(title = "公共字段管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mdMedPubfldService.deleteMdMedPubfldByIdPubflds(ids));
    }
}
