package com.bst.web.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.base.domain.BaseReport;
import com.bst.base.service.IBaseReportService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-05-23
 */
@Controller
@RequestMapping("/base/report")
public class BaseReportController extends BaseController
{
    private String prefix = "system/base/report";

    @Autowired
    private IBaseReportService baseReportService;

    @GetMapping("")
    public String report(ModelMap mmap)
    {
        return prefix + "/report";
    }

    @GetMapping("/mainReport")
    @Log(title = "上报首页", businessType = BusinessType.QUERY)
    public String mainReport(ModelMap mmap)
    {
        List<BaseReport> reports = baseReportService.selectBaseReportList(new BaseReport());
        mmap.put("reports",reports);
        return prefix + "/mainReport";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseReport baseReport)
    {
        startPage();
        List<BaseReport> list = baseReportService.selectBaseReportList(baseReport);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseReport baseReport)
    {
        List<BaseReport> list = baseReportService.selectBaseReportList(baseReport);
        ExcelUtil<BaseReport> util = new ExcelUtil<BaseReport>(BaseReport.class);
        return util.exportExcel(list, "【请填写功能名称】数据");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseReport baseReport)
    {
        return toAjax(baseReportService.insertBaseReport(baseReport));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{idReport}")
    public String edit(@PathVariable("idReport") Long idReport, ModelMap mmap)
    {
        BaseReport baseReport = baseReportService.selectBaseReportByIdReport(idReport);
        mmap.put("baseReport", baseReport);
        return prefix + "/edit";
    }

    @GetMapping("/toDetail/{idReport}")
    public String toDetail(@PathVariable("idReport") Long idReport,String type, ModelMap mmap)
    {
        BaseReport baseReport = baseReportService.selectBaseReportByIdReport(idReport);
        mmap.put("report", baseReport);
        mmap.put("type", type);
        return prefix + "/detail";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseReport baseReport)
    {
        return toAjax(baseReportService.updateBaseReport(baseReport));
    }

    /**
     * 删除【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseReportService.deleteBaseReportByIdReports(ids));
    }
}
