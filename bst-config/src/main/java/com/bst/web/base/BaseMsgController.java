package com.bst.web.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.base.domain.BaseMsg;
import com.bst.base.service.IBaseMsgService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 消息中心Controller
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Controller
@RequestMapping("/system/msg")
public class BaseMsgController extends BaseController
{
    private String prefix = "system/msg";

    @Autowired
    private IBaseMsgService baseMsgService;

    @GetMapping()
    public String msg()
    {
        return prefix + "/msg";
    }

    /**
     * 查询消息中心列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseMsg baseMsg)
    {
        startPage();
        List<BaseMsg> list = baseMsgService.selectBaseMsgList(baseMsg);
        return getDataTable(list);
    }

    /**
     * 导出消息中心列表
     */
    @Log(title = "消息中心", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseMsg baseMsg)
    {
        List<BaseMsg> list = baseMsgService.selectBaseMsgList(baseMsg);
        ExcelUtil<BaseMsg> util = new ExcelUtil<BaseMsg>(BaseMsg.class);
        return util.exportExcel(list, "消息中心数据");
    }

    /**
     * 新增消息中心
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存消息中心
     */
    @Log(title = "消息中心", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseMsg baseMsg)
    {
        return toAjax(baseMsgService.insertBaseMsg(baseMsg));
    }

    /**
     * 修改消息中心
     */
    @GetMapping("/edit/{idMsg}")
    public String edit(@PathVariable("idMsg") Long idMsg, ModelMap mmap)
    {
        BaseMsg baseMsg = baseMsgService.selectBaseMsgByIdMsg(idMsg);
        mmap.put("baseMsg", baseMsg);
        return prefix + "/edit";
    }

    /**
     * 修改保存消息中心
     */
    @Log(title = "消息中心", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseMsg baseMsg)
    {
        return toAjax(baseMsgService.updateBaseMsg(baseMsg));
    }

    /**
     * 删除消息中心
     */
    @Log(title = "消息中心", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseMsgService.deleteBaseMsgByIdMsgs(ids));
    }
}
