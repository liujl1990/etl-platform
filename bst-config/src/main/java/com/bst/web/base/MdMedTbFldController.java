package com.bst.web.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.md.domain.MdMedTbFld;
import com.bst.md.service.IMdMedTbFldService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 元数据字段Controller
 * 
 * @author ruoyi
 * @date 2022-07-02
 */
@Controller
@RequestMapping("/md/med/fld")
public class MdMedTbFldController extends BaseController
{
    private String prefix = "system/fld";

    @Autowired
    private IMdMedTbFldService mdMedTbFldService;

    @GetMapping()
    public String fld()
    {
        return prefix + "/fld";
    }

    @GetMapping("/queryByTb")
    public String queryByTb(String cdTb, ModelMap mmap)
    {
        MdMedTbFld mdMedTbFld = new MdMedTbFld();
        mdMedTbFld.setCdTb(cdTb);
        List<MdMedTbFld> list = mdMedTbFldService.selectMdMedTbFldList(mdMedTbFld);
        mmap.put("data", list);
        return "/public/fieldsFromMd";
    }

    /**
     * 查询元数据字段列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestBody MdMedTbFld mdMedTbFld)
    {
        List<MdMedTbFld> list = mdMedTbFldService.selectMdMedTbFldList(mdMedTbFld);
        return getDataTable(list);
    }

    /**
     * 导出元数据字段列表
     */
    @Log(title = "元数据字段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MdMedTbFld mdMedTbFld)
    {
        List<MdMedTbFld> list = mdMedTbFldService.selectMdMedTbFldList(mdMedTbFld);
        ExcelUtil<MdMedTbFld> util = new ExcelUtil<MdMedTbFld>(MdMedTbFld.class);
        return util.exportExcel(list, "元数据字段数据");
    }

    /**
     * 新增元数据字段
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存元数据字段
     */
    @Log(title = "元数据字段", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MdMedTbFld mdMedTbFld)
    {
        return toAjax(mdMedTbFldService.insertMdMedTbFld(mdMedTbFld));
    }

    /**
     * 修改元数据字段
     */
    @GetMapping("/edit/{idTbFld}")
    public String edit(@PathVariable("idTbFld") Long idTbFld, ModelMap mmap)
    {
        MdMedTbFld mdMedTbFld = mdMedTbFldService.selectMdMedTbFldByIdTbFld(idTbFld);
        mmap.put("mdMedTbFld", mdMedTbFld);
        return prefix + "/edit";
    }

    /**
     * 修改保存元数据字段
     */
    @Log(title = "元数据字段", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MdMedTbFld mdMedTbFld)
    {
        return toAjax(mdMedTbFldService.updateMdMedTbFld(mdMedTbFld));
    }

    /**
     * 删除元数据字段
     */
    @Log(title = "元数据字段", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mdMedTbFldService.deleteMdMedTbFldByIdTbFlds(ids));
    }
}
