package com.bst.web.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.bst.common.vo.DimDataVO;
import com.bst.md.domain.MdMedPubfld;
import com.bst.md.service.IMdMedDimService;
import com.bst.md.service.IMdMedPubfldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.base.domain.BaseDimComp;
import com.bst.base.service.IBaseDimCompService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 维度对照Controller
 * 
 * @author laoliu
 * @date 2023-02-12
 */
@Controller
@RequestMapping("/base/dim/comp")
public class BaseDimCompController extends BaseController
{
    private String prefix = "system/base/dim_comp/comp";

    @Autowired
    private IBaseDimCompService baseDimCompService;
    @Autowired
    private IMdMedDimService mdMedDimService;
    @Autowired
    IMdMedPubfldService pubfldService;

    @GetMapping()
    public String comp()
    {
        return prefix + "/comp";
    }

    /**
     * 查询维度对照列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseDimComp baseDimComp)
    {
        startPage();
        List<BaseDimComp> list = baseDimCompService.selectBaseDimCompList(baseDimComp);
        return getDataTable(list);
    }

    /**
     * 导出维度对照列表
     */
    @Log(title = "维度对照", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseDimComp baseDimComp)
    {
        List<BaseDimComp> list = baseDimCompService.selectBaseDimCompList(baseDimComp);
        ExcelUtil<BaseDimComp> util = new ExcelUtil<BaseDimComp>(BaseDimComp.class);
        return util.exportExcel(list, "维度对照数据");
    }

    /**
     * 新增维度对照
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    @GetMapping("/result/{idDimComp}")
    public String result(@PathVariable("idDimComp") Long idDimComp, ModelMap mmap)
    {
        BaseDimComp comp = baseDimCompService.selectBaseDimCompByIdDimComp(idDimComp);
        String idPubfldTar = comp.getIdPubfldTar();
        MdMedPubfld pubfld = pubfldService.selectMdMedPubfldByIdPubfld(idPubfldTar);
        List<DimDataVO> dataVOS = mdMedDimService.queryDimData(pubfld.getCdTbDim());
        List<Map<String,Object>> tarList = new ArrayList<>();
        Map<String,Object> tar = new HashMap<>();
        tar.put("text","");
        tar.put("value","");
        tar.put("name","");
        tarList.add(tar);
        for(DimDataVO vo:dataVOS) {
            tar = new HashMap<>();
            tar.put("text",vo.getNa()+"["+vo.getCd()+"]");
            tar.put("value",vo.getCd());
            tar.put("name",vo.getNa());
            tarList.add(tar);
        }
        mmap.put("idDimComp",idDimComp);
        mmap.put("cdDimComp",comp.getCd());
        mmap.put("tarSource", JSONObject.toJSONString(tarList));
        return prefix + "/result";
    }

    /**
     * 新增保存维度对照
     */
    @Log(title = "维度对照", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseDimComp baseDimComp)
    {
        return toAjax(baseDimCompService.insertBaseDimComp(baseDimComp));
    }

    /**
     * 修改维度对照
     */
    @GetMapping("/edit/{idDimComp}")
    public String edit(@PathVariable("idDimComp") Long idDimComp, ModelMap mmap)
    {
        BaseDimComp baseDimComp = baseDimCompService.selectBaseDimCompByIdDimComp(idDimComp);
        mmap.put("baseDimComp", baseDimComp);
        return prefix + "/edit";
    }

    /**
     * 修改保存维度对照
     */
    @Log(title = "维度对照", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseDimComp baseDimComp)
    {
        return toAjax(baseDimCompService.updateBaseDimComp(baseDimComp));
    }

    /**
     * 删除维度对照
     */
    @Log(title = "维度对照", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseDimCompService.deleteBaseDimCompByIdDimComps(ids));
    }
}
