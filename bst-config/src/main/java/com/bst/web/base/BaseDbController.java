package com.bst.web.base;

import java.util.List;

import com.bst.base.domain.BaseDb;
import com.bst.base.service.IBaseDbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2022-05-17
 */
@Controller
@RequestMapping("/base/db")
public class BaseDbController extends BaseController
{
    private String prefix = "system/base/db";

    @Autowired
    private IBaseDbService baseDbService;

    @GetMapping()
    public String db()
    {
        return prefix + "/db";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseDb baseDb)
    {
        startPage();
        List<BaseDb> list = baseDbService.selectBaseDbList(baseDb);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseDb baseDb)
    {
        List<BaseDb> list = baseDbService.selectBaseDbList(baseDb);
        ExcelUtil<BaseDb> util = new ExcelUtil<BaseDb>(BaseDb.class);
        return util.exportExcel(list, "【请填写功能名称】数据");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseDb baseDb)
    {
        return toAjax(baseDbService.insertBaseDb(baseDb));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{idDb}")
    public String edit(@PathVariable("idDb") Integer idDb, ModelMap mmap)
    {
        BaseDb baseDb = baseDbService.selectBaseDbById(idDb);
        mmap.put("baseDb", baseDb);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseDb baseDb)
    {
        return toAjax(baseDbService.updateBaseDb(baseDb));
    }

    /**
     * 删除【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseDbService.deleteBaseDbByIdDbs(ids));
    }

    @GetMapping("/testDB")
    @ResponseBody
    public String testDB(int idDb)
    {
        return baseDbService.testDB(idDb);
    }
}

