package com.bst.web.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.base.domain.BaseDbfldComb;
import com.bst.base.service.IBaseDbfldCombService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 数据库字段映射Controller
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Controller
@RequestMapping("/base/comb")
public class BaseDbfldCombController extends BaseController
{
    private String prefix = "system/base/comb";

    @Autowired
    private IBaseDbfldCombService baseDbfldCombService;

    @GetMapping()
    public String comb()
    {
        return prefix + "/comb";
    }

    /**
     * 查询数据库字段映射列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BaseDbfldComb baseDbfldComb)
    {
        startPage();
        List<BaseDbfldComb> list = baseDbfldCombService.selectBaseDbfldCombList(baseDbfldComb);
        return getDataTable(list);
    }

    /**
     * 导出数据库字段映射列表
     */
    @Log(title = "数据库字段映射", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BaseDbfldComb baseDbfldComb)
    {
        List<BaseDbfldComb> list = baseDbfldCombService.selectBaseDbfldCombList(baseDbfldComb);
        ExcelUtil<BaseDbfldComb> util = new ExcelUtil<BaseDbfldComb>(BaseDbfldComb.class);
        return util.exportExcel(list, "数据库字段映射数据");
    }

    /**
     * 新增数据库字段映射
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存数据库字段映射
     */
    @Log(title = "数据库字段映射", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BaseDbfldComb baseDbfldComb)
    {
        return toAjax(baseDbfldCombService.insertBaseDbfldComb(baseDbfldComb));
    }

    /**
     * 修改数据库字段映射
     */
    @GetMapping("/edit/{idDbfldComb}")
    public String edit(@PathVariable("idDbfldComb") Integer idDbfldComb, ModelMap mmap)
    {
        BaseDbfldComb baseDbfldComb = baseDbfldCombService.selectBaseDbfldCombByIdDbfldComb(idDbfldComb);
        mmap.put("baseDbfldComb", baseDbfldComb);
        return prefix + "/edit";
    }

    /**
     * 修改保存数据库字段映射
     */
    @Log(title = "数据库字段映射", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BaseDbfldComb baseDbfldComb)
    {
        return toAjax(baseDbfldCombService.updateBaseDbfldComb(baseDbfldComb));
    }

    /**
     * 删除数据库字段映射
     */
    @Log(title = "数据库字段映射", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(baseDbfldCombService.deleteBaseDbfldCombByIdDbfldCombs(ids));
    }
}
