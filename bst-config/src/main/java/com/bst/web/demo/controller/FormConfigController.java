package com.bst.web.demo.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/demo/form")
public class FormConfigController {

    private String prefix = "demo";

    /**
     * 模态窗口
     */
    @GetMapping("/config")
    @RequiresPermissions("demo:form:config:view")
    public String dialog()
    {
        return prefix + "/formConfig";
    }
}
