package com.bst.web.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.system.domain.HosWorkFlowState;
import com.bst.system.service.IHosWorkFlowStateService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-04-18
 */
@Controller
@RequestMapping("/system/state")
public class HosWorkFlowStateController extends BaseController
{
    private String prefix = "system/state";

    @Autowired
    private IHosWorkFlowStateService hosWorkFlowStateService;

    @RequiresPermissions("system:state:view")
    @GetMapping()
    public String state()
    {
        return prefix + "/state";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("system:state:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HosWorkFlowState hosWorkFlowState)
    {
        startPage();
        List<HosWorkFlowState> list = hosWorkFlowStateService.selectHosWorkFlowStateList(hosWorkFlowState);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("system:state:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HosWorkFlowState hosWorkFlowState)
    {
        List<HosWorkFlowState> list = hosWorkFlowStateService.selectHosWorkFlowStateList(hosWorkFlowState);
        ExcelUtil<HosWorkFlowState> util = new ExcelUtil<HosWorkFlowState>(HosWorkFlowState.class);
        return util.exportExcel(list, "【请填写功能名称】数据");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("system:state:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HosWorkFlowState hosWorkFlowState)
    {
        return toAjax(hosWorkFlowStateService.insertHosWorkFlowState(hosWorkFlowState));
    }

    /**
     * 修改【请填写功能名称】
     */
    @RequiresPermissions("system:state:edit")
    @GetMapping("/edit/{stateId}")
    public String edit(@PathVariable("stateId") Integer stateId, ModelMap mmap)
    {
        HosWorkFlowState hosWorkFlowState = hosWorkFlowStateService.selectHosWorkFlowStateByStateId(stateId);
        mmap.put("hosWorkFlowState", hosWorkFlowState);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("system:state:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HosWorkFlowState hosWorkFlowState)
    {
        return toAjax(hosWorkFlowStateService.updateHosWorkFlowState(hosWorkFlowState));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("system:state:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hosWorkFlowStateService.deleteHosWorkFlowStateByStateIds(ids));
    }
}
