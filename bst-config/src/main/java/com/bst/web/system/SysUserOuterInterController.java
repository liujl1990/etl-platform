package com.bst.web.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.system.domain.SysUserOuterInter;
import com.bst.system.service.ISysUserOuterInterService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;

/**
 * 用户接口权限Controller
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
@Controller
@RequestMapping("/system/user/outer/inter")
public class SysUserOuterInterController extends BaseController
{
    private String prefix = "system/inter";

    @Autowired
    private ISysUserOuterInterService sysUserOuterInterService;

    /**
     * 新增保存用户接口权限
     */
    @Log(title = "用户批量保存", businessType = BusinessType.INSERT)
    @PostMapping("/addBatch")
    @ResponseBody
    public AjaxResult addBatch(@RequestBody List<SysUserOuterInter> list)
    {
        return toAjax(sysUserOuterInterService.insertBatchSysUserOuterInter(list));
    }
}
