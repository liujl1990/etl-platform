package com.bst.web.system;

import com.alibaba.fastjson.JSON;
import com.bst.common.constant.JobConstant;
import com.bst.common.utils.DateUtil;
import com.bst.common.vo.RedisInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.*;

@Controller
@RequestMapping("/system/redis")
@Api(tags="redis管理")
public class RedisController {

    @Resource
    private RedisConnectionFactory redisConnectionFactory;
    @Autowired
    StringRedisTemplate redisTemplate;

    @GetMapping()
    public String mainPage()
    {
        return "system/redis/redis";
    }

    @ApiOperation(value="redis键值对数量")
    @RequestMapping(value="/msgUse", method= RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> msgUse() {
        RedisConnection connection = redisConnectionFactory.getConnection();
        Long dbSize = connection.dbSize();
        Properties info = connection.info();
        Double memary= Double.valueOf(0);
        for (Map.Entry<Object, Object> entry : info.entrySet()) {
            String key = entry.getKey()==null?"":entry.getKey().toString();
            if ("used_memory".equals(key)) {
                memary = Double.parseDouble(entry.getValue().toString())/1024/1024;
                break;
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("dateTime", DateUtil.FORMAT_YYYY_MM_DD_HHMMSS.format(new Date()));
        map.put("dbSize", dbSize);
        map.put("memary",new DecimalFormat("######0.00").format(memary));
        map.put("dimData", JSON.toJSONString(this.getRedisInfoVO(JobConstant.REDIS_PREF_DIMDATA)));
        return map;
    }

    @ApiOperation(value="获取redis全部数据")
    @RequestMapping(value="/getRedisInfo", method= RequestMethod.GET)
    @ResponseBody
    public List<RedisInfoVO> getRedisInfoVO(String prefix) {
        Set<String> keys = redisTemplate.keys(prefix+"*");
        List<RedisInfoVO> infoList = new ArrayList<>();
        RedisInfoVO RedisInfoVO = null;
        SetOperations operations = redisTemplate.opsForSet();
        for (String key : keys) {
            RedisInfoVO = new RedisInfoVO();
            RedisInfoVO.setKey(key);
            RedisInfoVO.setValue(operations.members(key).toString());
            infoList.add(RedisInfoVO);
        }
        return infoList;
    }

    @ApiOperation(value="redis数据删除")
    @RequestMapping(value="/delete", method= RequestMethod.GET)
    @ResponseBody
    public void delete(String prefix) {
        Set<String> keys = redisTemplate.keys(prefix+"_*");
        redisTemplate.delete(keys);
    }

    @ApiOperation(value="redis删除所有数据")
    @RequestMapping(value="/deleteAll", method= RequestMethod.GET)
    @ResponseBody
    public void deleteAll() {
        Set<String> keys = redisTemplate.keys("*");
        redisTemplate.delete(keys);
    }
}
