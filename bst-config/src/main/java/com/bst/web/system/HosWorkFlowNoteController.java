package com.bst.web.system;

import com.bst.common.annotation.Log;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.core.page.TableDataInfo;
import com.bst.common.enums.BusinessType;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.utils.self.LoginAPIUtils;
import com.bst.system.domain.HosWorkFlowNote;
import com.bst.system.service.IHosWorkFlowNoteService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 单据评论Controller
 * 
 * @author cbh
 * @date 2023-04-08
 */
@Controller
@RequestMapping("/system/note")
public class HosWorkFlowNoteController extends BaseController
{
    private String prefix = "system/flow";

    @Autowired
    private IHosWorkFlowNoteService hosWorkFlowNoteService;

    @RequiresPermissions("system:note:view")
    @GetMapping()
    public String note()
    {
        return prefix + "/note";
    }

    /**
     * 查询单据评论列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(int workId)
    {
        HosWorkFlowNote hosWorkFlowNote = new HosWorkFlowNote();
        hosWorkFlowNote.setWorkId(workId);
        List<HosWorkFlowNote> list = hosWorkFlowNoteService.selectHosWorkFlowNoteList(hosWorkFlowNote);
        return getDataTable(list);
    }

    /**
     * 导出单据评论列表
     */
    @Log(title = "单据评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HosWorkFlowNote hosWorkFlowNote)
    {
        List<HosWorkFlowNote> list = hosWorkFlowNoteService.selectHosWorkFlowNoteList(hosWorkFlowNote);
        ExcelUtil<HosWorkFlowNote> util = new ExcelUtil<HosWorkFlowNote>(HosWorkFlowNote.class);
        return util.exportExcel(list, "单据评论数据");
    }

    /**
     * 新增单据评论
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存单据评论
     */
    @Log(title = "单据评论", businessType = BusinessType.INSERT)
    @PostMapping("/addNote")
    @ResponseBody
    public AjaxResult addNote(String note,int workId)
    {
        HosWorkFlowNote hosWorkFlowNote = new HosWorkFlowNote();
        hosWorkFlowNote.setNote(note);
        hosWorkFlowNote.setWorkId(workId);
        hosWorkFlowNote.setCreateBy(LoginAPIUtils.getLoginUsename());
        hosWorkFlowNote.setDtSysCre(new Date());
        return toAjax(hosWorkFlowNoteService.insertHosWorkFlowNote(hosWorkFlowNote));
    }

    /**
     * 修改单据评论
     */
    @GetMapping("/edit/{noteId}")
    public String edit(@PathVariable("noteId") String noteId, ModelMap mmap)
    {
        HosWorkFlowNote hosWorkFlowNote = hosWorkFlowNoteService.selectHosWorkFlowNoteByNoteId(noteId);
        mmap.put("hosWorkFlowNote", hosWorkFlowNote);
        return prefix + "/edit";
    }

    /**
     * 修改保存单据评论
     */
    @Log(title = "单据评论", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HosWorkFlowNote hosWorkFlowNote)
    {
        return toAjax(hosWorkFlowNoteService.updateHosWorkFlowNote(hosWorkFlowNote));
    }

    /**
     * 删除单据评论
     */
    @Log(title = "单据评论", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hosWorkFlowNoteService.deleteHosWorkFlowNoteByNoteIds(ids));
    }
}
