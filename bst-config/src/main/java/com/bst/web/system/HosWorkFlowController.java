package com.bst.web.system;

import com.bst.common.annotation.Log;
import com.bst.common.constant.UserConstants;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.core.domain.entity.SysUser;
import com.bst.common.core.page.TableDataInfo;
import com.bst.common.enums.BusinessType;
import com.bst.common.utils.StringUtils;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.utils.self.LoginAPIUtils;
import com.bst.system.domain.*;
import com.bst.system.service.IHosWorkFlowNoteService;
import com.bst.system.service.IHosWorkFlowService;
import com.bst.system.service.IHosWorkFlowStateService;
import com.bst.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 单据提交Controller
 *
 * @author ruoyi
 * @date 2023-04-08
 */
@Controller
@RequestMapping("/system/flow")
public class HosWorkFlowController extends BaseController
{
    private String prefix = "system/base/flow";

    @Autowired
    private IHosWorkFlowService hosWorkFlowService;

    @Autowired
    private IHosWorkFlowNoteService hosWorkFlowNoteService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private IHosWorkFlowStateService hosWorkFlowStateService;

    @GetMapping()
    public String flow(ModelMap mmap)
    {
        SysUser user = sysUserService.selectUserById(LoginAPIUtils.getLoginUserId());
        List<HosWorkFlow> flows;
        if(UserConstants.SYSTEM_USER_TYPE.equals(user.getUserType())) {
            flows = hosWorkFlowService.selectDeptsFromHosWorkFlow();
        } else {
            flows = new ArrayList<>();
            HosWorkFlow flow = new HosWorkFlow();
            flow.setDeptId(user.getDeptId());
            flow.setDeptName(user.getDept().getDeptName());
            flows.add(flow);
        }
        mmap.put("depts",flows);
        mmap.put("username",user.getUserName());
        return prefix + "/flow";
    }

    @GetMapping("/submit")
    public String flowSubmit(ModelMap mmap)
    {
        SysUser user = sysUserService.selectUserById(LoginAPIUtils.getLoginUserId());
        List<HosWorkFlow> flows;
        if(UserConstants.SYSTEM_USER_TYPE.equals(user.getUserType())) {
            flows = hosWorkFlowService.selectDeptsFromHosWorkFlow();
        } else {
            flows = new ArrayList<>();
            HosWorkFlow flow = new HosWorkFlow();
            flow.setDeptId(user.getDeptId());
            flow.setDeptName(user.getDept().getDeptName());
            flows.add(flow);
        }
        mmap.put("depts",flows);
        mmap.put("username",user.getUserName());
        return prefix + "/flowSubmit";
    }

    @GetMapping("/check")
    public String flowCheck(ModelMap mmap)
    {
        List<HosWorkFlow> flows = hosWorkFlowService.selectDeptsFromHosWorkFlow();
        mmap.put("depts",flows);
        //mmap.put("username",user.getUserName());
        return prefix + "/flowCheck";
    }

    @GetMapping("/print/{workId}")
    public String print(@PathVariable("workId") int workId, ModelMap mmap)
    {
        HosWorkFlow flow = hosWorkFlowService.selectHosWorkFlowListById(workId);
        mmap.put("flow", flow);
        return prefix + "/print";
    }

    /**
     * 查询单据提交列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HosWorkFlow hosWorkFlow)
    {
        startPage();
        List<HosWorkFlow> list = hosWorkFlowService.selectHosWorkFlowList(hosWorkFlow);
        return getDataTable(list);
    }

    /**
     * 查询单据提交列表
     */
    @PostMapping("/listWithAuth")
    @ResponseBody
    public TableDataInfo listWithAuth(HosWorkFlow hosWorkFlow)
    {
        startPage();
        List<HosWorkFlow> list = hosWorkFlowService.selectHosWorkFlowListWithAuth(hosWorkFlow);
        return getDataTable(list);
    }

    /**
     * 导出单据提交列表
     */

    @Log(title = "单据提交", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HosWorkFlow hosWorkFlow)
    {
        List<HosWorkFlow> list = hosWorkFlowService.selectHosWorkFlowList(hosWorkFlow);
        ExcelUtil<HosWorkFlow> util = new ExcelUtil<HosWorkFlow>(HosWorkFlow.class);
        return util.exportExcel(list, "单据提交数据");
    }

    /**
     * 新增单据提交
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存单据提交
     */

    @Log(title = "单据提交", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestBody HosWorkFlow hosWorkFlow)
    {
        hosWorkFlow.setStatusCode("0");
        hosWorkFlow.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        hosWorkFlow.setDtSysCre(new Date());
        return toAjax(hosWorkFlowService.insertHosWorkFlow(hosWorkFlow));
    }

    /**
     * 修改单据提交
     */

    @GetMapping("/edit/{workId}")
    public String edit(@PathVariable("workId") int workId, ModelMap mmap)
    {
        HosWorkFlow hosWorkFlow = hosWorkFlowService.selectHosWorkFlowListById(workId);
        mmap.put("hosWorkFlow", hosWorkFlow);
        return prefix + "/edit";
    }

    /**
     * 修改保存单据提交
     */

    @Log(title = "单据提交", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestBody HosWorkFlow hosWorkFlow)
    {
        String statusCode = hosWorkFlow.getStatusCode();
        hosWorkFlow.setStatusCode(statusCode==null?"0":statusCode);
        hosWorkFlow.setNaEmpModi(LoginAPIUtils.getLoginUsename());
        hosWorkFlow.setDtSysModi(new Date());
        return toAjax(hosWorkFlowService.updateHosWorkFlow(hosWorkFlow));
    }

    /**
     * 删除单据提交
     */
    @Log(title = "单据提交", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hosWorkFlowService.deleteHosWorkFlowByIdIndexs(ids));
    }

    @GetMapping("/updateStatus")
    @ResponseBody
    public String updateStatus(String ids,String status,String days,String naEmp)
    {
        HosWorkFlow hosWorkFlow = new HosWorkFlow();
        int workId;
        for(String id:ids.split(",")) {
            if(StringUtils.isNotEmpty(id)) {
                hosWorkFlow = hosWorkFlowService.selectHosWorkFlowListById(Integer.parseInt(id));
                hosWorkFlow.setWorkId(Integer.valueOf(id));
                hosWorkFlow.setStatusCode(status);
                hosWorkFlow.setNaEmp(LoginAPIUtils.getLoginUsename());
                if(StringUtils.isNotEmpty(days)) {
                    hosWorkFlow.setDays(new BigDecimal(days));
                }
                if(StringUtils.isNotEmpty(naEmp)) {
                    hosWorkFlow.setNaEmp(naEmp);
                }
                if("5".equals(status)) {
                    hosWorkFlow.setDtEnd(new Date());
                }
                hosWorkFlowService.updateHosWorkFlow(hosWorkFlow);
                insertHosWorkFlowState(hosWorkFlow);
            }
        }
        return "操作成功";
    }

    private void insertHosWorkFlowState(HosWorkFlow hosWorkFlow) {
        HosWorkFlowState state = new HosWorkFlowState();
        state.setWorkId(Long.parseLong(hosWorkFlow.getWorkId()+""));
        state.setDeptId(hosWorkFlow.getDeptId());
        state.setDeptName(hosWorkFlow.getDeptName());
        state.setStatusCodeNow(hosWorkFlow.getStatusCode());
        hosWorkFlowStateService.insertHosWorkFlowState(state);
    }

    //-------------------------note----------------------------------------------
    /**
     * 修改单据评论
     */
    @GetMapping("/editNote")
    public String editNote( int workId, ModelMap mmap)
    {
        HosWorkFlow hosWorkFlow = hosWorkFlowService.selectHosWorkFlowListById(workId);
        mmap.put("hosWorkFlow", hosWorkFlow);
        mmap.put("username",LoginAPIUtils.getLoginUsename());
        HosWorkFlowState state = new HosWorkFlowState();
        state.setWorkId(Long.parseLong(workId+""));
        List<HosWorkFlowState> stateList = hosWorkFlowStateService.selectHosWorkFlowStateList(state);
        mmap.put("stateList",stateList);
        return prefix + "/note";
    }

    /**
     * 新增保存单据评论
     */
    @Log(title = "单据评论", businessType = BusinessType.INSERT)
    @PostMapping("/addNote")
    @ResponseBody
    public AjaxResult addNote(String note,String workId)
    {
        HosWorkFlowNote hosWorkFlowNote = new HosWorkFlowNote();
        hosWorkFlowNote.setNote(note);
        hosWorkFlowNote.setWorkId(Integer.valueOf(workId));
        hosWorkFlowNote.setNaEmpCre(LoginAPIUtils.getLoginUsename());
        hosWorkFlowNote.setDtSysCre(new Date());
        return toAjax(hosWorkFlowNoteService.insertHosWorkFlowNote(hosWorkFlowNote));
    }
    /**
     * 查询单据评论列表
     */
    @PostMapping("/notelist")
    @ResponseBody
    public TableDataInfo notelist(int workId)
    {
        HosWorkFlowNote hosWorkFlowNote = new HosWorkFlowNote();
        hosWorkFlowNote.setWorkId(workId);
        List<HosWorkFlowNote> list = hosWorkFlowNoteService.selectHosWorkFlowNoteList(hosWorkFlowNote);
        return getDataTable(list);
    }

}
