package com.bst.web.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bst.system.framework.utils.AESUtils;
import com.bst.system.domain.SysUserDataauth;
import com.bst.system.domain.SysUserOuter;
import com.bst.system.service.ISysUserDataauthService;
import com.bst.system.service.ISysUserOuterService;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.core.domain.entity.SysUser;
import com.bst.common.core.text.Convert;
import com.bst.common.enums.UserStatus;
import com.bst.common.utils.ServletUtils;
import com.bst.common.utils.StringUtils;
import com.bst.system.framework.jwt.utils.JwtUtils;
import com.bst.system.framework.shiro.service.SysPasswordService;
import com.bst.system.framework.web.service.ConfigService;
import com.bst.system.service.ISysUserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登录验证
 * 
 * @author ruoyi
 */
@Controller
public class SysLoginController extends BaseController
{
    /**
     * 是否开启记住我功能
     */
    @Value("${shiro.rememberMe.enabled: false}")
    private boolean rememberMe;
    @Value("${ruoyi.platform.title}")
    private String platform_title;
    
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ISysUserOuterService userOuterService;

    @Autowired
    private SysPasswordService passwordService;

    @Autowired
    private ConfigService configService;
    @Autowired
    private ISysUserDataauthService userDataauthService;

    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response, ModelMap mmap)
    {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request))
        {
            return ServletUtils.renderString(response, "{\"code\":\"1\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }
        // 是否开启记住我
        mmap.put("isRemembered", rememberMe);
        // 是否开启用户注册
        mmap.put("isAllowRegister", Convert.toBool(configService.getKey("sys.account.registerUser"), false));
        mmap.put("title",platform_title);
        return "login";
    }

    @PostMapping("/login")
    @ResponseBody
    public AjaxResult ajaxLogin(String username, String password, Boolean rememberMe)
    {
        username = AESUtils.decrypt(username);
        password = AESUtils.decrypt(password);
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
        Subject subject = SecurityUtils.getSubject();
        try
        {
            subject.login(token);
            SysUser user = userService.selectUserByLoginName(username);
            String token2 = JwtUtils.createToken(username, user.getPassword(),false);
            return AjaxResult.success("登录成功").put("token", token2);
        }
        catch (AuthenticationException e)
        {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage()))
            {
                msg = e.getMessage();
            }
            return error(msg);
        }
    }

    @PostMapping("/jwt/login")
    @ResponseBody
    public AjaxResult jwtLogin(String username, String password)
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            return AjaxResult.error("账号和密码不能为空!");
        }

        SysUser user = userService.selectUserByLoginName(username);
        if (user == null)
        {
            return AjaxResult.error("用户不存在/密码错误!");
        }

        if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
        {
            return AjaxResult.error("对不起，您的账号已被删除!");
        }

        if (UserStatus.DISABLE.getCode().equals(user.getStatus()))
        {
            return AjaxResult.error("用户已封禁，请联系管理员!");
        }

        if (!passwordService.matches(user, password))
        {
            return AjaxResult.error("用户不存在/密码错误!");
        }

        String token = JwtUtils.createToken(username, user.getPassword(),false);
        return AjaxResult.success("登录成功,请妥善保管您的token信息").put("token", token);
    }

    /**
     * 此方法主要用于外部接口对接时使用
     * @param username
     * @param password
     * @return
     */
    @GetMapping("/getToken")
    @ResponseBody
    public AjaxResult getToken(String username, String password)
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            return AjaxResult.error("账号和密码不能为空!");
        }

        SysUserOuter user = userOuterService.selectByLoginName(username);
        if (user == null)
        {
            return AjaxResult.error("用户不存在/密码错误!");
        }

        if (UserStatus.DELETED.getCode().equals(user.getFgAct()==0))
        {
            return AjaxResult.error("用户已封禁，请联系管理员!");
        }

        if (!passwordService.matches2(user, password))
        {
            return AjaxResult.error("用户不存在/密码错误!");
        }

        String token = JwtUtils.createToken(username, user.getPassword(),true);
        return AjaxResult.success("登录成功,请妥善保管您的token信息").put("token", token);
    }

    @GetMapping("/unauth")
    public String unauth()
    {
        return "error/unauth";
    }

    @GetMapping("/hdw/userData")
    @ResponseBody
    @ApiOperation(value="用户权限数据")
    public AjaxResult userData(String sysType)
    {
        // 取身份信息
        SysUser user = getSysUser();
        user.setPassword(null);
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("user",user);
        SysUserDataauth model = new SysUserDataauth();
        model.setUserId(user.getUserId());
        if("HL".equals(sysType)) {
            model.setSdDataauthType("MD_DIM_DEPTNUR");
        }
        List<SysUserDataauth> dataauthList = userDataauthService.selectSysUserDataauthList(model);
        dataMap.put("deptNurList",dataauthList);
        return AjaxResult.success(dataMap);
    }
}
