package com.bst.web.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.system.domain.SysUserDataauth;
import com.bst.system.service.ISysUserDataauthService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 用户数据权限Controller
 * 
 * @author ruoyi
 * @date 2023-01-20
 */
@Controller
@RequestMapping("/system/dataauth")
public class SysUserDataauthController extends BaseController
{
    private String prefix = "system/user/dataauth";

    @Autowired
    private ISysUserDataauthService sysUserDataauthService;

    @GetMapping()
    public String dataauth()
    {
        return prefix + "/dataauth";
    }

    /**
     * 查询用户数据权限列表
     */
    @PostMapping("/prefix")
    @ResponseBody
    public TableDataInfo list(@RequestBody SysUserDataauth sysUserDataauth)
    {
        List<SysUserDataauth> list = sysUserDataauthService.selectSysUserDataauthList(sysUserDataauth);
        return getDataTable(list);
    }

    /**
     * 导出用户数据权限列表
     */
    @Log(title = "用户数据权限", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysUserDataauth sysUserDataauth)
    {
        List<SysUserDataauth> list = sysUserDataauthService.selectSysUserDataauthList(sysUserDataauth);
        ExcelUtil<SysUserDataauth> util = new ExcelUtil<SysUserDataauth>(SysUserDataauth.class);
        return util.exportExcel(list, "用户数据权限数据");
    }

    /**
     * 新增用户数据权限
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户数据权限
     */
    @Log(title = "用户数据权限", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysUserDataauth sysUserDataauth)
    {
        return toAjax(sysUserDataauthService.insertSysUserDataauth(sysUserDataauth));
    }

    /**
     * 新增保存用户数据权限
     */
    @Log(title = "用户数据权限", businessType = BusinessType.INSERT)
    @PostMapping("/addList")
    @ResponseBody
    public AjaxResult addSaveList(@RequestBody List<SysUserDataauth> list)
    {
        return toAjax(sysUserDataauthService.batchInsertSysUserDataauth(list));
    }

    /**
     * 修改用户数据权限
     */
    @GetMapping("/edit/{dataauthId}")
    public String edit(@PathVariable("dataauthId") Long dataauthId, ModelMap mmap)
    {
        SysUserDataauth sysUserDataauth = sysUserDataauthService.selectSysUserDataauthByDataauthId(dataauthId);
        mmap.put("sysUserDataauth", sysUserDataauth);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户数据权限
     */
    @Log(title = "用户数据权限", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysUserDataauth sysUserDataauth)
    {
        return toAjax(sysUserDataauthService.updateSysUserDataauth(sysUserDataauth));
    }

    /**
     * 删除用户数据权限
     */
    @Log(title = "用户数据权限", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysUserDataauthService.deleteSysUserDataauthByDataauthIds(ids));
    }

    @GetMapping("/dimData")
    public String dimData(String cd,Long userId, ModelMap mmap)
    {
        mmap.put("tableName",cd);
        mmap.put("userId",userId);
        return prefix+"/dimData";
    }
}
