package com.bst.web.etl;

import java.util.List;

import com.bst.common.vo.TableDDLColumnVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.etl.domain.EtlDrawBase;
import com.bst.etl.service.IEtlDrawBaseService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 基础抽取Controller
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
@Controller
@RequestMapping("/etl/base")
@Api(tags="基础抽取管理")
public class EtlDrawBaseController extends BaseController
{
    private String prefix = "etl/base";

    @Autowired
    private IEtlDrawBaseService etlDrawBaseService;

    @GetMapping()
    public String etl()
    {
        return prefix + "/base";
    }

    @GetMapping("/inter")
    public String inter()
    {
        return "etl/base_inter/base_inter";
    }

    @GetMapping("/inter/add")
    public String inter_add()
    {
        return "etl/base_inter/add";
    }

    /**
     * 修改基础抽取
     */
    @GetMapping("/inter/edit/{idDrawBase}")
    public String inter_edit(@PathVariable("idDrawBase") Long idDrawBase, ModelMap mmap)
    {
        EtlDrawBase etlDrawBase = etlDrawBaseService.selectEtlDrawBaseByIdDrawBase(idDrawBase);
        mmap.put("etlDrawBase", etlDrawBase);
        return "etl/base_inter/edit";
    }

    /**
     * 查询基础抽取列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(EtlDrawBase etlDrawBase)
    {
        startPage();
        List<EtlDrawBase> list = etlDrawBaseService.selectEtlDrawBaseList(etlDrawBase);
        return getDataTable(list);
    }

    /**
     * 导出基础抽取列表
     */
    @Log(title = "基础抽取", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(EtlDrawBase etlDrawBase)
    {
        List<EtlDrawBase> list = etlDrawBaseService.selectEtlDrawBaseList(etlDrawBase);
        ExcelUtil<EtlDrawBase> util = new ExcelUtil<EtlDrawBase>(EtlDrawBase.class);
        return util.exportExcel(list, "基础抽取数据");
    }

    @GetMapping("/createTb/{idDrawBase}")
    public String createTb(@PathVariable("idDrawBase") Long idDrawBase, ModelMap mmap) {
        EtlDrawBase etlDrawBase = etlDrawBaseService.selectEtlDrawBaseByIdDrawBase(idDrawBase);
        //List<TableDDLColumnVO> columnList = etlDrawBaseService.createOrModiTableColumnList(idDrawBase);
        mmap.put("etlDrawBase", etlDrawBase);
        //mmap.put("columnList", columnList);
        return prefix + "/createTb";
    }

    @ApiOperation(value = "返回建表字字段列表")
    @RequestMapping(value = "findColums/{idDrawBase}", method = RequestMethod.GET)
    @ResponseBody
    public TableDataInfo gerTableColumns(@PathVariable("idDrawBase") Long idDrawBase) {
        if (idDrawBase == null) {
            return null;
        }
        try {
            List<TableDDLColumnVO> columnList = etlDrawBaseService.createOrModiTableColumnList(idDrawBase);
            return getDataTable(columnList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @ApiOperation(value = "执行建表字段列表")
    @RequestMapping(value = "/execTableDDLColumns/{idDrawBase}", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult execTableDDLColumns(@PathVariable("idDrawBase") Long idDrawBase) {
        try {
            etlDrawBaseService.execTableDDLColumns(idDrawBase);
            return AjaxResult.success();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 新增基础抽取
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存基础抽取
     */
    @Log(title = "基础抽取", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(EtlDrawBase etlDrawBase)
    {
        return toAjax(etlDrawBaseService.insertEtlDrawBase(etlDrawBase));
    }

    /**
     * 修改基础抽取
     */
    @GetMapping("/edit/{idDrawBase}")
    public String edit(@PathVariable("idDrawBase") Long idDrawBase, ModelMap mmap)
    {
        EtlDrawBase etlDrawBase = etlDrawBaseService.selectEtlDrawBaseByIdDrawBase(idDrawBase);
        mmap.put("etlDrawBase", etlDrawBase);
        return prefix + "/edit";
    }

    /**
     * 修改保存基础抽取
     */
    @Log(title = "基础抽取", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(EtlDrawBase etlDrawBase)
    {
        return toAjax(etlDrawBaseService.updateEtlDrawBase(etlDrawBase));
    }

    /**
     * 删除基础抽取
     */
    @Log(title = "基础抽取", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(etlDrawBaseService.deleteEtlDrawBaseByIdDrawBases(ids));
    }
}
