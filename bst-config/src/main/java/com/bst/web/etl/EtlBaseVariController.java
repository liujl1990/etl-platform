package com.bst.web.etl;

import java.util.List;

import com.bst.etl.domain.EtlBaseVariJob;
import com.bst.etl.service.IEtlBaseVariJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.etl.domain.EtlBaseVari;
import com.bst.etl.service.IEtlBaseVariService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.page.TableDataInfo;

/**
 * 抽取变量Controller
 * 
 * @author ruoyi
 * @date 2023-03-10
 */
@Controller
@RequestMapping("/etl/vari")
public class EtlBaseVariController extends BaseController
{
    private String prefix = "etl/vari";

    @Autowired
    private IEtlBaseVariService etlBaseVariService;
    @Autowired
    private IEtlBaseVariJobService etlBaseVariJobService;

    @GetMapping()
    public String vari()
    {
        return prefix + "/vari";
    }

    /**
     * 查询抽取变量列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(EtlBaseVari etlBaseVari)
    {
        startPage();
        List<EtlBaseVari> list = etlBaseVariService.selectEtlBaseVariList(etlBaseVari);
        return getDataTable(list);
    }

    @PostMapping("/child")
    @ResponseBody
    public TableDataInfo list(EtlBaseVariJob variJob)
    {
        List<EtlBaseVariJob> list = etlBaseVariJobService.selectEtlBaseVariJobList(variJob);
        return getDataTable(list);
    }
}
