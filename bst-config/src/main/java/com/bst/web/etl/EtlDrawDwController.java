package com.bst.web.etl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bst.common.utils.StringUtils;
import com.bst.etl.service.IEtlDrawBaseService;
import com.bst.etl.service.IEtlDrawDwFldService;
import com.bst.etl.vo.EtlTaskDwFldVO;
import com.bst.etl.domain.EtlDrawBase;
import com.bst.etl.domain.EtlDrawDwFld;
import com.bst.md.domain.MdMedPubfld;
import com.bst.md.service.IMdMedPubfldService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.etl.domain.EtlDrawDw;
import com.bst.etl.service.IEtlDrawDwService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 数据仓库Controller
 * 
 * @author ruoyi
 * @date 2022-06-20
 */
@Controller
@RequestMapping("/etl/dw")
public class EtlDrawDwController extends BaseController
{
    private String prefix = "etl/dw";

    @Autowired
    private IEtlDrawDwService etlDrawDwService;
    @Autowired
    private IEtlDrawBaseService baseService;
    @Autowired
    private IMdMedPubfldService pubfldService;
    @Autowired
    private IEtlDrawDwFldService fldService;

    @GetMapping()
    public String dw()
    {
        return prefix + "/dw";
    }

    /**
     * 查询数据仓库列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(EtlDrawDw etlDrawDw)
    {
        startPage();
        List<EtlDrawDw> list = etlDrawDwService.selectEtlDrawDwList(etlDrawDw);
        return getDataTable(list);
    }
    /**
     * 查询数据仓库列表
     */
    @PostMapping("/selectByCls")
    @ResponseBody
    public TableDataInfo selectByCls(EtlDrawDw etlDrawDw)
    {
        String sdDwclses = etlDrawDw.getSdDwcls();
        List<String> arr = null;
        if(StringUtils.isNotEmpty(sdDwclses)) {
            arr = new ArrayList<>();
            arr = Arrays.asList(sdDwclses.split(","));
        }
        startPage();
        List<EtlDrawDw> list = etlDrawDwService.selectEtlDrawDwByDwclses(arr);
        return getDataTable(list);
    }

    /**
     * 导出数据仓库列表
     */
    @Log(title = "数据仓库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(EtlDrawDw etlDrawDw)
    {
        List<EtlDrawDw> list = etlDrawDwService.selectEtlDrawDwList(etlDrawDw);
        ExcelUtil<EtlDrawDw> util = new ExcelUtil<EtlDrawDw>(EtlDrawDw.class);
        return util.exportExcel(list, "数据仓库数据");
    }

    /**
     * 新增数据仓库
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存数据仓库
     */
    @Log(title = "数据仓库", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(EtlDrawDw etlDrawDw)
    {
        return toAjax(etlDrawDwService.insertEtlDrawDw(etlDrawDw));
    }

    /**
     * 修改数据仓库
     */
    @GetMapping("/edit/{idDrawDw}")
    public String edit(@PathVariable("idDrawDw") Long idDrawDw, ModelMap mmap)
    {
        EtlDrawDw etlDrawDw = etlDrawDwService.selectEtlDrawDwByIdDrawDw(idDrawDw);
        mmap.put("etlDrawDw", etlDrawDw);
        return prefix + "/edit";
    }

    @GetMapping("/createTb/{idDrawDw}")
    @ResponseBody
    public AjaxResult createTb(@PathVariable("idDrawDw") Long idDrawDw)
    {
        AjaxResult result = etlDrawDwService.createTable(idDrawDw);
        if(AjaxResult.isSuccess(result)) {
            EtlDrawDw etlDrawDw = etlDrawDwService.selectEtlDrawDwByIdDrawDw(idDrawDw);
            etlDrawDw.setEuStatus(1);
            etlDrawDwService.updateEtlDrawDw(etlDrawDw);
        }
        return result;
    }

    @GetMapping("/fldMapping/{idDrawDw}")
    public String fldMapping(@PathVariable("idDrawDw") Long idDrawDw, ModelMap mmap)
    {
        EtlDrawDw etlDrawDw = etlDrawDwService.selectEtlDrawDwByIdDrawDw(idDrawDw);
        mmap.put("etlDrawDw", etlDrawDw);
        List<EtlTaskDwFldVO> fldVOList = etlDrawDwService.findAllFld(etlDrawDw.getIdDrawDw(),etlDrawDw.getIdDrawBase());
        mmap.put("fldVOList", fldVOList);
        List<MdMedPubfld> pubflds = pubfldService.selectMdMedPubfldList(new MdMedPubfld());
        mmap.put("pubflds", pubflds);
        List<MdMedPubfld> timeFlds = pubfldService.findByFldPrefix("ID_DIM_TIME_");
        mmap.put("timeflds", timeFlds);
        return prefix + "/fldMapping";
    }



    @GetMapping("/addList/{idBases}")
    @ResponseBody
    public AjaxResult addList(@PathVariable("idBases") String idBases)
    {
        EtlDrawBase etlDrawBase;
        EtlDrawDw etlDrawDw = new EtlDrawDw();Long idDrwaBase;
        List<EtlDrawDw> list;
        for(String idBase:idBases.split(",")) {
            idDrwaBase = Long.parseLong(idBase);
            etlDrawDw.setIdDrawBase(idDrwaBase);
            list = etlDrawDwService.selectEtlDrawDwList(etlDrawDw);
            if(list.size()>0) {
                continue;
            }
            etlDrawBase = baseService.selectEtlDrawBaseByIdDrawBase(idDrwaBase);
            etlDrawDw.setFgAct(1);
            etlDrawDw.setNa(etlDrawBase.getNa()+"(DW)");
            etlDrawDw.setCdTbTarOds(etlDrawBase.getCdTbTar());
            etlDrawDw.setSdSys(etlDrawBase.getSdSys());
            etlDrawDwService.insertEtlDrawDw(etlDrawDw);
        }
        return AjaxResult.success();
    }


    /**
     * 修改保存数据仓库
     */
    @Log(title = "数据仓库", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(EtlDrawDw etlDrawDw)
    {
        return toAjax(etlDrawDwService.updateEtlDrawDw(etlDrawDw));
    }

    /**
     * 删除数据仓库
     */
    @Log(title = "数据仓库", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(etlDrawDwService.deleteEtlDrawDwByIdDrawDws(ids));
    }

    /*****************************************************   字段   *********************************************************/

    /**
     * 新增保存数据仓库
     */
    @Log(title = "保存dw字段字段", businessType = BusinessType.INSERT)
    @PostMapping("/saveFld")
    @ResponseBody
    public AjaxResult saveFld(@RequestBody List<EtlDrawDwFld> fldList)
    {
        Long idDrawDw = fldList.get(0).getIdDrawDw();
        EtlDrawDw drawDw = etlDrawDwService.selectEtlDrawDwByIdDrawDw(idDrawDw);
        if(drawDw.getEuStatus()==1) {
            drawDw.setEuStatus(2);
        }
        fldService.saveList(fldList);
        return AjaxResult.success("保存成功");
    }
}
