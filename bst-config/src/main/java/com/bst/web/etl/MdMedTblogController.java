package com.bst.web.etl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.md.domain.MdMedTblog;
import com.bst.md.service.IMdMedTblogService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 元数据创建日志Controller
 * 
 * @author ruoyi
 * @date 2022-07-17
 */
@Controller
@RequestMapping("/system/tblog")
public class MdMedTblogController extends BaseController
{
    private String prefix = "system/tblog";

    @Autowired
    private IMdMedTblogService mdMedTblogService;

    @GetMapping()
    public String tblog()
    {
        return prefix + "/tblog";
    }

    /**
     * 查询元数据创建日志列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MdMedTblog mdMedTblog)
    {
        startPage();
        List<MdMedTblog> list = mdMedTblogService.selectMdMedTblogList(mdMedTblog);
        return getDataTable(list);
    }

    /**
     * 导出元数据创建日志列表
     */
    @Log(title = "元数据创建日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MdMedTblog mdMedTblog)
    {
        List<MdMedTblog> list = mdMedTblogService.selectMdMedTblogList(mdMedTblog);
        ExcelUtil<MdMedTblog> util = new ExcelUtil<MdMedTblog>(MdMedTblog.class);
        return util.exportExcel(list, "元数据创建日志数据");
    }

    /**
     * 新增元数据创建日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存元数据创建日志
     */
    @Log(title = "元数据创建日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MdMedTblog mdMedTblog)
    {
        return toAjax(mdMedTblogService.insertMdMedTblog(mdMedTblog));
    }

    /**
     * 修改元数据创建日志
     */
    @GetMapping("/edit/{idTblog}")
    public String edit(@PathVariable("idTblog") Long idTblog, ModelMap mmap)
    {
        MdMedTblog mdMedTblog = mdMedTblogService.selectMdMedTblogByIdTblog(idTblog);
        mmap.put("mdMedTblog", mdMedTblog);
        return prefix + "/edit";
    }

    /**
     * 修改保存元数据创建日志
     */
    @Log(title = "元数据创建日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MdMedTblog mdMedTblog)
    {
        return toAjax(mdMedTblogService.updateMdMedTblog(mdMedTblog));
    }

    /**
     * 删除元数据创建日志
     */
    @Log(title = "元数据创建日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(mdMedTblogService.deleteMdMedTblogByIdTblogs(ids));
    }
}
