package com.bst.web.etl;

import java.util.List;

import com.bst.etl.domain.EtlDrawBase;
import com.bst.etl.mapper.EtlDrawBaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.bst.common.annotation.Log;
import com.bst.common.enums.BusinessType;
import com.bst.etl.domain.EtlJobDrawbase;
import com.bst.etl.service.IEtlJobDrawbaseService;
import com.bst.common.core.controller.BaseController;
import com.bst.common.core.domain.AjaxResult;
import com.bst.common.utils.poi.ExcelUtil;
import com.bst.common.core.page.TableDataInfo;

/**
 * 定时调度Controller
 * 
 * @author ruoyi
 * @date 2022-08-07
 */
@Controller
@RequestMapping("/etl/job/drawbase")
public class EtlJobDrawbaseController extends BaseController
{
    private String prefix = "etl/jobdraw";

    @Autowired
    private IEtlJobDrawbaseService etlJobDrawbaseService;
    @Autowired
    private EtlDrawBaseMapper etlDrawBaseMapper;

    @GetMapping()
    public String drawbase()
    {
        return prefix + "/jobdraw";
    }

    /**
     * 查询定时调度列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestBody EtlJobDrawbase etlJobDrawbase)
    {
        startPage();
        List<EtlJobDrawbase> list = etlJobDrawbaseService.selectEtlJobDrawbaseList(etlJobDrawbase);
        return getDataTable(list);
    }

    /**
     * 导出定时调度列表
     */
    @Log(title = "定时调度", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(EtlJobDrawbase etlJobDrawbase)
    {
        List<EtlJobDrawbase> list = etlJobDrawbaseService.selectEtlJobDrawbaseList(etlJobDrawbase);
        ExcelUtil<EtlJobDrawbase> util = new ExcelUtil<EtlJobDrawbase>(EtlJobDrawbase.class);
        return util.exportExcel(list, "定时调度数据");
    }

    /**
     * 新增定时调度
     */
    @GetMapping("/selectByJobId/{jobId}")
    @ResponseBody
    public TableDataInfo selectByJobId(@PathVariable("jobId") Integer jobId, ModelMap mmap)
    {
        List<EtlDrawBase> list = etlDrawBaseMapper.selectByJobId(jobId);
        return getDataTable(list);
    }

    /**
     * 新增定时调度
     */
    @GetMapping("/add/{jobId}")
    public String add(@PathVariable("jobId") Integer jobId, ModelMap mmap)
    {
        mmap.put("list",etlDrawBaseMapper.selectNotInJobList(jobId));
        mmap.put("jobId",jobId);
        return prefix + "/drawlist";
    }

    /**
     * 新增保存定时调度
     */
    @Log(title = "定时调度", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestBody List<EtlJobDrawbase> list)
    {
        return toAjax(etlJobDrawbaseService.insertList(list));
    }

    /**
     * 修改定时调度
     */
    @GetMapping("/edit/{idJobDrawbase}")
    public String edit(@PathVariable("idJobDrawbase") Long idJobDrawbase, ModelMap mmap)
    {
        EtlJobDrawbase etlJobDrawbase = etlJobDrawbaseService.selectEtlJobDrawbaseByIdJobDrawbase(idJobDrawbase);
        mmap.put("etlJobDrawbase", etlJobDrawbase);
        return prefix + "/edit";
    }

    /**
     * 修改保存定时调度
     */
    @Log(title = "定时调度", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(EtlJobDrawbase etlJobDrawbase)
    {
        return toAjax(etlJobDrawbaseService.updateEtlJobDrawbase(etlJobDrawbase));
    }

    /**
     * 删除定时调度
     */
    @Log(title = "定时调度", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(etlJobDrawbaseService.deleteEtlJobDrawbaseByIdJobDrawbases(ids));
    }

    @GetMapping("/delete")
    @ResponseBody
    public AjaxResult delete(Long jobId, Long idDrawBase)
    {
        etlJobDrawbaseService.deleteByIdDrawBase(jobId,idDrawBase);
        return AjaxResult.success();
    }
}
