package com.bst.web.common;

import com.bst.common.vo.HeaderAndBodyVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenerateData {

    public HeaderAndBodyVO dataDemp1()
    {
        HeaderAndBodyVO headerAndBodyVO = new HeaderAndBodyVO();
        Map<String,String> header = new HashMap<>();
        header.put("ID_DIM_NURSINGLEVEL","护理级别");header.put("NA_DIM_NURSINGLEVEL","护理级别名称");header.put("ID_DIM_DEPTNUR","病区");header.put("NA_DIM_DEPTNUR","病区名称");
        header.put("IDX_HLDJ","护理等级数量");
        headerAndBodyVO.setThead(header);
        List<Map<String,Object>> dataList = new ArrayList<>();
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("ID_DIM_NURSINGLEVEL","?0");dataMap.put("NA_DIM_NURSINGLEVEL","特级护理");dataMap.put("IDX_HLDJ",3);
        dataMap.put("ID_DIM_DEPTNUR","01");dataMap.put("NA_DIM_DEPTNUR","内一病区");
        dataList.add(dataMap);
        dataMap = new HashMap<>();
        dataMap.put("ID_DIM_NURSINGLEVEL","1");dataMap.put("NA_DIM_NURSINGLEVEL","一级护理");dataMap.put("IDX_HLDJ",10);
        dataMap.put("ID_DIM_DEPTNUR","02");dataMap.put("NA_DIM_DEPTNUR","骨伤病区");
        dataList.add(dataMap);
        dataMap = new HashMap<>();
        dataMap.put("ID_DIM_NURSINGLEVEL","2");dataMap.put("NA_DIM_NURSINGLEVEL","二级护理");dataMap.put("IDX_HLDJ",30);
        dataMap.put("ID_DIM_DEPTNUR","03");dataMap.put("NA_DIM_DEPTNUR","儿科病区");
        dataList.add(dataMap);
        dataMap = new HashMap<>();
        dataMap.put("ID_DIM_NURSINGLEVEL","3");dataMap.put("NA_DIM_NURSINGLEVEL","三级护理");dataMap.put("IDX_HLDJ",50);
        dataMap.put("ID_DIM_DEPTNUR","04");dataMap.put("NA_DIM_DEPTNUR","心病病区");
        dataList.add(dataMap);
        headerAndBodyVO.setTbody(dataList);
        return headerAndBodyVO;
    }
}
