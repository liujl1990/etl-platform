const addAttributes = (el, attributes) => {
  Object.keys(attributes).forEach(attr => {
    el.style[attr] = attributes[attr];
  });
};
const createWaterMarkElement = (text = '已审核') => {
  const div = document.createElement('div');
  div.innerText = text;
  div.setAttribute('name','waterMarkDiv');
  addAttributes(div, {
    position: 'absolute',
    fontSize: `16px`,
    color: 'red',
    opacity: 0.7,
    transform: `rotate(-30deg)`,
    userSelect: 'none',
  });
  return div;
};

// idIndexs为数组
var isDataCheck = function (idIndexs,month) {
  $.ajax({
    url: '/etl/check/record/isCheck?idIndexs=' + idIndexs + '&month='+month,
    type: 'get',
    dataType: 'json',
    success: function (result) {
      if (result.code == 0) {
        var isCheck = result.data;
        if(isCheck==true) { //审核
          $("#tableDiv").addClass("wrap1");
        } else {
          $("#tableDiv").removeClass("wrap1");
        }
        showWatermark();
      }
    }
  });
};

// idIndexs为数组
var isCheckWithJudge = function (idIndexs,month) {
  $.ajax({
    url: '/etl/check/record/isCheckWithJudge?idIndexs=' + idIndexs + '&month='+month,
    type: 'get',
    dataType: 'json',
    success: function (result) {
      if (result.code == 0) {
        var isCheck = result.data;
        if(isCheck==false) { //未审核
          $("#tableDiv").removeClass("wrap1");
        } else {
          $("#tableDiv").addClass("wrap1");
        }
        showWatermark();
      }
    }
  });
};

var showWatermark = function() {
  const wrap = document.querySelector('.wrap1');
  if(wrap==null) {
      $("div[name='waterMarkDiv']").remove();
      return ;
  }
  const { clientWidth, clientHeight } = wrap;
  const waterHeight = 30;
  const waterWidth = 300;
  // 能放下几行几列
  const [columns, rows] = [Math.ceil(clientWidth / waterWidth), Math.ceil(clientHeight / waterHeight)]
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j <= rows; j++) {
      if(j==0 && i>0 && i<columns-1) {
        // 生成水印块
        const watcerMarkElement = createWaterMarkElement();
        // 动态设置偏移值
        addAttributes(watcerMarkElement, {
          width: `${waterWidth}px`,
          height: `${waterHeight}px`,
          left: `${waterWidth + (i-1) * waterWidth + 10}px`,
          top: `${waterHeight + (j-1) * waterHeight + 10}px`,
        });
        wrap.appendChild(watcerMarkElement)
      }
    }
  }
};

