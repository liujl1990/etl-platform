package com.bst.common.exception.user;

import com.bst.common.exception.base.BaseRuntimeException;

/**
 * 用户信息异常类
 * 
 * @author ruoyi
 */
public class UserException extends BaseRuntimeException
{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
