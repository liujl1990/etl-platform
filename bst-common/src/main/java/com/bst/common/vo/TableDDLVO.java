package com.bst.common.vo;

import java.util.List;

public class TableDDLVO {
    private String taskId;
    private List<TableDDLColumnVO> columns;
    private TableDDLSqlVO sqls;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public List<TableDDLColumnVO> getColumns() {
        return columns;
    }

    public void setColumns(List<TableDDLColumnVO> columns) {
        this.columns = columns;
    }

    public TableDDLSqlVO getSqls() {
        return sqls;
    }

    public void setSqls(TableDDLSqlVO sqls) {
        this.sqls = sqls;
    }
}
