package com.bst.common.vo;

import java.io.Serializable;

/**
 * Created by liujunlei on 2021/5/28.
 */
public class FieldAttrVO implements Serializable {
    private String tbName;
    private String  field;
    private String  euType;
    private Integer  numLength;
    private Integer  numPrec;

    public String getTbName() {
        return tbName;
    }

    public void setTbName(String tbName) {
        this.tbName = tbName;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getEuType() {
        return euType;
    }

    public void setEuType(String euType) {
        this.euType = euType;
    }

    public Integer getNumLength() {
        return numLength;
    }

    public void setNumLength(Integer numLength) {
        this.numLength = numLength;
    }

    public Integer getNumPrec() {
        return numPrec;
    }

    public void setNumPrec(Integer numPrec) {
        this.numPrec = numPrec;
    }
}
