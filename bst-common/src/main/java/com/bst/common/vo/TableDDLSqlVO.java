package com.bst.common.vo;


public class TableDDLSqlVO {
    String createSQL;
    String modifySQL;

    public String getCreateSQL() {
        return createSQL;
    }

    public void setCreateSQL(String createSQL) {
        this.createSQL = createSQL;
    }

    public String getModifySQL() {
        return modifySQL;
    }

    public void setModifySQL(String modifySQL) {
        this.modifySQL = modifySQL;
    }
}
