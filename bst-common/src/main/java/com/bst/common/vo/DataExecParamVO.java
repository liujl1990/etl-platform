package com.bst.common.vo;

import com.bst.common.constant.JobConstant;
import com.bst.common.utils.DateUtil;

import java.io.Serializable;
import java.util.Date;

public class DataExecParamVO implements Serializable {
    private Date startDate;  //结束时间
    private Date endDate;   //开始时间
    private String startDay;
    private String endDay;
    private String des;      //任务描述
    private String delSql;   //删除sql
    private String querySql; //查询sql
    private String tbTar; //目标表
    private String jobType; //任务类型
    private Long idExec;
    private String idDbSou = JobConstant.DB_CLS_DW; // 来源库
    private String idDbTar = JobConstant.DB_CLS_DW; // 目标库

    private String url;
    private String euReq;// get,post
    private String inter;//接口名称

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEuReq() {
        return euReq;
    }

    public void setEuReq(String euReq) {
        this.euReq = euReq;
    }

    public String getInter() {
        return inter;
    }

    public void setInter(String inter) {
        this.inter = inter;
    }

    public Long getIdExec() {
        return idExec;
    }

    public void setIdExec(Long idExec) {
        this.idExec = idExec;
    }

    public String getIdDbSou() {
        return idDbSou;
    }

    public void setIdDbSou(String idDbSou) {
        this.idDbSou = idDbSou;
    }

    public String getIdDbTar() {
        return idDbTar;
    }

    public void setIdDbTar(String idDbTar) {
        this.idDbTar = idDbTar;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getTbTar() {
        return tbTar;
    }

    public void setTbTar(String tbTar) {
        this.tbTar = tbTar;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getDelSql() {
        return delSql;
    }

    public void setDelSql(String delSql) {
        this.delSql = delSql;
    }

    public String getQuerySql() {
        return querySql;
    }

    public void setQuerySql(String querySql) {
        this.querySql = querySql;
    }


    private Integer exeType; //执行方式
    public Integer getExeType() {
        return exeType;
    }
    public void setExeType(Integer exeType) {
        this.exeType = exeType;
    }

    public String getStartDay() {
        return DateUtil.FORMAT_YYYYMMDD.format(startDate);
    }

    public String getEndDay() {
        return DateUtil.FORMAT_YYYYMMDD.format(endDate);
    }

    public void setEndDay(String endDay) {
        this.endDay = endDay;
    }
}
