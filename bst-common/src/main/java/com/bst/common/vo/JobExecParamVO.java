package com.bst.common.vo;

import java.io.Serializable;
import java.util.Date;

public class JobExecParamVO implements Serializable {

    private String startDay;
    private  String endDay;
    private  String idTasks;
    private Date dtNow;
    private  String type;    // base,dw,dm,all
    private Integer execType; // 0 自动执行  1手工执行
    private Long jobLogId;

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    public void setEndDay(String endDay) {
        this.endDay = endDay;
    }

    public void setExecType(Integer execType) {
        this.execType = execType;
    }

    public Date getDtNow() {
        return dtNow;
    }

    public void setDtNow(Date dtNow) {
        this.dtNow = dtNow;
    }

    public Long getJobLogId() {
        return jobLogId;
    }

    public void setJobLogId(Long jobLogId) {
        this.jobLogId = jobLogId;
    }

    public Integer getExecType() {
        return execType;
    }

    public String getIdTasks() {
        return idTasks;
    }

    public void setIdTasks(String idTasks) {
        this.idTasks = idTasks;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartDay() {
        return startDay.replaceAll("-","");
    }

    public String getEndDay() {
        return endDay.replaceAll("-","");
    }
}
