package com.bst.common.vo;

import java.io.Serializable;
import java.util.Map;

public class JobDimVO implements Serializable {

    private String sdDimtp;

    private String dimTable;

    private Map<String,String> data; //只存自动填充维度数据

    public String getDimTable() {
        return dimTable;
    }

    public void setDimTable(String dimTable) {
        this.dimTable = dimTable;
    }

    public String getSdDimtp() {
        return sdDimtp;
    }

    public void setSdDimtp(String sdDimtp) {
        this.sdDimtp = sdDimtp;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
