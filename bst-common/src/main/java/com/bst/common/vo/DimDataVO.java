package com.bst.common.vo;

import java.util.Date;

public class DimDataVO {
    private String cd;
    private String na;
    private String euTp;//0 自动插入， 1 人工插入 ，2 sql抽取
    private Date dtSysCre;
    private Date dtSysModi;

    private String tableName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getEuTp() {
        return euTp;
    }

    public void setEuTp(String euTp) {
        this.euTp = euTp;
    }

    public Date getDtSysCre() {
        return dtSysCre;
    }

    public void setDtSysCre(Date dtSysCre) {
        this.dtSysCre = dtSysCre;
    }

    public Date getDtSysModi() {
        return dtSysModi;
    }

    public void setDtSysModi(Date dtSysModi) {
        this.dtSysModi = dtSysModi;
    }

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getNa() {
        return na;
    }

    public void setNa(String na) {
        this.na = na;
    }
}
