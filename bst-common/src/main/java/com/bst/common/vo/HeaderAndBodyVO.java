package com.bst.common.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class HeaderAndBodyVO implements Serializable {
    private Map<String,String> thead;
    private List<Map<String,Object>> tbody;
    private String[] headId;

    public String[] getHeadId() {
        if(thead==null) {
            return new String[]{};
        } else {
            return thead.keySet().toArray(new String[0]);
        }
    }

    public Map<String, String> getThead() {
        return thead;
    }

    public void setThead(Map<String, String> thead) {
        this.thead = thead;
    }

    public List<Map<String, Object>> getTbody() {
        return tbody;
    }

    public void setTbody(List<Map<String, Object>> tbody) {
        this.tbody = tbody;
    }
}
