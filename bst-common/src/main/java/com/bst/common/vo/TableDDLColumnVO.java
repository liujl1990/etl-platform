package com.bst.common.vo;

public class TableDDLColumnVO {

    public static final String OP_TYPE_ADD = "OP_TYPE_ADD";
    public static final String OP_TYPE_DROP = "OP_TYPE_DROP";
    public static final String OP_TYPE_MODIFY = "OP_TYPE_MODIFY";

    private String newColumnName;
    private Integer newColumnType;
    private String newColumnTypeName;
    private Integer newColumnPrecision;
    private Integer newColumnScale;

    private String oldColumnName;
    private Integer oldColumnType;
    private String oldColumnTypeName;
    private Integer oldColumnPrecision;
    private Integer oldColumnScale;

    private String opType;

    public TableDDLColumnVO(MetaDataVO newMetaDataVO, MetaDataVO OldMetaData) {
        if (newMetaDataVO != null) {
            this.newColumnName = newMetaDataVO.getColumnName();
            this.newColumnType = newMetaDataVO.getColumnType();
            this.newColumnTypeName = newMetaDataVO.getColumnTypeName();
            this.newColumnPrecision = newMetaDataVO.getColumnPrecision();
            this.newColumnScale = newMetaDataVO.getColumnScale();
        }

        if (OldMetaData != null) {
            this.oldColumnName = OldMetaData.getColumnName();
            this.oldColumnType = OldMetaData.getColumnType();
            this.oldColumnTypeName = OldMetaData.getColumnTypeName();
            this.oldColumnPrecision = OldMetaData.getColumnPrecision();
            this.oldColumnScale = OldMetaData.getColumnScale();
        }
    }

    public TableDDLColumnVO() {
    }

    public String getNewColumnName() {
        return newColumnName;
    }

    public void setNewColumnName(String newColumnName) {
        this.newColumnName = newColumnName;
    }

    public Integer getNewColumnType() {
        return newColumnType;
    }

    public void setNewColumnType(Integer newColumnType) {
        this.newColumnType = newColumnType;
    }

    public String getNewColumnTypeName() {
        return newColumnTypeName;
    }

    public void setNewColumnTypeName(String newColumnTypeName) {
        this.newColumnTypeName = newColumnTypeName;
    }

    public Integer getNewColumnPrecision() {
        return newColumnPrecision;
    }

    public void setNewColumnPrecision(Integer newColumnPrecision) {
        this.newColumnPrecision = newColumnPrecision;
    }

    public Integer getNewColumnScale() {
        return newColumnScale;
    }

    public void setNewColumnScale(Integer newColumnScale) {
        this.newColumnScale = newColumnScale;
    }

    public String getOldColumnName() {
        return oldColumnName;
    }

    public void setOldColumnName(String oldColumnName) {
        this.oldColumnName = oldColumnName;
    }

    public Integer getOldColumnType() {
        return oldColumnType;
    }

    public void setOldColumnType(Integer oldColumnType) {
        this.oldColumnType = oldColumnType;
    }

    public String getOldColumnTypeName() {
        return oldColumnTypeName;
    }

    public void setOldColumnTypeName(String oldColumnTypeName) {
        this.oldColumnTypeName = oldColumnTypeName;
    }

    public Integer getOldColumnPrecision() {
        return oldColumnPrecision;
    }

    public void setOldColumnPrecision(Integer oldColumnPrecision) {
        this.oldColumnPrecision = oldColumnPrecision;
    }

    public Integer getOldColumnScale() {
        return oldColumnScale;
    }

    public void setOldColumnScale(Integer oldColumnScale) {
        this.oldColumnScale = oldColumnScale;
    }

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }
}
