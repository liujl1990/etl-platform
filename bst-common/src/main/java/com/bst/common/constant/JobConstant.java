package com.bst.common.constant;

public interface JobConstant {
    String JOB_TYPE_BASE = "base";
    String JOB_TYPE_BASE_INTER = "base_inter";
    String JOB_TYPE_DW = "dw";
    String JOB_TYPE_DM = "dm";
    String JOB_TYPE_ALL = "all";

    //表分类
    String SD_TBCA_DW = "DW";   //数据仓库
    String SD_TBCA_DM = "DM";   //数据集市
    String SD_TBCA_DIM = "DIM"; //维度
    String SD_TBCA_APP = "APP"; //维度

    //job参数名称定义
    String JOB_TABLENAME = "tableName"; //目标表名
    String JOB_DELSQL = "delSql"; //删除sql/前置SQL
    String JOB_QUERYSQL = "querySql"; //查询sql
    String JOB_DT_DAY_BEGIN = "DT_DAY_BEGIN";
    String JOB_DAY = "DAY";
    String JOB_DT_DAY_END = "DT_DAY_END";
    String JOB_DES = "des";//抽取描述
    String JOB_TYPE = "jobType";//任务类型
    String JOB_DB_SOU = "idDbSou";//来源库
    String JOB_DB_TAR = "idDbTar";//目标库
    String JOB_ID_EXEC = "idExec";//执行日志主键
    String JOB_EXE_TYPE = "exeType";//执行方式

    //数据库分类
    String DB_CLS_DW = "dw";
    String DB_CLS_META = "0";

    String DB_TYPE_ORACLE = "oracle";
    String DB_TYPE_SQLSERVER = "sqlserver";
    String DB_TYPE_MYSQL = "mysql";
    String DB_TYPE_POSTGREY = "postgrey";


    //value值
    String VALUE_INT = "value_int";
    String VALUE_FLOAT = "value_float";
    String VALUE_PERCENT = "value_percent";

    String ID_DIM_PREX = "ID_DIM_"; //维度前缀
    String NA_DIM_PREX = "NA_DIM_"; //维度名称前缀

    //字段
    String ID_DIM_DAY_OCCUR="ID_DIM_DAY_OCCUR";//发生时间（dm的唯一时间标识）
    String ID_DIM_MONTH_OCCUR="ID_DIM_MONTH_OCCUR";//发生时间（dm的唯一时间标识）
    String ID_DIM_ORG="ID_DIM_ORGAN";  //机构
    String NA_DIM_ORG="NA_DIM_ORGAN";  //机构
    String ID_DIM_SYSTEM="ID_DIM_SYSTEM";  //系统
    String ID="ID";  //系统
    String SYS_DIM_MODI="SYS_DIM_MODI";// 系统字段，更新标志

    //redis前缀
    String REDIS_PREF_PUBFLD="pubfld_";
    String REDIS_PREF_DIMDATA = "dimdata_";
    String REDIS_PREF_USERNAME="username_";

    //消息类型
    String MSG_TYPE_ERROR = "ERROR";
    String MSG_TYPE_WARING = "WARING";
    String MSG_TYPE_INFO = "INFO";
    //消息编码
    String MSG_CODE_A01 = "A01"; //数据源不可用
    String MSG_CODE_B01 = "B01"; //
    String MSG_CODE_C02 = "C01"; //维度缺少数据
    String MSG_CODE_D01 = "D01"; //新增维度
    String MSG_CODE_D02 = "D02"; //新增维度失败

    Integer JOB_EXE_TYPE_SCHEDULE = 0;  //调度执行
    Integer JOB_EXE_TYPE_MANUAL = 1;  //手动执行

    //job名称
    String JOBNAME_DW_DM = "dwToDmJobBean";
    String JOBNAME_HOS_HDW = "hosToHdwJobBean";
    String JOBNAME_HOS_HDW_INTER = "hosToBaseByInterJobBean";
    String JOBNAME_ODS_DW="odsToDwJobBean";

    //表约束类型
    String SD_CONSTP_PK = "pk";
    String SD_CONSTP_NOTNULL = "not null";
    String SD_CONSTP_UNIQUE = "unique";
    String SD_CONSTP_INDEX="index";

    //表修改参数类型
    String PARAM_TYPE_STRING = "STRING";
    String PARAM_TYPE_JSON = "JSON";
}
