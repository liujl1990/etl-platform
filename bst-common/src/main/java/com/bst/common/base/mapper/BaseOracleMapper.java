package com.bst.common.base.mapper;

import com.bst.common.vo.FieldAttrVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by liujunlei on 2021/5/28.
 */
public interface BaseOracleMapper {

    List<FieldAttrVO> findFieldsByTable(@Param("tableName") String tableName, @Param("schema") String schema);

    List<FieldAttrVO> findTables(@Param("tableName") String tableName, @Param("schema") String schema);

}
