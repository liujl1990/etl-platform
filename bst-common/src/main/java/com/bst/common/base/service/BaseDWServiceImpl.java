package com.bst.common.base.service;

import com.bst.common.annotation.DataSource;
import com.bst.common.base.api.BaseDWService;
import com.bst.common.base.mapper.BaseMapper;
import com.bst.common.constant.JobConstant;
import com.bst.common.utils.sql.EtlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by liujunlei on 2021/6/1.
 */
@Service
public class BaseDWServiceImpl implements BaseDWService {

    @Autowired
    private BaseMapper baseMapper;

    @Override
    @DataSource(value= JobConstant.DB_CLS_DW)
    public List<Map<String, Object>> findBySql(String sql) {
        return baseMapper.findBySql(sql);
    }

    @DataSource(value= JobConstant.DB_CLS_DW)
    public List<Map<String, Object>> findPageBySql(String sql,Integer pageNum,Integer pageSize,String orderBy,String dbType) {
        sql = EtlUtil.getMaxNumSql(sql,pageSize,dbType,orderBy);
        return findBySql(sql);
    }

    @Override
    @DataSource(value= JobConstant.DB_CLS_DW)
    public int insertMapList(String tableName, Map<String, Object> colMap, List<Map<String, Object>> mapList) {
        return baseMapper.insertMapList(tableName,colMap,mapList);
    }

    @Override
    @DataSource(value= JobConstant.DB_CLS_DW)
    public int insertMap(String tableName, Map<String, Object> dataMap) {
        return baseMapper.insertMap(tableName,dataMap);
    }

    @Override
    @DataSource(value= JobConstant.DB_CLS_DW)
    public int insert(String sql) {
        return baseMapper.insert(sql);
    }
}
