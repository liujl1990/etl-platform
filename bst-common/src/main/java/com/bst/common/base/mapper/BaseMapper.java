package com.bst.common.base.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by liujunlei on 2021/5/25.
 */

public interface BaseMapper {

    List<Map<String,Object>> findBySql(@Param("sql")String sql);

    int insertMapList(@Param("tableName") String tableName, @Param("colMap") Map<String,Object> colMap, @Param("mapList") List<Map<String,Object>> mapList);

    int insertMap(@Param("tableName") String tableName,  @Param("dataMap") Map<String,Object> dataMap);

    int execDdlSql(@Param("sql") String sql);

    int execDelSql(@Param("sql") String sql);

    int insert(@Param("sql") String sql);
}
