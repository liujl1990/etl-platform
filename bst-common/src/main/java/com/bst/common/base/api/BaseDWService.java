package com.bst.common.base.api;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface BaseDWService {

    List<Map<String,Object>> findBySql(@Param("sql")String sql);

    List<Map<String, Object>> findPageBySql(String sql,Integer pageNum,Integer pageSize,String orderBy,String dbType);

    int insertMapList(@Param("tableName") String tableName, @Param("colMap") Map<String,Object> colMap, @Param("mapList") List<Map<String,Object>> mapList);

    int insertMap(@Param("tableName") String tableName,  @Param("dataMap") Map<String,Object> dataMap);

    int insert(@Param("sql") String sql);
}
