package com.bst.common.utils.self;

import com.bst.common.core.domain.entity.SysUser;
import org.apache.shiro.SecurityUtils;

public class LoginAPIUtils {

    public static Long getLoginUserId() {
        return ((SysUser) SecurityUtils.getSubject().getPrincipal()).getUserId();
    }

    public static String getLoginUsename() {
        return ((SysUser) SecurityUtils.getSubject().getPrincipal()).getUserName();
    }
}
